//
//  InviteViewController.m
//  FamillyLocator
//
//  Created by Amira on 11/28/16.
//  Copyright © 2016 Amira. All rights reserved.
//

#import "InviteViewController.h"
#import "Constant.h"
#import "MBProgressHUD.h"
#import "Animation.h"
#import <Contacts/Contacts.h>
#import "UIViewController+RemoteNotificationData.h"
#import "SideMenuViewController.h"
#import "InviteMemberFromContactsVC.h"

@interface InviteViewController ()
{
    WebServiceFunc *webService;
    MBProgressHUD *hud;
    NSMutableArray*membersData;
    NSString *memberID;
    int flag;
    UITapGestureRecognizer *tapGesture;
}
@end

@implementation InviteViewController

- (void)viewDidLoad {
    [super viewDidLoad];
  
    // Do any additional setup after loading the view.
    [self RegisterkeyboardObservers];
    
    tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(StouchesBegan)];
    
    
    autocompleteTableView = [[UITableView alloc] initWithFrame:CGRectMake(0,inviteMember.frame.origin
                                                                          .y+30, self.view.frame.size.width, 120) style:UITableViewStylePlain];
    autocompleteTableView.delegate = self;
    autocompleteTableView.dataSource = self;
    autocompleteTableView.scrollEnabled = YES;
    autocompleteTableView.hidden = YES;
    [inviteMember setDelegate:self];
    [inviteNewMember setDelegate:self];
    [scrollView addSubview:autocompleteTableView];
    
    [message setDelegate:self];
    [[message layer] setBorderWidth:1.0f];
    [[message layer] setBorderColor:[UIColor colorWithRed:224.0/255.0f green:224.0/255.0f blue:224.0/255.0f alpha:1.0f].CGColor];
    [Animation roundCornerForView:message withAngle:5.0f];
    memberID=@"";
    
    
    
    
    
}

-(void)viewDidDisappear:(BOOL)animated{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

-(void)StouchesBegan {
    
  
    [message resignFirstResponder];
    [inviteMember resignFirstResponder];
    [inviteNewMember resignFirstResponder];
}

-(IBAction)InviteFromContacts:(id)sender{

    InviteMemberFromContactsVC * InviteMemberFromContacts = [self.storyboard instantiateViewControllerWithIdentifier:@"InviteMemberFromContactsVC"];
    InviteMemberFromContacts.deleget =self;
    [self.navigationController pushViewController:InviteMemberFromContacts animated:YES];
    
    [message resignFirstResponder];
    [inviteMember resignFirstResponder];
    [inviteNewMember resignFirstResponder];


}
- (void)SelectMemberOrGroupD:(NSDictionary *)MemberOrGroup{

    if ([MemberOrGroup isKindOfClass:[CNContact class]]) {
        CNContact * Contact  = (CNContact *)MemberOrGroup;
        
        
        
            if(Contact.emailAddresses.count >0)
             inviteNewMember.text =[[Contact.emailAddresses objectAtIndex:0] valueForKey:@"value"];
        }else{
        inviteNewMember.text =@"";}
        
        
        
    
    
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)viewWillAppear:(BOOL)animated{
    webService=[[WebServiceFunc alloc]init];
    [webService setDelegate:self];
    flag=1;
    hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    hud.mode = MBProgressHUDModeIndeterminate;
    
    NSString *urlString=[NSString stringWithFormat:@"%@%@",membernotingroup,self.groupID];
    [webService getData:urlString withflagLogin:YES];
}

- (IBAction)showMenu
{
    
    SideMenuViewController *sideMenu=[self.storyboard instantiateViewControllerWithIdentifier:@"SideMenu"];
    [self.navigationController presentViewController:sideMenu animated:YES completion:nil];
}

-(IBAction)inviteMember:(id)sender{
    flag=2;
    webService=[[WebServiceFunc alloc]init];
    [webService setDelegate:self];
    hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    hud.mode = MBProgressHUDModeIndeterminate;
    NSString *urlString;
    if ([message.text isEqualToString:@"Message"]) {
        message.text=@"";
    }
    if ([memberID isEqualToString:@""]) {
        if ([self validateemailwithEmail:inviteNewMember.text]) {
            urlString=[NSString stringWithFormat:@"%@/user/user/InviteUserToSystem?email=%@&message=%@",MainAPi,inviteNewMember.text,message.text];
        }
    }else{
        urlString=[NSString stringWithFormat:@"%@/group/usergroup/InviteUserToGroup?id=%@&groupid=%@",MainAPi,memberID,self.groupID];
    }
    [webService getData:urlString withflagLogin:YES];
    
}

-(void)userWebserviceCallDidSuccessWithRespone:(NSMutableArray *)response{
    [hud setHidden:YES];
    if (flag==1) {
        membersData=response;
    }else if (flag==2){
        [self.navigationController popViewControllerAnimated:YES];
    }
    
    
    
}
-(void)userWebserviceCallDidFaildwithError:(NSError *)error andMessage:(NSString *)message{
    [hud setHidden:YES];
    NSLog(@"%@",error.localizedDescription);
    if ([message isEqualToString:@"401" ]) {
        UIStoryboard *storyboard=[UIStoryboard storyboardWithName:@"Main" bundle:nil];
        UIViewController* viewController=[storyboard instantiateViewControllerWithIdentifier:@"LoginController"];
        [self presentViewController:viewController animated:YES completion:nil];
    }
    
}


- (void)searchAutocompleteEntriesWithSubstring:(NSString *)substring {
    
    // Put anything that starts with this substring into the autocompleteUrls array
    // The items in this array is what will show up in the table view
    [autocompleteName removeAllObjects];
    autocompleteName=[[NSMutableArray alloc]init];
    for (int i=0; i<membersData.count; i++) {
        
        
        NSString * Value =[[membersData objectAtIndex:i] objectForKey:@"firstName"];
        
        NSString * userid =[[membersData objectAtIndex:i] objectForKey:@"id"];
        NSRange substringRange = [[Value lowercaseString] rangeOfString:[substring lowercaseString]];
        
        if (substringRange.location == 0) {
            NSDictionary *data=[[NSDictionary alloc]initWithObjectsAndKeys:Value,@"name",userid,@"id", nil];
            [autocompleteName addObject:data];
            NSLog(@"%@",autocompleteName);
        }
    }
    
    [autocompleteTableView reloadData];
}

#pragma mark UITextFieldDelegate methods

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    
    if ([textField isEqual:inviteMember]) {
        
        
        if ([textField.text isEqualToString:@""]) {
            autocompleteTableView.hidden = YES;
        }else{
            autocompleteTableView.hidden = NO;
            NSString *substring = [NSString stringWithString:textField.text];
            substring = [substring stringByReplacingCharactersInRange:range withString:string];
            [self searchAutocompleteEntriesWithSubstring:substring];
        }
        
    }else{
        
        autocompleteTableView.hidden = YES;
    }
    
    return YES;
}

-(void)textFieldDidEndEditing:(UITextField *)textField{
    autocompleteTableView.hidden=YES;
    ActiveTextField=nil;
}
-(void)textFieldDidBeginEditing:(UITextField *)textField{
    ActiveTextField = textField;
    if (![textField isEqual:inviteMember]) {
        [self.view addGestureRecognizer:tapGesture];
    }else{
        [self.view removeGestureRecognizer:tapGesture];
    }
    
}
#pragma mark UITableViewDataSource methods

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger) section {
    return autocompleteName.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    UITableViewCell *cell = nil;
    static NSString *AutoCompleteRowIdentifier = @"AutoCompleteRowIdentifier";
    cell = [tableView dequeueReusableCellWithIdentifier:AutoCompleteRowIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc]
                initWithStyle:UITableViewCellStyleDefault reuseIdentifier:AutoCompleteRowIdentifier] ;
    }
    NSString *name=[[autocompleteName objectAtIndex:indexPath.row] objectForKey:@"name"];
    cell.textLabel.text = name;
    cell.textLabel.textColor=[UIColor blackColor];
    return cell;
}
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    // This will create a "invisible" footer
    return 0.01f;
}


- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    return [UIView new];
    
}

#pragma mark UITableViewDelegate methods

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    UITableViewCell *selectedCell = [tableView cellForRowAtIndexPath:indexPath];
    inviteMember.text = selectedCell.textLabel.text;
    memberID= [[autocompleteName objectAtIndex:indexPath.row] objectForKey:@"id"];
    autocompleteTableView.hidden=YES;
    
    
}


#pragma End- Search in table

- (void)textViewDidBeginEditing:(UITextView *)textView{
    
    if ([textView.text isEqualToString:@"Message"]) {
        textView.text=@"";
    }
    textView.textColor=[UIColor blackColor];
    
}
-(void)textViewDidEndEditing:(UITextView *)textView{
    
    
    [scrollView removeGestureRecognizer:tapGesture];
}

-(void)RegisterkeyboardObservers{
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillShow:)
                                                 name:UIKeyboardWillShowNotification
                                               object:nil];
    
    // Register notification when the keyboard will be hide
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillHide:)
                                                 name:UIKeyboardWillHideNotification
                                               object:nil];
    
}

- (void)keyboardWillShow:(NSNotification *)notification {
    CGRect keyboardBounds;
    // [scrollView addGestureRecognizer:tapGesture];
    [[notification.userInfo valueForKey:UIKeyboardFrameBeginUserInfoKey] getValue:&keyboardBounds];
    
    
    [scrollView setContentSize:CGSizeMake(scrollView.frame.size.width, (scrollView.contentSize.height+keyboardBounds.size.height))];
    //   [scrollView setContentOffset:CGPointMake(scrollView.contentOffset.x,scrollView.contentOffset.y+keyboardBounds.size.height) animated:YES];
    NSLog(@"scrollView.contentSize.height=%f",scrollView.contentSize.height);
    
    // Do something with keyboard height
}

- (void)keyboardWillHide:(NSNotification *)notification {
    CGRect keyboardBounds;
    // [scrollView removeGestureRecognizer:tapGesture];
    [[notification.userInfo valueForKey:UIKeyboardFrameBeginUserInfoKey] getValue:&keyboardBounds];
    [scrollView setContentSize:CGSizeMake(scrollView.frame.size.width, (scrollView.contentSize.height-keyboardBounds.size.height))];
    NSLog(@"scrollView.contentSize.height=%f",scrollView.contentSize.height);
    // Do something with keyboard height
}

//If you want the keyboard to hide when you press the return
- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    return YES;
}

-(BOOL)validateemailwithEmail:(NSString *)email
{
    email=[email stringByReplacingOccurrencesOfString:@" " withString:@""];
    NSString *emailRegex = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,6}";
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:email];
    
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
