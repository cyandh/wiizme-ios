//
//  NeedHelpViewController.m
//  wiizme
//
//  Created by Amira on 12/19/16.
//  Copyright © 2016 Amira. All rights reserved.
//

#import "NeedHelpViewController.h"
#import "MBProgressHUD.h"
#import "Animation.h"
#import "Constant.h"
#import "CustomCollectionViewFlowLayout.h"
#import "NeedHelpCollectionViewCell.h"
#import "UIViewController+RemoteNotificationData.h"
#import "AppDelegate.h"
#import "SideMenuViewController.h"
@interface NeedHelpViewController ()
{
    WebServiceFunc *webService;
    MBProgressHUD *hud;
    AKTagsInputView *_tagsInputView;
    NSMutableArray *userList,*groupList,*actionsList;
    int serviceType;
    NSIndexPath *selectedAction;
    double userLatitude ,userLogitude;
    UITapGestureRecognizer *tapGesture;
    NSArray *phones;
}
@end
static NSString * const reuseIdentifier = @"Needcell";


@implementation NeedHelpViewController

- (void)viewDidLoad {
    [super viewDidLoad];


    
    [scrollView addSubview:[self createTagsInputView]];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWasShown:)
                                                 name:UIKeyboardDidShowNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWasHide:)
                                                 name:UIKeyboardDidHideNotification
                                               object:nil];
    collectionView.collectionViewLayout = [[CustomCollectionViewFlowLayout alloc] init];
    
    mapView.showsUserLocation = YES;
    mapView.showsBuildings = YES;
    [mapView setDelegate:self];
    self.locatioManager = [[CLLocationManager alloc]init];
    if ([self.locatioManager respondsToSelector:@selector(requestWhenInUseAuthorization)]) {
        [self.locatioManager requestWhenInUseAuthorization];
    }
    [self.locatioManager startUpdatingLocation];
    [mapView setUserInteractionEnabled:NO];
    webService=[[WebServiceFunc alloc]init];
    [webService setDelegate:self];
    hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    hud.mode = MBProgressHUDModeIndeterminate;
    
    NSString *urlString=ActionList;
    serviceType=1;
    [webService getData:urlString withflagLogin:YES];
    
    userList=[[NSMutableArray alloc]init];
    groupList=[[NSMutableArray alloc] init];
    
    [message setDelegate:self];
    [Animation roundCornerForView:message withAngle:5.0f];
    [[message layer] setBorderWidth:1.0f];
    [[message layer] setBorderColor:[UIColor colorWithRed:224.0/255.0f green:224.0/255.0f blue:224.0/255.0f alpha:1.0f].CGColor];
    [Animation roundCornerForView:nearByMe withAngle:6.0f];
    [Animation roundCornerForView:send withAngle:15.0f];
    

}

-(void)viewDidDisappear:(BOOL)animated{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}
-(void)StouchesBegan {
    if (autocompleteTableView.hidden) {
        [_tagsInputView resignFirstResponder];
        [message resignFirstResponder];
    }else{
        [scrollView removeGestureRecognizer:tapGesture];
               [message resignFirstResponder];
        [_tagsInputView resignFirstResponder];
    }
    

    
}
- (void)keyboardWasShown:(NSNotification *)notification
{
    tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(StouchesBegan)];
    [tapGesture setDelegate:self];
    [scrollView addGestureRecognizer:tapGesture];
}

- (void)keyboardWasHide:(NSNotification *)notification
{
    [scrollView removeGestureRecognizer:tapGesture];
}


-(AKTagsInputView*)createTagsInputView
{
    _tagsInputView = [[AKTagsInputView alloc] initWithFrame:self.selectGroups.frame];
    _tagsInputView.autoresizingMask = UIViewAutoresizingFlexibleWidth;
    _tagsInputView.selectedTags=[[NSMutableArray alloc]init];
    _tagsInputView.selectedTagsDic=[[NSMutableArray alloc]init];

        [_tagsInputView.selectedTags addObject:self.inviterName];
        [_tagsInputView.selectedTagsDic addObject:self.inviterId];
    
    
    //  _tagsInputView.selectedTags = [NSMutableArray arrayWithArray:@[@"some", @"predefined", @"tags"]];
    [_tagsInputView setDelegate:self];
    _tagsInputView.enableTagsLookup = NO;
    
    [Animation roundCornerForView:_tagsInputView withAngle:5.0f];
    [[_tagsInputView layer] setBorderWidth:1.0f];
    [[_tagsInputView layer] setBorderColor:[UIColor colorWithRed:224.0/255.0f green:224.0/255.0f blue:224.0/255.0f alpha:1.0f].CGColor];
    return _tagsInputView;
}
-(void)tagsInputViewDidBeginEditing:(AKTagsInputView*)inputView{
    if ([_tagsInputView.selectedTagsDic count]>0) {
        
    
    NSString *groupID=[_tagsInputView.selectedTagsDic objectAtIndex:0];
    if ([groupID isEqualToString:self.inviterId]) {
//        [_tagsInputView.selectedTags removeObjectAtIndex:0];
//        [_tagsInputView.selectedTagsDic removeObjectAtIndex:0];
        [_tagsInputView deleteItemAt:[NSIndexPath indexPathForRow:0 inSection:0] completion:nil];
        
    }
    }
    
}

-(void)textFieldChange:(NSString *)tagName{
    
    if (tagName.length>3) {
        [autocompleteTableView setHidden:NO];
        tagName = [tagName stringByReplacingOccurrencesOfString:@" " withString:@""];
        
        NSString * ttt  = [tagName copy];

        NSPredicate *resultPredicate = [NSPredicate predicateWithFormat:@"firstName contains[c] %@",ttt];
        autocompleteName = [[NSMutableArray alloc] init];
        autocompleteName =[[autocompletesearch filteredArrayUsingPredicate:resultPredicate] mutableCopy];
        [autocompleteTableView reloadData];
    }else{
        autocompleteName=autocompletesearch;
        [autocompleteTableView reloadData];
    }
}
-(void)tagsInputViewDidEndEditing:(AKTagsInputView*)inputView{
   if( _tagsInputView.selectedTags.count==0){
       [_tagsInputView AddItem:self.inviterName];
       [_tagsInputView.selectedTagsDic addObject:self.inviterId];
    }
}
-(void)tapReturn{
    [autocompleteTableView setHidden:YES];
}

- (IBAction)showMenu
{
    
    SideMenuViewController *sideMenu=[self.storyboard instantiateViewControllerWithIdentifier:@"SideMenu"];
    [self.navigationController presentViewController:sideMenu animated:YES completion:nil];
}

-(IBAction)callHelp:(id)sender{
    if (selectedAction.row>actionsList.count) {
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Attention" message:@"Please,Select Help Type First." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];

        [alert show];
    }else{
    hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    hud.mode = MBProgressHUDModeIndeterminate;
        [self validate];
           NSMutableDictionary *userData = [[NSMutableDictionary alloc] initWithObjectsAndKeys:
                                         [NSNumber numberWithDouble:userLatitude],@"Latitude",
                                         [NSNumber numberWithDouble:userLogitude],@"Langitude",
                                         message.text,@"Message",
                                         _tagsInputView.selectedTagsDic,@"Users",
                                         nil];
        serviceType=3;
        [webService postDataWithUrlString:CallHelp withData:userData withflagLogin:YES];

    }
}

-(void)validate{
    if ([[_tagsInputView.selectedTagsDic objectAtIndex:0] isEqualToString:self.inviterId]) {
        [_tagsInputView.selectedTagsDic removeAllObjects];
        for (int i=0; i<autocompletesearch.count; i++) {
            [_tagsInputView.selectedTagsDic addObject:[[autocompletesearch objectAtIndex:i] objectForKey:@"id"]];
        }
    }
    if (userLatitude==0) {
        CLLocation *locate=((AppDelegate *)[[UIApplication sharedApplication] delegate]).UserOldLocation;
        userLatitude=locate.coordinate.latitude;
        userLogitude=locate.coordinate.longitude;
    }
    if ([message.text isEqualToString:@"Message"]) {
        message.text=@"";
    }

}
-(void)userWebserviceCallDidSuccessWithRespone:(NSMutableArray *)response{
    [hud setHidden:YES];
     if(serviceType==1){
         actionsList=response;
         
        // selectedAction=[NSIndexPath indexPathForRow:response.count+1 inSection:0];
         [collectionView reloadData];
         [webService setDelegate:self];
         hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
         hud.mode = MBProgressHUDModeIndeterminate;
         
         NSString *urlString=[NSString stringWithFormat:@"%@%@",GroupMembers,self.inviterId];
         serviceType=2;
         [webService getData:urlString withflagLogin:YES];
         
     }else if (serviceType==2){
        autocompleteName =response;
        autocompletesearch=response;
        autocompleteTableView = [[UITableView alloc] initWithFrame:CGRectMake(0,self.selectGroups.frame.origin.y+35, self.view.frame.size.width, 120) style:UITableViewStylePlain];
        autocompleteTableView.delegate = self;
        autocompleteTableView.dataSource = self;
        autocompleteTableView.scrollEnabled = YES;
        autocompleteTableView.hidden = YES;
        
        [autocompleteTableView.layer setShadowColor:[[UIColor darkGrayColor] CGColor]];
        [autocompleteTableView.layer setShadowOffset:CGSizeMake(0, 0)];
        [autocompleteTableView.layer setShadowRadius:20.0];
        [autocompleteTableView.layer setShadowOpacity:1];
        autocompleteTableView.clipsToBounds = NO;
        autocompleteTableView.layer.masksToBounds = NO;
        [scrollView addSubview:autocompleteTableView];
        [autocompleteTableView reloadData];
        
     }else if(serviceType==3){
         NSDictionary *dicResponse=(NSDictionary*)response;
         phones=[dicResponse objectForKey:@"phoneList"];
//         NSString *tel=[phones objectAtIndex:0];
//         for (int i=1; i<phones.count; i++) {
//             tel = [tel stringByAppendingFormat:@",%@",[phones objectAtIndex:i]];
//         }
//         NSString * URLSMS  =[NSString stringWithFormat:@"sms:%@",tel];
//         NSString *url = [URLSMS stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding ];
//         [[UIApplication sharedApplication]  openURL: [NSURL URLWithString: url]];
        
         NSMutableDictionary *userData=[dicResponse objectForKey:@"post"];
         serviceType=4;
         [webService postDataWithUrlString:UploadPost withData:userData withflagLogin:YES];
     }else{
         if (phones.count==0) {
             UIAlertView *warningAlert = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Don't have Phone Numbers" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
             [warningAlert show];
         }else{
         [self showSMS];
         }
         
    }
    
}
-(void)userWebserviceCallDidFaildwithError:(NSError *)error andMessage:(NSString *)message{
    [hud setHidden:YES];
    NSLog(@"%@",error.localizedDescription);
    if ([message isEqualToString:@"401" ]) {
        UIStoryboard *storyboard=[UIStoryboard storyboardWithName:@"Main" bundle:nil];
        UIViewController* viewController=[storyboard instantiateViewControllerWithIdentifier:@"LoginController"];
        [self presentViewController:viewController animated:YES completion:nil];
    }
    
}

- (void)showSMS{
    
    if(![MFMessageComposeViewController canSendText]) {
        UIAlertView *warningAlert = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Your device doesn't support SMS!" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [warningAlert show];
        return;
    }
    
    
    NSString *message =@"I need help From You";
    
    MFMessageComposeViewController *messageController = [[MFMessageComposeViewController alloc] init];
    messageController.messageComposeDelegate = self;
    [messageController setRecipients:phones];
    [messageController setBody:message];
    
    // Present message view controller on screen
    [self presentViewController:messageController animated:YES completion:nil];
}

- (void)messageComposeViewController:(MFMessageComposeViewController *)controller didFinishWithResult:(MessageComposeResult) result
{
    switch (result) {
        case MessageComposeResultCancelled:
            break;
            
        case MessageComposeResultFailed:
        {
            UIAlertView *warningAlert = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Failed to send SMS!" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [warningAlert show];
            break;
        }
            
        case MessageComposeResultSent:
            break;
            
        default:
            break;
    }
    
    [self dismissViewControllerAnimated:YES completion:nil];
     [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark UITableViewDataSource methods
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger) section {
    
    return  autocompleteName.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    UITableViewCell *cell = nil;
    static NSString *AutoCompleteRowIdentifier = @"AutoCompleteRowIdentifier";
    cell = [tableView dequeueReusableCellWithIdentifier:AutoCompleteRowIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc]
                initWithStyle:UITableViewCellStyleDefault reuseIdentifier:AutoCompleteRowIdentifier] ;
    }
    NSString *name=[[autocompleteName objectAtIndex:indexPath.row] objectForKey:@"fullName"];
//    BOOL isUser=[[[autocompleteName objectAtIndex:indexPath.row] objectForKey:@"isUser"] boolValue];
//    if (isUser) {
//        
//        cell.detailTextLabel.text=@"Member";
//        
//    }else{
//        cell.detailTextLabel.text=@"Group";
//    }
    cell.textLabel.text = name;
    cell.textLabel.textColor=[UIColor blackColor];
    //cell.detailTextLabel.textColor=[UIColor lightGrayColor];
    return cell;
}

#pragma mark UITableViewDelegate methods

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    UITableViewCell *selectedCell = [tableView cellForRowAtIndexPath:indexPath];
    //    inviteMember.text = selectedCell.textLabel.text;
    //    memberID= [[autocompleteName objectAtIndex:indexPath.row] objectForKey:@"id"];
    [_tagsInputView.selectedTagsDic addObject:[[autocompleteName objectAtIndex:indexPath.row]objectForKey:@"id"]];
    autocompleteTableView.hidden=YES;
    [_tagsInputView AddItem:[[autocompleteName objectAtIndex:indexPath.row] objectForKey:@"fullName"]];
    
    [self.view endEditing:YES];
}

#pragma mark <UICollectionViewDataSource>

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 1;
}


- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return actionsList.count;
}

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView
                        layout:(UICollectionViewLayout *)collectionViewLayout
        insetForSectionAtIndex:(NSInteger)section{
    return UIEdgeInsetsMake(5, 5,5, 5);
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    NeedHelpCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:reuseIdentifier forIndexPath:indexPath];
    NSURL *imageUrl=[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",PhotosUrl,[[actionsList objectAtIndex:indexPath.row] objectForKey:@"iconId"]]];
   [cell.Img setImageURL:imageUrl];
    [Animation roundCornerForView:cell.Img withAngle:30.0f];
    
    cell.Lbl.text=[[actionsList objectAtIndex:indexPath.row] objectForKey:@"name"];
    if (indexPath==selectedAction) {
        cell.Lbl.backgroundColor=[UIColor lightGrayColor];
    }else{
        
        cell.Lbl.backgroundColor=[UIColor clearColor];
        
    }
    return cell;
}

#pragma mark <UICollectionViewDelegate>


- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    NeedHelpCollectionViewCell *selectedCell ;
    if (selectedAction.row<actionsList.count){
    
        selectedCell=[collectionView cellForItemAtIndexPath:selectedAction];
        [selectedCell.Lbl setBackgroundColor:[UIColor clearColor]];
    }
    
    selectedCell=[collectionView cellForItemAtIndexPath:indexPath];
    selectedAction=indexPath;

    [selectedCell.Lbl setBackgroundColor:[UIColor lightGrayColor]];
}


    


-(void)mapView:(MKMapView *)mapView didUpdateUserLocation:(MKUserLocation *)userLocation {
    
    MKMapCamera *camera = [MKMapCamera cameraLookingAtCenterCoordinate:userLocation.coordinate fromEyeCoordinate:CLLocationCoordinate2DMake(userLocation.coordinate.latitude, userLocation.coordinate.longitude) eyeAltitude:1020];
    [mapView setCamera:camera animated:YES];
    
    userLatitude=[userLocation coordinate].latitude;
    userLogitude=[userLocation coordinate].longitude;
    
    
    
    [self.locatioManager stopUpdatingLocation];
    
    NSLog(@"latitude:%f",userLatitude);
    NSLog(@"longitude:%f",userLogitude);
    
    
 
}
-(void)textFieldTagView:(NSString *)tagName{
    
    
       autocompleteName=autocompletesearch;

    if (tagName.length>3) {
        [autocompleteTableView setHidden:NO];

        NSPredicate *resultPredicate = [NSPredicate predicateWithFormat:@"fullName contains[c] %@", tagName];
        
        autocompleteName = [[autocompleteName filteredArrayUsingPredicate:resultPredicate] mutableCopy];
        [autocompleteTableView reloadData];
    }else{
        autocompleteName=autocompletesearch;
        [autocompleteTableView reloadData];
    }
}
- (void)textViewDidBeginEditing:(UITextView *)textView{
    if ([textView.text isEqualToString:@"Message"]) {
        textView.text=@"";
    }
    [self animateTextView:textView up: YES];
}



- (void)textViewDidEndEditing:(UITextView *)textView
{
    [self animateTextView:textView up: NO];
}

- (void) animateTextView: (UITextView*) textView up: (BOOL) up
{
    
    
    const int movementDistance = 180; // tweak as needed
    const float movementDuration = 0.3f; // tweak as needed
    
    int movement = (up ? -movementDistance : movementDistance);
    
    [UIView beginAnimations: @"anim" context: nil];
    [UIView setAnimationBeginsFromCurrentState: YES];
    [UIView setAnimationDuration: movementDuration];
    self.view.frame = CGRectOffset(self.view.frame, 0, movement);
    [UIView commitAnimations];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
