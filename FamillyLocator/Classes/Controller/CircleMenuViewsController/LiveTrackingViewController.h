//
//  LifeTrackingViewController.h
//  wiizme
//
//  Created by Amira on 1/29/17.
//  Copyright © 2017 Amira. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>
#import <Pusher/Pusher.h>
#import "WebServiceFunc.h"
#import "AsyncImageView.h"
#import "GroupData.h"
#import "MultiRowAnnotation.h"
#import "LPPopupListView.h"

@interface LiveTrackingViewController : UIViewController<MKMapViewDelegate,WebServiceProtocol,LPPopupListViewDelegate>{
    __weak IBOutlet AsyncImageView *img;
    __weak IBOutlet UILabel *name;
    __weak IBOutlet UILabel *viewTitle;
}

@property(nonatomic,strong)PTPusher *client;
@property (nonatomic,weak) IBOutlet MKMapView *mapView;
@property (nonatomic,strong) MKAnnotationView *selectedAnnotationView;
@property (nonatomic,strong) MultiRowAnnotation *calloutAnnotation;
@property(nonatomic,retain)CLLocationManager *locatioManager;
@property (nonatomic, retain) MKPolyline *routeLine; //your line
@property (nonatomic, retain) MKPolylineView *routeLineView; //overlay vie
@property (nonatomic,strong)NSMutableArray *locations;
@property (nonatomic,strong)NSMutableArray *usersId;
@property (nonatomic)BOOL isGroup;
@property (nonatomic,strong) GroupData *groupData;
@property (nonatomic, strong) NSMutableIndexSet *selectedIndexes;

@end
