//
//  AddMeetEventViewController.h
//  wiizme
//
//  Created by Amira on 12/13/16.
//  Copyright © 2016 Amira. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>
#import "WebServiceFunc.h"
#import "ASJTagsView.h"
#import "AKTagsInputView.h"
#import "AKTagsDefines.h"
#import "DateCustomButton.h"
#import "MainViewControler.h"


@interface AddMeetEventViewController : MainViewControler<WebServiceProtocol,UITableViewDelegate,UITableViewDataSource,UITextFieldDelegate,AKTagsInputViewDelegate>
{
    __weak IBOutlet UISearchBar *searchBar;
    __weak IBOutlet UIScrollView *scrollView;
    __weak IBOutlet MKMapView *mapView;
    __weak IBOutlet UITextField *eventName;
    
    __weak IBOutlet DateCustomButton *startDate;
    __weak IBOutlet DateCustomButton *endDate;
    __weak IBOutlet UITextView *message;
    __weak IBOutlet UIButton *send;
    __weak IBOutlet UIButton *edit;
    __weak IBOutlet UIView * LeaveView;
      __weak IBOutlet UIView * AcceptView;
    CGSize OriginalScrollContentSize;
    NSMutableArray *autocompleteName,*autocompletesearch;
    UITableView *autocompleteTableView;
}
@property (nonatomic, strong)IBOutlet UIView *selectGroups;
@property (nonatomic) BOOL editView;
@property (nonatomic,strong) NSDictionary *eventData;
@property (nonatomic,strong) NSString *inviterId;
@property (nonatomic,strong) NSString *inviterName;
@property (nonatomic) BOOL isUser;
@end
