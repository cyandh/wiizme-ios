//
//  AddMeetEventViewController.m
//  wiizme
//
//  Created by Amira on 12/13/16.
//  Copyright © 2016 Amira. All rights reserved.
//

#import "AddMeetEventViewController.h"
#import "MBProgressHUD.h"
#import "Animation.h"
#import "Constant.h"
#import "UIViewController+HeaderView.h"
#import "Validation.h"
#import "UIViewController+RemoteNotificationData.h"
#import "SPGooglePlacesAutocompleteViewController.h"
#import "MyLocation.h"
#import "SideMenuViewController.h"
@interface AddMeetEventViewController ()
{
    WebServiceFunc *webService;
    MBProgressHUD *hud;
    UIDatePicker *datePicker;
    NSString *startdate,*enddate;
    AKTagsInputView *_tagsInputView;
    
    NSMutableArray *userList,*groupList;
    int serviceType;
    BOOL creator,validateDates,enddat;
    int editDataFlag;
    double userLatitude,userLogitude;
    NSString *address;
}
@end


@implementation AddMeetEventViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    validateDates=true;
    CurrentScrollview = scrollView;
    [self RegisterkeyboardObservers];
    
    
    
    [Animation roundCornerForView:send withAngle:15.0F];
    // Do any additional setup after loading the view.
    creator=[[self.eventData objectForKey:@"isCreator"] boolValue];
    [scrollView addSubview:[self createTagsInputView]];
    edit.hidden=YES;
    if (![[_eventData objectForKey:@"accepted"] boolValue]) {
        AcceptView.hidden = NO;
        LeaveView.hidden = YES;
    }else{
        
        AcceptView.hidden = YES;
        LeaveView.hidden = NO;
    }
    if ((creator)||(!self.editView)) {
        webService=[[WebServiceFunc alloc]init];
        [webService setDelegate:self];
        hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        hud.mode = MBProgressHUDModeIndeterminate;
        NSString *urlString=ListMembersAndGroups;
        serviceType=1;
        [webService getData:urlString withflagLogin:YES];
        
    }
    if (self.editView) {
        [self updatetxtcolor:[UIColor lightGrayColor]];
        eventName.text=[self.eventData objectForKey:@"title"];
        NSDictionary *dic=[self editDateAndTime:[self.eventData objectForKey:@"from"]];
        startdate=[self.eventData objectForKey:@"from"];
        NSString *start=[NSString stringWithFormat:@" Starting on:%@ %@",[dic objectForKey:@"date"],[dic objectForKey:@"time"]];
        [startDate setTitle:start forState:UIControlStateNormal];
        dic=[self editDateAndTime:[self.eventData objectForKey:@"to"]];
        enddate=[self.eventData objectForKey:@"to"];
        NSString *end=[NSString stringWithFormat:@" Ending on:%@ %@",[dic objectForKey:@"date"],[dic objectForKey:@"time"]];
        [endDate setTitle:end forState:UIControlStateNormal];
        message.text=[self.eventData objectForKey:@"description"];
        searchBar.placeholder=[self.eventData objectForKey:@"place"];
        
        NSArray *invitation=[self.eventData objectForKey:@"groups"];
        
        for (int i=0; i<invitation.count; i++) {
            [_tagsInputView.selectedTags addObject:[[invitation objectAtIndex:i] objectForKey:@"name"]];
            [_tagsInputView.selectedTagsDic addObject:[NSDictionary dictionaryWithObjectsAndKeys:[NSNumber numberWithBool:0],@"IsUser",[[invitation objectAtIndex:i]objectForKey:@"id"],@"id",nil]];
        }
        invitation=[self.eventData objectForKey:@"users"];
        for (int i=0; i<invitation.count; i++) {
            [_tagsInputView.selectedTags addObject:[[invitation objectAtIndex:i] objectForKey:@"firstName"]];
            [_tagsInputView.selectedTagsDic addObject:[NSDictionary dictionaryWithObjectsAndKeys:[NSNumber numberWithBool:1],@"IsUser",[[invitation objectAtIndex:i]objectForKey:@"id"],@"id",nil]];
        }
        
        eventName.userInteractionEnabled=NO;
        message.userInteractionEnabled=NO;
        startDate.userInteractionEnabled=NO;
        endDate.userInteractionEnabled=NO;
        searchBar.userInteractionEnabled=NO;
        mapView.userInteractionEnabled=NO;
        BOOL *isInvited=[[self.eventData objectForKey:@"isInvited"] boolValue];
        if (!isInvited) {
            [send setHidden:YES];
        }
        if (!creator) {
            [send setTitle:@"Leave Event" forState:UIControlStateNormal];
            
        }else{
            edit.hidden=NO;
            [send setTitle:@"Cancel Event" forState:UIControlStateNormal];
        }
        _tagsInputView.allowDeleteTags=NO;
    }
    userList=[[NSMutableArray alloc]init];
    groupList=[[NSMutableArray alloc] init];
    
    datePicker = [[UIDatePicker alloc]init];
    datePicker.hidden=YES;
    [self updateButton:startDate];
    [self updateButton:endDate];
    [Animation roundCornerForView:message withAngle:5.0f];
    [[message layer] setBorderWidth:1.0f];
    [[message layer] setBorderColor:[UIColor colorWithRed:224.0/255.0f green:224.0/255.0f blue:224.0/255.0f alpha:1.0f].CGColor];
    
    
    
}

-(void)viewDidDisappear:(BOOL)animated{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

-(void)updatetxtcolor:(UIColor*)color{
    [eventName setTextColor:color];
    [message setTextColor:color];
    [startDate setTitleColor:color forState:UIControlStateNormal];
    [endDate setTitleColor:color forState:UIControlStateNormal];
}
-(void)updateButton:(UIButton*)button{
    [Animation roundCornerForView:button withAngle:6.0f];
    [[button layer] setBorderWidth:1.0f];
    [[button layer] setBorderColor:[UIColor colorWithRed:224.0/255.0f green:224.0/255.0f blue:224.0/255.0f alpha:1.0f].CGColor];
    button.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
}

-(AKTagsInputView*)createTagsInputView
{
    _tagsInputView = [[AKTagsInputView alloc] initWithFrame:self.selectGroups.frame];
    _tagsInputView.autoresizingMask = UIViewAutoresizingFlexibleWidth;
    _tagsInputView.selectedTags=[[NSMutableArray alloc]init];
    _tagsInputView.selectedTagsDic=[[NSMutableArray alloc]init];
    if (!_editView) {
        [_tagsInputView.selectedTags addObject:self.inviterName];
        [_tagsInputView.selectedTagsDic addObject:[NSDictionary dictionaryWithObjectsAndKeys:[NSNumber numberWithBool:self.isUser],@"IsUser",self.inviterId,@"id",nil]];
    }
    
    
    //  _tagsInputView.selectedTags = [NSMutableArray arrayWithArray:@[@"some", @"predefined", @"tags"]];
    [_tagsInputView setDelegate:self];
    _tagsInputView.enableTagsLookup = NO;
    
    [Animation roundCornerForView:_tagsInputView withAngle:5.0f];
    [[_tagsInputView layer] setBorderWidth:1.0f];
    [[_tagsInputView layer] setBorderColor:[UIColor colorWithRed:224.0/255.0f green:224.0/255.0f blue:224.0/255.0f alpha:1.0f].CGColor];
    return _tagsInputView;
}
-(void)textFieldTagView:(NSString *)tagName{
    if (tagName.length>3) {
        [autocompleteTableView setHidden:NO];
        tagName = [tagName stringByReplacingOccurrencesOfString:@" " withString:@""];
        //        NSData* data = [tagName dataUsingEncoding:NSUTF8StringEncoding];
        //        NSString* newStr = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
        NSString *stringf=tagName.stringByStandardizingPath;
        NSString *rest=[NSString stringWithString:stringf];
        NSPredicate *resultPredicate = [NSPredicate predicateWithFormat:@"name contains[c] %@", rest];
        
        autocompleteName = [[autocompleteName filteredArrayUsingPredicate:resultPredicate] mutableCopy];
        [autocompleteTableView reloadData];
    }else{
        autocompleteName=autocompletesearch;
        [autocompleteTableView reloadData];
    }
}

-(void)tapReturn{
    [autocompleteTableView setHidden:YES];
}
-(void)StouchesBegan {
    if (![datePicker isHidden]) {
        if (enddat) {
            
            if (!validateDates) {
                UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"" message:NSLocalizedString(@"Please Check Event Dates",nil) delegate:nil cancelButtonTitle:@"ok" otherButtonTitles:nil];
                
                [alert show];
            }
        }
        [datePicker setHidden:YES];
        [scrollView setContentSize:CGSizeMake(scrollView.frame.size.width, (scrollView.contentSize.height-datePicker.frame.size.height))];
        
        
    }
    
    [super StouchesBegan];
    
}

- (IBAction)showMenu
{
    
    SideMenuViewController *sideMenu=[self.storyboard instantiateViewControllerWithIdentifier:@"SideMenu"];
    [self.navigationController presentViewController:sideMenu animated:YES completion:nil];
}
-(IBAction)editView:(id)sender{
    editDataFlag=1;
    [self updatetxtcolor:[UIColor blackColor]];
    eventName.userInteractionEnabled=YES;
    message.userInteractionEnabled=YES;
    startDate.userInteractionEnabled=YES;
    endDate.userInteractionEnabled=YES;
    searchBar.userInteractionEnabled=YES;
    mapView.userInteractionEnabled=YES;
    [send setTitle:@"Save Data" forState:UIControlStateNormal];
    [_tagsInputView setAllowDeleteTags:YES];
}
-(IBAction)sendData:(id)sender{
    if(![self validateData]){
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"" message:NSLocalizedString(@"Please Check Your Data Again and make sure to Select Place",nil) delegate:nil cancelButtonTitle:@"ok" otherButtonTitles:nil];
        
        [alert show];
    }else{
        NSString *eventid=[self.eventData objectForKey:@"id"];
        if (_editView) {
            if (!creator) {
                webService=[[WebServiceFunc alloc]init];
                [webService setDelegate:self];
                hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
                hud.mode = MBProgressHUDModeIndeterminate;
                serviceType=2;
                NSString *urlString=[NSString stringWithFormat:@"%@%@",LeaveEvent,eventid];
                [webService getData:urlString withflagLogin:YES];
                
            }else{
                if (editDataFlag==1) {
                    webService=[[WebServiceFunc alloc]init];
                    [webService setDelegate:self];
                    hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
                    hud.mode = MBProgressHUDModeIndeterminate;
                    if (_tagsInputView.selectedTagsDic.count==0) {
                        [self.eventData setValue:eventName.text forKey:@"title"];
                        [self.eventData setValue:searchBar.text forKey:@"place"];
                        [self.eventData setValue:startdate forKey:@"from"];
                        [self.eventData setValue:enddate forKey:@"to"];
                        [self.eventData setValue:message.text forKey:@"description"];
                        if (!validateDates) {
                            UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"" message:NSLocalizedString(@"Please Check Event Dates",nil) delegate:nil cancelButtonTitle:@"ok" otherButtonTitles:nil];
                            
                            [alert show];
                        }else{
                            serviceType=2;
                            [webService postDataWithUrlString:EditEvent withData:self.eventData withflagLogin:YES];
                        }
                    }else{
                        for (int i=0; i<_tagsInputView.selectedTagsDic.count; i++) {
                            BOOL isUser=[[[_tagsInputView.selectedTagsDic objectAtIndex:i] objectForKey:@"IsUser"] boolValue];
                            if (isUser) {
                                [ userList addObject:[[_tagsInputView.selectedTagsDic objectAtIndex:i] objectForKey:@"id"]];
                            }else{
                                [ groupList addObject:[[_tagsInputView.selectedTagsDic objectAtIndex:i] objectForKey:@"id"]];
                            }
                        }
                        NSMutableDictionary *userData = [[NSMutableDictionary alloc] initWithObjectsAndKeys:eventid,@"eventId",
                                                         groupList,@"groupIds",
                                                         userList,@"userIds",
                                                         nil];
                        serviceType=3;
                        [webService postDataWithUrlString:invits withData:userData withflagLogin:YES];
                    }
                }else{
                    webService=[[WebServiceFunc alloc]init];
                    [webService setDelegate:self];
                    hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
                    hud.mode = MBProgressHUDModeIndeterminate;
                    serviceType=2;
                    NSString *urlString=[NSString stringWithFormat:@"%@%@",CancelEvent,eventid];
                    [webService getData:urlString withflagLogin:YES];
                }}}else{
                    webService=[[WebServiceFunc alloc]init];
                    [webService setDelegate:self];
                    for (int i=0; i<_tagsInputView.selectedTagsDic.count; i++) {
                        BOOL isUser=[[[_tagsInputView.selectedTagsDic objectAtIndex:i] objectForKey:@"IsUser"] boolValue];
                        if (isUser) {
                            [ userList addObject:[NSDictionary dictionaryWithObject:[[_tagsInputView.selectedTagsDic objectAtIndex:i] objectForKey:@"id"]forKey:@"id"]];
                        }else{
                            [ groupList addObject:[NSDictionary dictionaryWithObject:[[_tagsInputView.selectedTagsDic objectAtIndex:i] objectForKey:@"id"]forKey:@"id"]];
                        }
                    }
                    
                    NSMutableDictionary *userData = [[NSMutableDictionary alloc] initWithObjectsAndKeys:eventName.text,@"title",
                                                     [NSNumber numberWithDouble:userLatitude],@"latitude",
                                                     [NSNumber numberWithDouble:userLatitude],@"longitude",
                                                     message.text,@"description",
                                                     searchBar.text,@"place",
                                                     startdate,@"from",
                                                     enddate,@"to",
                                                     groupList,@"groups",
                                                     userList,@"users",
                                                     nil];
                    
                    if (!validateDates) {
                        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"" message:NSLocalizedString(@"Please Check Event Dates",nil) delegate:nil cancelButtonTitle:@"ok" otherButtonTitles:nil];
                        
                        [alert show];
                    }else{
                        hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
                        hud.mode = MBProgressHUDModeIndeterminate;
                        serviceType=2;
                        [webService postDataWithUrlString:AddEvent withData:userData withflagLogin:YES];
                    }
                }
    }
}
-(IBAction)datepicker:(id)sender{
    [eventName resignFirstResponder];
    [searchBar resignFirstResponder];
    [message resignFirstResponder];
    
    if ((creator)||(!self.editView)) {
        tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(StouchesBegan)];
        [tapGesture setDelegate:self];
        [self.view addGestureRecognizer:tapGesture];
        if (![datePicker isHidden]) {
            
        } else{
            
            datePicker.frame=CGRectMake(0, self.view.frame.size.height-datePicker.frame.size.height, self.view.frame.size.width,datePicker.frame.size.height);
            [scrollView setContentSize:CGSizeMake(scrollView.frame.size.width, (scrollView.contentSize.height+datePicker.frame.size.height))];
            [scrollView setContentOffset:CGPointMake(scrollView.contentOffset.x,scrollView.contentOffset.y+datePicker.frame.size.height) animated:YES];
            datePicker.backgroundColor = [UIColor blackColor];
            [datePicker setValue:[UIColor whiteColor] forKey:@"textColor"];
            datePicker.datePickerMode = UIDatePickerModeDateAndTime;
            datePicker.hidden = NO;
            
            [datePicker addTarget:self action:@selector(dateText:) forControlEvents:UIControlEventValueChanged];
            
            [self.view addSubview:datePicker];
            if ([sender tag]==100) {
                datePicker.tag=100;
                [datePicker setDate:[NSDate date]];
                [datePicker setMinimumDate:[NSDate date]];
                enddat=NO;
            }else{
                datePicker.tag=200;
                [datePicker setDate:[NSDate date]];
                [datePicker setMinimumDate:[NSDate date]];
                enddat=YES;
            }
            
        }
        
    }else{
        
    }
    
    
    
}

-(void)dateText:(id)sender
{
    
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    NSLocale *locale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US"];
    [dateFormat setLocale:locale];
    
    [dateFormat  setDateFormat:@"yyyy-MM-dd hh:mm a"];
    if ([sender tag]==100) {
        [startDate setTitle:[dateFormat stringFromDate:datePicker.date]  forState:UIControlStateNormal] ;
        startdate=[dateFormat stringFromDate:datePicker.date];
    }else{
        [endDate setTitle:[dateFormat stringFromDate:datePicker.date]  forState:UIControlStateNormal] ;
        enddate=[dateFormat stringFromDate:datePicker.date];
    }
    
    if ((startdate.length==0)||(enddate.length==0)) {
        
    }else{
        NSDate *datestart =[dateFormat dateFromString:startdate];
        NSDate *dateend =[dateFormat dateFromString:enddate];
        switch ([datestart compare:dateend]) {
            case NSOrderedAscending:
                //Do your logic when date1 > date2
                validateDates=true;
                break;
                
            case NSOrderedDescending:
                //Do your logic when date1 < date2
                validateDates=false;
                break;
                
            case NSOrderedSame:
                //Do your logic when date1 = date2
                validateDates=false;
                break;
        }
    }
    
    
    
    // birthday=datePicker.date;
    
}

-(void)userWebserviceCallDidSuccessWithRespone:(NSMutableArray *)response{
    [hud setHidden:YES];
    if (serviceType==3) {
        [self.eventData setValue:eventName.text forKey:@"name"];
        [self.eventData setValue:searchBar.text forKey:@"place"];
        [self.eventData setValue:startdate forKey:@"from"];
        [self.eventData setValue:enddate forKey:@"to"];
        [self.eventData setValue:message.text forKey:@"description"];
        if (!validateDates) {
            UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"" message:NSLocalizedString(@"Please Check Event Dates",nil) delegate:nil cancelButtonTitle:@"ok" otherButtonTitles:nil];
            
            [alert show];
        }else{
            serviceType=2;
            [webService postDataWithUrlString:EditEvent withData:self.eventData withflagLogin:YES];
        }
    }else if(serviceType==1){
        autocompleteName =response;
        autocompletesearch=response;
        autocompleteTableView = [[UITableView alloc] initWithFrame:CGRectMake(0,self.selectGroups.frame.origin.y+35, self.view.frame.size.width, 120) style:UITableViewStylePlain];
        autocompleteTableView.delegate = self;
        autocompleteTableView.dataSource = self;
        autocompleteTableView.scrollEnabled = YES;
        autocompleteTableView.hidden = YES;
        
        [autocompleteTableView.layer setShadowColor:[[UIColor darkGrayColor] CGColor]];
        [autocompleteTableView.layer setShadowOffset:CGSizeMake(0, 0)];
        [autocompleteTableView.layer setShadowRadius:20.0];
        [autocompleteTableView.layer setShadowOpacity:1];
        autocompleteTableView.clipsToBounds = NO;
        autocompleteTableView.layer.masksToBounds = NO;
        [self.view addSubview:autocompleteTableView];
        [autocompleteTableView reloadData];
        
    }else{
        [self.navigationController popViewControllerAnimated:YES];
    }
    
}
-(void)userWebserviceCallDidFaildwithError:(NSError *)error andMessage:(NSString *)message{
    [hud setHidden:YES];
    NSLog(@"%@",error.localizedDescription);
    if ([message isEqualToString:@"401" ]) {
        UIStoryboard *storyboard=[UIStoryboard storyboardWithName:@"Main" bundle:nil];
        UIViewController* viewController=[storyboard instantiateViewControllerWithIdentifier:@"LoginController"];
        [self presentViewController:viewController animated:YES completion:nil];
    }
    
}

-(void)tagsInputViewDidBeginEditing:(AKTagsInputView*)inputView{
    //if(){
    NSLog(@"dsdd");
    // }
}


#pragma Mark - Search in table


- (BOOL)searchBarShouldBeginEditing:(UISearchBar *)searchBar{
    SPGooglePlacesAutocompleteViewController *placesView=[self.storyboard instantiateViewControllerWithIdentifier:@"AutocompleteController"];
    placesView.myDelegate=self;
    [self.navigationController pushViewController:placesView animated:YES];
    [searchBar endEditing:YES];
    return NO;
}
- (void)secondViewControllerDismissed:(NSDictionary *)locationData
{
    userLatitude=[[locationData objectForKey:@"latitude"] doubleValue];
    userLogitude=[[locationData objectForKey:@"longitude"] doubleValue];
    address=[locationData objectForKey:@"address"];
    
    searchBar.text=address;
    
    
    
    [self postLocation];
    
}



-(void)postLocation{
    
    
    CLLocationCoordinate2D coordinate;
    coordinate.latitude = userLatitude;
    coordinate.longitude = userLogitude;
    
    MyLocation *annotation = [[MyLocation alloc] initWithName:address coordinate:coordinate] ;
    
    [mapView addAnnotation:annotation];
    
    
    
    [mapView setCenterCoordinate:coordinate animated:YES];
    MKCoordinateSpan span =
    { .longitudeDelta = mapView.region.span.longitudeDelta / 2,
        .latitudeDelta  =mapView.region.span.latitudeDelta  / 2 };
    
    // Create a new MKMapRegion with the new span, using the center we want.
    MKCoordinateRegion region = { .center = coordinate, .span = span };
    [mapView setRegion:region animated:YES];
}
#pragma Mark -  Add Annotations for Locations From Service

//This is the method that gets called for every annotation you added to the map
- (MKAnnotationView *)mapView:(MKMapView *)mapView viewForAnnotation:(id <MKAnnotation>)annotation {
    static NSString *identifier = @"MyLocation";
    if ([annotation isKindOfClass:[MyLocation class]]) {
        
        MKAnnotationView *annotationView = (MKAnnotationView *) [mapView dequeueReusableAnnotationViewWithIdentifier:identifier];
        if (annotationView == nil) {
            annotationView = [[MKAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:identifier];
            annotationView.enabled = YES;
            annotationView.canShowCallout = YES;
            annotationView.image = [UIImage imageNamed:@"arrest.png"];//here we use a nice image instead of the default pins
            annotationView.rightCalloutAccessoryView = [UIButton buttonWithType:UIButtonTypeDetailDisclosure];
            
        } else {
            annotationView.annotation = annotation;
        }
        
        return annotationView;
    }
    
    return nil;
}
- (void)mapView:(MKMapView *)mapView didSelectAnnotationView:(MKAnnotationView *)view {
    NSLog(@"Latitude: %f", view.annotation.coordinate.latitude);
    NSLog(@"Longitude: %f", view.annotation.coordinate.longitude);
}


- (void)mapView:(MKMapView *)mapView annotationView:(MKAnnotationView *)view calloutAccessoryControlTapped:(UIControl *)control {
    
    NSLog(@"%@",view.annotation.title);
    NSLog(@"%@", mapView.selectedAnnotations);
    
    
}


#pragma End- Search in table

#pragma mark UITableViewDataSource methods
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 2;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger) section {
    
    return  autocompleteName.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    UITableViewCell *cell = nil;
    static NSString *AutoCompleteRowIdentifier = @"AutoCompleteRowIdentifier";
    cell = [tableView dequeueReusableCellWithIdentifier:AutoCompleteRowIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc]
                initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:AutoCompleteRowIdentifier] ;
    }
    NSString *name=[[autocompleteName objectAtIndex:indexPath.row] objectForKey:@"name"];
    BOOL isUser=[[[autocompleteName objectAtIndex:indexPath.row] objectForKey:@"isUser"] boolValue];
    if (isUser) {
        
        cell.detailTextLabel.text=@"Member";
        
    }else{
        cell.detailTextLabel.text=@"Group";
    }
    cell.textLabel.text = name;
    cell.textLabel.textColor=[UIColor blackColor];
    cell.detailTextLabel.textColor=[UIColor lightGrayColor];
    return cell;
}

#pragma mark UITableViewDelegate methods

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    UITableViewCell *selectedCell = [tableView cellForRowAtIndexPath:indexPath];
    //    inviteMember.text = selectedCell.textLabel.text;
    //    memberID= [[autocompleteName objectAtIndex:indexPath.row] objectForKey:@"id"];
    BOOL isuser=[[[autocompleteName objectAtIndex:indexPath.row]objectForKey:@"isUser"] boolValue];
    [_tagsInputView.selectedTagsDic addObject:[NSDictionary dictionaryWithObjectsAndKeys:[NSNumber numberWithBool:isuser],@"IsUser",[[autocompleteName objectAtIndex:indexPath.row]objectForKey:@"id"],@"id",nil]];
    autocompleteTableView.hidden=YES;
    [_tagsInputView AddItem:[[autocompleteName objectAtIndex:indexPath.row] objectForKey:@"name"]];
    
    
}


-(BOOL)validateData
{
    if (![Validation validateusernamewithName:eventName.text]) {
        return false;
    }else if (![Validation validateusernamewithName:startdate]){
        return false;
    }else if(![Validation validateusernamewithName:searchBar.text]){
        return false;
    }else{
        return true;
    }
    
    
}


- (void)keyboardWasShown:(NSNotification *)notification
{
    tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(StouchesBegan)];
    [tapGesture setDelegate:self];
    [self.view addGestureRecognizer:tapGesture];
}

- (void)keyboardWasHide:(NSNotification *)notification
{
    [self.view removeGestureRecognizer:tapGesture];
}


-(void)textFieldDidBeginEditing:(UITextField *)textField{
    if (!datePicker.hidden) {
        [datePicker setHidden:YES];
    }
    CurrentTextField= textField;
}
- (void)textViewDidBeginEditing:(UITextView *)textView{
    if (!datePicker.hidden) {
        [datePicker setHidden:YES];
    }
    if ([textView.text isEqualToString:@"Message"]) {
        textView.text=@"";
    }
    textView.textColor=[UIColor blackColor];
    
}


- (void)textViewDidEndEditing:(UITextView *)textView
{
    [textView resignFirstResponder];
}


//If you want the keyboard to hide when you press the return
- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    return YES;
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(IBAction)AcceptEvents:(id)sender{
    
    webService=[[WebServiceFunc alloc]init];
    [webService setDelegate:self];
    hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    hud.mode = MBProgressHUDModeIndeterminate;
    NSString *urlString=AcceptEvent;
    serviceType=4;
    [webService getData:urlString withflagLogin:YES];
    
}



/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
