//
//  RequestPermissionsViewController.h
//  wiizme
//
//  Created by Amira on 2/13/17.
//  Copyright © 2017 Amira. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "AsyncImageView.h"
#import "WebServiceFunc.h"
@interface RequestPermissionsViewController : UIViewController<WebServiceProtocol>{
    __weak IBOutlet UILabel *userName;
    __weak IBOutlet AsyncImageView *UserImg;
    __weak IBOutlet UITableView *tableView;
    __weak IBOutlet UIButton *send;
    __weak IBOutlet UIButton *decline;
}
@property(nonatomic,strong)NSMutableDictionary *pusherData;
@end
