//
//  NeedHelpViewController.h
//  wiizme
//
//  Created by Amira on 12/19/16.
//  Copyright © 2016 Amira. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>
#import "WebServiceFunc.h"
#import "ASJTagsView.h"
#import "AKTagsInputView.h"
#import "AKTagsDefines.h"
#import <MessageUI/MessageUI.h>

@interface NeedHelpViewController : UIViewController<WebServiceProtocol,UITableViewDelegate,UITableViewDataSource,UITextFieldDelegate,AKTagsInputViewDelegate,UICollectionViewDelegate,UICollectionViewDataSource,UITextViewDelegate,MFMessageComposeViewControllerDelegate>
{
   IBOutlet UIScrollView *scrollView;
    __weak IBOutlet MKMapView *mapView;
    __weak IBOutlet UITextView *message;
    __weak IBOutlet UIButton *send;
    __weak IBOutlet UIButton *nearByMe;
    __weak IBOutlet UICollectionView *collectionView;
    NSMutableArray *autocompleteName,*autocompletesearch;
    UITableView *autocompleteTableView;
    
}
@property (nonatomic, strong)IBOutlet UIView *selectGroups;
@property(nonatomic,retain)CLLocationManager *locatioManager;
@property (nonatomic,strong) NSString *inviterId;
@property (nonatomic,strong) NSString *inviterName;
@property (nonatomic) BOOL isUser;

@end
