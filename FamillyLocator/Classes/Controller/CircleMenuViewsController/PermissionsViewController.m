//
//  PermissionsViewController.m
//  wiizme
//
//  Created by Amira on 2/8/17.
//  Copyright © 2017 Amira. All rights reserved.
//

#import "PermissionsViewController.h"
#import "MBProgressHUD.h"
#import "Constant.h"
#import "permissionTableViewCell.h"
#import "permissionTableViewCechkAllCell.h"
#import "Animation.h"
#import "SideMenuViewController.h"
@interface PermissionsViewController ()
{
    MBProgressHUD *hud;
    WebServiceFunc *webService;
    NSMutableArray *permiData;
    int serviceType;
}
@end

@implementation PermissionsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    CheckAllFlag = NO;
    CheckAllChecked= NO;
    //   permiView.userData=[[NSDictionary alloc]initWithObjectsAndKeys:self.groupData.groupID,@"userID",self.groupData.groupName,@"userName",self.groupData.logoID,@"userImage", nil];
    NSURL *imageUrl=[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",PhotosUrl,[self.userData objectForKey:@"userImage"]]];
    [UserImg setImageURL:imageUrl];
    userName.text=[self.userData objectForKey:@"userName"];
    
    webService=[[WebServiceFunc alloc]init];
    [webService setDelegate:self];
    hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    hud.mode = MBProgressHUDModeIndeterminate;
    NSString *urlString=[NSString stringWithFormat:@"%@%@",GetPermission,[self.userData objectForKey:@"userID"]];
    serviceType=1;
    [webService getData:urlString withflagLogin:YES];
    [Animation roundCornerForView:send withAngle:15.0f];
    //[send setHidden:YES];
    
}

#pragma TableView DataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return permiData.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    return [[permiData objectAtIndex:section] count];
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 50;
}

- (nullable UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    UIView *views=[[UIView alloc]initWithFrame:CGRectMake(0,0,self.view.frame.size.width,50)];
    UILabel *lbl= [[UILabel alloc] initWithFrame:CGRectMake(0,5,self.view.frame.size.width-100,40)];
    lbl.textColor=[UIColor blackColor];
    
    lbl.font=[UIFont fontWithName:@"Cairo" size:13.0f];
    views.backgroundColor=[UIColor whiteColor];
    
    UILabel *lbl2= [[UILabel alloc] initWithFrame:CGRectMake(self.view.frame.size.width-100,0,100,50)];
    lbl2.textColor=[UIColor blackColor];
    
    lbl2.text= @"User can Delete Permissions";
    
    lbl2.font=[UIFont fontWithName:@"Cairo" size:11.0f];
    lbl2.backgroundColor=[UIColor whiteColor];
    lbl2.numberOfLines = 2;
    
    if (section==0) {
        lbl.text= @"   His Permission On You";
        [views addSubview:lbl];
        [views addSubview:lbl2];
    } else   if (section==1) {
        lbl.text= @"  Check All ";
        UISwitch *CheckAllSwitch = [[UISwitch alloc] init];
        CheckAllSwitch.frame = CGRectMake(self.view.frame.size.width-100, 0, CheckAllSwitch.frame.size.width, CheckAllSwitch.frame.size.height);
        [CheckAllSwitch setOn:NO];
        
        [CheckAllSwitch addTarget:self action:@selector(flip:) forControlEvents:UIControlEventValueChanged];
        
        [views addSubview:CheckAllSwitch];
        [views addSubview:lbl];
    }else{
        
        lbl.text= @"   Your Permission On Him";
        
        [views addSubview:lbl];
        [views addSubview:lbl2];
    }
    return views;
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    
    permissionTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"PermiCell"];
    
    
    
    cell.CellData =[[permiData objectAtIndex:indexPath.section] objectAtIndex:indexPath.row];
    
    cell.CheckAllChecked = CheckAllChecked;
    cell.CheckAllFlag=CheckAllFlag;
    cell.cellid = indexPath.row;
    cell.sectionid = indexPath.section;
    [cell loadCellData];
    return cell;
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    // This will create a "invisible" footer
    return 0.01f;
}


- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    return [UIView new];
    
}



-(void) permiswitchStatusChange:(NSInteger) section setindex:(NSInteger)index setcelldata:(NSMutableDictionary *)celldata{
    
    [[permiData objectAtIndex:section] replaceObjectAtIndex:index withObject:celldata];
    
    
}
#pragma Webservice 

-(void)userWebserviceCallDidSuccessWithRespone:(NSMutableArray *)response{
    [hud setHidden:YES];
    if (serviceType==1) {
        
        NSMutableArray * Results = [[NSMutableArray alloc] init];
        Results = response;
        
        NSMutableArray * PermOnYouSection= [[NSMutableArray alloc] init];
        NSMutableArray * PermOnHimSection= [[NSMutableArray alloc] init];
        for (int i =0;i<Results.count; i++) {
            if ([[[Results objectAtIndex:i] objectForKey:@"myPermission"] intValue]==0) {
                
                [PermOnYouSection addObject:[[Results objectAtIndex:i] mutableCopy]];
                
            }else{
                [PermOnHimSection addObject:[[Results objectAtIndex:i] mutableCopy]];
            }
            
            
        }
        permiData = [[NSMutableArray alloc] init];
        [permiData addObject:[PermOnYouSection mutableCopy]];
        [permiData addObject: [[NSMutableArray alloc] init]];
        [permiData addObject: [PermOnHimSection mutableCopy]];
        
        [tableView reloadData];
        
    }else if (serviceType==2){
        
        [self.navigationController popViewControllerAnimated:YES];
        
    }
    
    
}
-(void)userWebserviceCallDidFaildwithError:(NSError *)error andMessage:(NSString *)message{
    [hud setHidden:YES];

    if ([message isEqualToString:@"401" ]) {
        UIStoryboard *storyboard=[UIStoryboard storyboardWithName:@"Main" bundle:nil];
        UIViewController* viewController=[storyboard instantiateViewControllerWithIdentifier:@"LoginController"];
        [self presentViewController:viewController animated:YES completion:nil];
    }
    
}

- (IBAction) flip: (id) sender {
    UISwitch *onoff = (UISwitch *) sender;
    CheckAllChecked=YES;
    CheckAllFlag=onoff.on;
    
    NSDictionary * userInfo = [NSDictionary dictionaryWithObjectsAndKeys:[NSNumber numberWithBool:onoff.on],@"CheckAllFlag", nil];
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"CheckAll" object:nil userInfo:userInfo];
    
    NSLog(@"%@", onoff.on ? @"On" : @"Off");
}

- (IBAction)showMenu
{
    
    SideMenuViewController *sideMenu=[self.storyboard instantiateViewControllerWithIdentifier:@"SideMenu"];
    [self.navigationController presentViewController:sideMenu animated:YES completion:nil];
}

-(IBAction)sendChange:(id)sender{
    hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    hud.mode = MBProgressHUDModeIndeterminate;
    serviceType=2;
    
    NSMutableArray* Permitionsdata = [[NSMutableArray alloc] init];
    
    [Permitionsdata addObjectsFromArray:[permiData objectAtIndex:0]];
    [Permitionsdata addObjectsFromArray:[permiData objectAtIndex:2]];
    
    
    [webService postDataWithUrlString:Sendpermission withData:Permitionsdata withflagLogin:YES];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
