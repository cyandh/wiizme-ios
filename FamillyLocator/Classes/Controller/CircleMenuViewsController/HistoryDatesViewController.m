//
//  HistoryDatesViewController.m
//  wiizme
//
//  Created by Amira on 2/25/17.
//  Copyright © 2017 Amira. All rights reserved.
//

#import "HistoryDatesViewController.h"
#import "Animation.h"
@interface HistoryDatesViewController ()
{
    UIDatePicker *datePicker;
    NSString *startDate,*endDate;
    UITapGestureRecognizer *tapGesture;
    BOOL validateDates,enddat;
}
@end

@implementation HistoryDatesViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [Animation borderView:startDateBut withBorderColor:[UIColor colorWithRed:128.0/255.0f green:130.0/255.0f blue:129.0/255.0f alpha:1.0f] andWidth:1.0f];
    [Animation borderView:endDateBut withBorderColor:[UIColor colorWithRed:128.0/255.0f green:130.0/255.0f blue:129.0/255.0f alpha:1.0f] andWidth:1.0f];
    
    tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(StouchesBegan)];
    [tapGesture setDelegate:self];
    [self.view addGestureRecognizer:tapGesture];
    
}

-(void)StouchesBegan {
    if (![datePicker isHidden]) {
        if (enddat) {
            
            if (!validateDates) {
                UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"" message:NSLocalizedString(@"Please Check End Date",nil) delegate:nil cancelButtonTitle:@"ok" otherButtonTitles:nil];
                
                [alert show];
            }
        }
        [datePicker setHidden:YES];
       // [self.view removeGestureRecognizer:tapGesture];
    }
    
}

-(IBAction)selectDate:(id)sender{
    if (datePicker==nil) {
    
    datePicker = [[UIDatePicker alloc]init];
    datePicker.frame=CGRectMake(0, self.view.frame.size.height-datePicker.frame.size.height, self.view.frame.size.width,datePicker.frame.size.height);
    datePicker.backgroundColor = [UIColor blackColor];
    [datePicker setValue:[UIColor whiteColor] forKey:@"textColor"];
    datePicker.datePickerMode = UIDatePickerModeDate;
    datePicker.hidden = NO;
    
    [datePicker addTarget:self action:@selector(dateText:) forControlEvents:UIControlEventValueChanged];
    
    [self.view addSubview:datePicker];
  
    }else{
        if ([datePicker isHidden]) {
            
            [datePicker setHidden:NO];

        }
    }
    if ([sender tag]==100) {
        datePicker.tag=100;
        [datePicker setDate:[NSDate date]];
        [datePicker setMaximumDate:[NSDate date]];
        enddat=NO;
    }else{
        datePicker.tag=200;
        [datePicker setDate:[NSDate date]];
        [datePicker setMaximumDate:[NSDate date]];
        enddat=YES;
    }
}
-(IBAction)doneButAction:(id)sender{
    if (!validateDates) {
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"" message:NSLocalizedString(@"Please Check History Dates",nil) delegate:nil cancelButtonTitle:@"ok" otherButtonTitles:nil];
        
        [alert show];
    }else{
    NSDictionary *dates=[[NSDictionary alloc]initWithObjectsAndKeys:startDate,@"StartDate",endDate,@"EndDate", nil];
    [self.myDelegate historyViewControllerDismissed:dates];
    [self dismissViewControllerAnimated:YES completion:nil];
    }
}

-(IBAction)closeView:(id)sender{
    [self dismissViewControllerAnimated:YES completion:nil];
}
-(void)dateText:(id)sender
{
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    NSLocale *locale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US"];
    [dateFormat setLocale:locale];
    [dateFormat  setDateFormat:@"yyyy-MM-dd"];
    if ([sender tag]==100) {
        [startDateBut setTitle:[dateFormat stringFromDate:datePicker.date]  forState:UIControlStateNormal] ;
        startDate=[dateFormat stringFromDate:datePicker.date];
    }else{
        [endDateBut setTitle:[dateFormat stringFromDate:datePicker.date]  forState:UIControlStateNormal] ;
        endDate=[dateFormat stringFromDate:datePicker.date];
    }
    
    if ((startDate.length==0)||(endDate.length==0)) {
        
    }else{
        NSDate *datestart =[dateFormat dateFromString:startDate];
        NSDate *dateend =[dateFormat dateFromString:endDate];
    
        
        switch ([dateend compare:datestart]) {
            case NSOrderedAscending:
                //Do your logic when date1 > date2
                validateDates=false;
                break;
                
            case NSOrderedDescending:
                //Do your logic when date1 < date2
                validateDates=true;
                break;
                
            case NSOrderedSame:
                //Do your logic when date1 = date2
                validateDates=false;
                break;
        }
    }
    

    



    
    // birthday=datePicker.date;
    
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
