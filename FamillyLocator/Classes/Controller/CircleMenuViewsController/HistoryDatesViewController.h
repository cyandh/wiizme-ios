//
//  HistoryDatesViewController.h
//  wiizme
//
//  Created by Amira on 2/25/17.
//  Copyright © 2017 Amira. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol HistoryDatesDelegate <NSObject>
-(void) historyViewControllerDismissed:(NSDictionary *)dates;
@end

@interface HistoryDatesViewController : UIViewController
{
    __weak IBOutlet UIButton *startDateBut;
    __weak IBOutlet UIButton *endDateBut;
    
}

@property (nonatomic, assign) id<HistoryDatesDelegate>    myDelegate;

@end
