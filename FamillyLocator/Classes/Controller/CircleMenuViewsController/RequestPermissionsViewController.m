//
//  RequestPermissionsViewController.m
//  wiizme
//
//  Created by Amira on 2/13/17.
//  Copyright © 2017 Amira. All rights reserved.
//

#import "RequestPermissionsViewController.h"
#import "MBProgressHUD.h"
#import "Constant.h"
#import "permissionTableViewCell.h"
#import "Animation.h"
#import "SideMenuViewController.h"

@interface RequestPermissionsViewController ()
{
    NSMutableArray *permiData;
    WebServiceFunc *webService;
    MBProgressHUD *hud;
}
@end

@implementation RequestPermissionsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.

    permiData=[NSMutableArray arrayWithArray:[[self.pusherData objectForKey:@"Premissions"] mutableCopy]];
    NSString *firstName=[[self.pusherData objectForKey:@"User"] objectForKey:@"FirstName"];
    NSString *lastName=[[self.pusherData objectForKey:@"User"] objectForKey:@"FamilyName"];
    userName.text=[NSString stringWithFormat:@"%@ %@",firstName,lastName];
    NSURL *imageUrl=[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",PhotosUrl,[[self.pusherData objectForKey:@"User"] objectForKey:@"PhotoId"]]];
    [UserImg setImageURL:imageUrl];
    [tableView reloadData];
    [Animation roundCornerForView:send withAngle:15.0f];
    [Animation roundCornerForView:decline withAngle:15.0f];
}

- (IBAction)showMenu
{
    
    SideMenuViewController *sideMenu=[self.storyboard instantiateViewControllerWithIdentifier:@"SideMenu"];
    [self.navigationController presentViewController:sideMenu animated:YES completion:nil];
}

-(IBAction)acceptPermi:(id)sender{
    webService=[[WebServiceFunc alloc]init];
    [webService setDelegate:self];
    hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    hud.mode = MBProgressHUDModeIndeterminate;
    [webService postDataWithUrlString:RespondRequestPermission withData:permiData withflagLogin:YES];
}

-(IBAction)declinePermi{
    [self.navigationController popViewControllerAnimated:YES];
    
}
#pragma TableView DataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return permiData.count;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 50;
}




- (nullable UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    UIView *views=[[UIView alloc]initWithFrame:CGRectMake(0,0,self.view.frame.size.width,50)];
    UILabel *lbl= [[UILabel alloc] initWithFrame:CGRectMake(0,5,self.view.frame.size.width-100,40)];
    lbl.textColor=[UIColor blackColor];
    
    lbl.font=[UIFont fontWithName:@"Cairo" size:13.0f];
    views.backgroundColor=[UIColor whiteColor];
    
    UILabel *lbl2= [[UILabel alloc] initWithFrame:CGRectMake(self.view.frame.size.width-100,0,100,50)];
    lbl2.textColor=[UIColor blackColor];
    
    lbl2.text= @"User can Delete Permissions";
    
    lbl2.font=[UIFont fontWithName:@"Cairo" size:11.0f];
    lbl2.backgroundColor=[UIColor whiteColor];
    lbl2.numberOfLines = 2;

        lbl.text= @"   Has request the following Permissions";
        [views addSubview:lbl];
        [views addSubview:lbl2];

    return views;
    
}

//- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
//
//}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    permissionTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"PermiCell"];
    
    
    cell.permiLBL.text=[[[permiData objectAtIndex:indexPath.row] objectForKey:@"ActionType"] objectForKey:@"Name"];
    [cell.cancelableSwitch setUserInteractionEnabled:NO];
    BOOL userInteract=[[[permiData objectAtIndex:indexPath.row]  objectForKey:@"Cancelable"] boolValue];
    if (userInteract) {
        [cell.permiSwitch setUserInteractionEnabled:YES];
        
    }else{
        [cell.permiSwitch setUserInteractionEnabled:NO];
    }
    [cell.permiSwitch setTag:indexPath.row];
    BOOL switchValue=[[[permiData objectAtIndex:indexPath.row] objectForKey:@"IsConfirmed"] boolValue];
    
    if (switchValue) {
        [cell.permiSwitch setOn:YES animated:YES];
        
    }else{
        [cell.permiSwitch setOn:NO animated:YES];
    }
    if (userInteract) {
        // [cell.permiSwitch setThumbTintColor:[UIColor redColor]];
         [cell.cancelableSwitch setOn:YES animated:YES];
        cell.permiSwitch.tintColor = [UIColor colorWithRed:239.0/255.0f green:239.0/255.0f blue:244.0/255.0f alpha:1.0f]; // the "off" color
        cell.permiSwitch.onTintColor = [UIColor colorWithRed:41.0/255.0f green:71.0/255.0f blue:95.0/255.0f alpha:1.0f]; // the "on" color
    }else {
        [cell.cancelableSwitch setOn:NO animated:YES];
        cell.permiSwitch.tintColor = [UIColor colorWithRed:239.0/255.0f green:239.0/255.0f blue:244.0/255.0f alpha:1.0f]; // the "off" color
        cell.permiSwitch.onTintColor = [UIColor colorWithRed:239.0/255.0f green:239.0/255.0f blue:244.0/255.0f alpha:1.0f]; // the "on" color
    }

    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    // This will create a "invisible" footer
    return 0.01f;
}


- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    return [UIView new];
    
}

- (IBAction)switchStatusChange:(UISwitch *)sender
{
    [send setHidden:NO];
    int i=sender.tag;
    BOOL value;
    if (sender.isOn) {
        value=true;
    }else{
        value=false;
    }

    [[permiData objectAtIndex:i] setValue:[NSNumber numberWithBool:value] forKey:@"IsConfirmed"];
 
    
}

-(void)userWebserviceCallDidSuccessWithRespone:(NSMutableArray *)response{
    [hud setHidden:YES];
    [self.navigationController popViewControllerAnimated:YES];
 
}
-(void)userWebserviceCallDidFaildwithError:(NSError *)error andMessage:(NSString *)message{
    [hud setHidden:YES];
    NSLog(@"%@",error.localizedDescription);
    if ([message isEqualToString:@"401" ]) {
        UIStoryboard *storyboard=[UIStoryboard storyboardWithName:@"Main" bundle:nil];
        UIViewController* viewController=[storyboard instantiateViewControllerWithIdentifier:@"LoginController"];
        [self presentViewController:viewController animated:YES completion:nil];
    }
    
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
