//
//  InviteViewController.h
//  FamillyLocator
//
//  Created by Amira on 11/28/16.
//  Copyright © 2016 Amira. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "WebServiceFunc.h"
@interface InviteViewController : UIViewController<WebServiceProtocol,UITableViewDelegate,UITableViewDataSource,UITextFieldDelegate>
{
    __weak IBOutlet UIScrollView *scrollView;
    __weak IBOutlet UITextField *inviteMember;
    __weak IBOutlet UITextField *inviteNewMember;
    __weak IBOutlet UIButton *selectGroup;
    __weak IBOutlet UITextView *message;
    __weak IBOutlet UIButton *send;
    
    NSMutableArray *autocompleteName;
    UITableView *autocompleteTableView;
    UITextField * ActiveTextField;
}

@property(nonatomic,strong)NSString *groupID;
@end
