//
//  PermissionsViewController.h
//  wiizme
//
//  Created by Amira on 2/8/17.
//  Copyright © 2017 Amira. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AsyncImageView.h"
#import "WebServiceFunc.h"
@interface PermissionsViewController : UIViewController<WebServiceProtocol>{
    __weak IBOutlet UIScrollView *scrollView;
    __weak IBOutlet UILabel *userName;
    __weak IBOutlet AsyncImageView *UserImg;
    __weak IBOutlet UITableView *tableView;
    __weak IBOutlet UIButton *send;
    BOOL CheckAllFlag;
    BOOL CheckAllChecked;
}
@property(nonatomic,strong)NSDictionary *userData;
@property(nonatomic)BOOL fromGroup;
@end
