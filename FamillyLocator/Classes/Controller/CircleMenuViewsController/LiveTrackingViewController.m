//
//  LifeTrackingViewController.m
//  wiizme
//
//  Created by Amira on 1/29/17.
//  Copyright © 2017 Amira. All rights reserved.
//

#import "LiveTrackingViewController.h"
#import "MyLocation.h"
#import "MBProgressHUD.h"
#import "Constant.h"
#import "UIViewController+RemoteNotificationData.h"
#import "AppDelegate.h"
#import "District.h"
#import "GenericPinAnnotationView.h"
#import "MultiRowCalloutAnnotationView.h"
#import "UIViewController+HeaderView.h"
#import "District.h"
#import "GenericPinAnnotationView.h"
#import "MultiRowCalloutAnnotationView.h"
#import "UIViewController+HeaderView.h"
#import "SideMenuViewController.h"
#import "NotificationPostViewController.h"
@interface LiveTrackingViewController ()
{
    double userLatitude,userLogitude;
    WebServiceFunc *webService;
    MBProgressHUD *hud;
    NSString *address;
    MyLocation *annotation;
    CLLocationCoordinate2D coordinate;
    NSMutableArray *categoriesId,*categoryList;
    int viewAction,serviceType;
    LPPopupListView *listView;
    
}
@end

@implementation LiveTrackingViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    

    NSLog(@"%@",self.groupData.profileImg);
    [img setImageURL:[NSURL URLWithString:self.groupData.profileImg]];
    name.text=self.groupData.groupName;
    
    self.mapView.showsUserLocation = NO;
    self.mapView.showsBuildings = YES;
    
    self.locatioManager = [CLLocationManager new];
    if ([self.locatioManager respondsToSelector:@selector(requestWhenInUseAuthorization)]) {
        [self.locatioManager requestWhenInUseAuthorization];
    }
}

-(void)viewWillDisappear:(BOOL)animated{
    if (viewAction==1) {
        
    }else{
    webService =[[WebServiceFunc alloc]init];
    [webService setDelegate:self];
    hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    hud.mode = MBProgressHUDModeIndeterminate;

    [webService postDataWithUrlString:ExitLiveTracking withData:self.usersId withflagLogin:YES];
    }
}

- (IBAction)showMenu
{
    
    SideMenuViewController *sideMenu=[self.storyboard instantiateViewControllerWithIdentifier:@"SideMenu"];
    [self.navigationController presentViewController:sideMenu animated:YES completion:nil];
}
-(void)listenerUpdatedLocations{
    
    self.client = ((AppDelegate *)[[UIApplication sharedApplication] delegate]).client;
    
    NSString *userID=[[NSUserDefaults standardUserDefaults] valueForKey:@"UserId"];
    
    PTPusherChannel *channel = [self.client subscribeToChannelNamed:userID];
    
    [channel bindToEventNamed:@"send_live_tracked" handleWithBlock:^(PTPusherEvent *channelEvent) {
        // channelEvent.data is a NSDictianary of the JSON object received
        NSMutableArray *message= [channelEvent.data mutableCopy] ;
        NSLog(@"%@",channelEvent.data);
        NSString *userid=[[[message objectAtIndex:0] objectForKey:@"User"] objectForKey:@"Id"];
        for (id<MKAnnotation> ann in self.mapView.annotations)
        {
            if ([ann isKindOfClass:[District class]])
            {
                District *yacAnn = (District *)ann;
                NSLog(@"%@",yacAnn.userID);
                if ([yacAnn.userID isEqualToString:userid])
                {
                    CLLocationCoordinate2D coordinate;
                    coordinate.latitude =[[[message objectAtIndex:0] objectForKey:@"LastKnowingLatitude"] doubleValue];
                    coordinate.longitude =[[[message objectAtIndex:0] objectForKey:@"LastKnowingLangitude"] doubleValue];
                    yacAnn.coordinate = coordinate;
                    break;
                }
            }
        }
        
    }];
    
    [self.client connect];
}

-(void)viewDidDisappear:(BOOL)animated{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}




-(void)postLocation{
    for (int i=0; i< self.locations.count; i++) {
        NSDictionary *dic=[self.locations objectAtIndex:i];
         [_mapView addAnnotation:[District demoAnnotationFactory:dic]];
    }
    
//      MKMapCamera *camera = [MKMapCamera cameraLookingAtCenterCoordinate:coordinate fromEyeCoordinate:CLLocationCoordinate2DMake(coordinate.latitude,coordinate.longitude) eyeAltitude:1000];
//    [self.mapView setCamera:camera animated:YES];
    if (!self.locations.count==0) {
      [self recenterMap];
    }
    
}

- (void)recenterMap {
    NSArray *coordinates = [self.mapView valueForKeyPath:@"annotations.coordinate"];
    // look for the minimum and maximum coordinate
    CLLocationCoordinate2D maxCoord = {-90.0f, -180.0f};
    CLLocationCoordinate2D minCoord = {90.0f, 180.0f};
    for(NSValue *value in coordinates) {
        CLLocationCoordinate2D coord = {0.0f, 0.0f};
        [value getValue:&coord];
        if(coord.longitude > maxCoord.longitude) {
            maxCoord.longitude = coord.longitude;
        }
        if(coord.latitude > maxCoord.latitude) {
            maxCoord.latitude = coord.latitude;
        }
        if(coord.longitude < minCoord.longitude) {
            minCoord.longitude = coord.longitude;
        }
        if(coord.latitude < minCoord.latitude) {
            minCoord.latitude = coord.latitude;
        }
    }
    // create a region
    MKCoordinateRegion region = {{0.0f, 0.0f}, {0.0f, 0.0f}};
    region.center.longitude = (minCoord.longitude + maxCoord.longitude) / 2.0;
    region.center.latitude = (minCoord.latitude + maxCoord.latitude) / 2.0;
    // calculate the span
    region.span.longitudeDelta = maxCoord.longitude - minCoord.longitude;
    region.span.latitudeDelta = maxCoord.latitude - minCoord.latitude;
    // center the map on that region
    [self.mapView setRegion:region animated:YES];
}

#pragma Mark - Current Location


- (void)reverseGeocoder:(MKReverseGeocoder *)geocoder didFindPlacemark:(MKPlacemark *)placemark {
    NSLog(@"I'm at %@", placemark.locality);
    NSLog(@"add %@",placemark.description);
    NSLog(@"add %@",placemark.name);
    NSLog(@"cs%@",placemark.thoroughfare);
    address=placemark.name;
    annotation = [[MyLocation alloc] initWithName:address coordinate:coordinate] ;
    
    [self.mapView addAnnotation:annotation];
}

#pragma Mark -  Add Annotations for Locations From Service

//This is the method that gets called for every annotation you added to the map
- (MKAnnotationView *)mapView:(MKMapView *)mapView viewForAnnotation:(id <MKAnnotation>)annotation {
    if ([annotation isKindOfClass:[MKUserLocation class]])
        return nil;
    
    if (![annotation conformsToProtocol:@protocol(MultiRowAnnotationProtocol)])
        return nil;
    
    NSObject <MultiRowAnnotationProtocol> *newAnnotation = (NSObject <MultiRowAnnotationProtocol> *)annotation;
    if (newAnnotation == _calloutAnnotation)
    {
        MultiRowCalloutAnnotationView *annotationView = (MultiRowCalloutAnnotationView *)[mapView dequeueReusableAnnotationViewWithIdentifier:MultiRowCalloutReuseIdentifier];
        if (!annotationView)
        {
            annotationView = [MultiRowCalloutAnnotationView calloutWithAnnotation:newAnnotation onCalloutAccessoryTapped:^(MultiRowCalloutCell *cell, UIControl *control, NSDictionary *userData) {
                // This is where I usually push in a new detail view onto the navigation controller stack, using the object's ID
                NSLog(@"Representative (%@) with ID '%@' was tapped.", cell.subtitle, userData[@"id"]);
                if (viewAction==1) {
                    NotificationPostViewController *notifyView=[self.storyboard instantiateViewControllerWithIdentifier:@"NotificationPostController"];

                        notifyView.timeLinePost=[[NSDictionary alloc]init];
                        notifyView.postId=userData[@"id"];
                        [self.navigationController pushViewController:notifyView animated:YES];
                
                }
            }];
        }
        else
        {
            annotationView.annotation = newAnnotation;
        }
        annotationView.parentAnnotationView = _selectedAnnotationView;
        annotationView.mapView = mapView;
        return annotationView;
    }
    GenericPinAnnotationView *annotationView = (GenericPinAnnotationView *)[mapView dequeueReusableAnnotationViewWithIdentifier:GenericPinReuseIdentifier];
    if (!annotationView)
    {
        annotationView = [GenericPinAnnotationView pinViewWithAnnotation:newAnnotation];
    }
    annotationView.annotation = newAnnotation;
    return annotationView;
}

-(void)mapView:(MKMapView *)mapView didUpdateUserLocation:(MKUserLocation *)userLocation {
    
        MKMapCamera *camera = [MKMapCamera cameraLookingAtCenterCoordinate:userLocation.coordinate fromEyeCoordinate:CLLocationCoordinate2DMake(userLocation.coordinate.latitude, userLocation.coordinate.longitude) eyeAltitude:1000];
        [mapView setCamera:camera animated:YES];
    
    userLatitude=[userLocation coordinate].latitude;
    userLogitude=[userLocation coordinate].longitude;
    
    NSLog(@"latitude:%f",userLatitude);
    NSLog(@"longitude:%f",userLogitude);
    // [self compareAnnotationsFromService];
    
    
    
}

- (void)mapView:(MKMapView *)mapView didSelectAnnotationView:(MKAnnotationView *)aView
{
    id<MKAnnotation> annotation = aView.annotation;
    if (!annotation || ![aView isSelected])
        return;
    if ( NO == [annotation isKindOfClass:[MultiRowCalloutCell class]] &&
        [annotation conformsToProtocol:@protocol(MultiRowAnnotationProtocol)] )
    {
        NSObject <MultiRowAnnotationProtocol> *pinAnnotation = (NSObject <MultiRowAnnotationProtocol> *)annotation;
        if (!_calloutAnnotation)
        {
            _calloutAnnotation = [[MultiRowAnnotation alloc] init];
            [_calloutAnnotation copyAttributesFromAnnotation:pinAnnotation];
            [mapView addAnnotation:_calloutAnnotation];
        }
        _selectedAnnotationView = aView;
        return;
    }
    [mapView setCenterCoordinate:annotation.coordinate animated:YES];
    _selectedAnnotationView = aView;
}

- (void)mapView:(MKMapView *)mapView didDeselectAnnotationView:(MKAnnotationView *)aView
{
    if ( NO == [aView.annotation conformsToProtocol:@protocol(MultiRowAnnotationProtocol)] )
        return;
    if ([aView.annotation isKindOfClass:[MultiRowAnnotation class]])
        return;
    GenericPinAnnotationView *pinView = (GenericPinAnnotationView *)aView;
    if (_calloutAnnotation && !pinView.preventSelectionChange)
    {
        [mapView removeAnnotation:_calloutAnnotation];
        _calloutAnnotation = nil;
    }
}



-(void)compareAnnotationsFromService{
    
    //initialize your map view and add it to your view hierarchy - **set its delegate to self***
    CLLocationCoordinate2D coordinateArray[2];
    coordinateArray[0] = CLLocationCoordinate2DMake(userLatitude, userLogitude);
    //  coordinateArray[1] = CLLocationCoordinate2DMake(self.lat, self.lon);
    
    
    self.routeLine = [MKPolyline polylineWithCoordinates:coordinateArray count:2];
    [self.mapView setVisibleMapRect:[self.routeLine boundingMapRect]]; //If you want the route to be visible
    
    [self.mapView addOverlay:self.routeLine];
}

-(MKOverlayRenderer *)mapView:(MKMapView *)mapView rendererForOverlay:(id<MKOverlay>)overlay {
    
    MKPolylineRenderer *renderer = [[MKPolylineRenderer alloc] initWithOverlay:overlay];
    renderer.strokeColor =  [UIColor colorWithRed:47.0/255.0 green:61.0/255.0 blue:70.0/255.0 alpha:1.0f];
    renderer.lineWidth = 4.0;
    
    return renderer;
}



-(void)userWebserviceCallDidSuccessWithRespone:(NSMutableArray *)response{
    [hud setHidden:YES];
    if (serviceType==1) {
        categoryList=response;
        [self chooseCategory];
    }else if (serviceType==2){
        
        if (response.count>0) {
            NSMutableArray *arr=[(NSDictionary*)response objectForKey:@"posts"];
            for (int i=0; i<arr.count; i++) {
                NSString *firstName=[[[arr objectAtIndex:i] objectForKey:@"user"] objectForKey:@"firstName"];
                NSString *lastName=[[[arr objectAtIndex:i]objectForKey:@"user"] objectForKey:@"familyName"];
                NSString *name=[NSString stringWithFormat:@"%@ %@",firstName,lastName];
                NSDictionary *dic=[self editDateAndTime:[[arr objectAtIndex:i] objectForKey:@"lastPostionDate"] ];
                NSString *date=[NSString stringWithFormat:@"%@  %@",[dic objectForKey:@"date"],[dic objectForKey:@"time"]];
                NSString *battery;
                if ([[arr objectAtIndex:i] objectForKey:@"batteryStatue"]==[NSNull null]) {
                    battery=@"";
                }else{
                    battery=[[[arr objectAtIndex:i] objectForKey:@"user"]objectForKey:@"batteryStatue"];
                }
                dic=[NSDictionary dictionaryWithObjectsAndKeys:[NSNumber numberWithDouble:[[[[arr objectAtIndex:i]objectForKey:@"post"]objectForKey:@"latitude"]doubleValue]],@"lat",[NSNumber numberWithDouble:[[[[arr objectAtIndex:i] objectForKey:@"post"]objectForKey:@"longitude"]doubleValue]],@"lng",name,@"name",battery,@"battery",date,@"date",[[[arr objectAtIndex:i] objectForKey:@"post"] objectForKey:@"id"],@"userid",[NSNumber numberWithBool:YES],@"fromSeeThis", nil];
                [self.locations addObject:dic];
                [_mapView addAnnotation:[District demoAnnotationFactory:dic]];
            }
            
        }
        
        
    }

}
-(void)userWebserviceCallDidFaildwithError:(NSError *)error andMessage:(NSString *)message{
    [hud setHidden:YES];
    NSLog(@"%@",error.localizedDescription);
    if ([message isEqualToString:@"401" ]) {
        UIStoryboard *storyboard=[UIStoryboard storyboardWithName:@"Main" bundle:nil];
        UIViewController* viewController=[storyboard instantiateViewControllerWithIdentifier:@"LoginController"];
        [self presentViewController:viewController animated:YES completion:nil];
    }
    
}

-(NSMutableAttributedString *)attribut:(NSString*)imgname andtxtName:(NSString*)txtName{
    NSTextAttachment *attachment = [[NSTextAttachment alloc] init];
    attachment.image = [UIImage imageNamed:imgname];
    
    NSAttributedString *attachmentString = [NSAttributedString attributedStringWithAttachment:attachment];
    NSAttributedString *mystring = [[NSAttributedString alloc]initWithString:[NSString stringWithFormat:@"  %@  ",txtName]];
    NSMutableAttributedString *myStringimg= [[NSMutableAttributedString alloc] initWithAttributedString:attachmentString];
    
    [myStringimg appendAttributedString:mystring];
    
    return myStringimg;
}

-(IBAction)selectActionView:(id)sender{
    
    UIAlertController * alert=   [UIAlertController
                                  alertControllerWithTitle:nil
                                  message:nil
                                  preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* seeThisPost = [UIAlertAction
                               actionWithTitle:@"See This"
                               style:UIAlertActionStyleDefault
                               handler:^(UIAlertAction * action)
                               {
                                   [_mapView removeAnnotations:_mapView.annotations];
                                   viewAction=1;
                                   viewTitle.text=@"See This";
                                   webService =[[WebServiceFunc alloc]init];
                                   [webService setDelegate:self];
                                   hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
                                   hud.mode = MBProgressHUDModeIndeterminate;
                                   NSString *urlString=CategoryList;
                                   serviceType=1;
                                   [webService getData:urlString withflagLogin:YES];
                                   
                                   [alert dismissViewControllerAnimated:YES completion:nil];
                                   
                               }];
    
    alert.view.tintColor = [UIColor grayColor];
    
    [seeThisPost setValue:[[UIImage imageNamed:@"menu-seeThis.png"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal] forKey:@"image"];
    
    UIAlertAction* LifeTracking = [UIAlertAction
                                  actionWithTitle:@"Life Tracking"
                                  style:UIAlertActionStyleDefault
                                  handler:^(UIAlertAction * action)
                                  {
                                      [_mapView removeAnnotations:_mapView.annotations];
                                      viewAction=2;
                                      viewTitle.text=@"Live Tracking";
                                      [self listenerUpdatedLocations];
                                      [self postLocation];
                                      [alert dismissViewControllerAnimated:YES completion:nil];
                                      
                                  }];
    
    [LifeTracking setValue:[[UIImage imageNamed:@"menu-map.png"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal] forKey:@"image"];
    
    [alert addAction:seeThisPost];
    [alert addAction:LifeTracking];

    
    [self presentViewController:alert animated:YES completion:nil];
    
    
    //    UIAlertView * alert=[[UIAlertView alloc]initWithTitle:nil message:nil delegate:self cancelButtonTitle:@"cancel" otherButtonTitles:@"All Groups",@"Select Groups",@"Select Member", nil];
    //
    //    [alert show];
    
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    if (buttonIndex==1) {
        viewAction=1;
        
    } else if(buttonIndex==2){
        

    }
    
    NSLog(@"%li",buttonIndex);
    NSLog(@"%@",[alertView buttonTitleAtIndex:buttonIndex]);
}


-(void)chooseCategory{
    
    float paddingTopBottom = 20.0f;
    float paddingLeftRight = 20.0f;
    
    CGPoint point = CGPointMake(paddingLeftRight, (self.navigationController.navigationBar.frame.size.height + paddingTopBottom) + paddingTopBottom);
    CGSize size = CGSizeMake((self.view.frame.size.width - (paddingLeftRight * 2)), self.view.frame.size.height - ((self.navigationController.navigationBar.frame.size.height + paddingTopBottom) + (paddingTopBottom * 2)));
    
    self.selectedIndexes=nil;
    listView=[[LPPopupListView alloc]initWithTitle:@"" list:[self list] selectedIndexes:self.selectedIndexes point:point size:size multipleSelection:YES disableBackgroundInteraction:NO];
    
    [listView.layer setShadowColor:[[UIColor darkGrayColor] CGColor]];
    [listView.layer setShadowOffset:CGSizeMake(0, 0)];
    [listView.layer setShadowRadius:20.0];
    [listView.layer setShadowOpacity:1];
    listView.clipsToBounds = NO;
    listView.layer.masksToBounds = NO;
    
    listView.searchBar.hidden=YES;
    
    [listView.searchBar setDelegate:self];
    
    listView.delegate = self;
    
    [listView showInView:self.navigationController.view animated:YES];
}


#pragma mark - Array List

- (NSArray *)list
{
    NSMutableArray *activ=[[NSMutableArray alloc]init];

    for (int i=0; i<categoryList.count;i++) {
            
        [activ addObject:[[categoryList objectAtIndex:i] objectForKey:@"value"]];
        
    }
    
    return activ;
}

#pragma mark - LPPopupListViewDelegate

- (void)popupListView:(LPPopupListView *)popUpListView didSelectIndex:(NSInteger)index
{
    NSLog(@"popUpListView - didSelectIndex: %d", index);
}

- (void)popupListViewDidHide:(LPPopupListView *)popUpListView selectedIndexes:(NSIndexSet *)selectedIndexes
{
    
    if ([selectedIndexes count]==0) {
        
    }else{
        categoriesId=[[NSMutableArray alloc]init];
        NSLog(@"popupListViewDidHide - selectedIndexes: %@", selectedIndexes.description);
        self.selectedIndexes = [[NSMutableIndexSet alloc] initWithIndexSet:selectedIndexes];
        //[selectedIndexes get]
        [selectedIndexes enumerateIndexesUsingBlock:^(NSUInteger idx, BOOL *stop) {
            [categoriesId addObject:[[categoryList objectAtIndex:idx]objectForKey:@"fieldId"]];
            
        }];
        
        webService =[[WebServiceFunc alloc]init];
        [webService setDelegate:self];
        hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        hud.mode = MBProgressHUDModeIndeterminate;
        NSString *urlString;
        if (self.isGroup) {
            urlString=[NSString stringWithFormat:@"%@%@",seeThisMapGroup,self.groupData.groupID];
        }else{
        urlString=[NSString stringWithFormat:@"%@%@",seethisMapUser,self.groupData.groupID];
        }
        
        serviceType=2;
        [webService postDataWithUrlString:urlString withData:categoriesId withflagLogin:YES];
        
        
    }
    
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
