//
//  ShowTableViewCell.h
//  MIxFM
//
//  Created by agamil on 5/15/17.
//  Copyright © 2017 cyandh. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AsyncImageView.h"

#import <QuartzCore/QuartzCore.h>



@interface NewsListCell : UITableViewCell{
    
    IBOutlet UIView * Content;
    IBOutlet AsyncImageView * CellImage;
    IBOutlet UIImageView * LiveImage;
    IBOutlet UILabel * titleLbl;
      IBOutlet UILabel * DescriptionLbl;
    IBOutlet UIView * titleView;
}

-(void)loadCellData:(NSDictionary* )CellData;


@end
