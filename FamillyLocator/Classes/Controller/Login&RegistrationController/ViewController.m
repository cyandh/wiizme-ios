//
//  ViewController.m
//  FamilyLocator
//
//  Created by Amira on 9/18/16.
//  Copyright © 2016 Amira. All rights reserved.
//

#import "ViewController.h"
#import "FacebookLogin.h"
#import "Animation.h"
#import "MBProgressHUD.h"
#import "RegistrationViewController.h"
#import "Validation.h"
#import "Reachability.h"
#import "SignUpLinkedinViewController.h"
#import "Constant.h"
#import "ConfirmationCodeViewController.h"
#import "ForgetPasswordViewController.h"





@interface ViewController ()
{
    WebServiceFunc *webserivce;
    MBProgressHUD *hud;
    int checkremember;
    NSDictionary *fbData;
    int serviceType;
}
@end
@implementation ViewController
@synthesize loginFB;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(StouchesBegan)];
    
    [scrollview addGestureRecognizer:tapGesture];
    
    [Animation roundCornerForView:emailTXT withAngle:19.0F];
    [Animation roundCornerForView:password withAngle:19.0f];
    [Animation roundCornerForView:loginBTN withAngle:19.0f];
    [Animation roundCornerForView:createBTN withAngle:19.0f];
    
    [[emailTXT layer] setBorderWidth:1.0f];
    [[emailTXT layer] setBorderColor:[UIColor colorWithRed:128.0/255.0f green:130.0/255.0f blue:129.0/255.0f alpha:1.0f].CGColor];
    UIView *paddingView = [[UIView alloc] initWithFrame:CGRectMake(0, 0,10, 20)];
    emailTXT.leftView = paddingView;
    emailTXT.leftViewMode = UITextFieldViewModeAlways;
    
    [[password layer] setBorderWidth:1.0f];
    [[password layer] setBorderColor:[UIColor colorWithRed:128.0/255.0f green:130.0/255.0f blue:129.0/255.0f alpha:1.0f].CGColor];
    UIView *paddingView1 = [[UIView alloc] initWithFrame:CGRectMake(0, 0,10, 20)];
    password.leftView = paddingView1;
    password.leftViewMode = UITextFieldViewModeAlways;
    
    [[createBTN layer] setBorderWidth:1.0f];
    [[createBTN layer] setBorderColor:[UIColor colorWithRed:6.0/255.0f green:188.0/255.0f blue:228.0/255.0f alpha:1.0f].CGColor];
    
}

-(IBAction)rememberme:(id)sender{
    if (remmber.selected) {
        [remmber setSelected:NO];
        checkremember=0;
    }else{
        [remmber setSelected:YES];
        checkremember=1;
    }
}
-(void)StouchesBegan {
    
    [emailTXT resignFirstResponder];
    [password resignFirstResponder];
    
}

-(BOOL)validateData
{
    if(![Validation validateemailwithEmail:emailTXT.text])
    {
        return false;
    }else if([password.text stringByReplacingOccurrencesOfString:@" " withString:@""].length==0){
        return false;
    }else{
        return true;
    }
    
}


-(void)getFacebookData:(NSDictionary*)linkedInData
{
    NSLog(@"%@ %@ %@",[linkedInData valueForKey:@"id"] ,[linkedInData valueForKey:@"email"] ,[linkedInData valueForKey:@"name"]);
    NSLog(@"%@",linkedInData);
    fbData=linkedInData;
    
    [self performSegueWithIdentifier:@"registView" sender:nil];
}
-(void)failWithResponseFaceBook:(NSString*)message
{
    // [self endLoadingWithErrorMessage:message];
}


-(void)getLinkedInData:(NSDictionary*)linkedInData{
    NSLog(@"%@",linkedInData);
}
-(void)failWithResponseLinkedin:(NSString*)message{
    NSLog(@"error");
    [self dismissViewControllerAnimated:YES completion:nil];
}

-(BOOL)checkReachability{
    
    Reachability *networkReachability = [Reachability reachabilityForInternetConnection];
    
    NetworkStatus networkStatus = [networkReachability currentReachabilityStatus];
    
    if (networkStatus == NotReachable) {
        
        return false;
    } else {
        
        return true;
    }
}


-(void)userWebserviceCallDidSuccessWithRespone:(NSMutableArray *)response{
    if (serviceType==1) {
        NSDictionary *dic=(NSDictionary*)response;
        NSString *userid=[dic objectForKey:@"userId"];
        
        [[NSUserDefaults standardUserDefaults] setValue:userid forKey:@"UserId"];
        
        serviceType=2;
        NSString *url=SignIn;
        
        NSString *deviceId = [[[UIDevice currentDevice] identifierForVendor] UUIDString];
        
        NSString *deviceToken =[[NSUserDefaults standardUserDefaults]objectForKey:@"DeviceToken"];
        
        NSMutableDictionary *userData = [[NSMutableDictionary alloc] initWithObjectsAndKeys:emailTXT.text,@"Email",password.text,@"Password",deviceToken,@"DeviceId",
                                         @"IOS",@"OperatingSystem",nil];
        
        
        
        NSString *post =[NSString stringWithFormat:@"grant_type=password&email=%@&password=%@&deviceId=%@&deviceType=IOS",emailTXT.text,password.text,deviceToken];
        
        [webserivce postDataLogin:url withData:post];
    }else{
        [hud setHidden:YES];
        if (checkremember==1) {
            [[NSUserDefaults standardUserDefaults] setObject:emailTXT.text forKey:@"UserEmail"];
        }else{
            
        }
        
        NSString *tokenString=[response valueForKey:@"access_token"];
        [[NSUserDefaults standardUserDefaults] setObject:tokenString forKey:@"Token"];
        
        NSString *string=[[NSUserDefaults standardUserDefaults]objectForKey:@"Token"];
        [self performSegueWithIdentifier:@"homelogin" sender:nil];
    }
}
-(void)userWebserviceCallDidFaildwithError:(NSError *)error andMessage:(NSString *)message{
    [hud setHidden:YES];
    int status= message.integerValue;
    if (status==404) {
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Error" message:@"Error In Email" delegate:nil cancelButtonTitle:@"ok" otherButtonTitles:nil];
        
        [alert show];
    }else if (status==302){
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Error" message:@"This Email is not confirmed" delegate:nil cancelButtonTitle:@"ok" otherButtonTitles:nil];
        
        [alert show];
        [self performSegueWithIdentifier:@"cofirmView" sender:nil];
    }else if (status==205){
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Error" message:@"You Are Blocked" delegate:nil cancelButtonTitle:@"ok" otherButtonTitles:nil];
        
        [alert show];
    }else if (status==406){
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Error" message:@"Wrong password" delegate:nil cancelButtonTitle:@"ok" otherButtonTitles:nil];
        
        [alert show];
    }
}

- (IBAction)openmail:(id)sender{
    NSString * URLEMail  = @"mailto:contact@coppelis.com";
    NSString *url = [URLEMail stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding ];
    [[UIApplication sharedApplication]  openURL: [NSURL URLWithString: url]];
    
}

//If you want the keyboard to hide when you press the return
- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    return YES;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([[segue identifier] isEqualToString:@"registView"]){
        RegistrationViewController *regist=[segue destinationViewController];
        regist.facebookData=[[NSDictionary alloc]init];
        regist.facebookData=fbData;
    }else if ([[segue identifier] isEqualToString:@"cofirmView"]){
        ConfirmationCodeViewController *confirmView=[segue destinationViewController];
        confirmView.userData=[[NSDictionary alloc]initWithObjectsAndKeys:emailTXT.text,@"Email",password.text,@"Password", nil];
        confirmView.isForget=NO;
    }
    
    
}

-(IBAction)loginBtn:(id)sender{
    if ([self checkReachability]) {
        if(![self validateData]){
            UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"" message:NSLocalizedString(@"Please Enter Email and Password",nil) delegate:nil cancelButtonTitle:@"ok" otherButtonTitles:nil];
            
            [alert show];
        }else{
            hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
            hud.mode = MBProgressHUDModeIndeterminate;
            
            webserivce =[[WebServiceFunc alloc]init];
            [webserivce setDelegate:self];
            serviceType=1;
            NSString *urlString=[NSString stringWithFormat:@"%@%@&password=%@",CheckUser,emailTXT.text,password.text];
            [webserivce getData:urlString withflagLogin:NO];
            
        }
    }
    else{
        
        [hud setHidden:YES];
        UIAlertView *
        alert=[[UIAlertView alloc]initWithTitle:@"Error" message:NSLocalizedString(@"No Internet Connection",nil) delegate:nil cancelButtonTitle:@"ok" otherButtonTitles:nil];
        
        [alert show];
        
    }
    
}

- (IBAction)forgetPass:(id)sender {
    
}

-(IBAction)socialBtns:(id)sender{
    
    if ([sender tag]==100) {
        FacebookLogin* fbLogin=[FacebookLogin new];
        fbLogin.delegate=(id)self;
        [fbLogin useExistingFBAccount];
    }else if ([sender tag]==200){
        NSLog(@"googleButton");
    }else{
        NSLog(@"linkedinButton");
        SignUpLinkedinViewController *inLogin=[self.storyboard instantiateViewControllerWithIdentifier:@"LinkedInView"];
        inLogin.delegate=(id)self;
        [self presentViewController:inLogin animated:YES completion:nil];
    }
    
}

@end
