//
//  POPUP InterestViewController.h
//  FamillyLocator
//
//  Created by Amira on 10/31/16.
//  Copyright © 2016 Amira. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "WebServiceFunc.h"

@protocol SecondDelegate <NSObject>
-(void) secondViewControllerDismissed:(NSMutableArray *)userIntrest;
@end


@interface POPUP_InterestViewController : UIViewController<UITableViewDelegate,UITableViewDataSource,UISearchBarDelegate,WebServiceProtocol>
{
    __weak IBOutlet UISearchBar *searchBar;
    __weak IBOutlet UITableView *tableView;
    
}

@property (nonatomic, assign) id<SecondDelegate>    myDelegate;
@property(nonatomic,strong)NSMutableArray *interestArr;
@end
