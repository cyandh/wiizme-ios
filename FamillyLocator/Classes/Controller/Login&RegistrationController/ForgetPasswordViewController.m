//
//  ForgetPasswordViewController.m
//  FamillyLocator
//
//  Created by Amira on 10/25/16.
//  Copyright © 2016 Amira. All rights reserved.
//

#import "ForgetPasswordViewController.h"
#import "Animation.h"
#import "WebServiceFunc.h"
#import "MBProgressHUD.h"
#import "Constant.h"
#import "ConfirmationCodeViewController.h"
@interface ForgetPasswordViewController ()
{
    MBProgressHUD *hud;
    WebServiceFunc *webSerivce;
}
@end

@implementation ForgetPasswordViewController

- (void)viewDidLoad {
       UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(StouchesBegan)];
    
   // [tapGesture setDelegate:self];
    [self.view addGestureRecognizer:tapGesture];
    
    
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self updateLayout];
}
-(void)updateLayout{
    
    [Animation roundCornerForView:sendBTN withAngle:15.0F];
    [Animation roundCornerForView:emailTXT withAngle:15.0f];
    [[emailTXT layer] setBorderWidth:1.0f];
    [[emailTXT layer] setBorderColor:[UIColor colorWithRed:128.0/255.0f green:130.0/255.0f blue:129.0/255.0f alpha:1.0f].CGColor];
    UIView *paddingView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 5, 20)];
    emailTXT.leftView = paddingView;
    emailTXT.leftViewMode = UITextFieldViewModeAlways;

}
-(IBAction)back:(id)sender{
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)sendForCode:(id)sender {
    if([emailTXT.text stringByReplacingOccurrencesOfString:@" " withString:@""].length==0){
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"" message:NSLocalizedString(@"Please Enter your Email ",nil) delegate:nil cancelButtonTitle:@"ok" otherButtonTitles:nil];
        
        [alert show];
    }else{
        hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        hud.mode = MBProgressHUDModeIndeterminate;
        
        webSerivce =[[WebServiceFunc alloc]init];
        [webSerivce setDelegate:self];
        NSString *url=[NSString stringWithFormat:@"%@%@",ForgetPassword,emailTXT.text];
        [webSerivce getData:url withflagLogin:NO];
    }
    
}
-(void)userWebserviceCallDidSuccessWithRespone:(NSMutableArray *)response{
    [hud setHidden:YES];

    [self performSegueWithIdentifier:@"cofirmView" sender:nil];

}
-(void)userWebserviceCallDidFaildwithError:(NSError *)error andMessage:(NSString *)message{
    [hud setHidden:YES];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)StouchesBegan {

    [emailTXT resignFirstResponder];
}
 

#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
    if ([[segue identifier] isEqualToString:@"cofirmView"]){
        ConfirmationCodeViewController *confirmView=[segue destinationViewController];
        confirmView.userData=[[NSDictionary alloc]initWithObjectsAndKeys:emailTXT.text,@"Email",@"",@"Password", nil];
        confirmView.isForget=YES;
    }
}


@end
