//
//  ConfirmationCodeViewController.h
//  FamillyLocator
//
//  Created by Amira on 10/25/16.
//  Copyright © 2016 Amira. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "WebServiceFunc.h"
#import "UserData.h"
@interface ConfirmationCodeViewController : UIViewController<WebServiceProtocol,UITextFieldDelegate>
{
    __weak IBOutlet UITextField *codeTXT;
    __weak IBOutlet UIButton *confimBTN;
    __weak IBOutlet UIButton *registBTN;
    __weak IBOutlet UITextField *emailTXT;
    __weak IBOutlet UIButton *resendConfimation;
    
}
@property (nonatomic,strong)NSDictionary *userData;
@property(nonatomic) BOOL isForget;
@end
