//
//  POPUP InterestViewController.m
//  FamillyLocator
//
//  Created by Amira on 10/31/16.
//  Copyright © 2016 Amira. All rights reserved.
//

#import "POPUP InterestViewController.h"
#import "UserData.h"
#import "Constant.h"
@interface POPUP_InterestViewController ()
{
    WebServiceFunc *webService;
    NSMutableArray * userintrest;
}
@end

@implementation POPUP_InterestViewController
@synthesize myDelegate;
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

-(IBAction)addinterest:(id)sender{
    
    if (userintrest.count>0) {
       [self.myDelegate secondViewControllerDismissed:userintrest];
         [self dismissModalViewControllerAnimated:YES];
    }else{
        if([searchBar.text stringByReplacingOccurrencesOfString:@" " withString:@""].length==0){
            UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"" message:NSLocalizedString(@"Please insert your interest first",nil) delegate:nil cancelButtonTitle:@"ok" otherButtonTitles:nil];
            
            [alert show];
        }else{
            NSString *searchtxt=searchBar.text;
            UserData *userdata=[[UserData alloc]init];
            userdata.fieldId=@"00000000-0000-0000-0000-000000000000";
            userdata.value=searchtxt;
            userintrest=[[NSMutableArray alloc]init];
            userintrest=userdata;
            [self.myDelegate secondViewControllerDismissed:userintrest];
             [self dismissModalViewControllerAnimated:YES];
        }
    }
    
   
}

#pragma mark - UITableView

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.interestArr.count;
    
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString * cellid=@"id";
    UITableViewCell *cell=[tableView dequeueReusableCellWithIdentifier:cellid];
    if(!cell)
    {
        cell=[[UITableViewCell alloc]initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:cellid];
    }

    [cell.textLabel setText:[[self.interestArr  objectAtIndex:indexPath.row]objectForKey:@"value"]];

    return cell;
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    searchBar.text=[[self.interestArr objectAtIndex:indexPath.row]objectForKey:@"value"];
    userintrest=[self.interestArr objectAtIndex:indexPath.row];
}

#pragma end - UItableView



#pragma Mark - Search in table
- (void)searchBarTextDidBeginEditing:(UISearchBar *)searchBar
{
    [searchBar setShowsCancelButton:YES animated:YES];
}
- (void)searchBarTextDidEndEditing:(UISearchBar *)searchBar
{
    [searchBar setShowsCancelButton:NO animated:YES];
    searchBar.text=@"";
}
- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText
{
    if (searchText.length>=3) {
        userintrest=[[NSMutableArray alloc]init];
        searchText = [searchText stringByReplacingOccurrencesOfString:@" " withString:@""];
        webService=[[WebServiceFunc alloc]init];
        [webService setDelegate:self];
        NSString *url=[NSString stringWithFormat:@"%@/user/interest/find?txt=%@",MainAPi,searchText];
        [webService getData:url withflagLogin:NO];
        
        
    }
}
-(IBAction)back:(id)sender{
    [self dismissViewControllerAnimated:YES completion:nil];
    
}
- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar
{
    [searchBar resignFirstResponder];
}

- (void)searchBarCancelButtonClicked:(UISearchBar *)searchBar
{
    [searchBar resignFirstResponder];
    [searchBar setShowsCancelButton:NO animated:YES];
    searchBar.text=@"";
    [tableView reloadData];
}

#pragma End- Search in table

-(void)userWebserviceCallDidSuccessWithRespone:(NSMutableArray *)response{
    _interestArr=response;
    [tableView reloadData];
}
-(void)userWebserviceCallDidFaildwithError:(NSError *)error andMessage:(NSString *)message{
    NSLog(@"%@",error.localizedDescription);
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
