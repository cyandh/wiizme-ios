//
//  ViewController.h
//  FamilyLocator
//
//  Created by Amira on 9/18/16.
//  Copyright © 2016 Amira. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <FBSDKLoginKit/FBSDKLoginKit.h>
#import "FacebookLogin.h"
#import "SSCheckBoxView.h"
#import "WebServiceFunc.h"
#import "SignUpLinkedinViewController.h"



@interface ViewController : UIViewController<faceBookDelegate,WebServiceProtocol,UITextFieldDelegate,linkedinSignDelegate>
{
    __weak IBOutlet UIScrollView *scrollview;
    __weak IBOutlet UIImageView *logoImg;
    __weak IBOutlet UITextField *emailTXT;
    __weak IBOutlet UITextField *password;
    __weak IBOutlet UIView *checkbox;
    __weak IBOutlet UIButton *loginBTN;
    __weak IBOutlet UIButton *createBTN;
    __weak IBOutlet UIButton *remmber;
}



@property (retain, nonatomic) IBOutlet FBSDKLoginButton *loginFB;
@property (nonatomic, strong) NSMutableArray *checkboxes;

@end

