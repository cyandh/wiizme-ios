//
//  ForgetPasswordViewController.h
//  FamillyLocator
//
//  Created by Amira on 10/25/16.
//  Copyright © 2016 Amira. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ForgetPasswordViewController : UIViewController
{
    __weak IBOutlet UITextField *emailTXT;
    __weak IBOutlet UIButton *sendBTN;
}
@end
