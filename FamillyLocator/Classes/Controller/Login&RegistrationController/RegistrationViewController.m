//
//  RegistrationViewController.m
//  FamillyLocator
//
//  Created by Amira on 10/22/16.
//  Copyright © 2016 Amira. All rights reserved.
//

#import "RegistrationViewController.h"
#import "Animation.h"
#import "POPUP InterestViewController.h"
#import "ConfirmationCodeViewController.h"
#import "UserData.h"
#import "MBProgressHUD.h"
#import "Constant.h"
#import "Reachability.h"
#import "Validation.h"
#import "Helper.h"
@interface RegistrationViewController ()
{
    WebServiceFunc *webSevice;
    POPUP_InterestViewController *popup;
    NSMutableArray *tags;
    NSArray *userInterest;
    BOOL gender;
    UIDatePicker *datePicker;
    MBProgressHUD *hud;
    NSDate *birthday;
    int genderflag;
    int service;
    NSDictionary *userData;
}
@end

@implementation RegistrationViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self updateLayout];
    tags=[[NSMutableArray alloc]init];
    if (self.facebookData!=nil) {
        firstName.text=[self.facebookData valueForKey:@"first_name"];
        familyName.text=[self.facebookData valueForKey:@"last_name"];
        emailTXT.text=[self.facebookData valueForKey:@"email"];
        if ([[self.facebookData valueForKey:@"gender"] isEqualToString:@"female"]) {
            genderflag=1;
            gender=0;
            [femalecheck setSelected:YES];
        }else{
            genderflag=1;
            gender=1;
            [malecheck setSelected:YES];
        }
        NSURL *url=[NSURL URLWithString:[[[self.facebookData objectForKey:@"picture"] objectForKey:@"data"] objectForKey:@"url"]];
        [userPhoto setImageURL:url];
        
        
        
    }
    
    if (self.editView) {
        webSevice =[[WebServiceFunc alloc]init];
        [webSevice setDelegate:self];
        hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        hud.mode = MBProgressHUDModeIndeterminate;
        NSString *url=[NSString stringWithFormat:@"%@%@",HomeMember,@"00000000-0000-0000-0000-000000000000"];
        service=1;
        [webSevice getData:url withflagLogin:YES];
    }
    
    
    
}


-(void)updateLayout{
    
    [Animation roundCornerForView:_tagsView withAngle:15.0f];
    [Animation roundCornerForView:registeBTN withAngle:15.0f];
    [Animation roundCornerForView:userPhoto withAngle:30.0f];
    
    [self updateText:firstName];
    [self updateText:familyName];
    [self updateText:emailTXT];
    [self updateText:passwordTXT];
    [self updateText:phoneTXT];
    [self updateText:cityTXT];
    [self updateText:jobTXT];
    [self updateText:companyTXT];
    [self updateText:birthdayTXT];
    
    [[_tagsView layer] setBorderWidth:1.0f];
    [[_tagsView layer] setBorderColor:[UIColor colorWithRed:128.0/255.0f green:130.0/255.0f blue:129.0/255.0f alpha:1.0f].CGColor];
    
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(StouchesBegan)];
    
    [scrollview addGestureRecognizer:tapGesture];
    
    
}
-(void)updateText:(UITextField*)textField{
    [Animation roundCornerForView:textField withAngle:15.0F];
    [[textField layer] setBorderWidth:1.0f];
    [[textField layer] setBorderColor:[UIColor colorWithRed:128.0/255.0f green:130.0/255.0f blue:129.0/255.0f alpha:1.0f].CGColor];
    
    UIView *paddingView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 5, 20)];
    textField.leftView = paddingView;
    textField.leftViewMode = UITextFieldViewModeAlways;
}

-(void)updateView{
    firstName.text=[userData objectForKey:@"firstName"];
    familyName.text=[userData objectForKey:@"familyName"];
    emailTXT.text=[[[userData objectForKey:@"userEmails"] objectAtIndex:0] objectForKey:@"email"];
    NSArray *phones=[userData objectForKey:@"userPhones"];
    
    if (phones.count==0) {
        
    }else{
        phoneTXT.text=[[[userData objectForKey:@"userPhones"] objectAtIndex:0] objectForKey:@"phone"];
    }
    cityTXT.text=[userData objectForKey:@"currentCity"];
    jobTXT.text=[userData objectForKey:@"job"];
    
    companyTXT.text=[userData objectForKey:@"company"];
    NSDictionary *dic=[Helper editDateAndTime:[userData objectForKey:@"birthday"]];
    birthdayTXT.text=[dic objectForKey:@"date"];
    BOOL genderRet=[[userData objectForKey:@"gender"] boolValue];
    if (genderRet) {
        genderflag=1;
        gender=1;
        [malecheck setSelected:YES];
    }else{
        genderflag=1;
        gender=0;
        [femalecheck setSelected:YES];
    }
    
    [emailTXT setUserInteractionEnabled:NO];
    [phoneTXT setUserInteractionEnabled:NO];
    [registeBTN setTitle:@"Update" forState:UIControlStateNormal];
    
}
-(void)StouchesBegan {
    
    [firstName resignFirstResponder];
    [familyName resignFirstResponder];
    [emailTXT resignFirstResponder];
    [passwordTXT resignFirstResponder];
    [phoneTXT resignFirstResponder];
    [cityTXT resignFirstResponder];
    [jobTXT resignFirstResponder];
    [companyTXT resignFirstResponder];
    [birthdayTXT resignFirstResponder];
    [registeBTN resignFirstResponder];
    [datePicker removeFromSuperview];
    
}

#pragma - Gender CheckBoxes

-(IBAction)maleBox:(id)sender{
    genderflag=1;
    [malecheck setSelected:YES];
    [femalecheck setSelected:NO];
    
    gender=1;
}

-(IBAction)femaleBox:(id)sender{
    genderflag=1;
    [malecheck setSelected:NO];
    [femalecheck setSelected:YES];
    gender=0;
}



#pragma - Profile Picker Image

- (IBAction)uploadImg:(id)sender {
    UIImagePickerController *picker = [[UIImagePickerController alloc] init];
    picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    picker.delegate = self;
    [self presentViewController:picker animated:YES completion:nil];
    
}

- (void)imagePickerController:(UIImagePickerController *)picker
        didFinishPickingImage:(UIImage *)image
                  editingInfo:(NSDictionary *)editingInfo
{
    
    userPhoto.image = image;
    [self dismissViewControllerAnimated:YES completion:nil];
    
}



#pragma mark - Birthday PickerView

- (IBAction)birthdayPicker:(id)sender {
    
    datePicker = [[UIDatePicker alloc]init];
    [datePicker setDate:[NSDate date]];
    [datePicker addTarget:self action:@selector(updateTextField:) forControlEvents:UIControlEventValueChanged];
    datePicker.datePickerMode = UIDatePickerModeDate;
    [birthdayTXT setInputView:datePicker];
    
}

-(void)updateTextField:(id)sender
{
    
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    NSLocale *locale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US"];
    [dateFormat setLocale:locale];
    [dateFormat  setDateFormat:@"yyyy-MM-dd"];
    
    datePicker = (UIDatePicker*)birthdayTXT.inputView;
    datePicker.datePickerMode = UIDatePickerModeDate;
    
    birthdayTXT.text = [dateFormat stringFromDate:datePicker.date] ;
    birthday=datePicker.date;
    
}

-(IBAction)hidePicker:(id)sender{
    // datePicker.hidden=YES;
}

-(IBAction)back:(id)sender{
    [self dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - Interest PopUp View

- (IBAction)interestTags:(id)sender {
    popup = [self.storyboard instantiateViewControllerWithIdentifier:@"popupView"];
    popup.myDelegate = self;
    
    popup.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
    
    [self presentModalViewController:popup animated:YES];
}

- (void)secondViewControllerDismissed:(NSMutableArray *)userIntrest
{
    intrest.hidden=YES;
    
    [tags addObject:[NSDictionary dictionaryWithObjectsAndKeys:[userIntrest valueForKey:@"fieldId"],@"Id",[userIntrest valueForKey:@"value"],@"name", nil]];
    NSString *value=[userIntrest valueForKey:@"value"];
    [_tagsView addTag:value];
    
}


#pragma - TextField Delegate

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    
    [self animateTextField: textField up: YES];
}


- (void)textFieldDidEndEditing:(UITextField *)textField
{
    [self animateTextField: textField up: NO];
}

- (void) animateTextField: (UITextField*) textField up: (BOOL) up
{
    
    
    const int movementDistance = 160; // tweak as needed
    const float movementDuration = 0.3f; // tweak as needed
    
    int movement = (up ? -movementDistance : movementDistance);
    
    [UIView beginAnimations: @"anim" context: nil];
    [UIView setAnimationBeginsFromCurrentState: YES];
    [UIView setAnimationDuration: movementDuration];
    self.view.frame = CGRectOffset(self.view.frame, 0, movement);
    [UIView commitAnimations];
}
//If you want the keyboard to hide when you press the return
- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    return YES;
}

-(void)sendUpdate{
    hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    hud.mode = MBProgressHUDModeIndeterminate;
    
    webSevice=[[WebServiceFunc alloc]init];
    [webSevice setDelegate:self];
    if (userPhoto.image){
        
        NSMutableArray *imageObject=[[NSMutableArray alloc]init];
        UserData *user=[[UserData alloc]init];
        user.userImage=[webSevice encodeToBase64String:userPhoto.image];
        user.userImgId=ProfileImageID;
        
        
        NSMutableDictionary *imageData = [[NSMutableDictionary alloc] initWithObjectsAndKeys:user.userImgId,@"FileTypeId",user.userImage,@"Path",nil];
        [imageObject addObject:imageData];
        
        [webSevice updateUserImagewithimageobject:imageObject];
    }else{
        
        NSString *deviceId = [[[UIDevice currentDevice] identifierForVendor] UUIDString];
        
        
        NSArray* userEmails=[NSArray arrayWithObject:[NSDictionary dictionaryWithObject:emailTXT.text forKey:@"Email"]];
        NSArray *userPhone=[NSArray arrayWithObject:[NSDictionary dictionaryWithObject:phoneTXT.text forKey:@"Phone"]];
        
        
        // Convert string to date object
        
        NSMutableDictionary *userinfo = [[NSMutableDictionary alloc] initWithObjectsAndKeys:deviceId,@"DeviceId",
                                         firstName.text,@"FirstName",
                                         familyName.text,@"FamilyName",
                                         userEmails,@"UserEmails",
                                         passwordTXT.text,@"Password",
                                         userPhone,@"UserPhones",
                                         cityTXT.text,@"CurrentCity",
                                         tags,@"Interests",
                                         jobTXT.text,@"Job",
                                         companyTXT.text,@"Company",
                                         @([[NSNumber numberWithBool:gender] intValue]),@"Gender",birthdayTXT.text,@"Birthday",
                                         @"IOS",@"OperatingSystem",
                                         nil];
        NSLog(@"%@",userinfo);
        service=2;
        
        [webSevice postDataWithUrlString:EditUser withData:userinfo withflagLogin:YES];
    }
}

- (IBAction)registData:(id)sender {
    
    if ([self checkReachability]) {
        if (self.editView) {
            [self sendUpdate];
        }else{
            if(![self validateData]){
                UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"" message:NSLocalizedString(@"Please Check Your Data Again and make sure to upload image",nil) delegate:nil cancelButtonTitle:@"ok" otherButtonTitles:nil];
                
                [alert show];
            }else{
                hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
                hud.mode = MBProgressHUDModeIndeterminate;
                
                webSevice=[[WebServiceFunc alloc]init];
                [webSevice setDelegate:self];
                if (userPhoto.image){
                    
                    NSMutableArray *imageObject=[[NSMutableArray alloc]init];
                    UserData *user=[[UserData alloc]init];
                    user.userImage=[webSevice encodeToBase64String:userPhoto.image];
                    user.userImgId=ProfileImageID;
                    
                    
                    NSMutableDictionary *imageData = [[NSMutableDictionary alloc] initWithObjectsAndKeys:user.userImgId,@"FileTypeId",user.userImage,@"Path",nil];
                    [imageObject addObject:imageData];
                    
                    [webSevice updateUserImagewithimageobject:imageObject];
                }else{
                    
                    NSString *deviceId = [[[UIDevice currentDevice] identifierForVendor] UUIDString];
                    
                    
                    NSArray* userEmails=[NSArray arrayWithObject:[NSDictionary dictionaryWithObject:emailTXT.text forKey:@"Email"]];
                    NSArray *userPhone=[NSArray arrayWithObject:[NSDictionary dictionaryWithObject:phoneTXT.text forKey:@"Phone"]];
                    
                    
                    // Convert string to date object
                    
                    NSMutableDictionary *userinfo = [[NSMutableDictionary alloc] initWithObjectsAndKeys:deviceId,@"DeviceId",
                                                     firstName.text,@"FirstName",
                                                     familyName.text,@"FamilyName",
                                                     userEmails,@"UserEmails",
                                                     passwordTXT.text,@"Password",
                                                     userPhone,@"UserPhones",
                                                     cityTXT.text,@"CurrentCity",
                                                     tags,@"Interests",
                                                     jobTXT.text,@"Job",
                                                     companyTXT.text,@"Company",
                                                     @([[NSNumber numberWithBool:gender] intValue]),@"Gender",birthdayTXT.text,@"Birthday",
                                                     @"IOS",@"OperatingSystem",
                                                     nil];
                    NSLog(@"%@",userinfo);
                    
                    [webSevice postDataWithUrlString:SignUP withData:userinfo withflagLogin:NO];
                }
                
            }
        }
    }else{
        [hud setHidden:YES];
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Error" message:NSLocalizedString(@"No Internet Connection",nil) delegate:nil cancelButtonTitle:@"ok" otherButtonTitles:nil];
        
        [alert show];
        
    }
}

-(BOOL)checkReachability{
    
    Reachability *networkReachability = [Reachability reachabilityForInternetConnection];
    
    NetworkStatus networkStatus = [networkReachability currentReachabilityStatus];
    
    if (networkStatus == NotReachable) {
        
        return false;
    } else {
        
        return true;
    }
}


#pragma mark - Service Delegate

-(void)userImagesWebserviceCallDidSuccessWithRespone:(NSArray *)imageID{
    NSLog(@"%@",imageID);
    NSString *imageid=[imageID objectAtIndex:0];
    imageid=[imageid
             stringByReplacingOccurrencesOfString:@"\"" withString:@""];
    webSevice=[[WebServiceFunc alloc]init];
    [webSevice setDelegate:self];
    NSString *deviceId = [[[UIDevice currentDevice] identifierForVendor] UUIDString];
    
    
    NSArray* userEmails=[NSArray arrayWithObject:[NSDictionary dictionaryWithObject:emailTXT.text forKey:@"Email"]];
    NSArray *userPhone=[NSArray arrayWithObject:[NSDictionary dictionaryWithObject:phoneTXT.text forKey:@"Phone"]];
    
    
    // Convert string to date object
    
    NSMutableDictionary *userinfo = [[NSMutableDictionary alloc] initWithObjectsAndKeys:deviceId,@"DeviceId",
                                     firstName.text,@"FirstName",
                                     familyName.text,@"FamilyName",
                                     userEmails,@"UserEmails",
                                     passwordTXT.text,@"Password",
                                     userPhone,@"UserPhones",
                                     cityTXT.text,@"CurrentCity",
                                     tags,@"Interests",
                                     jobTXT.text,@"Job",
                                     companyTXT.text,@"Company",
                                     imageid,@"PhotoId",
                                     @([[NSNumber numberWithBool:gender] intValue]),@"Gender",birthdayTXT.text,@"Birthday",
                                     @"IOS",@"OperatingSystem",
                                     nil];
    NSLog(@"%@",userinfo);
    if (self.editView) {
        service=2;
        [webSevice postDataWithUrlString:EditUser withData:userinfo withflagLogin:YES];
    }else{
        [webSevice postDataWithUrlString:SignUP withData:userinfo withflagLogin:NO];
    }
    
}
-(void)userImagesWebserviceCallDidFaildwithError:(NSError *)error andMessage:(NSString *)message{
    [hud setHidden:YES];
    NSLog(@"%@",error.localizedDescription);
}


-(void)userWebserviceCallDidSuccessWithRespone:(NSMutableArray *)response{
    [hud setHidden:YES];
    if (self.editView) {
        if (service==1) {
            userData=(NSDictionary*)response;
            [self updateView];
        }else{
            [self dismissViewControllerAnimated:YES completion:nil];
        }
    }else{
        [self performSegueWithIdentifier:@"cofirmRegist" sender:nil];
    }
}
-(void)userWebserviceCallDidFaildwithError:(NSError *)error andMessage:(NSString *)message{
    [hud setHidden:YES];
    NSLog(@"%@",error.localizedDescription);
}


-(BOOL)validateData
{
    if (NotValidArray==nil) {
        NotValidArray = [[NSMutableArray alloc] init];
    }else{
        
        for (int i=0; i<[NotValidArray count]; i++) {
            UILabel * NotValidlbl =[NotValidArray objectAtIndex:i];
            [NotValidlbl removeFromSuperview];
        }
        [NotValidArray removeAllObjects];
        
    }
    
    BOOL IsValide = YES;
    if (![Validation validateusernamewithName:firstName.text]) {
        [self CreateStarLbl:firstName.frame];
        IsValide= NO;
    }
    if (![Validation validateusernamewithName:familyName.text]){
        [self CreateStarLbl:familyName.frame];
        IsValide= NO;
    }
    if(![Validation validateemailwithEmail:emailTXT.text])
    {
        [self CreateStarLbl:emailTXT.frame];
        IsValide= NO;
    }
    if(![Validation validatepasswordwithpass:passwordTXT.text]){
        [self CreateStarLbl:passwordTXT.frame];
        IsValide= NO;
    }
    if (![Validation validatephonewithPhone:phoneTXT.text]){
        [self CreateStarLbl:phoneTXT.frame];
        IsValide= NO;
    }
    if (![Validation validateusernamewithName:cityTXT.text]){
        [self CreateStarLbl:cityTXT.frame];
        IsValide= NO;
    }
   // if (![Validation validateusernamewithName:jobTXT.text]){
    //    [self CreateStarLbl:jobTXT.frame];
     //   IsValide= NO;
    //}
   // if (![Validation validateusernamewithName:companyTXT.text]){
   //     [self CreateStarLbl:companyTXT.frame];
   //     IsValide= NO;
   // }
  //  if (![Validation validateusernamewithName:birthdayTXT.text]){
    //    [self CreateStarLbl:birthdayTXT.frame];
      //  IsValide= NO;
    //}
    if (genderflag!=1){
        [self CreateStarLbl:CGRectMake(23, malecheck.frame.origin.y, 10, 30)];
        IsValide= NO;
    }
    return   IsValide;
}
-(void)CreateStarLbl:(CGRect)frame{
    UILabel * NotValid = [[UILabel alloc] initWithFrame:CGRectMake(frame.origin.x-10,frame.origin.y+3, 10, frame.size.height)];
    [NotValid setText:@"*"];
    [NotValid setTextColor:[UIColor redColor]];
    [NotValidArray addObject:NotValid];
    [regView addSubview:NotValid];
    
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
    if ([[segue identifier] isEqualToString:@"cofirmRegist"]){
        ConfirmationCodeViewController *confirmView=[segue destinationViewController];
        confirmView.userData=[[NSDictionary alloc]initWithObjectsAndKeys:emailTXT.text,@"Email",passwordTXT.text,@"Password", nil];
        confirmView.isForget=NO;
    }
}


@end
