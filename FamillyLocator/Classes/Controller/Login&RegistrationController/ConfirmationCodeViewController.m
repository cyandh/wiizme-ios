//
//  ConfirmationCodeViewController.m
//  FamillyLocator
//
//  Created by Amira on 10/25/16.
//  Copyright © 2016 Amira. All rights reserved.
//

#import "ConfirmationCodeViewController.h"
#import "Animation.h"
#import "MBProgressHUD.h"
#import "Constant.h"
#import "ResetPasswordViewController.h"
@interface ConfirmationCodeViewController ()
{
    WebServiceFunc *webService;
    int flag;
    MBProgressHUD *hud;
}
@end

@implementation ConfirmationCodeViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self updateLayout];
    emailTXT.text=[self.userData objectForKey:@"Email"];
    [emailTXT setUserInteractionEnabled:NO];
    [codeTXT setDelegate:self];
    [emailTXT setDelegate:self];
    
}

-(void)updateLayout{

    [self updateText:codeTXT];
    [Animation roundCornerForView:confimBTN withAngle:15.0f];
    [Animation roundCornerForView:registBTN withAngle:15.0f];
    [self updateText:emailTXT];
    [Animation roundCornerForView:resendConfimation withAngle:15.0f];
}

-(void)updateText:(UITextField*)textField{
    [Animation roundCornerForView:textField withAngle:15.0F];
    [[textField layer] setBorderWidth:1.0f];
    [[textField layer] setBorderColor:[UIColor colorWithRed:128.0/255.0f green:130.0/255.0f blue:129.0/255.0f alpha:1.0f].CGColor];
    
    UIView *paddingView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 5, 20)];
    textField.leftView = paddingView;
    textField.leftViewMode = UITextFieldViewModeAlways;
}

- (IBAction)sendConfirm:(id)sender {
    hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    hud.mode = MBProgressHUDModeIndeterminate;
    
    flag=1;
    webService =[[WebServiceFunc alloc]init];
    [webService setDelegate:self];

    NSString *url=[NSString stringWithFormat:@"%@/user/user/ConfirmUser?email=%@&code=%@",MainAPi,[self.userData objectForKey:@"Email"],codeTXT.text];
    [webService getData:url withflagLogin:NO];
    

    
}

-(IBAction)back:(id)sender{
    
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)confirmforget:(id)sender {
    hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    hud.mode = MBProgressHUDModeIndeterminate;
    if (self.isForget) {
        flag=3;
        webService =[[WebServiceFunc alloc]init];
        [webService setDelegate:self];
        
        NSString *url=[NSString stringWithFormat:@"%@%@&code=%@",validateCodeforgetPass,[self.userData objectForKey:@"Email"],codeTXT.text];
        [webService getData:url withflagLogin:NO];
    }else{
        flag=1;
        webService =[[WebServiceFunc alloc]init];
        [webService setDelegate:self];
        
        NSString *url=[NSString stringWithFormat:@"%@/user/user/ConfirmUser?email=%@&code=%@",MainAPi,[self.userData objectForKey:@"Email"],codeTXT.text];
        [webService getData:url withflagLogin:NO];

    }

    

}
- (IBAction)resendConfirm:(id)sender {
    hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    hud.mode = MBProgressHUDModeIndeterminate;
    flag=4;
    webService =[[WebServiceFunc alloc]init];
    [webService setDelegate:self];
    NSString *url=[NSString stringWithFormat:@"%@%@",ForgetPassword,[self.userData objectForKey:@"Email"]];
    [webService getData:url withflagLogin:NO];
}

-(void)userWebserviceCallDidSuccessWithRespone:(NSMutableArray *)response{
    if (flag==1) {
        UIStoryboard *storyboard=[UIStoryboard storyboardWithName:@"Main" bundle:nil];
        UIViewController* viewController=[storyboard instantiateViewControllerWithIdentifier:@"LoginController"];
        [self presentViewController:viewController animated:YES completion:nil];
    
    }else if (flag==3){
        [hud setHidden:YES];
         [self performSegueWithIdentifier:@"resetPassword" sender:nil];
    }else if (flag==4){
        [hud setHidden:YES];
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Success" message:@"Enter Code" delegate:nil cancelButtonTitle:@"ok" otherButtonTitles:nil];
        
        [alert show];
    }
    
}
-(void)userWebserviceCallDidFaildwithError:(NSError *)error andMessage:(NSString *)message{
    [hud setHidden:YES];
    UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"error" message:@"error Code" delegate:nil cancelButtonTitle:@"ok" otherButtonTitles:nil];
    
    [alert show];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    return YES;
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([[segue identifier] isEqualToString:@"resetPassword"]) {
    
    ResetPasswordViewController *reset= [segue destinationViewController];
    reset.email=[self.userData objectForKey:@"Email"];
        reset.code=codeTXT.text;
    }
    
}


@end
