//
//  ResetPasswordViewController.h
//  FamillyLocator
//
//  Created by Amira on 10/25/16.
//  Copyright © 2016 Amira. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "WebServiceFunc.h"
@interface ResetPasswordViewController : UIViewController<WebServiceProtocol>
{
    __weak IBOutlet UITextField *resetTXT;
    __weak IBOutlet UIButton *resetBTN;
}
@property(nonatomic,strong)NSString *email;
@property (nonatomic,strong)NSString *code;
@end
