//
//  ResetPasswordViewController.m
//  FamillyLocator
//
//  Created by Amira on 10/25/16.
//  Copyright © 2016 Amira. All rights reserved.
//

#import "ResetPasswordViewController.h"
#import "Animation.h"
#import "MBProgressHUD.h"
#import "Constant.h"
@interface ResetPasswordViewController ()
{
    MBProgressHUD *hud;
    WebServiceFunc *webService;
}
@end

@implementation ResetPasswordViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self updateLayout];
}
-(void)updateLayout{
    
    [Animation roundCornerForView:resetTXT withAngle:15.0F];
    [[resetTXT layer] setBorderWidth:1.0f];
    [[resetTXT layer] setBorderColor:[UIColor colorWithRed:128.0/255.0f green:130.0/255.0f blue:129.0/255.0f alpha:1.0f].CGColor];
    UIView *paddingView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 5, 20)];
    resetTXT.leftView = paddingView;
    resetTXT.leftViewMode = UITextFieldViewModeAlways;
    [Animation roundCornerForView:resetBTN withAngle:15.0f];
}
-(IBAction)back:(id)sender{
    [self dismissViewControllerAnimated:YES completion:nil];
}
- (IBAction)resetPassword:(id)sender {
    hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    hud.mode = MBProgressHUDModeIndeterminate;
    webService =[[WebServiceFunc alloc]init];
    [webService setDelegate:self];
    NSString *url=[NSString stringWithFormat:@"%@%@&code=%@&password=%@",ChangePass,self.email,self.code,resetTXT.text];
    [webService getData:url withflagLogin:NO];
}
-(void)userWebserviceCallDidSuccessWithRespone:(NSMutableArray *)response{
    UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Success" message:@"Your password has change" delegate:nil cancelButtonTitle:@"ok" otherButtonTitles:nil];
    
    [alert show];
    UIStoryboard *storyboard=[UIStoryboard storyboardWithName:@"Main" bundle:nil];
    UIViewController* viewController=[storyboard instantiateViewControllerWithIdentifier:@"LoginController"];
    [self presentViewController:viewController animated:YES completion:nil];
}
-(void)userWebserviceCallDidFaildwithError:(NSError *)error andMessage:(NSString *)message{
    [hud setHidden:YES];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
