//
//  RegistrationViewController.h
//  FamillyLocator
//
//  Created by Amira on 10/22/16.
//  Copyright © 2016 Amira. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SSCheckBoxView.h"
#import "WebServiceFunc.h"
#import "POPUP InterestViewController.h"
#import "ASJTagsView.h"
#import "AsyncImageView.h"
@interface RegistrationViewController : UIViewController<WebServiceProtocol,SecondDelegate,UITextFieldDelegate>
{
    NSMutableArray * NotValidArray;
    __weak IBOutlet UIScrollView *scrollview;
    __weak IBOutlet UITextField *firstName;
    __weak IBOutlet UITextField *familyName;
    __weak IBOutlet UITextField *emailTXT;
    __weak IBOutlet UITextField *passwordTXT;
    __weak IBOutlet UITextField *phoneTXT;
    __weak IBOutlet UITextField *cityTXT;
    __weak IBOutlet UILabel *intrest;
    __weak IBOutlet UITextField *jobTXT;
    __weak IBOutlet UITextField *companyTXT;
    __weak IBOutlet UITextField *birthdayTXT;
    __weak IBOutlet UIButton *malecheck;
    __weak IBOutlet UIButton *femalecheck;
    __weak IBOutlet UIButton *registeBTN;
    __weak IBOutlet AsyncImageView *userPhoto;
    IBOutlet UIView * regView;
   // __weak IBOutlet UIDatePicker *datePicker;

}
@property(nonatomic,strong)NSDictionary *facebookData;
@property (nonatomic, strong) NSMutableArray *checkboxes;
@property (weak, nonatomic) IBOutlet ASJTagsView *tagsView;
@property (nonatomic) BOOL editView;
@end
