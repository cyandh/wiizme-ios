//
//  MemberGroupsViewController.m
//  FamilyLocator
//
//  Created by Amira on 10/17/16.
//  Copyright © 2016 Amira. All rights reserved.
//

#import "MemberGroupsViewController.h"
#import "MemberGroupsTableViewCell.h"
#import "MBProgressHUD.h"
#import "Constant.h"
#import "Animation.h"
#import "HomeGroupViewController.h"
#import "SettingGroupViewController.h"
#import "UIViewController+RemoteNotificationData.h"
#import "SideMenuViewController.h"
@interface MemberGroupsViewController ()
{
    NSMutableArray *tableData;
    WebServiceFunc *webService;
    MBProgressHUD *hud;
    int selectedRow;
    int serviceNUm;
}
@end

@implementation MemberGroupsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.

    
    memberName.text=self.userData.name;
    NSURL *imageUrl=[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",PhotosUrl,self.userData.userImage]];
    [memberImg setImageURL:imageUrl];
    [Animation roundCornerForView:memberImg withAngle:25];
    webService=[[WebServiceFunc alloc]init];
    serviceNUm=1;
    [webService setDelegate:self];
    hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    hud.mode = MBProgressHUDModeIndeterminate;
    
    NSString *urlString=[NSString stringWithFormat:@"%@%@",MemberGroups,self.userData.fieldId];
    [webService getData:urlString withflagLogin:YES];
}

-(void)viewDidDisappear:(BOOL)animated{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}



- (IBAction)showMenu
{
    
    SideMenuViewController *sideMenu=[self.storyboard instantiateViewControllerWithIdentifier:@"SideMenu"];
    [self.navigationController presentViewController:sideMenu animated:YES completion:nil];
}

-(void)userWebserviceCallDidSuccessWithRespone:(NSMutableArray *)response{
    [hud setHidden:YES];
    if (serviceNUm==1) {
        tableData=response;
        [self.table reloadData];
    }else{
        
    }
    
    
}
-(void)userWebserviceCallDidFaildwithError:(NSError *)error andMessage:(NSString *)message{
    [hud setHidden:YES];
    NSLog(@"%@",error.localizedDescription);
    if ([message isEqualToString:@"401" ]) {
        UIStoryboard *storyboard=[UIStoryboard storyboardWithName:@"Main" bundle:nil];
        UIViewController* viewController=[storyboard instantiateViewControllerWithIdentifier:@"LoginController"];
        [self presentViewController:viewController animated:YES completion:nil];
    }
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma TableView DataSource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return tableData.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    MemberGroupsTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"membergroups"];
    cell.groupName.text=[[tableData objectAtIndex:indexPath.row] objectForKey:@"name"];
    NSURL *imageUrl=[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",PhotosUrl,[[tableData objectAtIndex:indexPath.row] objectForKey:@"logoId"]]];
    [cell.groupImg setImageURL:imageUrl];
    [Animation roundCornerForView:cell.groupImg withAngle:30.0f];
    
    bool member=[[[tableData objectAtIndex:indexPath.row] objectForKey:@"isMember"] boolValue];

    bool pending=[[[tableData objectAtIndex:indexPath.row] objectForKey:@"isPending"] boolValue];
    
        if (member) {
            cell.joinBTN.hidden=YES;
        }else if(pending){
            cell.joinBTN.hidden=NO;
            [cell.joinBTN setTitle:@"Pending" forState:UIControlStateNormal];
           // [cell.joinBTN addTarget:self action:@selector(cancelRequest:)  forControlEvents:UIControlEventTouchUpInside];
           // [cell.joinBTN setTag:indexPath.row];
             [Animation roundCornerForView:cell.joinBTN withAngle:25];
        }else{
            cell.joinBTN.hidden=NO;
            [cell.joinBTN setTitle:@"Join" forState:UIControlStateNormal];
            [cell.joinBTN addTarget:self action:@selector(joinGroup:)  forControlEvents:UIControlEventTouchUpInside];
            [cell.joinBTN setTag:indexPath.row];
            [Animation roundCornerForView:cell.joinBTN withAngle:25];
        }
    

    //  [cell loadNewsImagewithLabelString:videotitle andDate:videoDateFormate];
    
    return cell;
}
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    // This will create a "invisible" footer
    return 0.01f;
}


- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    return [UIView new];
    
}


#pragma TableView Delegate

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    selectedRow=indexPath.row;
    [self performSegueWithIdentifier:@"groupView" sender:nil];
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}


-(IBAction)joinGroup:(id)sender{
    UIButton *but=sender;
    webService=[[WebServiceFunc alloc]init];
    serviceNUm=2;
    [webService setDelegate:self];
    hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    hud.mode = MBProgressHUDModeIndeterminate;
    NSString *groupID=[[tableData objectAtIndex:but.tag] objectForKey:@"id"];
    NSString *urlString=[NSString stringWithFormat:@"%@%@",JoinGroup,groupID];
    [webService getData:urlString withflagLogin:YES];

}



#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    SettingGroupViewController *settingView=[segue destinationViewController];
    settingView.groupData=[[GroupData alloc]init];

    
    settingView.groupData.member=[[[tableData objectAtIndex:selectedRow] objectForKey:@"isMember"] boolValue];
    settingView.groupData.admin=[[[tableData objectAtIndex:selectedRow] objectForKey:@"isAdmin"] boolValue];
    settingView.groupData.onlyAdmin=[[[tableData objectAtIndex:selectedRow] objectForKey:@"isOnlyAdmin"] boolValue];
    settingView.groupData.pending=[[[tableData objectAtIndex:selectedRow] objectForKey:@"isPending"] boolValue];
    settingView.groupData.coverImg=[NSString stringWithFormat:@"%@%@",PhotosUrl,[[tableData objectAtIndex:selectedRow] objectForKey:@"coverPhotoId"]];
    settingView.groupData.coverID=[[tableData objectAtIndex:selectedRow] objectForKey:@"coverPhotoId"];
    settingView.groupData.profileImg=[[tableData objectAtIndex:selectedRow] objectForKey:@"logoId"];
    settingView.groupData.logoID=[[tableData objectAtIndex:selectedRow] objectForKey:@"logoId"];
    settingView.groupData.groupName=[[tableData objectAtIndex:selectedRow] objectForKey:@"name"];
    settingView.groupData.groupID=[[tableData objectAtIndex:selectedRow] objectForKey:@"id"];
    
    NSString *security=[[tableData objectAtIndex:selectedRow] objectForKey:@"groupTypeId"];
    settingView.groupData.privacyId=security;
    if ([security isEqualToString:publicPrivacy]) {
        settingView.groupData.privacy=@"Public";
    }else if ([security isEqualToString:privatePrivacy]){
        settingView.groupData.privacy=@"Private";
    }else{
        settingView.groupData.privacy=@"VIP";
    }
    
    settingView.groupData.activityType=[[tableData objectAtIndex:selectedRow] objectForKey:@"activityType"];
    settingView.groupData.activityTypeId=[[tableData objectAtIndex:selectedRow] objectForKey:@"activityTypeId"];
    settingView.groupData.activities=[[tableData objectAtIndex:selectedRow] objectForKey:@"tags"];
    settingView.groupData.desc=[[tableData objectAtIndex:selectedRow] objectForKey:@"description"];
    settingView.groupData.policy=[[tableData objectAtIndex:selectedRow] objectForKey:@"policies"];
    settingView.groupData.groupID=[[tableData objectAtIndex:selectedRow] objectForKey:@"id"];
    int memNUM=[[[tableData objectAtIndex:selectedRow] objectForKey:@"groupMembersNumber"] intValue];
    if (memNUM<=1) {
        settingView.groupData.groupNum=[NSString stringWithFormat:@"%i %@",memNUM,@"Member"];
    }else{
        settingView.groupData.groupNum=[NSString stringWithFormat:@"%i %@",memNUM,@"Members"];
    };
    settingView.groupData.groupObj=[tableData objectAtIndex:selectedRow];

    
}


@end
