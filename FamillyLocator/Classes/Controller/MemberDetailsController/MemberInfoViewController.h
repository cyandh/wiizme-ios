//
//  MemberInfoViewController.h
//  FamillyLocator
//
//  Created by Amira on 10/22/16.
//  Copyright © 2016 Amira. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UserData.h"
#import "AsyncImageView.h"
@interface MemberInfoViewController : UIViewController<UITableViewDataSource,UITableViewDelegate>
{
    __weak IBOutlet UITableView *tableView;
    __weak IBOutlet UILabel *memberName;
    __weak IBOutlet AsyncImageView *memberImg;
}
@property(nonatomic,strong)UserData *userData;
@end
