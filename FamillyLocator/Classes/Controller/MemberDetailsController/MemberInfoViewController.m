//
//  MemberInfoViewController.m
//  FamillyLocator
//
//  Created by Amira on 10/22/16.
//  Copyright © 2016 Amira. All rights reserved.
//

#import "MemberInfoViewController.h"
#import "MemberInfoTableViewCell.h"
#import "MemberAboutTableViewCell.h"
#import "Constant.h"
#import "Animation.h"
#import "UIViewController+RemoteNotificationData.h"
#import "SideMenuViewController.h"
@interface MemberInfoViewController ()
{
    NSArray *arr,*data;
}
@end

@implementation MemberInfoViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.

    
    memberName.text=self.userData.name;
    NSURL *imageUrl=[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",PhotosUrl,self.userData.userImage]];
    [memberImg setImageURL:imageUrl];
    [Animation roundCornerForView:memberImg withAngle:25];
    arr=@[@"AGE",@"GENDER",@"CITY",@"WORK",@"FRIENDS",@"GROUPS"];
    data=@[self.userData.age,self.userData.gender,self.userData.city,self.userData.job,self.userData.friendsNum,self.userData.groupsNum];

}

-(void)viewDidDisappear:(BOOL)animated{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}


- (IBAction)showMenu
{
    
    SideMenuViewController *sideMenu=[self.storyboard instantiateViewControllerWithIdentifier:@"SideMenu"];
    [self.navigationController presentViewController:sideMenu animated:YES completion:nil];
}

#pragma TableView DataSource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section==1) {
        return 169.0;
    }else{
        return 30.0;
    }
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if (section==0) {
       return arr.count;
    }else{
        return 1;
    }
    
}
-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    if (section==1) {
        UIView *view=[[UIView alloc]initWithFrame:CGRectMake(0, 0, 0, 30)];
        return view;
    }else{
        return nil;
    }
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    if (section==0) {
        return 0.0;
    }else{
        return 30.0;
    }
    
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section==0) {
        MemberInfoTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"infoCell"];
        cell.infoName.text=[arr objectAtIndex:indexPath.row];
        cell.data.text=[data objectAtIndex:indexPath.row];
        cell.backgroundColor=[UIColor colorWithRed:244.0/255.0 green:244.0/255.0 blue:244.0/255.0 alpha:1.0f];
        cell.layer.cornerRadius = 5.0f;
        cell.clipsToBounds = YES;
        
        UIView* separatorLineView = [[UIView alloc] initWithFrame:CGRectMake(0,0, tableView.frame.size.width, 5)];/// change size as you need.
        separatorLineView.backgroundColor = [UIColor whiteColor];// you can also put image here
        separatorLineView.layer.cornerRadius = 5.0f;
        separatorLineView.clipsToBounds = YES;
        [cell.contentView addSubview:separatorLineView];

        
        return cell;
   }else{
//        MemberAboutTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"aboutCell"];
//        cell.about.text=@"Lorem ipsum dolor sit er elit lamet, consectetaur cillium adipisicing pecu, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Nam liber te conscient to factor tum poen legum odioque civiuda.";
//       cell.backgroundColor=[UIColor colorWithRed:244.0/255.0 green:244.0/255.0 blue:244.0/255.0 alpha:1.0f];
//       cell.layer.cornerRadius = 30.0f;
//       cell.clipsToBounds = YES;
//        return cell;
       return nil;
    }

    
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    // This will create a "invisible" footer
    return 0.01f;
}


- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    return [UIView new];
    
}


#pragma TableView Delegate

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    // [self performSegueWithIdentifier:@"groupDetails" sender:nil];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
