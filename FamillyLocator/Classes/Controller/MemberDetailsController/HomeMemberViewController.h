//
//  HomeMemberViewController.h
//  FamillyLocator
//
//  Created by Amira on 11/1/16.
//  Copyright © 2016 Amira. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ALRadialMenu.h"
#import "UserData.h"
#import "WebServiceFunc.h"
#import "MBProgressHUD.h"
#import "AsyncImageView.h"
#import "LPPopupListView.h"
@interface HomeMemberViewController : UIViewController<ALRadialMenuDelegate,UIGestureRecognizerDelegate,WebServiceProtocol,LPPopupListViewDelegate,UISearchBarDelegate>
{
    __weak IBOutlet AsyncImageView *memberImg;
    __weak IBOutlet UILabel *memberName;
    __weak IBOutlet UIButton *profileBut;
    __weak IBOutlet UIButton *permissionBut;
    
}
@property (nonatomic, strong) NSMutableIndexSet *selectedIndexes;
@property (strong, nonatomic) ALRadialMenu *radialMenu;
@property (strong, nonatomic) NSArray *popups;
@property(strong ,nonatomic) UserData *userData;
@property(nonatomic)BOOL fromSearch;
@property(nonatomic)BOOL hasRelation;
@end
