//
//  HomeMemberViewController.m
//  FamillyLocator
//
//  Created by Amira on 11/1/16.
//  Copyright © 2016 Amira. All rights reserved.
//

#import "HomeMemberViewController.h"
#import "Animation.h"
#import "Constant.h"
#import "MemberInfoViewController.h"
#import "MemberGroupsViewController.h"
#import "MeetThereViewController.h"
#import "MapKitViewController.h"
#import "UIViewController+RemoteNotificationData.h"
#import "LiveTrackingViewController.h"
#import "UIViewController+HeaderView.h"
#import "PermissionsViewController.h"
#import "NeedHelpViewController.h"
#import "ConversationViewController.h"
#import "ConversationModel.h"
#import "UserModel.h"
@interface HomeMemberViewController ()
{
    WebServiceFunc *webService;
    MBProgressHUD *hud;
    NSDictionary*memberData;
    int serviceType;
    LPPopupListView *listView;
    NSMutableArray *groupList,*allGroupList;
    
}
@end

@implementation HomeMemberViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    
    webService=[[WebServiceFunc alloc]init];
    [webService setDelegate:self];
    hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    hud.mode = MBProgressHUDModeIndeterminate;
    NSURL *imageUrl=[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",PhotosUrl,self.userData.userImage]];
    [memberImg setImageURL:imageUrl];
    [Animation roundCornerForView:memberImg withAngle:75.0f];
    memberName.text=self.userData.name;
    serviceType=1;
    NSString *urlString=[NSString stringWithFormat:@"%@%@",HomeMember,self.userData.fieldId];
    [webService getData:urlString withflagLogin:YES];
    
    self.radialMenu = [[ALRadialMenu alloc] init];
    self.radialMenu.delegate = self;
    
    
    if (self.fromSearch) {
        if (self.hasRelation) {
            profileBut.hidden=NO;
            permissionBut.hidden=NO;
        }else{
            profileBut.hidden=YES;
            permissionBut.hidden=YES;
            
        }
    }
    
    [NSTimer scheduledTimerWithTimeInterval:0.5
                                     target:self
                                   selector:@selector(Tracehight)
                                   userInfo:nil
                                    repeats:NO];
    
}

-(void)viewDidDisappear:(BOOL)animated{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}


-(void)viewWillAppear:(BOOL)animated{
    
    [self.navigationController setNavigationBarHidden:YES];
    
}
-(void)viewWillDisappear:(BOOL)animated{
    [self.navigationController setNavigationBarHidden:NO];
    
}

-(void)groupAndMemberList{
    
    float paddingTopBottom = 20.0f;
    float paddingLeftRight = 20.0f;
    
    CGPoint point = CGPointMake(paddingLeftRight, (self.navigationController.navigationBar.frame.size.height + paddingTopBottom) + paddingTopBottom);
    CGSize size = CGSizeMake((self.view.frame.size.width - (paddingLeftRight * 2)), self.view.frame.size.height - ((self.navigationController.navigationBar.frame.size.height + paddingTopBottom) + (paddingTopBottom * 2)));
    
    self.selectedIndexes=nil;
    listView=[[LPPopupListView alloc]initWithTitle:@"" list:[self list] selectedIndexes:self.selectedIndexes point:point size:size multipleSelection:NO disableBackgroundInteraction:NO];
    
    [listView.layer setShadowColor:[[UIColor darkGrayColor] CGColor]];
    [listView.layer setShadowOffset:CGSizeMake(0, 0)];
    [listView.layer setShadowRadius:20.0];
    [listView.layer setShadowOpacity:1];
    listView.clipsToBounds = NO;
    listView.layer.masksToBounds = NO;
    
    
    
    [listView.searchBar setDelegate:self];
    
    listView.delegate = self;
    
    [listView showInView:self.navigationController.view animated:YES];
}

-(IBAction)closeView:(id)sender{
    [self.navigationController popViewControllerAnimated:YES];
}

-(IBAction)settingBtn:(id)sender{
    
    [self performSegueWithIdentifier:@"memberInfo" sender:nil];
}


-(IBAction)permissionBtn:(id)sender{
    
    [self performSegueWithIdentifier:@"permissionView" sender:nil];
}

-(void)Tracehight{
    
    [self.radialMenu buttonsWillAnimateFromButton:memberImg withFrame:memberImg.frame inView:self.view];
    
}

-(void)userWebserviceCallDidSuccessWithRespone:(NSMutableArray *)response{
    [hud setHidden:YES];
    if (serviceType==1) {
        if ([response isKindOfClass:[NSDictionary class]]) {
            memberData =(NSDictionary *)response;
            
        }
    }else if (serviceType==2){
        hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        hud.mode = MBProgressHUDModeCustomView;
        // Set an image view with a checkmark.
        UIImage *image = [[UIImage imageNamed:@"face"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
        hud.customView = [[UIImageView alloc] initWithImage:image];
        // Looks a bit nicer if we make it square.
        hud.square = YES;
        
        hud.label.text = NSLocalizedString(@"Done", nil);
        [hud hideAnimated:YES afterDelay:1.f];
    }else if (serviceType==3) {
        if (response.count==0) {
            UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Error" message:@"don't have Phone Numbers" delegate:nil cancelButtonTitle:@"ok" otherButtonTitles:nil];
            
            [alert show];
        }else{
            NSString *tel=[[response objectAtIndex:0] objectForKey:@"phone"];
            for (int i=1; i<response.count; i++) {
                tel = [tel stringByAppendingFormat:@",%@",[[response objectAtIndex:i] objectForKey:@"phone"]];
            }
            
            NSString * URLSMS  =[NSString stringWithFormat:@"sms:%@",tel];
            NSString *url = [URLSMS stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding ];
            [[UIApplication sharedApplication]  openURL: [NSURL URLWithString: url]];
        }
        
        
    }else if (serviceType==5){
        
        LiveTrackingViewController *liveTrack=[self.storyboard instantiateViewControllerWithIdentifier:@"liveTrackingController"];
        liveTrack.groupData=[[GroupData alloc]init];
        liveTrack.groupData.groupID=self.userData.fieldId;
        liveTrack.groupData.profileImg=[NSString stringWithFormat:@"%@%@",PhotosUrl,self.userData.userImage];
        liveTrack.groupData.groupName=self.userData.name;
        liveTrack.isGroup=NO;
        
        liveTrack.locations=[[NSMutableArray alloc]init];
        liveTrack.usersId=[[NSMutableArray alloc]init];
        
        NSDictionary *dic;
        for (int i=0; i<response.count; i++) {
            NSString *firstName=[[[response objectAtIndex:i] objectForKey:@"user"] objectForKey:@"firstName"];
            NSString *lastName=[[[response objectAtIndex:i] objectForKey:@"user"] objectForKey:@"familyName"];
            NSString *name=[NSString stringWithFormat:@"%@ %@",firstName,lastName];
            NSDictionary *dic=[self editDateAndTime:[[response objectAtIndex:i] objectForKey:@"lastPostionDate"] ];
            NSString *date=[NSString stringWithFormat:@"%@  %@",[dic objectForKey:@"date"],[dic objectForKey:@"time"]];
            NSString *battery;
            if ([[response objectAtIndex:i] objectForKey:@"batteryStatue"]==[NSNull null]) {
                battery=@"%0";
            }else{
                battery=[NSString stringWithFormat:@"%@ %@",@"%",[[response objectAtIndex:i] objectForKey:@"batteryStatue"]];
            }
            if (![[response objectAtIndex:i]objectForKey:@"lastKnowingLatitude"] ) {
                
                
                dic=[NSDictionary dictionaryWithObjectsAndKeys:[NSNumber numberWithDouble:[[[response objectAtIndex:i]objectForKey:@"lastKnowingLatitude"]doubleValue]],@"lat",[NSNumber numberWithDouble:[[[response objectAtIndex:i]objectForKey:@"lastKnowingLangitude"]doubleValue]],@"lng",name,@"name",battery,@"battery",date,@"date",self.userData.fieldId,@"userid",[NSNumber numberWithBool:NO],@"fromSeeThis", nil];
                [liveTrack.locations addObject:dic];
                [liveTrack.usersId addObject:self.userData.fieldId];
            }
        }
        
        
        
        [self.navigationController pushViewController:liveTrack animated:YES];
    }else if (serviceType==7){
        NSDictionary *dic=(NSDictionary *)response;
        NSString *userID=[[NSUserDefaults standardUserDefaults] objectForKey:@"UserId"];
        ConversationModel *convModel=[[ConversationModel alloc]init];
        RLMRealm *realm = [RLMRealm defaultRealm];
        [realm beginWriteTransaction];
        
        NSDateFormatter *format=[[NSDateFormatter alloc]init];
        NSLocale *locale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US"];
        [format setLocale:locale];
        [format setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSSS"];
        
        convModel.Id=[dic objectForKey:@"id"];
        convModel.ChatName=[dic objectForKey:@"chatName"];
        convModel.LogoId=[dic objectForKey:@"logoId"];
        convModel.UserId=[dic objectForKey:@"userId"];
        convModel.LastUpdateTime=[format dateFromString:[dic objectForKey:@"lastUpdateTime"]];
        convModel.UnseenMsgNo=0;
        convModel.CreatorId=[dic objectForKey:@"creatorId"];
        convModel.LastText=[dic objectForKey:@"lastText"];
        convModel.Mute=NO;
        convModel.CreationDate=[format dateFromString:[dic objectForKey:@"creationDate"]];
        [realm addOrUpdateObject:convModel];
        // now commit & end this transaction
        [realm commitWriteTransaction];
        
        
        UserModel *userModl=[[UserModel alloc]init];
        [realm beginWriteTransaction];
        
        userModl.Id=self.userData.fieldId;
        userModl.UserName=self.userData.name;
        
        [realm addOrUpdateObject:userModl];
        // now commit & end this transaction
        [realm commitWriteTransaction];
        
        UIStoryboard *chatStoryBoard=[UIStoryboard storyboardWithName:@"Chat" bundle:nil];
        ConversationViewController *conversationView=[chatStoryBoard instantiateViewControllerWithIdentifier:@"ConversationController"];
        
        conversationView.conversationData=convModel;
        [self.navigationController pushViewController:conversationView animated:YES];
        
    }else if (serviceType==9){
        groupList=[[NSMutableArray alloc]init];
        allGroupList=[[NSMutableArray alloc]init];
        
        
        for (int i=0; i<response.count; i++) {
            BOOL isuser=[[[response objectAtIndex:i]objectForKey:@"isUser"] boolValue];
            if (!isuser) {
                [groupList addObject:[response objectAtIndex:i]];
                [allGroupList addObject:[response objectAtIndex:i]];
                
            }
        }
        [self groupAndMemberList];
    }
    
    
    
    
}
-(void)userWebserviceCallDidFaildwithError:(NSError *)error andMessage:(NSString *)message{
    [hud setHidden:YES];
    NSLog(@"%@",error.localizedDescription);
    if ([message isEqualToString:@"401" ]) {
        UIStoryboard *storyboard=[UIStoryboard storyboardWithName:@"Main" bundle:nil];
        UIViewController* viewController=[storyboard instantiateViewControllerWithIdentifier:@"LoginController"];
        [self presentViewController:viewController animated:YES completion:nil];
    }else if([message isEqualToString:@"403" ]){
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Error" message:@"You don't have permission" delegate:nil cancelButtonTitle:@"ok" otherButtonTitles:nil];
        
        [alert show];
    }else if ([message isEqualToString:@"404"]){
        if (serviceType==7) {
            
            
            NSString *uuid = [[NSUUID UUID] UUIDString];
            uuid = [uuid lowercaseString];
            
            UIStoryboard *chatStoryBoard=[UIStoryboard storyboardWithName:@"Chat" bundle:nil];
            ConversationViewController *conversationView=[chatStoryBoard instantiateViewControllerWithIdentifier:@"ConversationController"];
            
            conversationView.firstconversationData=[NSDictionary dictionaryWithObjectsAndKeys:
                                                    uuid,@"ConversationId",
                                                    self.userData.fieldId , @"UserId",
                                                    self.userData.fieldId,@"fieldId",
                                                    self.userData.name,@"ChatName",
                                                    self.userData.userImage,@"ChatImage", nil];
            
            RLMRealm *realm = [RLMRealm defaultRealm];
            UserModel *userModl=[[UserModel alloc]init];
            [realm beginWriteTransaction];
            
            userModl.Id=self.userData.fieldId;
            userModl.UserName=self.userData.name;
            
            [realm addOrUpdateObject:userModl];
            // now commit & end this transaction
            [realm commitWriteTransaction];
            
            [self.navigationController pushViewController:conversationView animated:YES];
        }
    }
    
    
}


#pragma mark - radial menu delegate methods
- (NSInteger) numberOfItemsInRadialMenu:(ALRadialMenu *)radialMenu {
    // if ((self.fromSearch)&&(!self.hasRelation)) {
    return 9;
    //    }else{
    //        return 8;
    //    }
}


- (NSInteger) arcSizeForRadialMenu:(ALRadialMenu *)radialMenu {
    
    return 360;
}


- (NSInteger) arcRadiusForRadialMenu:(ALRadialMenu *)radialMenu {
    
    return 130;
}


- (ALRadialButton *) radialMenu:(ALRadialMenu *)radialMenu buttonForIndex:(NSInteger)index {
    ALRadialButton *button = [[ALRadialButton alloc] init];
    
    if (index == 1) {
        [button setImage:[UIImage imageNamed:@"menu-help"] forState:UIControlStateNormal];
    } else if (index == 2) {
        [button setImage:[UIImage imageNamed:@"menu-whereAreYou"] forState:UIControlStateNormal];
    } else if (index == 3) {
        [button setImage:[UIImage imageNamed:@"menu-meet"] forState:UIControlStateNormal];
    } else if (index == 4) {
        [button setImage:[UIImage imageNamed:@"menu-lastPosition"] forState:UIControlStateNormal];
    } else if (index == 5) {
        [button setImage:[UIImage imageNamed:@"menu-map"] forState:UIControlStateNormal];
    } else if (index == 6) {
        [button setImage:[UIImage imageNamed:@"menu-groups"] forState:UIControlStateNormal];
    } else if (index == 7) {
        [button setImage:[UIImage imageNamed:@"menu-chat"] forState:UIControlStateNormal];
    } else if (index == 8) {
        [button setImage:[UIImage imageNamed:@"menu-goHome"] forState:UIControlStateNormal];
    }
    //if ((self.fromSearch)&&(!self.hasRelation)) {
    if (index==9) {
        [button setImage:[UIImage imageNamed:@"menu-invite"] forState:UIControlStateNormal];
        //     }
    }
    
    
    if (button.imageView.image) {
        return button;
    }
    
    return nil;
}

-(void)checkPermission:(UIButton *)but{
    if (self.fromSearch) {
        if (self.hasRelation) {
            
        }else{
            [but setUserInteractionEnabled:NO];
            UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Error" message:@"You don't have permission" delegate:nil cancelButtonTitle:@"ok" otherButtonTitles:nil];
            
            [alert show];
        }
    }
}
- (NSString *) GetTitleForRadialMenu:(NSInteger) index{
    NSString * Title = [[NSString alloc] init];
    
    if (index == 1) {
        Title=@"Help";
    } else if (index == 2) {
        Title=@"Where Are You";
    } else if (index == 3) {
        Title=@"Meet You There";
    } else if (index == 4) {
        Title=@"last Position";
    } else if (index == 5) {
        Title=@"Map";
    } else if (index == 6) {
        Title=@"Groups";
    } else if (index == 7) {
        Title=@"Chat";
    } else if (index == 8) {
        Title=@"Go Home";
    }
    //  if ((self.fromSearch)&&(!self.hasRelation)) {
    else if (index == 9) {
        Title=@"Invite";
        //     }
    }
    
    
    
    return Title;
    
}

- (void) radialMenu:(ALRadialMenu *)radialMenu didSelectItemAtIndex:(NSInteger)index {
    
    if (index==1) {
        if ((self.fromSearch)&&(!self.hasRelation)) {
            UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Error" message:@"You don't have permission" delegate:nil cancelButtonTitle:@"ok" otherButtonTitles:nil];
            
            [alert show];
        }else{
            NeedHelpViewController *needView=[self.storyboard instantiateViewControllerWithIdentifier:@"needController"];
            needView.inviterId=self.userData.fieldId;
            needView.inviterName=self.userData.name;
            needView.isUser=YES;
            [self.navigationController pushViewController:needView animated:YES];
        }
    }else if(index==2){
        if ((self.fromSearch)&&(!self.hasRelation)) {
            UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Error" message:@"You don't have permission" delegate:nil cancelButtonTitle:@"ok" otherButtonTitles:nil];
            
            [alert show];
        }else{
            serviceType=2;
            NSString *urlString=[NSString stringWithFormat:@"%@%@",WhereAreYou,self.userData.fieldId];
            [webService getData:urlString withflagLogin:YES];
        }
    }else if (index==3){
        if ((self.fromSearch)&&(!self.hasRelation)) {
            UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Error" message:@"You don't have permission" delegate:nil cancelButtonTitle:@"ok" otherButtonTitles:nil];
            
            [alert show];
        }else{
            [self performSegueWithIdentifier:@"MeetThere" sender:nil];
        }
    }else if(index==4){
        if ((self.fromSearch)&&(!self.hasRelation)) {
            UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Error" message:@"You don't have permission" delegate:nil cancelButtonTitle:@"ok" otherButtonTitles:nil];
            
            [alert show];
        }else{
            MapKitViewController *mapView=[self.storyboard instantiateViewControllerWithIdentifier:@"MapController"];
            mapView.memberNam=self.userData.name;
            mapView.memberImage=[NSString stringWithFormat:@"%@%@",PhotosUrl,self.userData.userImage];
            mapView.userID=self.userData.fieldId;
            [self.navigationController pushViewController:mapView animated:YES];
        }
    }else if(index==5){
        if ((self.fromSearch)&&(!self.hasRelation)) {
            UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Error" message:@"You don't have permission" delegate:nil cancelButtonTitle:@"ok" otherButtonTitles:nil];
            
            [alert show];
        }else{
            serviceType=5;
            hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
            hud.mode = MBProgressHUDModeIndeterminate;
            NSString *urlString=[NSString stringWithFormat:@"%@%@",RequestLiveTracking,self.userData.fieldId];
            [webService getData:urlString withflagLogin:YES];
        }
        
    }else if(index==6){
        [self performSegueWithIdentifier:@"memberGroup" sender:nil];
    }else if (index==7){
        RLMResults<ConversationModel *> *convData = [ConversationModel objectsWhere:@"UserId == %@",self.userData.fieldId];
        UIStoryboard *chatStoryBoard=[UIStoryboard storyboardWithName:@"Chat" bundle:nil];
        ConversationViewController *conversationView=[chatStoryBoard instantiateViewControllerWithIdentifier:@"ConversationController"];
        if((unsigned long)convData.count>0){
            ConversationModel *dic=[convData objectAtIndex:0];
            
            conversationView.conversationData=dic;
            
            RLMRealm *realm = [RLMRealm defaultRealm];
            UserModel *userModl=[[UserModel alloc]init];
            [realm beginWriteTransaction];
            
            userModl.Id=self.userData.fieldId;
            userModl.UserName=self.userData.name;
            
            [realm addOrUpdateObject:userModl];
            // now commit & end this transaction
            [realm commitWriteTransaction];
            
            [self.navigationController pushViewController:conversationView animated:YES];
            
        }else{
            webService=[[WebServiceFunc alloc]init];
            [webService setDelegate:self];
            hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
            hud.mode = MBProgressHUDModeIndeterminate;
            serviceType=7;
            NSString *urlString=[NSString stringWithFormat:@"%@%@",CheckConversation,self.userData.fieldId];
            [webService getData:urlString withflagLogin:YES];
        }
    }else if (index==8){
        if ((self.fromSearch)&&(!self.hasRelation)) {
            UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Error" message:@"You don't have permission" delegate:nil cancelButtonTitle:@"ok" otherButtonTitles:nil];
            [alert show];
        }else{
            serviceType=3;
            hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
            hud.mode = MBProgressHUDModeIndeterminate;
            NSString *urlString=[NSString stringWithFormat:@"%@%@",GoHome,self.userData.fieldId];
            [webService getData:urlString withflagLogin:YES];
        }
    }else if (index==9){
        hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        hud.mode = MBProgressHUDModeIndeterminate;
        NSString *urlString=ListMembersAndGroups;
        serviceType=9;
        [webService getData:urlString withflagLogin:YES];
    }
    
}

- (BOOL)radialMenu:(ALRadialMenu *)radialMenu shouldRotateMenuButtonWhenItemsAppear:(UIButton *)button {
    if (radialMenu == self.radialMenu) {
        return YES;
    }
    
    return NO;
}

- (BOOL)radialMenu:(ALRadialMenu *)radialMenu shouldRotateMenuButtonWhenItemsDisappear:(UIButton *)button {
    if (radialMenu == self.radialMenu) {
        return NO;
    }
    
    return YES;
}




#pragma mark - Array List

- (NSArray *)list
{
    NSMutableArray *activ=[[NSMutableArray alloc]init];
    
    for (int i=0; i<groupList.count;i++) {
        
        [activ addObject:[[groupList objectAtIndex:i] objectForKey:@"name"]];
        
    }
    
    return activ;
}

#pragma mark - LPPopupListViewDelegate

- (void)popupListView:(LPPopupListView *)popUpListView didSelectIndex:(NSInteger)index
{
    NSLog(@"popUpListView - didSelectIndex: %d", index);
    
    hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    hud.mode = MBProgressHUDModeIndeterminate;
    NSString *urlString=[NSString stringWithFormat:@"%@%@&groupid=%@",InviteUser,self.userData.fieldId,[[groupList objectAtIndex:index]objectForKey:@"id"]];
    serviceType=4;
    [webService getData:urlString withflagLogin:YES];
    
}

- (void)popupListViewDidHide:(LPPopupListView *)popUpListView selectedIndexes:(NSIndexSet *)selectedIndexes
{
    
    if ([selectedIndexes count]==0) {
        
    }else{
        NSLog(@"popupListViewDidHide - selectedIndexes: %@", selectedIndexes.description);
        self.selectedIndexes = [[NSMutableIndexSet alloc] initWithIndexSet:selectedIndexes];
        //[selectedIndexes get]
        [selectedIndexes enumerateIndexesUsingBlock:^(NSUInteger idx, BOOL *stop) {
        }];
    }
    
}


#pragma Mark - Search in table
- (void)searchBarTextDidBeginEditing:(UISearchBar *)searchBar
{
    //  [self.view addGestureRecognizer:tap];
    [searchBar setShowsCancelButton:YES animated:YES];
}
- (void)searchBarTextDidEndEditing:(UISearchBar *)searchBar
{
    [searchBar setShowsCancelButton:NO animated:YES];
    
}
- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText
{
    
    if (searchText.length>3) {
        searchText = [searchText stringByReplacingOccurrencesOfString:@" " withString:@""];
        NSPredicate *resultPredicate = [NSPredicate predicateWithFormat:@"name contains[c] %@", searchText];
        groupList = [[groupList filteredArrayUsingPredicate:resultPredicate]mutableCopy];
        
        listView.arrayList=[self list];
        [listView.tableView reloadData];
    }else{
        
        groupList=allGroupList;
        
        
        listView.arrayList=[self list];
        NSLog(@"%@",listView.arrayList);
        [listView.tableView reloadData];
    }
}

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar
{
    // [self.view removeGestureRecognizer:tap];
    [searchBar resignFirstResponder];
    
}

- (void)searchBarCancelButtonClicked:(UISearchBar *)searchBar
{
    [searchBar setShowsCancelButton:NO animated:YES];
    searchBar.text=@"";
    // fetcharr=locationArray;
    
}

#pragma End- Search in table



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([[segue identifier] isEqualToString:@"memberInfo"]) {
        MemberInfoViewController *info=[segue destinationViewController];
        info.userData=[[UserData alloc]init];
        info.userData.name=self.userData.name;
        info.userData.userImage=self.userData.userImage;
        info.userData.age=[[memberData objectForKey:@"age"]stringValue];
        info.userData.city=[memberData objectForKey:@"currentCity"];
        BOOL gender=[[memberData objectForKey:@"gender"] boolValue];
        if (gender) {
            info.userData.gender=@"Male";
        }else{
            info.userData.gender=@"Female";
        }
        info.userData.job=[memberData objectForKey:@"job"];
        info.userData.friendsNum=[[memberData objectForKey:@"friendsNumber"] stringValue];
        info.userData.groupsNum=[[memberData objectForKey:@"groupsNumber"] stringValue];
    }
    else if([[segue identifier]isEqualToString:@"memberGroup"]){
        MemberGroupsViewController *info=[segue destinationViewController];
        info.userData=[[UserData alloc]init];
        info.userData.name=self.userData.name;
        info.userData.userImage=self.userData.userImage;
        info.userData.fieldId=self.userData.fieldId;
    }else if([[segue identifier] isEqualToString:@"MeetThere"]){
        MeetThereViewController *meetView=[segue destinationViewController];
        meetView.listID=self.userData.fieldId;
        meetView.listName=self.userData.name;
        meetView.isUser=YES;
    }else if ([[segue identifier] isEqualToString:@"permissionView"]){
        PermissionsViewController *permiView=[segue destinationViewController];
        permiView.userData=[[NSDictionary alloc]initWithObjectsAndKeys:self.userData.fieldId,@"userID",self.userData.name,@"userName",self.userData.userImage,@"userImage", nil];
        permiView.fromGroup=YES;
    }
}






@end
