//
//  MemberGroupsViewController.h
//  FamilyLocator
//
//  Created by Amira on 10/17/16.
//  Copyright © 2016 Amira. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UserData.h"
#import "WebServiceFunc.h"
@interface MemberGroupsViewController : UIViewController<WebServiceProtocol,UITableViewDelegate,UITableViewDataSource>{
    __weak IBOutlet UIImageView *memberImg;
    __weak IBOutlet UILabel *memberName;
}
@property(nonatomic,strong)IBOutlet UITableView *table;
@property(nonatomic,strong)UserData *userData;
@end
