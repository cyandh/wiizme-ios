//
//  LikeViewController.h
//  wiizme
//
//  Created by Amira on 12/24/16.
//  Copyright © 2016 Amira. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LikeViewController : UIViewController
<UITextViewDelegate,UITableViewDelegate,UITableViewDataSource>
{
    __weak IBOutlet UILabel *likes;
    __weak IBOutlet UITableView *tableView;
    __weak IBOutlet UIImageView *likeImg;
    __weak IBOutlet UITextView *commentTxt;
    __weak IBOutlet UIView * popView;
    __weak IBOutlet UIButton *likeView;
}
@property(nonatomic,strong) NSMutableArray * likesData;
@end
