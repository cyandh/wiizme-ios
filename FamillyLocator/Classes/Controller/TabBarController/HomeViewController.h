//
//  HomeViewController.h
//  FamilyLocator
//
//  Created by Amira on 10/12/16.
//  Copyright © 2016 Amira. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GoBackHome.h"

#import <Pusher/Pusher.h>
#import "HomeRedialMenuViewController.h"
#import "ZoonNotificationListVC.h"
#import "SelectIctionTypeViewController.h"
#import "MBProgressHUD.h"
#import "IamFine.h"
#import "whereareyou.h"
#import "ArrivaAtPopUpVC.h"
#import "HomeRedialMenuViewController.h"
#import "SOSData.h"
#import "HomeNotifications.h"
#import "MapKitViewController.h"


@interface HomeViewController : UIViewController<RadialMenuDelegate,UIScrollViewDelegate>{
    BOOL isShown;
    IBOutlet UIScrollView * scrollview;
    BOOL RedialMenuFlag;
    int SelectTypeId;
    SelectIctionTypeViewController * SelectIctionTypePopup;
    BOOL isGroup;
    NSDictionary * selectedMenuItemData;
    MBProgressHUD * hud;
    NSInteger FID;
    IamFine * iamfinfunction;
    UIButton * CurrentPageBt;
    NSMutableArray * HomePages;
    NSMutableArray * pageTitlebtArray;
    whereareyou * whereareyouP;
    GoBackHome * GoBackHomeObj;
    int redialCount;
    ArrivaAtPopUpVC * ArrivaAtPopUp;
    ZoonNotificationListVC * ZoonNotificationList;
    HomeRedialMenuViewController * HomeRedialMenu;
    SOSData * SOS ;
    NSDictionary * SOSResponse;
    HomeNotifications *HomeNotification;
    MapKitViewController * MapKitVC;
    NSDictionary *HomeNotificationDic;
}

@property (weak, nonatomic) IBOutlet UIButton *button;

@property (strong, nonatomic) NSArray *popups;
@property(nonatomic,strong)PTPusher *client;
@property (nonatomic,strong) UIWindow *window2;


@end
