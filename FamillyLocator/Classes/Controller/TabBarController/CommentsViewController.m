//
//  CommentsViewController.m
//  wiizme
//
//  Created by Amira on 12/24/16.
//  Copyright © 2016 Amira. All rights reserved.
//

#import "CommentsViewController.h"
#import "Animation.h"
#import "Constant.h"
#import "GroupTableViewCell.h"
#import "MBProgressHUD.h"
#import "LikeViewController.h"
#import "UIViewController+RemoteNotificationData.h"
#import "Helper.h"
#import "SideMenuViewController.h"
@interface CommentsViewController ()
{
    WebServiceFunc *webService;
    MBProgressHUD *hud;
    NSMutableArray *commentsData,*likesData;
    int serviceType , keyboardHeight;
    
}
@end

@implementation CommentsViewController

- (void)viewDidLoad {
    


    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWasShown:)
                                                 name:UIKeyboardDidShowNotification
                                               object:nil];
    [[CommentView layer] setBorderWidth:1.5f];
    [[CommentView layer] setBorderColor:[UIColor colorWithRed:224.0/255.0f green:224.0/255.0f blue:224.0/255.0f alpha:1.0f].CGColor];
    [Animation roundCornerForView:CommentView withAngle:5.0f];
    
    webService=[[WebServiceFunc alloc]init];
    [webService setDelegate:self];
        hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        hud.mode = MBProgressHUDModeIndeterminate;
        NSString *urlString=[NSString stringWithFormat:@"%@%@",GetReactions,self.postId];
        serviceType=1;
        [webService getData:urlString withflagLogin:YES];
    [popView.layer setShadowColor:[[UIColor darkGrayColor] CGColor]];
    [popView.layer setShadowOffset:CGSizeMake(0, 0)];
    [popView.layer setShadowRadius:20.0];
    [popView.layer setShadowOpacity:1];
    popView.clipsToBounds = NO;
    popView.layer.masksToBounds = NO;
    
    
    [Animation roundCornerForView:popView withAngle:10.0f];
    
    UITapGestureRecognizer *likeGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(likeView:)];
    
    [likeup addGestureRecognizer:likeGesture];
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

-(void)viewDidDisappear:(BOOL)animated{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}



-(void)viewDidAppear:(BOOL)animated{
    
    [UIView beginAnimations: @"anim" context: nil];
    [UIView setAnimationBeginsFromCurrentState: YES];
    [UIView setAnimationDelegate:self];
    [UIView setAnimationDidStopSelector:@selector(OperHolderAnimation)];
    [UIView setAnimationDuration: .5];
    self.view.alpha =1;
    [UIView commitAnimations];
    
    
}


-(void)OperHolderAnimation{
    
    [UIView beginAnimations: @"anim" context: nil];
    [UIView setAnimationBeginsFromCurrentState: YES];
    [UIView setAnimationDelegate:self];
    
    [UIView setAnimationDuration: .5];
    popView.alpha =1;
    [UIView commitAnimations];
    
    
}
- (IBAction)showMenu
{
    
    SideMenuViewController *sideMenu=[self.storyboard instantiateViewControllerWithIdentifier:@"SideMenu"];
    [self.navigationController presentViewController:sideMenu animated:YES completion:nil];
}

-(IBAction)likeView:(id)sender{
    [self performSegueWithIdentifier:@"LikesView" sender:nil];
}
-(IBAction)likeViewBt:(id)sender{
    // [self.delegate CloseView];
    [self performSegueWithIdentifier:@"LikesView" sender:nil];
}
-(IBAction)writecomment:(id)sender{
    // [self.delegate GotoDetails:DTO];
    hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    hud.mode = MBProgressHUDModeIndeterminate;
    NSMutableDictionary *userData = [[NSMutableDictionary alloc] initWithObjectsAndKeys:@"00000000-0000-0000-0000-000000000000",@"userId",
                                     CommentPostID,@"reactionTypeId",
                                     self.postId,@"postId",
                                     commentTxt.text,@"text",
                                     nil];
    serviceType=2;
    [webService postDataWithUrlString:MakeReaction withData:userData withflagLogin:YES];
    
}

-(void)userWebserviceCallDidSuccessWithRespone:(NSMutableArray *)response{
    [hud setHidden:YES];
    if (serviceType==1) {
        if ([response isKindOfClass:[NSDictionary class]]) {
            NSDictionary *responseData =(NSDictionary *)response;
            commentsData=[responseData objectForKey:@"commentList"];
            likesData=[responseData objectForKey:@"likesList"];
            if (likesData.count==1) {
                [likeView setHidden:NO];
              likes.text=[NSString stringWithFormat:@"%li Like",likesData.count];
            }else if (likesData.count>1){
                [likeView setHidden:NO];
                likes.text=[NSString stringWithFormat:@"%li Likes",likesData.count];
            }else{
                [likeView setHidden:YES];
            }
            
        }
        [tableView reloadData];
        
    }else if (serviceType==3){
        
    }else{
        [self.navigationController popViewControllerAnimated:YES];
    }
    
    
}
-(void)userWebserviceCallDidFaildwithError:(NSError *)error andMessage:(NSString *)message{
    [hud setHidden:YES];
    NSLog(@"%@",error.localizedDescription);
    if ([message isEqualToString:@"401" ]) {
        UIStoryboard *storyboard=[UIStoryboard storyboardWithName:@"Main" bundle:nil];
        UIViewController* viewController=[storyboard instantiateViewControllerWithIdentifier:@"LoginController"];
        [self presentViewController:viewController animated:YES completion:nil];
    }
    
}


#pragma TableView DataSource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.row<commentsData.count) {
    UILabel *lbl=[[UILabel alloc]initWithFrame:CGRectMake(83, 37, self.view.frame.size.width-83-6,21)];
    lbl=[self customizeLBLwithString:lbl andtxt:[[commentsData objectAtIndex:indexPath.row] objectForKey:@"text"] andfintsize:13];
    CGFloat height=lbl.frame.size.height+40;
    return height;
    }else{
        return 0;
    }
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    return commentsData.count;
    
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    GroupTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"groupCell"];
    
    
    NSString *firstName=[[[commentsData objectAtIndex:indexPath.row] objectForKey:@"user"] objectForKey:@"firstName"];
    NSString *lastName=[[[commentsData objectAtIndex:indexPath.row] objectForKey:@"user"] objectForKey:@"familyName"];
    cell.groupName.text=[NSString stringWithFormat:@"%@ %@",firstName,lastName];
    
    cell.groupsecur.text=[[commentsData objectAtIndex:indexPath.row] objectForKey:@"text"];
    cell.groupsecur=[self customizeLBLwithString:cell.groupsecur andtxt:[[commentsData objectAtIndex:indexPath.row] objectForKey:@"text"] andfintsize:13];
    cell.memberNum.attributedText=[Helper timeTXT:[Helper timeLeftSinceDate:[[commentsData objectAtIndex:indexPath.row] objectForKey:@"date"]]];
    NSURL *imageUrl=[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",PhotosUrl,[[[commentsData objectAtIndex:indexPath.row] objectForKey:@"user"] objectForKey:@"photoId"]]];
    [cell.image setImageURL:imageUrl];
    NSLog(@"%f",cell.image.frame.size.width);
    [Animation roundCornerForView:cell.image withAngle:30.0f];
    
    
    //  [cell loadNewsImagewithLabelString:videotitle andDate:videoDateFormate];
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    // This will create a "invisible" footer
    return 0.01f;
}


- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    return [UIView new];
    
}


-(UILabel*)customizeLBLwithString:(UILabel*)lbl andtxt:(NSString*)lblstr andfintsize:(int)fontSize{
    //UILabel *lbl=[[UILabel alloc]initWithFrame:frame];
    lbl.numberOfLines = 0; //will wrap text in new line
    CGSize maximumLabelSize = CGSizeMake(lbl.frame.size.width, FLT_MAX);
    CGSize expectedLabelSize = [lblstr sizeWithFont:[UIFont fontWithName:@"Cairo-SemiBold" size:fontSize] constrainedToSize:
                                maximumLabelSize lineBreakMode:lbl.lineBreakMode];
    //adjust the label the the new height.
    CGRect newFrame = lbl.frame;
    newFrame.size.height = expectedLabelSize.height+5;
    lbl.frame = newFrame;
    [lbl setFont:[UIFont fontWithName:@"Cairo-SemiBold" size:fontSize]];
    lbl.textAlignment=NSTextAlignmentLeft;
    lbl.backgroundColor=[UIColor whiteColor];
    [lbl setNeedsUpdateConstraints];
    //  [scrollView setContentSize:CGSizeMake(self.view.frame.size.width, ContainerView.frame.origin.y+details.frame.origin.y+details.frame.size.height+60)];
    return lbl;
}


- (void)moveBannerOffScreen {
    [self.view layoutIfNeeded];
    _tableHeightConstraint.constant =_tableHeightConstraint.constant-10;
    _CommentTXTHeightConstraint.constant=_CommentTXTHeightConstraint.constant+10;
    [UIView animateWithDuration:5
                     animations:^{
                         [self.view layoutIfNeeded]; // Called on parent view
                     }];
     [self animateTextView:commentTxt up:YES];
}

- (void)textViewDidBeginEditing:(UITextView *)textView{
    if ([textView.text isEqualToString:@"Write Comment"]) {
        textView.text=@"";
    }
    textView.textColor=[UIColor blackColor];
    [self animateTextView:textView up:YES];
}
-(void)textViewDidEndEditing:(UITextView *)textView{
    [self animateTextView:textView up:NO];
}

- (void) animateTextView: (UITextView*) textView up: (BOOL) up
{
    
    const int movementDistance =  keyboardHeight; // tweak as needed
    const float movementDuration = 0.1f; // tweak as needed
    
    int movement = (up ? -movementDistance : movementDistance);
    
    [UIView beginAnimations: @"anim" context: nil];
    [UIView setAnimationBeginsFromCurrentState: YES];
    [UIView setAnimationDuration: movementDuration];
    CommentView.frame = CGRectOffset(CommentView.frame, 0, movement);
    [UIView commitAnimations];
}

- (void)keyboardWasShown:(NSNotification *)notification
{
    
    // Get the size of the keyboard.
    CGSize keyboardSize = [[[notification userInfo] objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    if(keyboardHeight==0){
        //Given size may not account for screen rotation
        keyboardHeight = MIN(keyboardSize.height,keyboardSize.width);
        
        const int movementDistance =  keyboardHeight; // tweak as needed
        const float movementDuration = 0.1f; // tweak as needed
        
        int movement = (/* DISABLES CODE */ (YES) ? -movementDistance : movementDistance);
        
        [UIView beginAnimations: @"anim" context: nil];
        [UIView setAnimationBeginsFromCurrentState: YES];
        [UIView setAnimationDuration: movementDuration];
        CommentView.frame = CGRectOffset(CommentView.frame, 0, movement);
        [UIView commitAnimations];
    }
    
    //your other code here..........
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text {
    return YES;
}

- (void)textViewDidChange:(UITextView *)textView{
    
    CGFloat maxHeight = 312.0f;
    CGFloat fixedWidth = commentTxt.frame.size.width;
    CGFloat fixedHight = commentTxt.frame.size.height;
    CGSize newSize = [commentTxt sizeThatFits:CGSizeMake(fixedWidth, MAXFLOAT)];
    CGRect newFrame = commentTxt.frame;
    newFrame.size = CGSizeMake(fmaxf(newSize.width, fixedWidth), fminf(newSize.height, maxHeight));
    [UIView beginAnimations:@"" context:nil];
    [UIView setAnimationDuration:0.1];
    
    
    
    
    
    CGFloat Def =newFrame.size.height-fixedHight;
    if (Def>0&&commentTxt.frame.size.height<80) {
        commentTxt.frame = newFrame;
        [self moveBannerOffScreen];
        // commentBar.frame = CGRectMake(commentBar.frame.origin.x, commentBar.frame.origin.y-Def, commentBar.frame.size.width, commentBar.frame.size.height+Def);
        //   tableview.frame = CGRectMake(tableview.frame.origin.x, tableview.frame.origin.y, tableview.frame.size.width, tableview.frame.size.height-Def);
    }
    if (Def<0&&commentTxt.frame.size.height) {
        commentTxt.frame = newFrame;
        // commentBar.frame = CGRectMake(commentBar.frame.origin.x, commentBar.frame.origin.y-Def, commentBar.frame.size.width, commentBar.frame.size.height+Def);
        //  tableview.frame = CGRectMake(tableview.frame.origin.x, tableview.frame.origin.y, tableview.frame.size.width, tableview.frame.size.height-Def);
    }
    [UIView commitAnimations];
    
}



 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
     if ([[segue identifier] isEqualToString:@"LikesView"]) {
         LikeViewController *likeView=[segue destinationViewController];
         likeView.likesData=[[NSMutableArray alloc]init];
         likeView.likesData=likesData;
     }
 }


@end
