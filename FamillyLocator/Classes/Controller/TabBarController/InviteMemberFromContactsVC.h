//
//  SelectMemberOrGroupViewController.h
//  wiizme
//
//  Created by agamil on 5/12/17.
//  Copyright © 2017 Amira. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MBProgressHUD.h"
#import "WebServiceFunc.h"


@protocol InviteMemberOrGroupDelegate <NSObject>

@required
- (void)SelectMemberOrGroupD:(NSDictionary *)MemberOrGroup;



@end


@interface InviteMemberFromContactsVC : UIViewController{
    UITapGestureRecognizer* tapGesture;
    IBOutlet UITableView * tableView;
    WebServiceFunc *webService;
    MBProgressHUD *hud;
    NSMutableArray *members,*admins;
    int memberIndx,serviceNUM;
    NSIndexPath *selectedIndex;
    NSIndexPath * selectedItem;
    IBOutlet UISearchBar * searchBar;
    NSMutableArray * allList;

}

@property(nonatomic,readwrite)BOOL isGroup;
@property(nonatomic,readwrite)BOOL isContact;
@property(nonatomic,weak)id<InviteMemberOrGroupDelegate> deleget;

@end
