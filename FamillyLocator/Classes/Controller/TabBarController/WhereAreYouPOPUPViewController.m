//
//  WhereAreYouPOPUPViewController.m
//  wiizme
//
//  Created by Amira on 3/25/17.
//  Copyright © 2017 Amira. All rights reserved.
//

#import "WhereAreYouPOPUPViewController.h"
#import "Animation.h"
@interface WhereAreYouPOPUPViewController ()

@end

@implementation WhereAreYouPOPUPViewController

@synthesize delegate,popView,Message,accept,userImg,userName,userData,uploadedImg;
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [popView.layer setShadowColor:[[UIColor darkGrayColor] CGColor]];
    [popView.layer setShadowOffset:CGSizeMake(0, 0)];
    [popView.layer setShadowRadius:50.0];
    [popView.layer setShadowOpacity:1];
    popView.clipsToBounds = NO;
    popView.layer.masksToBounds = NO;
    
    if ([[userData objectForKey:@"message"] isEqualToString:@""]) {
        self.popviewHeightConstraint.constant=self.popviewHeightConstraint.constant-self.txtHeightConstraint.constant;
        self.txtHeightConstraint.constant=0.0f;
    }
    if ([[userData objectForKey:@"UploadedImgUrl"]isEqualToString:@"http://testapi.wiizme.com/home/DisplayImage?id="]) {
        self.popviewHeightConstraint.constant=self.popviewHeightConstraint.constant-self.imgHeightConstraint.constant;
        self.imgHeightConstraint.constant=0.0f;
    }
    [self.view needsUpdateConstraints];
    
    userName.text=[userData objectForKey:@"Name"];
    Message.text=[userData objectForKey:@"message"];
    
    NSURL *imageUrl=[NSURL URLWithString:[userData objectForKey:@"UserImageUrl"]];
    [userImg setImageURL:imageUrl];
    
    imageUrl=[NSURL URLWithString:[userData objectForKey:@"UploadedImgUrl"]];
    [uploadedImg setImageURL:imageUrl];

    [Animation roundCornerForView:popView withAngle:10.0f];
    [Animation roundCornerForView:userImg withAngle:25.0f];
    [userImg setClipsToBounds:YES];
    [userImg.layer setBorderColor:[UIColor lightGrayColor].CGColor];
    [userImg.layer setBorderWidth:1.0f];
    
    [Animation roundCornerForView:accept withAngle:5.0f];
}

-(IBAction)declineAction:(id)sender{
    // [self.delegate CloseView];
    
    [self.delegate declineData];
    
}
-(IBAction)acceptAction:(id)sender{

    [self.delegate acceptData:userData];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
