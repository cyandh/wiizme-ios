//
//  MapKitViewController.h
//  wiizme
//
//  Created by Amira on 12/27/16.
//  Copyright © 2016 Amira. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>
#import "WebServiceFunc.h"
#import "MultiRowAnnotation.h"
#import "AsyncImageView.h"
#import "GroupData.h"
#import "HistoryDatesViewController.h"
#import "LiveTrackingViewController.h"
@interface MapKitViewController : UIViewController<MKMapViewDelegate,WebServiceProtocol,HistoryDatesDelegate>
{
    __weak IBOutlet UIButton *historyBut;
    __weak IBOutlet UIView *headerView;
    __weak IBOutlet UILabel *memberName;
    __weak IBOutlet AsyncImageView *memberImg;
    __weak IBOutlet UILabel *titleLBL;
    __weak IBOutlet UILabel * CurrnetView;
    LiveTrackingViewController *liveTrack;
    BOOL IsLiveTraking;
}

@property (nonatomic,weak) IBOutlet MKMapView *mapView;
@property (nonatomic,strong) MKAnnotationView *selectedAnnotationView;
@property (nonatomic,strong) MultiRowAnnotation *calloutAnnotation;
@property(nonatomic,retain)CLLocationManager *locatioManager;
@property (nonatomic, retain) MKPolyline *routeLine; //your line
@property (nonatomic, retain) MKPolylineView *routeLineView; //overlay vie
@property (nonatomic,strong)NSMutableArray *locations;
@property (nonatomic)BOOL isGroup;
@property (nonatomic,strong) GroupData *groupData;
@property (nonatomic,strong) NSString *userID;
@property (nonatomic)BOOL fromPost;

@property (nonatomic,strong)NSString *memberImage,*memberNam;

@end
