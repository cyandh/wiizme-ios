//
//  LikeViewController.m
//  wiizme
//
//  Created by Amira on 12/24/16.
//  Copyright © 2016 Amira. All rights reserved.
//

#import "LikeViewController.h"
#import "Animation.h"
#import "Constant.h"
#import "GroupTableViewCell.h"
#import "HomeMemberViewController.h"
#import "UIViewController+RemoteNotificationData.h"
#import "SideMenuViewController.h"
@interface LikeViewController ()
{
    NSMutableArray *comments;
    int selectedRow;
}
@end

@implementation LikeViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    

    // Do any additional setup after loading the view.
    if (self.likesData.count==1) {
        likes.text=[NSString stringWithFormat:@"%li Like",self.likesData.count];
    }else if (self.likesData.count>1){
        likes.text=[NSString stringWithFormat:@"%li Likes",self.likesData.count];
    }
}

-(void)viewDidDisappear:(BOOL)animated{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}



- (IBAction)showMenu
{
    
    SideMenuViewController *sideMenu=[self.storyboard instantiateViewControllerWithIdentifier:@"SideMenu"];
    [self.navigationController presentViewController:sideMenu animated:YES completion:nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma TableView DataSource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    return self.likesData.count;
    
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    GroupTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"groupCell"];
    
    NSString *firstName=[[[self.likesData objectAtIndex:indexPath.row] objectForKey:@"user"] objectForKey:@"firstName"];
    NSString *lastName=[[[self.likesData objectAtIndex:indexPath.row] objectForKey:@"user"] objectForKey:@"familyName"];
    cell.groupName.text=[NSString stringWithFormat:@"%@ %@",firstName,lastName];
    
    
    NSURL *imageUrl=[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",PhotosUrl,[[[self.likesData objectAtIndex:indexPath.row] objectForKey:@"user"] objectForKey:@"photoId"]]];
    [cell.image setImageURL:imageUrl];
    NSLog(@"%f",cell.image.frame.size.width);
    [Animation roundCornerForView:cell.image withAngle:30.0f];
    //  [cell loadNewsImagewithLabelString:videotitle andDate:videoDateFormate];
    
    return cell;
}
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    // This will create a "invisible" footer
    return 0.01f;
}


- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    return [UIView new];
    
}


-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    selectedRow=indexPath.row;
    [self performSegueWithIdentifier:@"memberHome" sender:nil];
}

#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    HomeMemberViewController *member=[segue destinationViewController];
    member.userData=[[self.likesData objectAtIndex:selectedRow] objectForKey:@"user"];
    member.userData =[[UserData alloc]init];
    member.userData.name=[NSString stringWithFormat:@"%@ %@",[[[self.likesData objectAtIndex:selectedRow] objectForKey:@"user"] objectForKey:@"firstName"],[[[self.likesData objectAtIndex:selectedRow] objectForKey:@"user"] objectForKey:@"familyName"]];
    member.userData.userImage=[[[self.likesData objectAtIndex:selectedRow] objectForKey:@"user"] objectForKey:@"photoId"];
    member.userData.fieldId=[[[self.likesData objectAtIndex:selectedRow] objectForKey:@"user"] objectForKey:@"id"];

}


@end
