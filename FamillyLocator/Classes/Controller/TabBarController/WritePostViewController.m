//
//  WritePostViewController.m
//  wiizme
//
//  Created by Amira on 12/23/16.
//  Copyright © 2016 Amira. All rights reserved.
//

#import "WritePostViewController.h"
#import "MBProgressHUD.h"
#import "Constant.h"
#import <MapKit/MapKit.h>
#import "MyLocation.h"
#import "Animation.h"
#import "Validation.h"
#import "SPGooglePlacesAutocompleteViewController.h"
#import "UIViewController+RemoteNotificationData.h"
#import "SideMenuViewController.h"
@interface WritePostViewController ()
{
    WebServiceFunc *webService;
    LPPopupListView *listView;
    MBProgressHUD *hud;
    NSMutableArray *memberList,*allMemberList,*groupList,*allGroupList;
    NSMutableArray *postPrivacies;
    int serviceType , privacyType,postType , keyboardHeight;
    UIImage *uploadImg , *screenShotImage;
    MKMapView *mapView;
    double userLatitude,userLogitude;
    NSString *address;
    CLLocation *userLocate;
    BOOL keyborad;
    
}
@end

@implementation WritePostViewController

- (void)viewDidLoad {
    
    [super viewDidLoad];
    if (_IsEdit) {
        postTXT.text = [[_PostData objectForKey:@"post"] objectForKey:@"text"];
    }
    
    userName.text=self.userNameTXT;
    NSURL *imageUrl=[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",PhotosUrl,self.userImgID]];
    [userImg setImageURL:imageUrl];
    [Animation roundCornerForView:userImg withAngle:5.0f];
    [Animation roundCornerForView:postBTN withAngle:2.0f];
    [postTXT setDelegate:self];
    postPrivacies=[[NSMutableArray alloc]init];
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(StouchesBegan)];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWasShown:)
                                                 name:UIKeyboardDidShowNotification
                                               object:nil];
    
    [self.view addGestureRecognizer:tapGesture];
    
    webService=[[WebServiceFunc alloc]init];
    [webService setDelegate:self];
    
    if (self.isGroup) {
        [postPrivacies addObject:[NSDictionary dictionaryWithObjectsAndKeys:self.userId,@"groupId",@"",@"userId",@"00000000-0000-0000-0000-000000000000",@"postId", nil]];
    }
    
    if (self.seeThis) {
        UIImagePickerController *picker = [[UIImagePickerController alloc] init];
        picker.delegate = self;
        picker.allowsEditing = YES;
        picker.sourceType = UIImagePickerControllerSourceTypeCamera;
        
        [self presentViewController:picker animated:YES completion:NULL];
        
    }
    
    
    
    
}

-(void)viewDidDisappear:(BOOL)animated{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}


-(void)StouchesBegan {
    
    [postTXT resignFirstResponder];
    
    
}

- (IBAction)showMenu
{
    
    SideMenuViewController *sideMenu=[self.storyboard instantiateViewControllerWithIdentifier:@"SideMenu"];
    [self.navigationController presentViewController:sideMenu animated:YES completion:nil];
}

-(NSMutableAttributedString *)attribut:(NSString*)imgname andtxtName:(NSString*)txtName{
    NSTextAttachment *attachment = [[NSTextAttachment alloc] init];
    attachment.image = [UIImage imageNamed:imgname];
    
    NSAttributedString *attachmentString = [NSAttributedString attributedStringWithAttachment:attachment];
    NSAttributedString *mystring = [[NSAttributedString alloc]initWithString:[NSString stringWithFormat:@"  %@  ",txtName]];
    NSMutableAttributedString *myStringimg= [[NSMutableAttributedString alloc] initWithAttributedString:attachmentString];
    
    [myStringimg appendAttributedString:mystring];
    
    return myStringimg;
}

-(IBAction)selectGroup:(id)sender{
    
    UIAlertController * alert=   [UIAlertController
                                  alertControllerWithTitle:nil
                                  message:nil
                                  preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* allGroup = [UIAlertAction
                               actionWithTitle:@"All Groups"
                               style:UIAlertActionStyleDefault
                               handler:^(UIAlertAction * action)
                               {
                                   privacyType=1;
                                   
                                   postPrivacies=@"";
                                   [alert dismissViewControllerAnimated:YES completion:nil];
                                   
                               }];
    
    alert.view.tintColor = [UIColor grayColor];
    
    [allGroup setValue:[[UIImage imageNamed:@"all-groups.png"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal] forKey:@"image"];
    
    UIAlertAction* selectGroup = [UIAlertAction
                                  actionWithTitle:@"Select Groups"
                                  style:UIAlertActionStyleDefault
                                  handler:^(UIAlertAction * action)
                                  {
                                      privacyType=2;
                                      NSString *urlString=ListMembersAndGroups;
                                      serviceType=1;
                                      [webService getData:urlString withflagLogin:YES];
                                      [alert dismissViewControllerAnimated:YES completion:nil];
                                      
                                  }];
    
    [selectGroup setValue:[[UIImage imageNamed:@"select-group.png"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal] forKey:@"image"];
    
    UIAlertAction* selectMember = [UIAlertAction
                                   actionWithTitle:@"Select Members"
                                   style:UIAlertActionStyleDefault
                                   handler:^(UIAlertAction * action)
                                   {
                                       privacyType=3;
                                       NSString *urlString=ListMembersAndGroups;
                                       serviceType=1;
                                       [webService getData:urlString withflagLogin:YES];
                                       [alert dismissViewControllerAnimated:YES completion:nil];
                                       
                                   }];
    
    [selectMember setValue:[[UIImage imageNamed:@"select-member.png"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal] forKey:@"image"];
    
    UIAlertAction* cancel = [UIAlertAction
                             actionWithTitle:@"Cancel"
                             style:UIAlertActionStyleDefault
                             handler:^(UIAlertAction * action)
                             {
                                 
                                 [alert dismissViewControllerAnimated:YES completion:nil];
                                 
                             }];
    
    [alert addAction:allGroup];
    [alert addAction:selectGroup];
    [alert addAction:selectMember];
    [alert addAction:cancel];
    
    [self presentViewController:alert animated:YES completion:nil];
    
    
    //    UIAlertView * alert=[[UIAlertView alloc]initWithTitle:nil message:nil delegate:self cancelButtonTitle:@"cancel" otherButtonTitles:@"All Groups",@"Select Groups",@"Select Member", nil];
    //
    //    [alert show];
    
}

// Called when a button is clicked. The view will be automatically dismissed after this call returns
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    
    if (buttonIndex==1) {
        privacyType=1;
        
        postPrivacies=@"";
    } else if(buttonIndex==2){
        privacyType=2;
        NSString *urlString=ListMembersAndGroups;
        serviceType=1;
        [webService getData:urlString withflagLogin:YES];
    }else if (buttonIndex==3){
        privacyType=3;
        NSString *urlString=ListMembersAndGroups;
        serviceType=1;
        [webService getData:urlString withflagLogin:YES];
        
    }
    
    NSLog(@"%li",buttonIndex);
    NSLog(@"%@",[alertView buttonTitleAtIndex:buttonIndex]);
}


-(void)groupAndMemberList{
    
    float paddingTopBottom = 20.0f;
    float paddingLeftRight = 20.0f;
    
    CGPoint point = CGPointMake(paddingLeftRight, (self.navigationController.navigationBar.frame.size.height + paddingTopBottom) + paddingTopBottom);
    CGSize size = CGSizeMake((self.view.frame.size.width - (paddingLeftRight * 2)), self.view.frame.size.height - ((self.navigationController.navigationBar.frame.size.height + paddingTopBottom) + (paddingTopBottom * 2)));
    
    self.selectedIndexes=nil;
    listView=[[LPPopupListView alloc]initWithTitle:@"" list:[self list] selectedIndexes:self.selectedIndexes point:point size:size multipleSelection:YES disableBackgroundInteraction:NO];
    
    [listView.layer setShadowColor:[[UIColor darkGrayColor] CGColor]];
    [listView.layer setShadowOffset:CGSizeMake(0, 0)];
    [listView.layer setShadowRadius:20.0];
    [listView.layer setShadowOpacity:1];
    listView.clipsToBounds = NO;
    listView.layer.masksToBounds = NO;
    
    
    
    [listView.searchBar setDelegate:self];
    
    listView.delegate = self;
    
    [listView showInView:self.navigationController.view animated:YES];
}



-(IBAction)postData:(id)sender{
    if (_IsEdit) {
        if(![self validateData]){
            UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"" message:NSLocalizedString(@"No Data To Post",nil) delegate:nil cancelButtonTitle:@"ok" otherButtonTitles:nil];
            
            [alert show];
        }else{
        
            hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
            hud.mode = MBProgressHUDModeIndeterminate;
            
      
                
                NSDateFormatter *dateFormatter=[[NSDateFormatter alloc] init];
                
                NSLocale *locale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US"];
                [dateFormatter setLocale:locale];
                
                [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss a"];
                
                NSString *currentDate=[dateFormatter stringFromDate:[NSDate date]];
                NSString *postTypeId=PostWithNoFiles;
                if ([postTXT.text isEqualToString:@"Write Your Post"]) {
                    postTXT.text=@"";
                }
            
                NSMutableDictionary *userData = [[NSMutableDictionary alloc] initWithObjectsAndKeys:@"00000000-0000-0000-0000-000000000000",@"originalPostId",
                                                 [[_PostData objectForKey:@"post"] objectForKey:@"postTypeId"],@"postTypeId",
                                                 postPrivacies,@"postPrivacies",
                                                 @"00000000-0000-0000-0000-000000000000",@"userId",
                                                 postTXT.text,@"Text",
                                                 currentDate,@"date",
                                                 [[_PostData objectForKey:@"post"] objectForKey:@"id"],@"id",
                                                 nil];
                serviceType=2;
                [webService postDataWithUrlString:EditPost withData:userData withflagLogin:YES];

        }
    }else{
        if(![self validateData]){
            UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"" message:NSLocalizedString(@"No Data To Post",nil) delegate:nil cancelButtonTitle:@"ok" otherButtonTitles:nil];
            
            [alert show];
        }else{
            hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
            hud.mode = MBProgressHUDModeIndeterminate;
            
            if (postType==0) {
                
                NSDateFormatter *dateFormatter=[[NSDateFormatter alloc] init];
                
                NSLocale *locale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US"];
                [dateFormatter setLocale:locale];
                
                [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss a"];
                
                NSString *currentDate=[dateFormatter stringFromDate:[NSDate date]];
                NSString *postTypeId=PostWithNoFiles;
                if ([postTXT.text isEqualToString:@"Write Your Post"]) {
                    postTXT.text=@"";
                }
                NSMutableDictionary *userData = [[NSMutableDictionary alloc] initWithObjectsAndKeys:@"00000000-0000-0000-0000-000000000000",@"originalPostId",
                                                 postTypeId,@"postTypeId",
                                                 postPrivacies,@"postPrivacies",
                                                 @"00000000-0000-0000-0000-000000000000",@"userId",
                                                 postTXT.text,@"Text",
                                                 currentDate,@"date",
                                                 nil];
                serviceType=2;
                [webService postDataWithUrlString:UploadPost withData:userData withflagLogin:YES];
                
            }else if(postType==2){
                NSDateFormatter *dateFormatter=[[NSDateFormatter alloc] init];
                
                NSLocale *locale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US"];
                [dateFormatter setLocale:locale];
                
                
                [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss a"];
                
                NSString *currentDate=[dateFormatter stringFromDate:[NSDate date]];
                NSString *postTypeId=PostWithLocationAndTxt;
                if ([postTXT.text isEqualToString:@"Write Your Post"]) {
                    postTXT.text=@"";
                }
                NSMutableDictionary *userData = [[NSMutableDictionary alloc] initWithObjectsAndKeys:@"00000000-0000-0000-0000-000000000000",@"originalPostId",
                                                 postTypeId,@"postTypeId",
                                                 postPrivacies,@"postPrivacies",
                                                 @"00000000-0000-0000-0000-000000000000",@"userId",
                                                 postTXT.text,@"Text",
                                                 currentDate,@"date",
                                                 [NSNumber numberWithDouble:userLatitude],@"latitude",
                                                 [NSNumber numberWithDouble:userLogitude],@"longitude",
                                                 nil];
                serviceType=2;
                [webService postDataWithUrlString:UploadPost withData:userData withflagLogin:YES];
                
            }else{
                NSMutableArray *imageObject=[[NSMutableArray alloc]init];
                UserData *user=[[UserData alloc]init];
                
                user.userImage=[webService encodeToBase64String:uploadImg];
                user.userImgId=PhotoPostId;
                
                NSMutableDictionary *imageData = [[NSMutableDictionary alloc] initWithObjectsAndKeys:user.userImgId,@"FileTypeId",user.userImage,@"Path",nil];
                [imageObject addObject:imageData];
                
                [webService updateUserImagewithimageobject:imageObject];
            }
        }
    }
}
-(IBAction)uploadImg:(id)sender{
    [postTXT resignFirstResponder];
    UIActionSheet *actionsh=[[UIActionSheet alloc]initWithTitle:@"" delegate:self cancelButtonTitle:@"cancel" destructiveButtonTitle:nil otherButtonTitles:@"Camera",@"Gallery", nil];
    
    [actionsh showInView:self.view];
    
}

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex==0) {
        
        UIImagePickerController *picker = [[UIImagePickerController alloc] init];
        picker.delegate = self;
        picker.allowsEditing = YES;
        picker.sourceType = UIImagePickerControllerSourceTypeCamera;
        
        [self presentViewController:picker animated:YES completion:NULL];
    }else{
        UIImagePickerController *picker = [[UIImagePickerController alloc] init];
        picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
        picker.delegate = self;
        [self presentViewController:picker animated:YES completion:nil];
    }
}

- (void)imagePickerController:(UIImagePickerController *)picker
        didFinishPickingImage:(UIImage *)image
                  editingInfo:(NSDictionary *)editingInfo
{
    postType=1;
    uploadImg=image;
    NSLog(@"%@",image);
    
    [self dismissViewControllerAnimated:YES completion:nil];
    CGRect frame=postTXT.frame;
    frame.size.height=120;
    postTXT.frame=frame;
    [self moveBannerOffScreen];
    UIImageView *img=[[UIImageView alloc]initWithFrame:CGRectMake(0,postTXT.frame.origin.y+postTXT.frame.size.height,self.view.frame.size.width,self.view.frame.size.height-keyboardHeight-postTXT.frame.origin.y-postTXT.frame.size.height-40)];
    img.image=uploadImg;
    [self.view addSubview:img];
    if (self.seeThis) {
        [self uploadLocation:nil];
    }
    
}

- (void)moveBannerOffScreen {
    [self.view layoutIfNeeded];
    _posttxtHeightConstraint.constant =90;
    [UIView animateWithDuration:5
                     animations:^{
                         [self.view layoutIfNeeded]; // Called on parent view
                     }];
}

-(IBAction)uploadLocation:(id)sender{
    [postTXT resignFirstResponder];
    if (self.seeThis) {
        hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        hud.mode = MBProgressHUDModeIndeterminate;
        
        mapView=[[MKMapView alloc]init];
        mapView.frame=CGRectMake(0,postTXT.frame.origin.y+postTXT.frame.size.height,self.view.frame.size.width,self.view.frame.size.height-keyboardHeight-postTXT.frame.origin.y-postTXT.frame.size.height-40);
        [self.view addSubview:mapView];
        
        mapView.showsUserLocation = YES;
        mapView.showsBuildings = YES;
        [mapView setDelegate:self];
        self.locatioManager = [CLLocationManager new];
        if ([self.locatioManager respondsToSelector:@selector(requestWhenInUseAuthorization)]) {
            [self.locatioManager requestWhenInUseAuthorization];
        }
        [self.locatioManager startUpdatingLocation];
    }else{
        SPGooglePlacesAutocompleteViewController *placesView=[self.storyboard instantiateViewControllerWithIdentifier:@"AutocompleteController"];
        placesView.myDelegate=self;
        [self.navigationController pushViewController:placesView animated:YES];
    }
    
    
}
- (void)secondViewControllerDismissed:(NSDictionary *)locationData
{
    postType=2;
    userLatitude=[[locationData objectForKey:@"latitude"] doubleValue];
    userLogitude=[[locationData objectForKey:@"longitude"] doubleValue];
    address=[locationData objectForKey:@"address"];
    
    mapView=[[MKMapView alloc]init];
    mapView.frame=CGRectMake(0,postTXT.frame.origin.y+postTXT.frame.size.height,self.view.frame.size.width,self.view.frame.size.height-keyboardHeight-postTXT.frame.origin.y-postTXT.frame.size.height-40);
    [self.view addSubview:mapView];
    
    mapView.showsUserLocation = YES;
    mapView.showsBuildings = YES;
    
    self.locatioManager = [CLLocationManager new];
    if ([self.locatioManager respondsToSelector:@selector(requestWhenInUseAuthorization)]) {
        [self.locatioManager requestWhenInUseAuthorization];
    }
    [self postLocation];
    
}
-(void)postLocation{
    
    
    CLLocationCoordinate2D coordinate;
    coordinate.latitude = userLatitude;
    coordinate.longitude = userLogitude;
    
    MyLocation *annotation = [[MyLocation alloc] initWithName:address coordinate:coordinate] ;
    
    [mapView addAnnotation:annotation];
    
    
    
    [mapView setCenterCoordinate:coordinate animated:YES];
    MKCoordinateSpan span =
    { .longitudeDelta = mapView.region.span.longitudeDelta / 2,
        .latitudeDelta  =mapView.region.span.latitudeDelta  / 2 };
    
    // Create a new MKMapRegion with the new span, using the center we want.
    MKCoordinateRegion region = { .center = coordinate, .span = span };
    [mapView setRegion:region animated:YES];
}
-(IBAction)uploadVedio:(id)sender{
    [postTXT resignFirstResponder];
}


-(void)mapView:(MKMapView *)mapView didUpdateUserLocation:(MKUserLocation *)userLocation {
    
    MKMapCamera *camera = [MKMapCamera cameraLookingAtCenterCoordinate:userLocation.coordinate fromEyeCoordinate:CLLocationCoordinate2DMake(userLocation.coordinate.latitude, userLocation.coordinate.longitude) eyeAltitude:1000];
    [mapView setCamera:camera animated:YES];
    
    userLatitude=[userLocation coordinate].latitude;
    userLogitude=[userLocation coordinate].longitude;
    
    
    
    
    
    NSLog(@"latitude:%f",userLatitude);
    NSLog(@"longitude:%f",userLogitude);
    
    
    postType=2;
    // Get Locations From Service
    if (uploadImg) {
        postType=3;
        [hud setHidden:YES];
        [mapView setHidden:YES];
        
    }
}


#pragma Mark -  Add Annotations for Locations From Service

//This is the method that gets called for every annotation you added to the map
- (MKAnnotationView *)mapView:(MKMapView *)mapView viewForAnnotation:(id <MKAnnotation>)annotation {
    static NSString *identifier = @"MyLocation";
    if ([annotation isKindOfClass:[MyLocation class]]) {
        
        MKAnnotationView *annotationView = (MKAnnotationView *) [mapView dequeueReusableAnnotationViewWithIdentifier:identifier];
        if (annotationView == nil) {
            annotationView = [[MKAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:identifier];
            annotationView.enabled = YES;
            annotationView.canShowCallout = YES;
            annotationView.image = [UIImage imageNamed:@"arrest.png"];//here we use a nice image instead of the default pins
            annotationView.rightCalloutAccessoryView = [UIButton buttonWithType:UIButtonTypeDetailDisclosure];
            
        } else {
            annotationView.annotation = annotation;
        }
        
        return annotationView;
    }
    
    return nil;
}
- (void)mapView:(MKMapView *)mapView didSelectAnnotationView:(MKAnnotationView *)view {
    NSLog(@"Latitude: %f", view.annotation.coordinate.latitude);
    NSLog(@"Longitude: %f", view.annotation.coordinate.longitude);
}


- (void)mapView:(MKMapView *)mapView annotationView:(MKAnnotationView *)view calloutAccessoryControlTapped:(UIControl *)control {
    
    NSLog(@"%@",view.annotation.title);
    NSLog(@"%@", mapView.selectedAnnotations);
    
    
}

-(void)userImagesWebserviceCallDidSuccessWithRespone:(NSArray *)imageID{
    NSLog(@"%@",imageID);
    
    NSString *imageid=[imageID objectAtIndex:0];
    imageid=[imageid stringByReplacingOccurrencesOfString:@"\"" withString:@""];
    
    NSDateFormatter *dateFormatter=[[NSDateFormatter alloc] init];
    NSLocale *locale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US"];
    [dateFormatter setLocale:locale];
    
    
    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss a"];
    
    NSString *currentDate=[dateFormatter stringFromDate:[NSDate date]];
    
    NSMutableArray *postFile=[[NSMutableArray alloc]init];
    [postFile addObject:[NSDictionary dictionaryWithObjectsAndKeys:@"00000000-0000-0000-0000-000000000000",@"postId",imageid,@"fileId", nil]];
    
    NSString *postTypeId;
    NSMutableDictionary *userData;
    if (postType==3){
        if (self.seeThis) {
            postTypeId=PostSeeThis;
        }else {
            postTypeId=PostWithLocationAndImage;
        }
        
        userData = [[NSMutableDictionary alloc] initWithObjectsAndKeys:@"00000000-0000-0000-0000-000000000000",@"originalPostId",
                    postTypeId,@"postTypeId",
                    postPrivacies,@"postPrivacies",
                    @"00000000-0000-0000-0000-000000000000",@"userId",
                    postTXT.text,@"Text",
                    currentDate,@"date",
                    postFile,@"postFiles",
                    [NSNumber numberWithDouble:userLatitude],@"latitude",
                    [NSNumber numberWithDouble:userLogitude],@"longitude",
                    nil];
    }else{
        postTypeId=PostWithImage;
        if ([postTXT.text isEqualToString:@"Write Your Post"]) {
            postTXT.text=@"";
        }
        
        userData = [[NSMutableDictionary alloc] initWithObjectsAndKeys:@"00000000-0000-0000-0000-000000000000",@"originalPostId",
                    postTypeId,@"postTypeId",
                    postPrivacies,@"postPrivacies",
                    @"00000000-0000-0000-0000-000000000000",@"userId",
                    postTXT.text,@"Text",
                    currentDate,@"date",
                    postFile,@"postFiles",
                    nil];
    }
    
    
    serviceType=2;
    [webService postDataWithUrlString:UploadPost withData:userData withflagLogin:YES];
}
-(void)userWebserviceCallDidSuccessWithRespone:(NSMutableArray *)response{
    [hud setHidden:YES];
    if (serviceType==1) {
        groupList=[[NSMutableArray alloc]init];
        allGroupList=[[NSMutableArray alloc]init];
        memberList=[[NSMutableArray alloc]init];
        allMemberList=[[NSMutableArray alloc]init];
        postPrivacies=[[NSMutableArray alloc]init];
        
        for (int i=0; i<response.count; i++) {
            BOOL isuser=[[[response objectAtIndex:i]objectForKey:@"isUser"] boolValue];
            if (isuser) {
                [memberList addObject:[response objectAtIndex:i]];
                [allMemberList addObject:[response objectAtIndex:i]];
            }else{
                [groupList addObject:[response objectAtIndex:i]];
                [allGroupList addObject:[response objectAtIndex:i]];
                
            }
        }
        [self groupAndMemberList];
        
    }else if (serviceType==3){
        
    }else{
        [self.navigationController popViewControllerAnimated:YES];
    }
    
    
}
-(void)userWebserviceCallDidFaildwithError:(NSError *)error andMessage:(NSString *)message{
    [hud setHidden:YES];
    NSLog(@"%@",error.localizedDescription);
    if ([message isEqualToString:@"401" ]) {
        UIStoryboard *storyboard=[UIStoryboard storyboardWithName:@"Main" bundle:nil];
        UIViewController* viewController=[storyboard instantiateViewControllerWithIdentifier:@"LoginController"];
        [self presentViewController:viewController animated:YES completion:nil];
    }
    
}

-(BOOL)validateData
{
    if (postType==0) {
        if ((![Validation validateusernamewithName:postTXT.text])||([postTXT.text isEqualToString:@"Write Your Post"])) {
            return false;
        }else{
            return true;
        }
    }else{
        return true;
    }
    
}
#pragma mark - Array List

- (NSArray *)list
{
    NSMutableArray *activ=[[NSMutableArray alloc]init];
    if (privacyType==2) {
        for (int i=0; i<groupList.count;i++) {
            
            [activ addObject:[[groupList objectAtIndex:i] objectForKey:@"name"]];
        }
    }else if(privacyType==3){
        for (int i=0; i<memberList.count;i++) {
            
            [activ addObject:[[memberList objectAtIndex:i] objectForKey:@"name"]];
        }
    }
    
    return activ;
}

#pragma mark - LPPopupListViewDelegate

- (void)popupListView:(LPPopupListView *)popUpListView didSelectIndex:(NSInteger)index
{
    NSLog(@"popUpListView - didSelectIndex: %d", index);
}

- (void)popupListViewDidHide:(LPPopupListView *)popUpListView selectedIndexes:(NSIndexSet *)selectedIndexes
{
    
    if ([selectedIndexes count]==0) {
        
    }else{
        postPrivacies=[[NSMutableArray alloc]init];
        NSLog(@"popupListViewDidHide - selectedIndexes: %@", selectedIndexes.description);
        self.selectedIndexes = [[NSMutableIndexSet alloc] initWithIndexSet:selectedIndexes];
        //[selectedIndexes get]
        [selectedIndexes enumerateIndexesUsingBlock:^(NSUInteger idx, BOOL *stop) {
            
            if (privacyType==2) {
                [postPrivacies addObject:[NSDictionary dictionaryWithObjectsAndKeys:[[groupList objectAtIndex:idx]objectForKey:@"id"],@"groupId",@"",@"userId",@"00000000-0000-0000-0000-000000000000",@"postId", nil]];
            }else{
                [postPrivacies addObject:[NSDictionary dictionaryWithObjectsAndKeys:[[memberList objectAtIndex:idx]objectForKey:@"id"],@"userId",@"",@"groupId",@"00000000-0000-0000-0000-000000000000",@"postId", nil]];
            }
            
        }];
        
        
        
        
    }
    
}


#pragma Mark - Search in table
- (void)searchBarTextDidBeginEditing:(UISearchBar *)searchBar
{
    //  [self.view addGestureRecognizer:tap];
    [searchBar setShowsCancelButton:YES animated:YES];
}
- (void)searchBarTextDidEndEditing:(UISearchBar *)searchBar
{
    [searchBar setShowsCancelButton:NO animated:YES];
    
}
- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText
{
    
    if (searchText.length>3) {
        searchText = [searchText stringByReplacingOccurrencesOfString:@" " withString:@""];
        NSPredicate *resultPredicate = [NSPredicate predicateWithFormat:@"name contains[c] %@", searchText];
        if (privacyType==2) {
            groupList = [[groupList filteredArrayUsingPredicate:resultPredicate]mutableCopy];
        }else{
            memberList = [[memberList filteredArrayUsingPredicate:resultPredicate]mutableCopy];
        }
        listView.arrayList=[self list];
        [listView.tableView reloadData];
    }else{
        if (privacyType==2) {
            groupList=allGroupList;
        }else{
            memberList=allMemberList;
        }
        
        listView.arrayList=[self list];
        NSLog(@"%@",listView.arrayList);
        [listView.tableView reloadData];
    }
}

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar
{
    // [self.view removeGestureRecognizer:tap];
    [searchBar resignFirstResponder];
    
}

- (void)searchBarCancelButtonClicked:(UISearchBar *)searchBar
{
    [searchBar setShowsCancelButton:NO animated:YES];
    searchBar.text=@"";
    // fetcharr=locationArray;
    
}

#pragma End- Search in table

- (void)textViewDidBeginEditing:(UITextView *)textView{
    if ([textView.text isEqualToString:@"Write Your Post"]) {
        textView.text=@"";
    }
    textView.textColor=[UIColor blackColor];
    keyborad=YES;
    [self animateTextView:textView up:YES];
}
-(void)textViewDidEndEditing:(UITextView *)textView{
    keyborad=NO;
    [self animateTextView:textView up:NO];
}

- (void) animateTextView: (UITextView*) textView up: (BOOL) up
{
    
    if (keyboardHeight==0) {
        
    }else{
        const int movementDistance =  keyboardHeight; // tweak as needed
        const float movementDuration = 0.1f; // tweak as needed
        
        int movement = (up ? -movementDistance : movementDistance);
        
        [UIView beginAnimations: @"anim" context: nil];
        [UIView setAnimationBeginsFromCurrentState: YES];
        [UIView setAnimationDuration: movementDuration];
        UploadsView.frame = CGRectOffset(UploadsView.frame, 0, movement);
        [UIView commitAnimations];
    }
}



- (void)keyboardWasShown:(NSNotification *)notification
{
    
    // Get the size of the keyboard.
    CGSize keyboardSize = [[[notification userInfo] objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    if(keyboardHeight==0){
        //Given size may not account for screen rotation
        keyboardHeight = MIN(keyboardSize.height,keyboardSize.width);
        
        const int movementDistance =  keyboardHeight; // tweak as needed
        const float movementDuration = 0.1f; // tweak as needed
        
        int movement = (keyborad ? -movementDistance : movementDistance);
        
        [UIView beginAnimations: @"anim" context: nil];
        [UIView setAnimationBeginsFromCurrentState: YES];
        [UIView setAnimationDuration: movementDuration];
        UploadsView.frame = CGRectOffset(UploadsView.frame, 0, movement);
        [UIView commitAnimations];
    }
    
    //your other code here..........
}
//If you want the keyboard to hide when you press the return
- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    return YES;
}




- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
