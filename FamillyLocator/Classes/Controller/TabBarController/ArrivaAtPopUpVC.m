//
//  ArrivaAtPopUpVC.m
//  wiizme
//
//  Created by abdelrhman gamil on 7/5/17.
//  Copyright © 2017 Amira. All rights reserved.
//

#import "ArrivaAtPopUpVC.h"
#import "Constant.h"
#import "AppDelegate.h"

@interface ArrivaAtPopUpVC ()

@end

@implementation ArrivaAtPopUpVC

@synthesize GroupID,UserID;

- (void)viewDidLoad {
    [super viewDidLoad];
    MessageLbl.delegate =self;
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(StouchesBegan)];
    
    [self.view addGestureRecognizer:tapGesture];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(IBAction)Send:(id)sender{

    [self ArriveAtPost:GroupID];
}

-(IBAction)Close:(id)sender{
    
    [self.view removeFromSuperview];
}

-(void)ArriveAtPost:(NSString * )groupID {
    
    webService=[[WebServiceFunc alloc]init];
    
    [webService setDelegate:self];
    
    
    
    NSDateFormatter *dateFormatter=[[NSDateFormatter alloc] init];
    
    NSLocale *locale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US"];
    
    [dateFormatter setLocale:locale];
    
    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss a"];
    
    NSMutableArray *postPrivacies=[[NSMutableArray alloc]init];
    
    [postPrivacies addObject:[NSDictionary dictionaryWithObjectsAndKeys:groupID,@"groupId",@"",@"userId",@"00000000-0000-0000-0000-000000000000",@"postId", nil]];
    
    NSString *currentDate=[dateFormatter stringFromDate:[NSDate date]];
    
    NSString *postTypeId=PostWithLocationAndTxt;
    
    double userLatitude=((AppDelegate *)[[UIApplication sharedApplication] delegate]).UserOldLocation.coordinate.latitude;
    double userLogitude=((AppDelegate *)[[UIApplication sharedApplication] delegate]).UserOldLocation.coordinate.longitude;
    
    
    
    NSMutableDictionary *userData = [[NSMutableDictionary alloc] initWithObjectsAndKeys:@"00000000-0000-0000-0000-000000000000",@"originalPostId",
                                     postTypeId,@"postTypeId",
                                     postPrivacies,@"postPrivacies",
                                     @"00000000-0000-0000-0000-000000000000",@"userId",
                                     MessageLbl.text,@"Text",
                                     currentDate,@"date",
                                     [NSNumber numberWithDouble:userLatitude],@"latitude",
                                     [NSNumber numberWithDouble:userLogitude],@"longitude",
                                     nil];
    
    [webService postDataWithUrlString:UploadPost withData:userData withflagLogin:YES];
    hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    hud.mode = MBProgressHUDModeIndeterminate;
}

-(void)userWebserviceCallDidSuccessWithRespone:(NSMutableArray *)response{
    
    hud.mode = MBProgressHUDModeCustomView;
    // Set an image view with a checkmark.
    UIImage *image = [[UIImage imageNamed:@"SCheckmark"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    hud.customView = [[UIImageView alloc] initWithImage:image];
    // Looks a bit nicer if we make it square.
    hud.square = YES;
    hud.label.text = NSLocalizedString(@"Done", @"");
    [hud hideAnimated:YES afterDelay:1.f];
     [self.view removeFromSuperview];
}
-(void)userWebserviceCallDidFaildwithError:(NSError *)error andMessage:(NSString *)message{
    
  
    NSLog(@"%@",error.localizedDescription);
    if ([message isEqualToString:@"401" ]) {
        UIStoryboard *storyboard=[UIStoryboard storyboardWithName:@"Main" bundle:nil];
        UIViewController* viewController=[storyboard instantiateViewControllerWithIdentifier:@"LoginController"];
        
    }
     [self.view removeFromSuperview];
}


- (void)textViewDidBeginEditing:(UITextView *)textField
{
    textField.text=@"";
    
    [self animateTextField: textField up: YES];
}


- (void)textViewDidEndEditing:(UITextView *)textField
{
    [self animateTextField: textField up: NO];
}

- (void) animateTextField: (UITextView*) textField up: (BOOL) up
{
    
    
    const int movementDistance = 160; // tweak as needed
    const float movementDuration = 0.3f; // tweak as needed
    
    int movement = (up ? -movementDistance : movementDistance);
    
    [UIView beginAnimations: @"anim" context: nil];
    [UIView setAnimationBeginsFromCurrentState: YES];
    [UIView setAnimationDuration: movementDuration];
    self.view.frame = CGRectOffset(self.view.frame, 0, movement);
    [UIView commitAnimations];
}

//If you want the keyboard to hide when you press the return

- (BOOL)textViewShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    return YES;
}

-(void)StouchesBegan {
    
    [MessageLbl resignFirstResponder];
  
    
}


@end
