//
//  CustomAlertViewController.h
//  wiizme
//
//  Created by Amira on 1/18/17.
//  Copyright © 2017 Amira. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AsyncImageView.h"
#import "UserData.h"
@protocol PopviewDelegate <NSObject>

@required

- (void)decline:(UserData*)userData;
-(void)accept:(UserData*)userData;

@end

@interface CustomAlertViewController : UIViewController

@property(nonatomic)  IBOutlet UIView * popView;
@property(nonatomic)  IBOutlet AsyncImageView * userImg;
@property (nonatomic) IBOutlet UILabel *userName;
@property(nonatomic)  IBOutlet UILabel *Message;
@property(nonatomic)  IBOutlet UITextView *sendMsg;
@property(nonatomic)  IBOutlet UIButton * accept;
@property(nonatomic)  IBOutlet UIButton * decline;


@property (nonatomic, weak) id <PopviewDelegate> delegate;
@property (nonatomic,strong) UserData *userData;
@end
