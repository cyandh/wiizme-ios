 //
//  MapKitViewController.m
//  wiizme
//
//  Created by Amira on 12/27/16.
//  Copyright © 2016 Amira. All rights reserved.
//

#import "MapKitViewController.h"
#import "MyLocation.h"
#import "MBProgressHUD.h"
#import "Constant.h"
#import "UIViewController+RemoteNotificationData.h"
#import "District.h"
#import "GenericPinAnnotationView.h"
#import "MultiRowCalloutAnnotationView.h"
#import "UIViewController+HeaderView.h"
#import "SideMenuViewController.h"
#import "HistoryDatesViewController.h"
#import "LiveTrackingViewController.h"
@interface MapKitViewController ()
{
    double userLatitude,userLogitude;
    WebServiceFunc *webService;
    MBProgressHUD *hud;
    NSString *address;
    MyLocation *annotation;
    CLLocationCoordinate2D coordinate;
    BOOL customAnnotation;
    int serviceType;
}
@end

@implementation MapKitViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    IsLiveTraking=NO;
    // Do any additional setup after loading the view.
    
   // self.mapView.showsUserLocation = YES;
    self.mapView.showsBuildings = YES;
    
    self.locatioManager = [CLLocationManager new];
    if ([self.locatioManager respondsToSelector:@selector(requestWhenInUseAuthorization)]) {
        [self.locatioManager requestWhenInUseAuthorization];
    }
    
    if (self.fromPost) {
        [headerView setHidden:YES];
        [titleLBL setHidden:YES];
        customAnnotation=false;
        [self postLocation];
    }else{
        [headerView setHidden:NO];
        [titleLBL setHidden:NO];
        
        [memberImg setImageURL:[NSURL URLWithString:self.memberImage]];
        memberName.text=self.memberNam;
        
        customAnnotation=true;
        hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        hud.mode = MBProgressHUDModeIndeterminate;
        webService=[[WebServiceFunc alloc]init];
        [webService setDelegate:self];
        NSString *urlString;
        if (self.isGroup) {
            [historyBut setHidden:YES];
           urlString=[NSString stringWithFormat:@"%@%@&actionType=%@",LastLocationGroup,self.userID,ActionTypeLastPostion];
        }else{
            [historyBut setHidden:NO];
            urlString=[NSString stringWithFormat:@"%@%@&actionTypeId=%@",LastlocationUser,self.userID,ActionTypeLastPostion];
        }
        serviceType=1;
        [webService getData:urlString withflagLogin:YES];
    }


    NSString *imagePath =[NSString stringWithFormat:@"%@%@",PhotosUrl,@"dccd6995-208f-4113-979e-4eb592b74657"];
     NSData *imageData = [[NSData alloc] initWithContentsOfURL: [NSURL URLWithString:imagePath]];
    
    CGDataProviderRef dataProvider = CGDataProviderCreateWithCFData((__bridge CFDataRef)imageData);
    CGImageRef imageRef = CGImageCreateWithPNGDataProvider(dataProvider, NULL, NO, kCGRenderingIntentDefault);
    UIImage *image = [UIImage imageWithCGImage:imageRef scale:[[UIScreen mainScreen] scale] orientation:UIImageOrientationUp];
    
    CGDataProviderRelease(dataProvider);
    CGImageRelease(imageRef);
     

}
-(IBAction)liveTracking:(id)sender{

   
    if(!IsLiveTraking){
        titleLBL.text = @"Live Tracking";
        IsLiveTraking=YES;
    webService=[[WebServiceFunc alloc]init];
     [webService setDelegate:self];
     serviceType=6;
     hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
     hud.mode = MBProgressHUDModeIndeterminate;
     NSString *urlString=[NSString stringWithFormat:@"%@%@",RequestLiveTrackingGroup,self.groupData.groupID];
     [webService getData:urlString withflagLogin:YES];
    }

}
-(IBAction)lastposition:(id)sender{
    IsLiveTraking=NO;
     titleLBL.text = @"Last Position";
    [liveTrack.view removeFromSuperview];

}
-(void)viewDidDisappear:(BOOL)animated{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (IBAction)showMenu
{
    
    SideMenuViewController *sideMenu=[self.storyboard instantiateViewControllerWithIdentifier:@"SideMenu"];
    [self.navigationController presentViewController:sideMenu animated:YES completion:nil];
}

-(IBAction)historyAction:(id)sender{
    HistoryDatesViewController *historyView=[self.storyboard instantiateViewControllerWithIdentifier:@"HistoryDateController"];
    [historyView setMyDelegate:self];
    historyView.modalPresentationStyle = UIModalPresentationOverCurrentContext;
    [self presentViewController:historyView animated:NO completion:nil];
}



-(void)postLocation{
    for (int i=0; i< self.locations.count; i++) {
        NSNumber *latitude=[[self.locations objectAtIndex:i]objectForKey:@"lat"];
        NSNumber * longitude =[[self.locations objectAtIndex:i]objectForKey:@"lng"];
        
     //   NSString * Description =[[self.locations objectAtIndex:i]objectForKey:@"name"];

        coordinate.latitude = latitude.doubleValue;
        coordinate.longitude = longitude.doubleValue;
        
        CLLocation *whereIAm =[[CLLocation alloc]initWithLatitude:latitude.doubleValue longitude:longitude.doubleValue];
        MKReverseGeocoder *reverseGeocoder = [[MKReverseGeocoder alloc] initWithCoordinate:whereIAm.coordinate];
        reverseGeocoder.delegate = self;
        [reverseGeocoder start];
        
    }

    coordinate.latitude = [[[self.locations objectAtIndex:0]objectForKey:@"lat"] doubleValue];
    coordinate.longitude = [[[self.locations objectAtIndex:0]objectForKey:@"lng"] doubleValue];
    
    [self.mapView setCenterCoordinate:coordinate animated:YES];
    MKCoordinateSpan span =
    { .longitudeDelta = self.mapView.region.span.longitudeDelta / 2,
        .latitudeDelta  = self.mapView.region.span.latitudeDelta  / 2 };
    
    // Create a new MKMapRegion with the new span, using the center we want.
    MKCoordinateRegion region = { .center = coordinate, .span = span };
    [self.mapView setRegion:region animated:YES];
}


-(void)historyViewControllerDismissed:(NSDictionary *)dates{
    NSLog(@"%@",dates);
    if (webService==nil) {
        webService =[[WebServiceFunc alloc]init];
        [webService setDelegate:self];
        
    }
    hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    hud.mode = MBProgressHUDModeIndeterminate;
    NSString *urlString=[NSString stringWithFormat:@"%@%@&startDate=%@&endDate=%@",HistoryPosition,self.userID,[dates objectForKey:@"StartDate"],[dates objectForKey:@"EndDate"]];
    serviceType=2;
    [webService getData:urlString withflagLogin:YES];
}

-(void)userWebserviceCallDidSuccessWithRespone:(NSMutableArray *)response{
    [hud setHidden:YES];
    self.locations=[[NSMutableArray alloc]init];
    NSDictionary *dic;
    if (serviceType==1) {

    if (response.count>0) {
        for (int i=0; i<response.count; i++) {
            NSString *firstName=[[[response objectAtIndex:i] objectForKey:@"user"] objectForKey:@"firstName"];
            NSString *lastName=[[[response objectAtIndex:i] objectForKey:@"user"] objectForKey:@"familyName"];
            NSString *name=[NSString stringWithFormat:@"%@ %@",firstName,lastName];
            NSDictionary *dic=[self editDateAndTime:[[response objectAtIndex:i] objectForKey:@"lastPostionDate"] ];
            NSString *date=[NSString stringWithFormat:@"%@  %@",[dic objectForKey:@"date"],[dic objectForKey:@"time"]];
            NSString *battery;
            if ([[response objectAtIndex:i] objectForKey:@"batteryStatue"]==[NSNull null]) {
                battery=@"%0";
            }else{
                battery=[NSString stringWithFormat:@"%@ %@",@"%",[[response objectAtIndex:i] objectForKey:@"batteryStatue"]];
            }
            
            NSLog(@"lastKnowingLatitude=%@",[[response objectAtIndex:i]objectForKey:@"lastKnowingLatitude"]);
            
            NSString *lastKnowingLatitude =[[response objectAtIndex:i]objectForKey:@"lastKnowingLatitude"];
            NSString *lastKnowingLangitude = [[response objectAtIndex:i]objectForKey:@"lastKnowingLangitude"];
            
            if (![lastKnowingLatitude isKindOfClass:[NSNull class]]) {
                
            
            dic=[NSDictionary dictionaryWithObjectsAndKeys:[NSNumber numberWithDouble:[lastKnowingLatitude doubleValue]],@"lat",[NSNumber numberWithDouble:[lastKnowingLangitude doubleValue]],@"lng",name,@"name",battery,@"battery",date,@"date",[NSNumber numberWithBool:NO],@"fromSeeThis", nil];
            
            [self.locations addObject:dic];
            [_mapView addAnnotation:[District demoAnnotationFactory:dic]];
            }
        }

    }
    }else if (serviceType==2){
        [_mapView removeAnnotations:_mapView.annotations];
        
        CLLocationCoordinate2D coordinates[response.count];
        
        int coordinatesIndex = 0;
        
        for (NSDictionary * c in response) {
            double x = [[c valueForKey:@"latitude"] doubleValue];
            double y = [[c valueForKey:@"langitude"] doubleValue];
            
            CLLocationCoordinate2D coordinate;
            coordinate.latitude = x;
            coordinate.longitude = y;
            
            //Put this coordinate in the C array...
            coordinates[coordinatesIndex] = coordinate;
            
            coordinatesIndex++;
        }
        
        //C array is ready, create the polyline...
        MKPolyline *polyline = [MKPolyline polylineWithCoordinates:coordinates count:response.count];
        
        //Add the polyline to the map...
        [self.mapView addOverlay:polyline];
        
        
    }else if(serviceType==6){
        liveTrack=[self.storyboard instantiateViewControllerWithIdentifier:@"liveTrackingController"];
        liveTrack.groupData=[[GroupData alloc]init];
        liveTrack.groupData.groupID=self.groupData.groupID;
        liveTrack.groupData.profileImg=self.groupData.profileImg;
        liveTrack.groupData.groupName=self.groupData.groupName;
        liveTrack.isGroup=self.isGroup;
        
        liveTrack.locations=[[NSMutableArray alloc]init];
        liveTrack.usersId=[[NSMutableArray alloc]init];
        
        NSDictionary *dic;
        
        for (int i=0; i<response.count; i++)
        {
            NSString *firstName=[[[response objectAtIndex:i] objectForKey:@"user"] objectForKey:@"firstName"];
            NSString *lastName=[[[response objectAtIndex:i] objectForKey:@"user"] objectForKey:@"familyName"];
            NSString *name=[NSString stringWithFormat:@"%@ %@",firstName,lastName];
            NSDictionary *dic=[self editDateAndTime:[[response objectAtIndex:i] objectForKey:@"lastPostionDate"] ];
            NSString *date=[NSString stringWithFormat:@"%@  %@",[dic objectForKey:@"date"],[dic objectForKey:@"time"]];
            NSString *battery;
            if ([[response objectAtIndex:i] objectForKey:@"batteryStatue"]==[NSNull null]) {
                battery=@"%0";
            }else{
                battery=[NSString stringWithFormat:@"%@ %@",@"%",[[response objectAtIndex:i] objectForKey:@"batteryStatue"]];
            }
            NSString *lastKnowingLatitude =[[response objectAtIndex:i]objectForKey:@"lastKnowingLatitude"];
            NSString *lastKnowingLangitude = [[response objectAtIndex:i]objectForKey:@"lastKnowingLangitude"];
            
            if (![lastKnowingLatitude isKindOfClass:[NSNull class]]) {
                dic=[NSDictionary dictionaryWithObjectsAndKeys:[NSNumber numberWithDouble:[lastKnowingLatitude doubleValue]],@"lat",[NSNumber numberWithDouble:[lastKnowingLangitude doubleValue]],@"lng",name,@"name",battery,@"battery",date,@"date",[[[response objectAtIndex:i] objectForKey:@"user"] objectForKey:@"id"],@"userid",[NSNumber numberWithBool:NO],@"fromSeeThis", nil];
                [liveTrack.locations addObject:dic];
                [liveTrack.usersId addObject:[[[response objectAtIndex:i] objectForKey:@"user"] objectForKey:@"id"]];
            }
        }
        liveTrack.view.frame = self.mapView.frame;
        [self.view addSubview:liveTrack.view];
    }
}

-(MKOverlayRenderer *)mapView:(MKMapView *)mapView rendererForOverlay:(id<MKOverlay>)overlay
{
    if ([overlay isKindOfClass:[MKPolyline class]])
    {
        MKPolylineRenderer *pr = [[MKPolylineRenderer alloc] initWithPolyline:overlay];
        pr.strokeColor = [UIColor redColor];
        pr.lineWidth = 5;
        return pr;
    }
    
    return nil;
}
-(void)userWebserviceCallDidFaildwithError:(NSError *)error andMessage:(NSString *)message{
    [hud setHidden:YES];
    NSLog(@"%@",error.localizedDescription);
    if ([message isEqualToString:@"401" ]) {
        UIStoryboard *storyboard=[UIStoryboard storyboardWithName:@"Main" bundle:nil];
        UIViewController* viewController=[storyboard instantiateViewControllerWithIdentifier:@"LoginController"];
        [self presentViewController:viewController animated:YES completion:nil];
    }else if ([message isEqualToString:@"403" ]){
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Attention" message:@"You don't have permission to show last position " delegate:nil cancelButtonTitle:@"Cancel" otherButtonTitles:nil];
        
        [alert show];
    }
    
}
#pragma Mark - Current Location


- (void)reverseGeocoder:(MKReverseGeocoder *)geocoder didFindPlacemark:(MKPlacemark *)placemark {
    NSLog(@"I'm at %@", placemark.locality);
    NSLog(@"add %@",placemark.description);
    NSLog(@"add %@",placemark.name);
    NSLog(@"cs%@",placemark.thoroughfare);
    address=placemark.name;
    annotation = [[MyLocation alloc] initWithName:address coordinate:coordinate] ;
    
    [self.mapView addAnnotation:annotation];
}

#pragma Mark -  Add Annotations for Locations From Service

//This is the method that gets called for every annotation you added to the map
- (MKAnnotationView *)mapView:(MKMapView *)mapView viewForAnnotation:(id <MKAnnotation>)annotation {
    if (customAnnotation) {
        if ([annotation isKindOfClass:[MKUserLocation class]])
            return nil;
        
        if (![annotation conformsToProtocol:@protocol(MultiRowAnnotationProtocol)])
            return nil;
        
        NSObject <MultiRowAnnotationProtocol> *newAnnotation = (NSObject <MultiRowAnnotationProtocol> *)annotation;
        if (newAnnotation == _calloutAnnotation)
        {
            MultiRowCalloutAnnotationView *annotationView = (MultiRowCalloutAnnotationView *)[mapView dequeueReusableAnnotationViewWithIdentifier:MultiRowCalloutReuseIdentifier];
            if (!annotationView)
            {
                annotationView = [MultiRowCalloutAnnotationView calloutWithAnnotation:newAnnotation onCalloutAccessoryTapped:^(MultiRowCalloutCell *cell, UIControl *control, NSDictionary *userData) {
                    // This is where I usually push in a new detail view onto the navigation controller stack, using the object's ID
                    NSLog(@"Representative (%@) with ID '%@' was tapped.", cell.subtitle, userData[@"id"]);
                }];
            }
            else
            {
                annotationView.annotation = newAnnotation;
            }
            annotationView.parentAnnotationView = _selectedAnnotationView;
            annotationView.mapView = mapView;
            return annotationView;
        }
        GenericPinAnnotationView *annotationView = (GenericPinAnnotationView *)[mapView dequeueReusableAnnotationViewWithIdentifier:GenericPinReuseIdentifier];
        if (!annotationView)
        {
            annotationView = [GenericPinAnnotationView pinViewWithAnnotation:newAnnotation];
        }
        annotationView.annotation = newAnnotation;
        return annotationView;

    }else{
    static NSString *identifier = @"MyLocation";
    if ([annotation isKindOfClass:[MyLocation class]]) {
        
        MKAnnotationView *annotationView = (MKAnnotationView *) [self.mapView dequeueReusableAnnotationViewWithIdentifier:identifier];
        if (annotationView == nil) {
            annotationView = [[MKAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:identifier];
            annotationView.enabled = YES;
            annotationView.canShowCallout = YES;
            annotationView.image = [UIImage imageNamed:@"arrest.png"];//here we use a nice image instead of the default pins
          //  annotationView.rightCalloutAccessoryView = [UIButton buttonWithType:UIButtonTypeDetailDisclosure];
       //     annotationView.detailCalloutAccessoryView=[self detailViewForAnnotation:annotation];
            
            annotationView.inputAccessoryView.backgroundColor=[UIColor blackColor];
            
        } else {
            annotationView.annotation = annotation;
        }
        
        return annotationView;
    }
    
    return nil;
    }
}


-(void)mapView:(MKMapView *)mapView didUpdateUserLocation:(MKUserLocation *)userLocation {
    
//    MKMapCamera *camera = [MKMapCamera cameraLookingAtCenterCoordinate:userLocation.coordinate fromEyeCoordinate:CLLocationCoordinate2DMake(userLocation.coordinate.latitude, userLocation.coordinate.longitude) eyeAltitude:1000];
//    [mapView setCamera:camera animated:YES];
    
    userLatitude=[userLocation coordinate].latitude;
    userLogitude=[userLocation coordinate].longitude;
    
    NSLog(@"latitude:%f",userLatitude);
    NSLog(@"longitude:%f",userLogitude);
   // [self compareAnnotationsFromService];
    

    
}
- (void)mapView:(MKMapView *)mapView didSelectAnnotationView:(MKAnnotationView *)aView
{
    id<MKAnnotation> annotation = aView.annotation;
    if (!annotation || ![aView isSelected])
        return;
    if ( NO == [annotation isKindOfClass:[MultiRowCalloutCell class]] &&
        [annotation conformsToProtocol:@protocol(MultiRowAnnotationProtocol)] )
    {
        NSObject <MultiRowAnnotationProtocol> *pinAnnotation = (NSObject <MultiRowAnnotationProtocol> *)annotation;
        if (!_calloutAnnotation)
        {
            _calloutAnnotation = [[MultiRowAnnotation alloc] init];
            [_calloutAnnotation copyAttributesFromAnnotation:pinAnnotation];
            [mapView addAnnotation:_calloutAnnotation];
        }
        _selectedAnnotationView = aView;
        return;
    }
    [mapView setCenterCoordinate:annotation.coordinate animated:YES];
    _selectedAnnotationView = aView;
}

- (void)mapView:(MKMapView *)mapView didDeselectAnnotationView:(MKAnnotationView *)aView
{
    if ( NO == [aView.annotation conformsToProtocol:@protocol(MultiRowAnnotationProtocol)] )
        return;
    if ([aView.annotation isKindOfClass:[MultiRowAnnotation class]])
        return;
    GenericPinAnnotationView *pinView = (GenericPinAnnotationView *)aView;
    if (_calloutAnnotation && !pinView.preventSelectionChange)
    {
        [mapView removeAnnotation:_calloutAnnotation];
        _calloutAnnotation = nil;
    }
}





- (void)mapView:(MKMapView *)mapView annotationView:(MKAnnotationView *)view calloutAccessoryControlTapped:(UIControl *)control {
    
    NSLog(@"%@",view.annotation.title);
    NSLog(@"%@", mapView.selectedAnnotations);
    
    
}

-(void)compareAnnotationsFromService{
    
    //initialize your map view and add it to your view hierarchy - **set its delegate to self***
    CLLocationCoordinate2D coordinateArray[2];
    coordinateArray[0] = CLLocationCoordinate2DMake(userLatitude, userLogitude);
  //  coordinateArray[1] = CLLocationCoordinate2DMake(self.lat, self.lon);
    
    
    self.routeLine = [MKPolyline polylineWithCoordinates:coordinateArray count:2];
    [self.mapView setVisibleMapRect:[self.routeLine boundingMapRect]]; //If you want the route to be visible
    
    [self.mapView addOverlay:self.routeLine];
}

//-(MKOverlayRenderer *)mapView:(MKMapView *)mapView rendererForOverlay:(id<MKOverlay>)overlay {
//    
//    MKPolylineRenderer *renderer = [[MKPolylineRenderer alloc] initWithOverlay:overlay];
//    renderer.strokeColor =  [UIColor colorWithRed:47.0/255.0 green:61.0/255.0 blue:70.0/255.0 alpha:1.0f];
//    renderer.lineWidth = 4.0;
//    
//    return renderer;
//}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
