//
//  SeeThisViewController.h
//  wiizme
//
//  Created by Amira on 2/11/17.
//  Copyright © 2017 Amira. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>
#import "UserData.h"
#import "WebServiceFunc.h"
#import "LPPopupListView.h"
#import "AsyncImageView.h"
@interface SeeThisViewController : UIViewController<WebServiceProtocol,LPPopupListViewDelegate,UISearchBarDelegate,UITextViewDelegate,UIPickerViewDelegate,UIPickerViewDataSource,MKMapViewDelegate>
{
    __weak IBOutlet UIScrollView *scrollView;
    __weak IBOutlet UITextView *message;
    __weak IBOutlet MKMapView *mapView;
    __weak IBOutlet UIButton *send;
    __weak IBOutlet UIButton *category;
    __weak IBOutlet UILabel *userName;
    __weak IBOutlet AsyncImageView *userImg;
    UITapGestureRecognizer *tapGesture;
}

@property(nonatomic,retain)CLLocationManager *locatioManager;
@property (nonatomic, strong) NSMutableIndexSet *selectedIndexes;
@property (nonatomic,strong) NSString *userId;
@property (nonatomic)BOOL isGroup;
@property (nonatomic,strong)NSString *userImgID;
@property (nonatomic,strong)NSString *userNameTXT;
@end
