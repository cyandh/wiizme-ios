//
//  NotificationPostViewController.h
//  wiizme
//
//  Created by Amira on 1/22/17.
//  Copyright © 2017 Amira. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FeedsTextTableViewCell.h"
#import "FeedsViewTableViewCell.h"
#import "FeedsViewTextTableViewCell.h"
#import "WebServiceFunc.h"
@interface NotificationPostViewController :UIViewController<UITableViewDataSource,UITableViewDelegate,FeedsTextCellDelegate,FeedsViewCellDelegate,FeedsViewTextCellDelegate,WebServiceProtocol>
{
    __weak IBOutlet UITableView *tableView;
}
@property(nonatomic,strong)NSDictionary *timeLinePost;
@property(nonatomic,strong)NSString *postId;
@end
