//
//  WritePostViewController.h
//  wiizme
//
//  Created by Amira on 12/23/16.
//  Copyright © 2016 Amira. All rights reserved.
//
#import <UIKit/UIKit.h>
#import "WebServiceFunc.h"
#import "AsyncImageView.h"
#import "LPPopupListView.h"
#import "UserData.h"
#import <MapKit/MapKit.h>
#import "SPGooglePlacesAutocompleteViewController.h"


@interface WritePostViewController : UIViewController<WebServiceProtocol,LPPopupListViewDelegate,UISearchBarDelegate,UITextViewDelegate,UIPickerViewDelegate,UIPickerViewDataSource,MKMapViewDelegate,SecondDelegate,UITextViewDelegate>
{
    __weak IBOutlet AsyncImageView *userImg;
    __weak IBOutlet UILabel *userName;
    __weak IBOutlet UIButton *postBTN;
    __weak IBOutlet UITextView *postTXT;
    __weak IBOutlet UIView *UploadsView;
    
    
}

@property(nonatomic,retain)NSDictionary * PostData;
@property(nonatomic,readwrite)BOOL IsEdit;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *posttxtHeightConstraint;
@property (nonatomic, strong) NSMutableIndexSet *selectedIndexes;
@property (nonatomic,strong) NSString *userId;
@property (nonatomic)BOOL isGroup;
@property (nonatomic,strong)NSString *userImgID;
@property (nonatomic,strong)NSString *userNameTXT;
@property(nonatomic,retain)CLLocationManager *locatioManager;
@property (nonatomic)BOOL seeThis;
@end
