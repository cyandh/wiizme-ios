//
//  FeedsViewController.m
//  FamillyLocator
//
//  Created by Amira on 12/3/16.
//  Copyright © 2016 Amira. All rights reserved.
//

#import "WritePostViewController.h"
#import "FeedsViewController.h"
#import "MBProgressHUD.h"
#import "Constant.h"
#import "UIViewController+HeaderView.h"
#import "CommentsViewController.h"
#import "Animation.h"

#import <Social/Social.h>
#import "MyLocation.h"
#import "MapKitViewController.h"
#import "Helper.h"
#import "NotificationPostViewController.h"
#import "UIViewController+RemoteNotificationData.h"
#import "SeeThisViewController.h"
#import "HomeMemberViewController.h"
#import "UserData.h"
#define ROWS_PER_PAGE 10
#define TOTAL_ROWS 300
#define HEIGHT_LOADING_VIEW 44.0

@interface FeedsViewController ()
{
    FeedsTextTableViewCell *txtCell;
    FeedsViewTableViewCell *viewCell;
    FeedsViewTextTableViewCell *viewTxtCell;
    WebServiceFunc *webService;
    MBProgressHUD *hud;
    LPPopupListView *listView;
    SLComposeViewController *mySLComposerSheet;
    NSMutableArray *timeLineData,*groupList,*allGroupList,*memberList,*allMemberList,*postPrivacies;
    NSDictionary *responseData;
    int serviceType,privacyType;
    int selectedPost;
    BOOL seeThis;
    double selectLat,selectLong;
    NSString *selectedLocationName;
    id sucessObj;
}
@end

@implementation FeedsViewController

- (void)viewDidLoad {
    
    RefreshTV =[[RefreshTableview alloc] init];
    RefreshTV.delegate =self;
    [RefreshTV initialization:tableView];
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    ISRefresh = NO;
    FeedsData = [[NSMutableArray alloc] init];
    if (self.isGroup) {
        [[NSNotificationCenter defaultCenter] postNotificationName:@"RemoveBadgeFeedsNumber"
                                                            object:nil
                                                          userInfo:nil];
    }
    tableView.rowHeight =UITableViewAutomaticDimension;
    tableView.estimatedRowHeight = 375.0;
}

- (void)loadDataSource {
    _currentPage =1;
    serviceType = 1;
    ISRefresh=YES;
    [self GetTableContent:1];
}

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    [FeedsData removeAllObjects];
    [tableView reloadData];
    webService=[[WebServiceFunc alloc]init];
    [webService setDelegate:self];
    hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    hud.mode = MBProgressHUDModeIndeterminate;
    _currentPage =1;
    serviceType = 1;
    [self initialization];
    [self GetTableContent:1];
}

-(void)viewDidDisappear:(BOOL)animated{
    [RefreshTV hideLoadingView];
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

-(void)groupAndMemberList{
    
    float paddingTopBottom = 20.0f;
    float paddingLeftRight = 20.0f;
    CGPoint point = CGPointMake(paddingLeftRight, (self.navigationController.navigationBar.frame.size.height + paddingTopBottom) + paddingTopBottom);
    CGSize size = CGSizeMake((self.view.frame.size.width - (paddingLeftRight * 2)), self.view.frame.size.height - ((self.navigationController.navigationBar.frame.size.height + paddingTopBottom) + (paddingTopBottom * 2)));
    self.selectedIndexes=nil;
    listView=[[LPPopupListView alloc]initWithTitle:@"" list:[self list] selectedIndexes:self.selectedIndexes point:point size:size multipleSelection:YES disableBackgroundInteraction:NO];
    [listView.layer setShadowColor:[[UIColor darkGrayColor] CGColor]];
    [listView.layer setShadowOffset:CGSizeMake(0, 0)];
    [listView.layer setShadowRadius:20.0];
    [listView.layer setShadowOpacity:1];
    listView.clipsToBounds = NO;
    listView.layer.masksToBounds = NO;
    
    
    
    [listView.searchBar setDelegate:self];
    
    listView.delegate = self;
    
    [listView showInView:self.navigationController.view animated:YES];
}

// Called when a button is clicked. The view will be automatically dismissed after this call returns
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    
    if (buttonIndex==1) {
        privacyType=1;
        NSDateFormatter *dateFormatter=[[NSDateFormatter alloc] init];
        NSLocale *locale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US"];
        [dateFormatter setLocale:locale];
        [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss a"];
        
        NSString *currentDate=[dateFormatter stringFromDate:[NSDate date]];
        NSString *postTypeId=PostWithNoFiles;
        NSString *posttype=[[[FeedsData objectAtIndex:selectedPost] objectForKey:@"post"] objectForKey:@"postTypeId"];
        NSString *originalPOst;
        if ([posttype isEqualToString:SharePostID]) {
            originalPOst=[[[FeedsData objectAtIndex:selectedPost] objectForKey:@"post"] objectForKey:@"originalPostId"];
        }else{
            originalPOst=[[[FeedsData objectAtIndex:selectedPost] objectForKey:@"post"] objectForKey:@"id"];
        }
        NSMutableDictionary *userData = [[NSMutableDictionary alloc] initWithObjectsAndKeys:originalPOst,@"originalPostId",
                                         SharePostID,@"postTypeId",
                                         @"",@"postPrivacies",
                                         @"00000000-0000-0000-0000-000000000000",@"userId",
                                         @"",@"Text",
                                         currentDate,@"date",
                                         nil];
        serviceType=2;
        [webService postDataWithUrlString:SharePost withData:userData withflagLogin:YES];
    } else if(buttonIndex==2){
        privacyType=2;
        NSString *urlString=ListMembersAndGroups;
        serviceType=3;
        [webService getData:urlString withflagLogin:YES];
        
    }else if (buttonIndex==3){
        privacyType=3;
        NSString *urlString=ListMembersAndGroups;
        serviceType=3;
        [webService getData:urlString withflagLogin:YES];
        
    }
    
    NSLog(@"%li",buttonIndex);
    NSLog(@"%@",[alertView buttonTitleAtIndex:buttonIndex]);
}



#pragma mark - Array List

- (NSArray *)list
{
    NSMutableArray *activ=[[NSMutableArray alloc]init];
    if (privacyType==2) {
        for (int i=0; i<groupList.count;i++) {
            
            [activ addObject:[[groupList objectAtIndex:i] objectForKey:@"name"]];
        }
    }else if(privacyType==3){
        for (int i=0; i<memberList.count;i++) {
            
            [activ addObject:[[memberList objectAtIndex:i] objectForKey:@"name"]];
        }
    }
    
    return activ;
}

#pragma mark - LPPopupListViewDelegate

- (void)popupListView:(LPPopupListView *)popUpListView didSelectIndex:(NSInteger)index
{
    NSLog(@"popUpListView - didSelectIndex: %d", index);
}

- (void)popupListViewDidHide:(LPPopupListView *)popUpListView selectedIndexes:(NSIndexSet *)selectedIndexes
{
    
    if ([selectedIndexes count]==0) {
        
    }else{
        postPrivacies=[[NSMutableArray alloc]init];
        NSLog(@"popupListViewDidHide - selectedIndexes: %@", selectedIndexes.description);
        self.selectedIndexes = [[NSMutableIndexSet alloc] initWithIndexSet:selectedIndexes];
        //[selectedIndexes get]
        [selectedIndexes enumerateIndexesUsingBlock:^(NSUInteger idx, BOOL *stop) {
            
            if (privacyType==2) {
                [postPrivacies addObject:[NSDictionary dictionaryWithObjectsAndKeys:[[groupList objectAtIndex:idx]objectForKey:@"id"],@"groupId",@"",@"userId",@"00000000-0000-0000-0000-000000000000",@"postId", nil]];
            }else{
                [postPrivacies addObject:[NSDictionary dictionaryWithObjectsAndKeys:[[memberList objectAtIndex:idx]objectForKey:@"id"],@"userId",@"",@"groupId",@"00000000-0000-0000-0000-000000000000",@"postId", nil]];
            }
            
        }];
        
        
        NSDateFormatter *dateFormatter=[[NSDateFormatter alloc] init];
        NSLocale *locale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US"];
        [dateFormatter setLocale:locale];
        [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss a"];
        
        NSString *currentDate=[dateFormatter stringFromDate:[NSDate date]];
        NSString *postTypeId=PostWithNoFiles;
        NSMutableDictionary *userData = [[NSMutableDictionary alloc] initWithObjectsAndKeys:[[[FeedsData objectAtIndex:selectedPost] objectForKey:@"post"] objectForKey:@"id"],@"originalPostId",
                                         SharePostID,@"postTypeId",
                                         postPrivacies,@"postPrivacies",
                                         @"00000000-0000-0000-0000-000000000000",@"userId",
                                         @"",@"Text",
                                         currentDate,@"date",
                                         nil];
        serviceType=2;
        [webService postDataWithUrlString:SharePost withData:userData withflagLogin:YES];
        
        
        
        
    }
    
    
}


#pragma Mark - Search in table
- (void)searchBarTextDidBeginEditing:(UISearchBar *)searchBar
{
    //  [self.view addGestureRecognizer:tap];
    [searchBar setShowsCancelButton:YES animated:YES];
}
- (void)searchBarTextDidEndEditing:(UISearchBar *)searchBar
{
    [searchBar setShowsCancelButton:NO animated:YES];
    
}
- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText
{
    
    if (searchText.length>3) {
        searchText = [searchText stringByReplacingOccurrencesOfString:@" " withString:@""];
        NSPredicate *resultPredicate = [NSPredicate predicateWithFormat:@"name contains[c] %@", searchText];
        if (privacyType==2) {
            groupList = [[groupList filteredArrayUsingPredicate:resultPredicate]mutableCopy];
        }else{
            memberList = [[memberList filteredArrayUsingPredicate:resultPredicate]mutableCopy];
        }
        listView.arrayList=[self list];
        [listView.tableView reloadData];
    }else{
        if (privacyType==2) {
            groupList=allGroupList;
        }else{
            memberList=allMemberList;
        }
        
        listView.arrayList=[self list];
        NSLog(@"%@",listView.arrayList);
        [listView.tableView reloadData];
    }
}

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar
{
    // [self.view removeGestureRecognizer:tap];
    [searchBar resignFirstResponder];
    
}

- (void)searchBarCancelButtonClicked:(UISearchBar *)searchBar
{
    [searchBar setShowsCancelButton:NO animated:YES];
    searchBar.text=@"";
    // fetcharr=locationArray;
    
}

#pragma End- Search in table



#pragma TableView DataSource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    NSString *postType=[[[FeedsData objectAtIndex:indexPath.row] objectForKey:@"post"]objectForKey:@"postTypeId"];
    if ([postType isEqualToString:SharePostID]) {
        postType=[[[[FeedsData objectAtIndex:indexPath.row] objectForKey:@"post"] objectForKey:@"originalPost"] objectForKey:@"postTypeId"];
        if ([postType isEqualToString:PostWithNoFiles]) {
            return 207.0;
        }else if (([postType isEqualToString:PostWithImage])||([postType isEqualToString:PostWithLocationAndTxt]) ||([postType isEqualToString:PostNeedHep])||(([postType isEqualToString:PostSeeThis])||([postType isEqualToString:PostWithLocationAndImage]))){
            NSString *txt=[[[[FeedsData objectAtIndex:indexPath.row] objectForKey:@"post"] objectForKey:@"originalPost"] objectForKey:@"text"];
            if ((txt==nil)||([txt isEqualToString:@""])) {
                return 290.0;
                
            }else{
                return 375.0;
            }
            
        }else{
            return 207.0;
        }
        
    }else{
        if ([postType isEqualToString:PostWithNoFiles]) {
            return 207.0;
        }else if (([postType isEqualToString:PostWithImage]) || ([postType isEqualToString:PostWithLocationAndTxt])||([postType isEqualToString:PostNeedHep]) || (([postType isEqualToString:PostSeeThis])||([postType isEqualToString:PostWithLocationAndImage]))){
            NSString *txt=[[[FeedsData objectAtIndex:indexPath.row] objectForKey:@"post"] objectForKey:@"text"];
            if ((txt==nil)||([txt isEqualToString:@""])) {
                return 290.0;
                
            }else{
                return 375.0;
            }
            
        }else{
            return 207.0;
        }
    }
    
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return FeedsData.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    NSString *postType=[[[FeedsData objectAtIndex:indexPath.row] objectForKey:@"post"]objectForKey:@"postTypeId"];
    if ([postType isEqualToString:SharePostID]) {
        postType=[[[[FeedsData objectAtIndex:indexPath.row] objectForKey:@"post"] objectForKey:@"originalPost"] objectForKey:@"postTypeId"];
        if ([postType isEqualToString:PostWithNoFiles]){
            txtCell = [tableView dequeueReusableCellWithIdentifier:@"textonly"];
            [txtCell setDelegate:self];
            txtCell.index = indexPath.row;
            [txtCell LoadCell:[FeedsData objectAtIndex:indexPath.row] setTag:(int)indexPath.row];
            
            return txtCell;
        }else if (([postType isEqualToString:PostWithImage])||([postType isEqualToString:PostWithLocationAndTxt])||([postType isEqualToString:PostNeedHep])||([postType isEqualToString:PostSeeThis])||([postType isEqualToString:PostWithLocationAndImage])){
            NSString *txt=[[[FeedsData objectAtIndex:indexPath.row] objectForKey:@"post"] objectForKey:@"text"];
            if ((txt==nil)||([txt isEqualToString:@""])) {
                viewCell=[tableView dequeueReusableCellWithIdentifier:@"viewonly"];
                
                viewCell.index = indexPath.row;
                [viewCell setDelegate:self];
                
                [viewCell LoadCell:[FeedsData objectAtIndex:indexPath.row] setTag:(int)indexPath.row];
                
                return viewCell;
                
            }else{
                viewTxtCell=[tableView dequeueReusableCellWithIdentifier:@"viewwithtext"];
                [viewTxtCell setDelegate:self];
                [viewTxtCell LoadCell:[FeedsData objectAtIndex:indexPath.row] setTag:(int) indexPath.row];
                return viewTxtCell;
                
            }
        }else{
            txtCell = [tableView dequeueReusableCellWithIdentifier:@"textonly"];
            txtCell.index = indexPath.row;
            [txtCell setDelegate:self];
            
            [txtCell LoadCell:[FeedsData objectAtIndex:indexPath.row] setTag:(int)indexPath.row];
            
            return txtCell;
        }
        
    }else{
        if ([postType isEqualToString:PostWithNoFiles]){
            txtCell = [tableView dequeueReusableCellWithIdentifier:@"textonly"];
            
            txtCell.index = indexPath.row;
            [txtCell setDelegate:self];
            [txtCell LoadCell:[FeedsData objectAtIndex:indexPath.row] setTag:(int)indexPath.row];
            
            return txtCell;
        }else if (([postType isEqualToString:PostWithImage])||([postType isEqualToString:PostWithLocationAndTxt])||([postType isEqualToString:PostNeedHep])||([postType isEqualToString:PostSeeThis])||([postType isEqualToString:PostWithLocationAndImage])){
            NSString *txt=[[[FeedsData objectAtIndex:indexPath.row] objectForKey:@"post"] objectForKey:@"text"];
            if ((txt==nil)||([txt isEqualToString:@""])) {
                viewCell=[tableView dequeueReusableCellWithIdentifier:@"viewonly"];
                
                
                [viewCell setDelegate:self];
                
                [viewCell LoadCell:[FeedsData objectAtIndex:indexPath.row] setTag:(int)indexPath.row];
                return viewCell;
                
                
            }else{
                viewTxtCell=[tableView dequeueReusableCellWithIdentifier:@"viewwithtext"];
                [viewTxtCell setDelegate:self];
                viewTxtCell.index = indexPath.row;
                [viewTxtCell LoadCell:[FeedsData objectAtIndex:indexPath.row] setTag:(int)indexPath.row];
                return viewTxtCell;
            }
        }else{
            txtCell = [tableView dequeueReusableCellWithIdentifier:@"textonly"];
            [txtCell setDelegate:self];
            txtCell.index = indexPath.row;
            [txtCell LoadCell:[FeedsData objectAtIndex:indexPath.row] setTag:(int)indexPath.row];
            
            return txtCell;
            
        }
    }
}

#pragma End- TableView DataSource


#pragma Mark = Paging
- (void) initialization{
    _backgroundView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, tableView.frame.size.width, 44.0)];
    tableView.tableFooterView = _backgroundView;
    UIColor *backgroundColor = tableView.backgroundColor;
    if(backgroundColor){
        _backgroundView.backgroundColor = backgroundColor;
    }
    _loadingView = [[UIView alloc] initWithFrame:CGRectMake(0, _backgroundView.frame.size.height - HEIGHT_LOADING_VIEW, _backgroundView.frame.size.width, HEIGHT_LOADING_VIEW)];
    _loadingView.backgroundColor = [UIColor clearColor];
    [_backgroundView addSubview:_loadingView];
    [_loadingView setHidden:YES];
}

- (void) initLoadingView{
    _activitiIndicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    [_activitiIndicator setFrame:CGRectMake(tableView.frame.size.width/2-(_activitiIndicator.frame.size.width/2), 11, 30, 30)];
    [_activitiIndicator startAnimating];
    [_loadingView addSubview:_activitiIndicator];
    _loadingLabel = [[UILabel alloc] initWithFrame:CGRectMake(_activitiIndicator.frame.origin.x- _activitiIndicator.frame.size.width-20, 14, 220, 20)];
    [_loadingView addSubview:_loadingLabel];
    _loadingLabel.text = @"";
    // _loadingLabel.textAlignment=NSTextAlignmentCenter;
    [_loadingView setHidden:YES];
}

- (void) showLoadingView:(BOOL)isAnimated{
    if (!_isLoading) {
        
        
        if(_activitiIndicator == nil || _loadingLabel == nil) {
            [self initLoadingView];
        }
        
        [_loadingView setHidden:NO];
        
        _currentPage++;
        [self GetTableContent:_currentPage];
        _isLoading=YES;
    }
    // [tableView setContentOffset:CGPointMake(0, tableView.contentSize.height - tableView.bounds.size.height + HEIGHT_LOADING_VIEW -10) animated:isAnimated];
}

- (void) scrollViewDidScroll:(UIScrollView *)scrollView{
    if(_shouldLoad){
        [self showLoadingView:YES];
    }
    
    
    
}

- (void) scrollViewWillEndDragging:(UIScrollView *)scrollView withVelocity:(CGPoint)velocity targetContentOffset:(inout CGPoint *)targetContentOffset{
    NSLog(@"tableView.contentOffset.y =%f ffff = %f,",tableView.contentOffset.y, tableView.contentSize.height - tableView.bounds.size.height);
    if (tableView.contentOffset.y >= tableView.contentSize.height - tableView.bounds.size.height){
        _shouldLoad = YES;
        
    }
    else{
        _shouldLoad = NO;
    }
    
}

#pragma End = Paging

-(void)imgLocationMapView:(int)tag{
    NSString *postType=[[[FeedsData objectAtIndex:tag] objectForKey:@"post"]objectForKey:@"postTypeId"];
    if ([postType isEqualToString:SharePostID]) {
        selectLat=[[[[[FeedsData objectAtIndex:tag] objectForKey:@"post"] objectForKey:@"originalPost"]objectForKey:@"latitude"] doubleValue];
        
        selectLong =[[[[[FeedsData objectAtIndex:tag] objectForKey:@"post"] objectForKey:@"originalPost"]objectForKey:@"longitude"]doubleValue];
        
    }else{
        selectLat=[[[[FeedsData objectAtIndex:tag] objectForKey:@"post"]objectForKey:@"latitude"]doubleValue ];
        selectLong=[[[[FeedsData objectAtIndex:tag] objectForKey:@"post"]objectForKey:@"longitude"] doubleValue];
    }
    //    NSString *firstName=[[[FeedsData objectAtIndex:tag] objectForKey:@"user"] objectForKey:@"firstName"];
    //    NSString *lastName=[[[FeedsData objectAtIndex:tag] objectForKey:@"user"] objectForKey:@"familyName"];
    //    selectedLocationName=[NSString stringWithFormat:@"%@ %@",firstName,lastName];
    [self performSegueWithIdentifier:@"MapView" sender:nil];
}

-(void)actionButtonsDelegateWith:(int)actionType andTagNumber:(int)tagNum andLikeSelect:(BOOL)isLike andsocialNum:(int)socialNum {
    //actionType=   1 likeBut   2 CommentBut       3 shareinApp   4 socialMedia   5 image location
    
    //update data in dictionary
    if (actionType==1) {
        long likeNum=[[[FeedsData objectAtIndex:tagNum] objectForKey:@"likesNumber"] integerValue];
        [[[FeedsData objectAtIndex:tagNum] objectForKey:@"post"]setValue:[NSNumber numberWithBool:isLike] forKey:@"isLike"];
        if (isLike) {
            [[FeedsData objectAtIndex:tagNum]setValue:[NSString stringWithFormat:@"%li",likeNum+1] forKey:@"likesNumber"];
            
        }else{
            [[FeedsData objectAtIndex:tagNum]setValue:[NSString stringWithFormat:@"%li",likeNum-1] forKey:@"likesNumber"];
        }
        hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        hud.mode = MBProgressHUDModeIndeterminate;
        NSString *postid=[[[FeedsData objectAtIndex:tagNum] objectForKey:@"post"] objectForKey:@"id"];
        NSMutableDictionary *userData = [[NSMutableDictionary alloc] initWithObjectsAndKeys:@"00000000-0000-0000-0000-000000000000",@"userId",
                                         LikePostID,@"reactionTypeId",
                                         postid,@"postId",
                                         @"",@"text"
                                         ,nil];
        serviceType=2;
        [webService postDataWithUrlString:MakeReaction withData:userData withflagLogin:YES];
    }else if (actionType==2){
        selectedPost=tagNum;
        [self performSegueWithIdentifier:@"commentSegue" sender:nil];
        
    }else if (actionType==3){
        selectedPost=tagNum;
        UIAlertView * alert=[[UIAlertView alloc]initWithTitle:nil message:nil delegate:self cancelButtonTitle:@"cancel" otherButtonTitles:@"All Groups",@"Select Groups",@"Select Member", nil];
        
        [alert show];
    }else if (actionType==4){
        selectedPost=tagNum;
        [self shareLink];
    }else{
        [self imgLocationMapView:tagNum];
    }
}

-(void)shareLink{
    NSDictionary *dic=[[NSDictionary alloc]init];
    dic=[self shareContent];
    NSArray *objectsToShare ;
    NSURL *newsShareUrl;
    NSString *txt;
    if([[dic objectForKey:@"ContentTXT"] stringByReplacingOccurrencesOfString:@" " withString:@""].length==0){
        newsShareUrl=[dic objectForKey:@"ContentImg"];
        objectsToShare = @[newsShareUrl];
    }else{
        if ([dic objectForKey:@"ContentImg"]){
            
            newsShareUrl=[dic objectForKey:@"ContentImg"];
            txt=[dic objectForKey:@"ContentTXT"];
            objectsToShare = @[newsShareUrl,txt];
            
        }else{
            txt=[dic objectForKey:@"ContentTXT"];
            objectsToShare = @[txt];
        }
    }
    
    
    UIActivityViewController *activityVC = [[UIActivityViewController alloc] initWithActivityItems:objectsToShare applicationActivities:nil];
    
    NSArray *excludeActivities = @[UIActivityTypeAirDrop,
                                   UIActivityTypePrint,
                                   UIActivityTypeAssignToContact,
                                   UIActivityTypeSaveToCameraRoll,
                                   UIActivityTypeAddToReadingList,
                                   UIActivityTypePostToFlickr,
                                   UIActivityTypePostToVimeo,
                                   ];
    
    activityVC.excludedActivityTypes = excludeActivities;
    if ( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad )
    {
        activityVC.popoverPresentationController.sourceView = self.view;
    }
    [self presentViewController:activityVC animated:YES completion:nil];
}

-(NSDictionary*)shareContent {
    NSString *contentTXT;
    NSURL *contentImg;
    NSString *postType=[[[FeedsData objectAtIndex:selectedPost] objectForKey:@"post"]objectForKey:@"postTypeId"];
    if ([postType isEqualToString:SharePostID]) {
        postType=[[[[FeedsData objectAtIndex:selectedPost] objectForKey:@"post"] objectForKey:@"originalPost"] objectForKey:@"postTypeId"];
        if ([postType isEqualToString:PostWithNoFiles]){
            
            contentTXT=[[[[FeedsData objectAtIndex:selectedPost] objectForKey:@"post"] objectForKey:@"originalPost"] objectForKey:@"text"];
        }else if ([postType isEqualToString:PostWithImage]||[postType isEqualToString:PostWithLocationAndTxt]||([postType isEqualToString:PostNeedHep])||[postType isEqualToString:PostSeeThis]||[postType isEqualToString:PostWithLocationAndImage]){
            NSString *txt=[[[FeedsData objectAtIndex:selectedPost] objectForKey:@"post"] objectForKey:@"text"];
            if ((txt==nil)||([txt isEqualToString:@""])) {
                contentTXT=@"";
                
            }else{
                
                contentTXT=[[[[FeedsData objectAtIndex:selectedPost] objectForKey:@"post"] objectForKey:@"originalPost"]objectForKey:@"text"];
                
            }
            double latitude,longitude;
            if (([postType isEqualToString:PostWithLocationAndTxt])||([postType isEqualToString:PostNeedHep])) {
                
                latitude=[[[[[FeedsData objectAtIndex:selectedPost] objectForKey:@"post"] objectForKey:@"originalPost"]objectForKey:@"latitude"] doubleValue];
                longitude= [[[[[FeedsData objectAtIndex:selectedPost] objectForKey:@"post"] objectForKey:@"originalPost"] objectForKey:@"longitude"]doubleValue];
            }
            
            if (([postType isEqualToString:PostWithLocationAndTxt])||([postType isEqualToString:PostNeedHep])) {
                
                NSString *staticMapUrl = [NSString stringWithFormat:@"http://maps.google.com/maps/api/staticmap?markers=color:red|%f,%f&zoom=13&size=%ix165&sensor=true,markers=color:blue%7@%f,%f",latitude,longitude,(int)self.view.frame.size.width,@"C",latitude,longitude];
                NSURL *mapUrl = [NSURL URLWithString:[staticMapUrl stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
                contentImg=mapUrl;
            }else{
                
                if ([[FeedsData objectAtIndex:selectedPost] objectForKey:@"postFiles"] == nil || [[[FeedsData objectAtIndex:selectedPost] objectForKey:@"postFiles"] count] == 0) {
                    
                }else{
                    contentImg=[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",PhotosUrl,[[[FeedsData objectAtIndex:selectedPost] objectForKey:@"postFiles"] objectAtIndex:0]]];
                    
                }
            }
        }
    }else{
        if ([postType isEqualToString:PostWithNoFiles]){
            contentTXT=[[[FeedsData objectAtIndex:selectedPost] objectForKey:@"post"] objectForKey:@"text"];
            
        }else if ([postType isEqualToString:PostWithImage]||[postType isEqualToString:PostWithLocationAndTxt]||([postType isEqualToString:PostNeedHep])||[postType isEqualToString:PostSeeThis]||[postType isEqualToString:PostWithLocationAndImage]){
            NSString *txt=[[[FeedsData objectAtIndex:selectedPost] objectForKey:@"post"] objectForKey:@"text"];
            if ((txt==nil)||([txt isEqualToString:@""])) {
                contentTXT=@"";
                
            }else{
                
                contentTXT=[[[FeedsData objectAtIndex:selectedPost] objectForKey:@"post"] objectForKey:@"text"];
                
                
            }
            double latitude,longitude;
            if (([postType isEqualToString:PostWithLocationAndTxt])||([postType isEqualToString:PostNeedHep])) {
                
                latitude=[[[[FeedsData objectAtIndex:selectedPost] objectForKey:@"post"] objectForKey:@"latitude"] doubleValue];
                longitude= [[[[FeedsData objectAtIndex:selectedPost] objectForKey:@"post"] objectForKey:@"longitude"]doubleValue];
            }
            
            if (([postType isEqualToString:PostWithLocationAndTxt])||([postType isEqualToString:PostNeedHep])) {
                
                NSString *staticMapUrl = [NSString stringWithFormat:@"http://maps.google.com/maps/api/staticmap?markers=color:red|%f,%f&zoom=13&size=%ix165&sensor=true,markers=color:blue%7@%f,%f",latitude,longitude,(int)self.view.frame.size.width,@"C",latitude,longitude];
                NSURL *mapUrl = [NSURL URLWithString:[staticMapUrl stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
                contentImg=mapUrl;
            }else{
                
                if ([[FeedsData objectAtIndex:selectedPost] objectForKey:@"postFiles"] == nil || [[[FeedsData objectAtIndex:selectedPost] objectForKey:@"postFiles"] count] == 0) {
                    
                }else{
                    contentImg=[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",PhotosUrl,[[[FeedsData objectAtIndex:selectedPost] objectForKey:@"postFiles"] objectAtIndex:0]]];
                }
            }
            
        }    }
    NSDictionary *dic=[[NSDictionary alloc]initWithObjectsAndKeys:contentTXT,@"ContentTXT",contentImg,@"ContentImg", nil];
    return dic;
    
    
}

-(void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex {
    NSDictionary *dic=[[NSDictionary alloc]init];
    dic=[self shareContent];
    if (buttonIndex == 0) {
        
        if ([[UIDevice currentDevice].systemVersion floatValue ] >= 6.0)
        {
            if([SLComposeViewController isAvailableForServiceType:SLServiceTypeFacebook]) //check if Facebook Account is linked
            {
                //  mySLComposerSheet = [[SLComposeViewController alloc] init]; //initiate the Social Controller
                mySLComposerSheet = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeFacebook]; //Tell him with what social plattform to use it, e.g. facebook or twitter
                if([[dic objectForKey:@"ContentTXT"] stringByReplacingOccurrencesOfString:@" " withString:@""].length==0){
                    [mySLComposerSheet addURL:[dic objectForKey:@"ContentImg"]];
                    
                }else{
                    [mySLComposerSheet setInitialText:[dic objectForKey:@"ContentTXT"]];
                }
                
                
                [self presentViewController:mySLComposerSheet animated:YES completion:nil];
            }
            [mySLComposerSheet setCompletionHandler:^(SLComposeViewControllerResult result) {
                NSString *output;
                switch (result) {
                    case SLComposeViewControllerResultCancelled:
                        output = @"Action Cancelled";
                        break;
                    case SLComposeViewControllerResultDone:
                        output = @"Post Successfull";
                        break;
                    default:
                        break;
                } //check if everythink worked properly. Give out a message on the state.
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Facebook" message:output delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
                [alert show];
            }];
        }else{
            
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Facebook" message:@"Please upgrade the IOS version to 6.0 or higher" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
            [alert show];
            
        }
    } else if (buttonIndex == 1) {
        
        if ([[UIDevice currentDevice].systemVersion floatValue ] >= 6.0)
        {
            if ([SLComposeViewController isAvailableForServiceType:SLServiceTypeTwitter])
            {
                SLComposeViewController *tweetSheet = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeTwitter];
                
                
                if([[dic objectForKey:@"ContentTXT"] stringByReplacingOccurrencesOfString:@" " withString:@""].length==0){
                    [tweetSheet addURL:[dic objectForKey:@"ContentImg"]];
                    
                }else{
                    [tweetSheet setInitialText:[dic objectForKey:@"ContentTXT"]];
                }
                
                
                
                
                
                [self presentViewController:tweetSheet animated:YES completion:NULL];
            }
        }else{
            
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Twitter" message:@"Please upgrade the IOS version to 6.0 or higher " delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
            [alert show];
        }
        
        
    }
    
    
    
}

-(IBAction)writePost:(id)sender{
    
    
    
    if ([sender tag]==100) {
        seeThis=NO;
        [self performSegueWithIdentifier:@"writePost" sender:nil];
    }else{
        [self performSegueWithIdentifier:@"SeeThisPost" sender:nil];
    }
    
}

#pragma mark - Webservice Delegate

-(void)GetTableContent:(NSInteger )currentPage{
    
    NSString *urlString;
    if (self.isGroup) {
        urlString=[NSString stringWithFormat:@"%@%@&index=%lu&size=10",LoadTimeLine,self.groupId,(unsigned long)currentPage];
    }else{
        self.groupId=@"00000000-0000-0000-0000-000000000000";
        urlString=[NSString stringWithFormat:@"%@""&index=%lu&size=10",LoadTimeLine,(unsigned long) currentPage];
    }
    serviceType=1;
    [webService getData:urlString withflagLogin:YES];
}

-(void)userWebserviceCallDidSuccessWithRespone:(NSMutableArray *)response{
    [RefreshTV hideLoadingView];
    _isLoading=NO;
    [hud setHidden:YES];
    if (ISRefresh) {
        [FeedsData removeAllObjects];
        ISRefresh=NO;
    }
    if (serviceType==1) {
        if ([response isKindOfClass:[NSDictionary class]]) {
            responseData =(NSDictionary *)response;
            timeLineData=[[NSMutableArray alloc]init];
            timeLineData=[responseData objectForKey:@"posts"];
            [FeedsData addObjectsFromArray:[responseData objectForKey:@"posts"]] ;
            [tableView reloadData];
            //            [self performSelector:@selector(wait:) withObject:sucessObj afterDelay:0];
        }else{
            
        }
        //        dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0);
        //        dispatch_async(queue, ^(void) {
        //            dispatch_async(dispatch_get_main_queue(), ^{
        //        [tableView reloadData];
        //        });
        //        });
    }else if (serviceType==3){
        groupList=[[NSMutableArray alloc]init];
        allGroupList=[[NSMutableArray alloc]init];
        memberList=[[NSMutableArray alloc]init];
        allMemberList=[[NSMutableArray alloc]init];
        for (int i=0; i<response.count; i++) {
            BOOL isuser=[[[response objectAtIndex:i]objectForKey:@"isUser"] boolValue];
            if (isuser) {
                [memberList addObject:[response objectAtIndex:i]];
                [allMemberList addObject:[response objectAtIndex:i]];
            }else{
                [groupList addObject:[response objectAtIndex:i]];
                [allGroupList addObject:[response objectAtIndex:i]];
            }
        }
        [self groupAndMemberList];
    }
}

-(void)userWebserviceCallDidFaildwithError:(NSError *)error andMessage:(NSString *)message{
    [RefreshTV hideLoadingView];
    _isLoading=NO;
    [hud setHidden:YES];
    NSLog(@"%@",error.localizedDescription);
    if ([message isEqualToString:@"401" ]) {
        UIStoryboard *storyboard=[UIStoryboard storyboardWithName:@"Main" bundle:nil];
        UIViewController* viewController=[storyboard instantiateViewControllerWithIdentifier:@"LoginController"];
        [self presentViewController:viewController animated:YES completion:nil];
    }
    
}

#pragma End - Webservice Delegate

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    if ([[segue identifier] isEqualToString:@"commentSegue"]) {
        CommentsViewController *commentView=[segue destinationViewController];
        commentView.postId=[[[FeedsData objectAtIndex:selectedPost] objectForKey:@"post"] objectForKey:@"id"];
        long likeNum=[[[FeedsData objectAtIndex:selectedPost] objectForKey:@"likesNumber"] integerValue];
        commentView.likeNum=likeNum;
        
    }else if ([[segue identifier] isEqualToString:@"writePost"]){
        WritePostViewController*writeView=[segue destinationViewController];
        writeView.userId=self.groupId;
        writeView.userImgID=[responseData objectForKey:@"photoId"];
        writeView.userNameTXT=[responseData objectForKey:@"userName"];
        writeView.seeThis=seeThis;
        writeView.isGroup=self.isGroup;
    }else if ([[segue identifier] isEqualToString:@"MapView"]){
        MapKitViewController *mapView=[segue destinationViewController];
        mapView.locations=[[NSMutableArray alloc]init];
        NSDictionary *dic=[NSDictionary dictionaryWithObjectsAndKeys:[NSNumber numberWithDouble:selectLat],@"lat",[NSNumber numberWithDouble:selectLong],@"lng", nil];
        [mapView.locations addObject:dic];
        mapView.fromPost=YES;
        
    }else if ([[segue identifier] isEqualToString:@"SeeThisPost"]){
        SeeThisViewController *seeThat=[segue destinationViewController];
        seeThat.userId=self.groupId;
        seeThat.userImgID=[responseData objectForKey:@"photoId"];
        seeThat.userNameTXT=[responseData objectForKey:@"userName"];
        seeThat.isGroup=self.isGroup;
    }
}
-(void)GotoMemeberView:(NSDictionary *)SelectedUserData{
    
    HomeMemberViewController *memberView=[self.storyboard instantiateViewControllerWithIdentifier:@"HomeMemberController"];
    memberView.userData=[[UserData alloc]init];
    memberView.userData.name=[NSString stringWithFormat:@"%@ %@",[SelectedUserData objectForKey:@"firstName"],[SelectedUserData objectForKey:@"familyName"]];
    memberView.userData.userImage=[SelectedUserData objectForKey:@"photoId"];
    memberView.userData.fieldId=[SelectedUserData objectForKey:@"id"];
    memberView.fromSearch=YES;
    memberView.hasRelation=YES;
    [self.navigationController pushViewController:memberView animated:YES];
    
}

-(void)EditPostAction:(NSDictionary *)PostData{
    
    WritePostVC = [self.storyboard instantiateViewControllerWithIdentifier:@"WritePostController"];
    WritePostVC.IsEdit = YES;
    WritePostVC.PostData = PostData;
    [self.navigationController pushViewController:WritePostVC animated:YES];
    
    
}
-(void)DeletePostAction:(NSDictionary *)PostData forIndex:(NSInteger)index{
    
    serviceType=4;
    [webService getData:[NSString stringWithFormat:DeletePost,[[PostData objectForKey:@"post"] objectForKey:@"id"]] withflagLogin:YES];
    [FeedsData removeObjectAtIndex:index];//or something similar to this based on your data source array structure
    //remove the corresponding object from your data source array before this or else you will get a crash
    [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:[NSIndexPath indexPathForRow:index inSection:0]] withRowAnimation:UITableViewRowAnimationFade];
    [tableView reloadData];
}
-(void)PopUpLargeImg:(NSString *)img{
    
    TGRImageVC = [[TGRImageViewController alloc] initWithNibName:@"TGRImageViewController" bundle:nil];
    TGRImageVC.imageURL = img;
    
    [self.navigationController presentViewController:TGRImageVC animated:YES completion:nil];
}

@end
