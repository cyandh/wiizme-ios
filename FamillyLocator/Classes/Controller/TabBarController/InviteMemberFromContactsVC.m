//
//  SelectMemberOrGroupViewController.m
//  wiizme
//
//  Created by agamil on 5/12/17.
//  Copyright © 2017 Amira. All rights reserved.
//

#import "InviteMemberFromContactsVC.h"
#import "WebServiceFunc.h"
#import "GroupMembersTableViewCell.h"
#import "MBProgressHUD.h"
#import "Constant.h"
#import "GroupData.h"
#import "Animation.h"
#import "UIViewController+RemoteNotificationData.h"
#import "SideMenuViewController.h"
#import "InviteContacts.h"

@interface InviteMemberFromContactsVC ()

@end

@implementation InviteMemberFromContactsVC
@synthesize isGroup,isContact;

- (void)viewDidLoad {
    [super viewDidLoad];
    tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(StouchesBegan)];
    
    searchBar.placeholder = @"Search";
    
    
    // Do any additional setup after loading the view.
}
- (IBAction)showMenu
{
    
    SideMenuViewController *sideMenu=[self.storyboard instantiateViewControllerWithIdentifier:@"SideMenu"];
    [self.navigationController presentViewController:sideMenu animated:YES completion:nil];
}


-(IBAction)selectItem:(id)sender{
    
    
}

-(void)StouchesBegan {
    [searchBar resignFirstResponder];
    [self.view removeGestureRecognizer:tapGesture];
    
}





-(IBAction)Done:(id)sender{
    if (selectedItem!=nil) {
        [self.deleget SelectMemberOrGroupD:selectedItem];
    }
    
    
    
}

-(void)viewWillAppear:(BOOL)animated{
    
    InviteContacts * InviteContact = [[InviteContacts alloc] init];
    
    members = [InviteContact getAllContact];
    allList =[members mutableCopy];
    
}


#pragma TableView DataSource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return members.count;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    GroupMembersTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"groupmembers"];
    
    CNContact *Contact =[members objectAtIndex:indexPath.row];
    if ([Contact.givenName isEqualToString:@""]) {
        cell.memberName.text=[[Contact.emailAddresses objectAtIndex:0] valueForKey:@"value"];
    }else{
        cell.memberName.text=[NSString stringWithFormat:@"%@ %@",Contact.givenName,Contact.familyName] ;
    }
    if (selectedIndex==indexPath) {
        cell.Admin.selected = YES;
    }else{
        cell.Admin.selected = NO;
        
    }
    if (Contact.imageData==nil) {
       [cell.memberImg setImage:[UIImage imageNamed:@"user-register"]];
    }else{
    [cell.memberImg setImage:[UIImage imageWithData:Contact.imageData]];
    
    }
    
    cell.memberImg.layer.cornerRadius = 30.0f;
    cell.memberImg.clipsToBounds = YES;
    
    cell.Admin.hidden=NO;
    
    //  [cell loadNewsImagewithLabelString:videotitle andDate:videoDateFormate];
    
    return cell;
}
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    // This will create a "invisible" footer
    return 0.01f;
}


- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    return [UIView new];
    
}



#pragma TableView Delegate

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if (selectedIndex == indexPath) {
        GroupMembersTableViewCell *OldselectedCell=[tableView cellForRowAtIndexPath:selectedIndex];
        OldselectedCell.Admin.selected = NO;
        selectedItem=nil;
        selectedIndex = nil;
    }else{
        GroupMembersTableViewCell *OldselectedCell=[tableView cellForRowAtIndexPath:selectedIndex];
        OldselectedCell.Admin.selected = NO;
        selectedIndex=indexPath;
        
        GroupMembersTableViewCell *selectedCell=[tableView cellForRowAtIndexPath:indexPath];
        selectedCell.Admin.selected = YES;
        selectedItem =[members objectAtIndex:indexPath.row];
    }
}


-(void)userWebserviceCallDidSuccessWithRespone:(NSMutableArray *)response{
    [hud setHidden:YES];
    if (serviceNUM==1) {
        NSMutableArray * FilteredList = [[NSMutableArray alloc] init];
        for (int i=0; i<response.count; i++) {
            if (isGroup) {
                if (![[[response objectAtIndex:i] objectForKey:@"isUser"] boolValue]) {
                    [FilteredList addObject:[response objectAtIndex:i]];
                };
            }else{
                if ([[[response objectAtIndex:i] objectForKey:@"isUser"] boolValue]) {
                    [FilteredList addObject:[response objectAtIndex:i]];
                };
                
            }
        }
        
        members=FilteredList;
        allList = [[NSMutableArray alloc] initWithArray:[FilteredList copy]];
        [tableView reloadData];
    }else if (serviceNUM==2) {
        [self.navigationController popViewControllerAnimated:YES];
    }
    
    
}
-(void)userWebserviceCallDidFaildwithError:(NSError *)error andMessage:(NSString *)message{
    [hud setHidden:YES];
    NSLog(@"%@",error.localizedDescription);
    if ([message isEqualToString:@"401" ]) {
        UIStoryboard *storyboard=[UIStoryboard storyboardWithName:@"Main" bundle:nil];
        UIViewController* viewController=[storyboard instantiateViewControllerWithIdentifier:@"LoginController"];
        [self presentViewController:viewController animated:YES completion:nil];
    }
    
}

-(IBAction)addAdmins:(id)sender{
    UIButton *but=sender;
    memberIndx=but.tag;
    if (but.selected) {
        [but setSelected:NO];
        NSString *memid=[[members objectAtIndex:memberIndx] objectForKey:@"id"];
        for (int i=0; i<admins.count; i++) {
            NSString *insid=[[admins objectAtIndex:i] objectForKey:@"id"];
            if ([memid isEqualToString:insid]) {
                [admins removeObjectAtIndex:i];
                break;
            }
        }
    }else{
        [but setSelected:YES];
        [admins addObject:[[members objectAtIndex:memberIndx] objectForKey:@"id"]];
    }
}


#pragma Mark - Search in table
- (void)searchBarTextDidBeginEditing:(UISearchBar *)searchBar
{
    [self.view addGestureRecognizer:tapGesture];
    [searchBar setShowsCancelButton:YES animated:YES];
}
- (void)searchBarTextDidEndEditing:(UISearchBar *)searchBar
{
    [self.view removeGestureRecognizer:tapGesture];
    [searchBar setShowsCancelButton:NO animated:YES];
    
}
- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText
{
    if (searchText.length>3) {
        searchText = [searchText stringByReplacingOccurrencesOfString:@" " withString:@""];
        NSPredicate *resultPredicate = [NSPredicate predicateWithFormat:@"givenName contains[c] %@", searchText];
        members = [[members filteredArrayUsingPredicate:resultPredicate]mutableCopy];
        [tableView reloadData];
    }else{
        members=allList;
        [tableView reloadData];
    }
}


- (void)textViewDidBeginEditing:(UITextView *)textView{
    if ([textView.text isEqualToString:@"Message"]) {
        textView.text=@"";
    }
    [self animateTextView:textView up: YES];
}



- (void)textViewDidEndEditing:(UITextView *)textView
{
    [self animateTextView:textView up: NO];
}

- (void) animateTextView: (UITextView*) textView up: (BOOL) up
{
    
    
    const int movementDistance = 190; // tweak as needed
    const float movementDuration = 0.3f; // tweak as needed
    int movement = (up ? -movementDistance : movementDistance);
    [UIView beginAnimations: @"anim" context: nil];
    [UIView setAnimationBeginsFromCurrentState: YES];
    [UIView setAnimationDuration: movementDuration];
    tableView.frame = CGRectMake(tableView.frame.origin.x, tableView.frame.origin.y, tableView.frame.size.width, tableView.frame.size.height+movement);
    [UIView commitAnimations];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
