//
//  NotificationPostViewController.m
//  wiizme
//
//  Created by Amira on 1/22/17.
//  Copyright © 2017 Amira. All rights reserved.
//

#import "NotificationPostViewController.h"
#import "Constant.h"
#import "MBProgressHUD.h"
#import "UIViewController+HeaderView.h"
#import "CommentsViewController.h"
#import "Animation.h"
#import "WritePostViewController.h"
#import <Social/Social.h>
#import "MyLocation.h"
#import "MapKitViewController.h"
#import "Helper.h"
#import "UIViewController+RemoteNotificationData.h"
#import "MapKitViewController.h"
#import "CommentsViewController.h"
#import "SideMenuViewController.h"
@interface NotificationPostViewController ()
{
    FeedsTextTableViewCell *txtCell;
    FeedsViewTableViewCell *viewCell;
    FeedsViewTextTableViewCell *viewTxtCell;
    WebServiceFunc *webService;
    MBProgressHUD *hud;
    
    SLComposeViewController *mySLComposerSheet;
    NSMutableArray *postData;
    int serviceType,privacyType;
    int selectedPost;
    double selectLat,selectLong;
    NSString *selectedLocationName;
    
}
@end

@implementation NotificationPostViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    webService=[[WebServiceFunc alloc]init];
    [webService setDelegate:self];
    
    hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    hud.mode = MBProgressHUDModeIndeterminate;
    serviceType=1;
    NSString *urlString=[NSString stringWithFormat:@"%@%@",PostDataNotify,self.postId];
    [webService getData:urlString withflagLogin:YES];
    [tableView setDelegate:self];
    [tableView setDataSource:self];
    
    
    
}


-(void)viewDidDisappear:(BOOL)animated{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (IBAction)showMenu
{
    
    SideMenuViewController *sideMenu=[self.storyboard instantiateViewControllerWithIdentifier:@"SideMenu"];
    [self.navigationController presentViewController:sideMenu animated:YES completion:nil];
}

-(void)userWebserviceCallDidSuccessWithRespone:(NSMutableArray *)response{
    [hud setHidden:YES];
    if (serviceType==1) {
        if ([response isKindOfClass:[NSDictionary class]]) {
            NSDictionary *dic =(NSDictionary *)response;
            postData=[dic objectForKey:@"posts"];
        }
        [tableView reloadData];
    }else if (serviceType==3){
        
        
        
    }
    
    
}
-(void)userWebserviceCallDidFaildwithError:(NSError *)error andMessage:(NSString *)message{
    [hud setHidden:YES];
    NSLog(@"%@",error.localizedDescription);
    if ([message isEqualToString:@"401" ]) {
        UIStoryboard *storyboard=[UIStoryboard storyboardWithName:@"Main" bundle:nil];
        UIViewController* viewController=[storyboard instantiateViewControllerWithIdentifier:@"LoginController"];
        [self presentViewController:viewController animated:YES completion:nil];
    }
    
}


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    NSLog(@"%@",self.timeLinePost);
    if (indexPath.section==0){
        NSString *postType=[[[postData objectAtIndex:0]objectForKey:@"post"]objectForKey:@"postTypeId"];
        if ([postType isEqualToString:SharePostID]) {
            postType=[[[[postData objectAtIndex:0] objectForKey:@"post"] objectForKey:@"originalPost"] objectForKey:@"postTypeId"];
            if ([postType isEqualToString:PostWithNoFiles]) {
                return 194.0;
            }else if (([postType isEqualToString:PostWithImage])||([postType isEqualToString:PostWithLocationAndTxt])||([postType isEqualToString:PostNeedHep]) ||(([postType isEqualToString:PostSeeThis])||([postType isEqualToString:PostWithLocationAndImage]))){
                NSString *txt=[[[[postData objectAtIndex:0]  objectForKey:@"post"] objectForKey:@"originalPost"] objectForKey:@"text"];
                if ((txt==nil)||([txt isEqualToString:@""])) {
                    return 277.0;
                    
                }else{
                    return 362.0;
                }
                
            }else{
                return 194.0;
            }
            
        }else{
            if ([postType isEqualToString:PostWithNoFiles]) {
                return 194.0;
            }else if (([postType isEqualToString:PostWithImage]) || ([postType isEqualToString:PostWithLocationAndTxt])||([postType isEqualToString:PostNeedHep]) || (([postType isEqualToString:PostSeeThis])||([postType isEqualToString:PostWithLocationAndImage]))){
                NSString *txt=[[[postData objectAtIndex:0]  objectForKey:@"post"] objectForKey:@"text"];
                if ((txt==nil)||([txt isEqualToString:@""])) {
                    return 277.0;
                    
                }else{
                    return 362.0;
                }
                
            }else{
                return 194.0;
            }
        }}else{
            return 0;
        }
    
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return postData.count;
    
    
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    txtCell = [tableView dequeueReusableCellWithIdentifier:@"textonly"];
    viewCell=[tableView dequeueReusableCellWithIdentifier:@"viewonly"];
    viewTxtCell=[tableView dequeueReusableCellWithIdentifier:@"viewwithtext"];
    
    [txtCell setDelegate:self];
    [viewCell setDelegate:self];
    [viewTxtCell setDelegate:self];
    NSString *postType=[[[postData objectAtIndex:0]  objectForKey:@"post"]objectForKey:@"postTypeId"];
    if ([postType isEqualToString:SharePostID]) {
        postType=[[[[postData objectAtIndex:0]  objectForKey:@"post"] objectForKey:@"originalPost"] objectForKey:@"postTypeId"];
        if ([postType isEqualToString:PostWithNoFiles]||[postType isEqualToString:PostWithtxt ]||[postType isEqualToString:PostGoBackHome]){
            [txtCell LoadCell:[postData objectAtIndex:0]   setTag:(int)indexPath.row];
            return txtCell;
        }else if (([postType isEqualToString:PostWithImage])||([postType isEqualToString:PostWithLocationAndTxt])||([postType isEqualToString:PostNeedHep])||([postType isEqualToString:PostSeeThis])||([postType isEqualToString:PostWithLocationAndImage])){
            NSString *txt=[[[postData objectAtIndex:0]  objectForKey:@"post"] objectForKey:@"text"];
            if ((txt==nil)||([txt isEqualToString:@""])) {
                [viewCell LoadCell:[postData objectAtIndex:0]  setTag:(int)indexPath.row];
                return viewCell;
                
            }else{
                [viewTxtCell LoadCell:[postData objectAtIndex:0]  setTag:(int)indexPath.row];
                return viewTxtCell;
            }
        }else{
            return nil;
        }
        
    }else{
        if ([postType isEqualToString:PostWithNoFiles]||[postType isEqualToString:PostWithtxt] || [postType isEqualToString:PostIamFine]||[postType isEqualToString:PostGoBackHome]){
            [txtCell LoadCell:[postData objectAtIndex:0]  setTag:(int)indexPath.row];
            
            return txtCell;
        }else if (([postType isEqualToString:PostWithImage])||([postType isEqualToString:PostWithLocationAndTxt])||([postType isEqualToString:PostNeedHep])||([postType isEqualToString:PostSeeThis])||([postType isEqualToString:PostWithLocationAndImage])){
            NSString *txt=[[[postData objectAtIndex:0]  objectForKey:@"post"] objectForKey:@"text"];
            if ((txt==nil)||([txt isEqualToString:@""])) {
                [viewCell LoadCell:[postData objectAtIndex:0]  setTag:(int)indexPath.row];
                return viewCell;
                
                
            }else{
                [viewTxtCell LoadCell:[postData objectAtIndex:0]  setTag:(int)indexPath.row];
                return viewTxtCell;
            }
        }else{
            return nil;
        }
    }
}


-(void)imgLocationMapView:(int)tag{
    NSString *postType=[[[postData objectAtIndex:tag] objectForKey:@"post"]objectForKey:@"postTypeId"];
    if ([postType isEqualToString:SharePostID]) {
        selectLat=[[[[[postData objectAtIndex:tag] objectForKey:@"post"] objectForKey:@"originalPost"]objectForKey:@"latitude"] doubleValue];
        
        selectLong =[[[[[postData objectAtIndex:tag] objectForKey:@"post"] objectForKey:@"originalPost"]objectForKey:@"longitude"]doubleValue];
        
    }else{
        selectLat=[[[[postData objectAtIndex:tag] objectForKey:@"post"]objectForKey:@"latitude"]doubleValue ];
        selectLong=[[[[postData objectAtIndex:tag] objectForKey:@"post"]objectForKey:@"longitude"] doubleValue];
    }
    //    NSString *firstName=[[[postData objectAtIndex:tag] objectForKey:@"user"] objectForKey:@"firstName"];
    //    NSString *lastName=[[[postData objectAtIndex:tag] objectForKey:@"user"] objectForKey:@"familyName"];
    //    selectedLocationName=[NSString stringWithFormat:@"%@ %@",firstName,lastName];
    
    MapKitViewController *mapView=[self.storyboard instantiateViewControllerWithIdentifier:@"MapController"];
    mapView.locations=[[NSMutableArray alloc]init];
    NSDictionary *dic=[NSDictionary dictionaryWithObjectsAndKeys:[NSNumber numberWithDouble:selectLat],@"lat",[NSNumber numberWithDouble:selectLong],@"lng", nil];
    [mapView.locations addObject:dic];
    mapView.fromPost=YES;
    [self.navigationController pushViewController:mapView animated:YES];
}


-(void)actionButtonsDelegateWith:(int)actionType andTagNumber:(int)tagNum andLikeSelect:(BOOL)isLike andsocialNum:(int)socialNum {
    //actionType=   1 likeBut   2 CommentBut       3 shareinApp   4 socialMedia   5 image location
    
    //update data in dictionary
    if (actionType==1) {
        long likeNum=[[[postData objectAtIndex:tagNum] objectForKey:@"likesNumber"] integerValue];
        [[[postData objectAtIndex:tagNum] objectForKey:@"post"]setValue:[NSNumber numberWithBool:isLike] forKey:@"isLike"];
        if (isLike) {
            [[postData objectAtIndex:tagNum]setValue:[NSString stringWithFormat:@"%li",likeNum+1] forKey:@"likesNumber"];
            
        }else{
            [[postData objectAtIndex:tagNum]setValue:[NSString stringWithFormat:@"%li",likeNum-1] forKey:@"likesNumber"];
        }
        hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        hud.mode = MBProgressHUDModeIndeterminate;
        NSString *postid=[[[postData objectAtIndex:tagNum] objectForKey:@"post"] objectForKey:@"id"];
        NSMutableDictionary *userData = [[NSMutableDictionary alloc] initWithObjectsAndKeys:@"00000000-0000-0000-0000-000000000000",@"userId",
                                         LikePostID,@"reactionTypeId",
                                         postid,@"postId",
                                         @"",@"text"
                                         ,nil];
        serviceType=2;
        [webService postDataWithUrlString:MakeReaction withData:userData withflagLogin:YES];
    }else if (actionType==2){
        selectedPost=tagNum;
        CommentsViewController *commentView=[self.storyboard instantiateViewControllerWithIdentifier:@"CommentsView"];
        commentView.postId=[[[postData objectAtIndex:selectedPost] objectForKey:@"post"] objectForKey:@"id"];
        long likeNum=[[[postData objectAtIndex:selectedPost] objectForKey:@"likesNumber"] integerValue];
        commentView.likeNum=likeNum;
        [self.navigationController pushViewController:commentView animated:YES];
        
    }else if (actionType==3){
        selectedPost=tagNum;
        UIAlertView * alert=[[UIAlertView alloc]initWithTitle:nil message:nil delegate:self cancelButtonTitle:@"cancel" otherButtonTitles:@"All Groups",@"Select Groups",@"Select Member", nil];
        
        [alert show];
    }else if (actionType==4){
        selectedPost=tagNum;
        [self shareLink];
    }else{
        [self imgLocationMapView:tagNum];
    }
}


-(void)shareLink{
    NSDictionary *dic=[[NSDictionary alloc]init];
    dic=[self shareContent];
    NSArray *objectsToShare ;
    NSURL *newsShareUrl;
    NSString *txt;
    if([[dic objectForKey:@"ContentTXT"] stringByReplacingOccurrencesOfString:@" " withString:@""].length==0){
        newsShareUrl=[dic objectForKey:@"ContentImg"];
        objectsToShare = @[newsShareUrl];
    }else{
        if ([dic objectForKey:@"ContentImg"]){
            
            newsShareUrl=[dic objectForKey:@"ContentImg"];
            txt=[dic objectForKey:@"ContentTXT"];
            objectsToShare = @[newsShareUrl,txt];
            
        }else{
            txt=[dic objectForKey:@"ContentTXT"];
            objectsToShare = @[txt];
        }
    }
    
    
    UIActivityViewController *activityVC = [[UIActivityViewController alloc] initWithActivityItems:objectsToShare applicationActivities:nil];
    
    NSArray *excludeActivities = @[UIActivityTypeAirDrop,
                                   UIActivityTypePrint,
                                   UIActivityTypeAssignToContact,
                                   UIActivityTypeSaveToCameraRoll,
                                   UIActivityTypeAddToReadingList,
                                   UIActivityTypePostToFlickr,
                                   UIActivityTypePostToVimeo,
                                   ];
    
    activityVC.excludedActivityTypes = excludeActivities;
    if ( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad )
    {
        activityVC.popoverPresentationController.sourceView = self.view;
    }
    [self presentViewController:activityVC animated:YES completion:nil];
}

-(NSDictionary*)shareContent {
    NSString *contentTXT;
    NSURL *contentImg;
    NSString *postType=[[[postData objectAtIndex:selectedPost] objectForKey:@"post"]objectForKey:@"postTypeId"];
    if ([postType isEqualToString:SharePostID]) {
        postType=[[[[postData objectAtIndex:selectedPost] objectForKey:@"post"] objectForKey:@"originalPost"] objectForKey:@"postTypeId"];
        if ([postType isEqualToString:PostWithNoFiles]){
            
            contentTXT=[[[[postData objectAtIndex:selectedPost] objectForKey:@"post"] objectForKey:@"originalPost"] objectForKey:@"text"];
        }else if ([postType isEqualToString:PostWithImage]){
            NSString *txt=[[[postData objectAtIndex:selectedPost] objectForKey:@"post"] objectForKey:@"text"];
            if ((txt==nil)||([txt isEqualToString:@""])) {
                
                contentImg=[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",PhotosUrl,[[[postData objectAtIndex:selectedPost] objectForKey:@"postFiles"] objectAtIndex:0]]];
                
            }else{
                
                contentTXT=[[[[postData objectAtIndex:selectedPost] objectForKey:@"post"] objectForKey:@"originalPost"]objectForKey:@"text"];
                
                contentImg=[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",PhotosUrl,[[[postData objectAtIndex:selectedPost] objectForKey:@"postFiles"] objectAtIndex:0]]];
                
            }}
    }else{
        if ([postType isEqualToString:PostWithNoFiles]){
            contentTXT=[[[postData objectAtIndex:selectedPost] objectForKey:@"post"] objectForKey:@"text"];
            
        }else if ([postType isEqualToString:PostWithImage]){
            NSString *txt=[[[postData objectAtIndex:selectedPost] objectForKey:@"post"] objectForKey:@"text"];
            if ((txt==nil)||([txt isEqualToString:@""])) {
                
                contentImg=[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",PhotosUrl,[[[postData objectAtIndex:selectedPost] objectForKey:@"postFiles"] objectAtIndex:0]]];
                
            }else{
                
                contentTXT=[[[postData objectAtIndex:selectedPost] objectForKey:@"post"] objectForKey:@"text"];
                
                contentImg=[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",PhotosUrl,[[[postData objectAtIndex:selectedPost] objectForKey:@"postFiles"] objectAtIndex:0]]];
                
            }
        }    }
    NSDictionary *dic=[[NSDictionary alloc]initWithObjectsAndKeys:contentTXT,@"ContentTXT",contentImg,@"ContentImg", nil];
    return dic;
    
    
}
-(void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex {
    NSDictionary *dic=[[NSDictionary alloc]init];
    dic=[self shareContent];
    if (buttonIndex == 0) {
        
        if ([[UIDevice currentDevice].systemVersion floatValue ] >= 6.0)
        {
            if([SLComposeViewController isAvailableForServiceType:SLServiceTypeFacebook]) //check if Facebook Account is linked
            {
                //  mySLComposerSheet = [[SLComposeViewController alloc] init]; //initiate the Social Controller
                mySLComposerSheet = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeFacebook]; //Tell him with what social plattform to use it, e.g. facebook or twitter
                if([[dic objectForKey:@"ContentTXT"] stringByReplacingOccurrencesOfString:@" " withString:@""].length==0){
                    [mySLComposerSheet addURL:[dic objectForKey:@"ContentImg"]];
                    
                }else{
                    [mySLComposerSheet setInitialText:[dic objectForKey:@"ContentTXT"]];
                }
                
                
                [self presentViewController:mySLComposerSheet animated:YES completion:nil];
            }
            [mySLComposerSheet setCompletionHandler:^(SLComposeViewControllerResult result) {
                NSString *output;
                switch (result) {
                    case SLComposeViewControllerResultCancelled:
                        output = @"Action Cancelled";
                        break;
                    case SLComposeViewControllerResultDone:
                        output = @"Post Successfull";
                        break;
                    default:
                        break;
                } //check if everythink worked properly. Give out a message on the state.
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Facebook" message:output delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
                [alert show];
            }];
        }else{
            
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Facebook" message:@"Please upgrade the IOS version to 6.0 or higher" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
            [alert show];
            
        }
    } else if (buttonIndex == 1) {
        
        if ([[UIDevice currentDevice].systemVersion floatValue ] >= 6.0)
        {
            if ([SLComposeViewController isAvailableForServiceType:SLServiceTypeTwitter])
            {
                SLComposeViewController *tweetSheet = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeTwitter];
                
                
                if([[dic objectForKey:@"ContentTXT"] stringByReplacingOccurrencesOfString:@" " withString:@""].length==0){
                    [tweetSheet addURL:[dic objectForKey:@"ContentImg"]];
                    
                }else{
                    [tweetSheet setInitialText:[dic objectForKey:@"ContentTXT"]];
                }
                
                
                
                
                
                [self presentViewController:tweetSheet animated:YES completion:NULL];
            }
        }else{
            
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Twitter" message:@"Please upgrade the IOS version to 6.0 or higher " delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
            [alert show];
        }
        
        
    }
    
    
    
}




- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
