//
//  SeeThisViewController.m
//  wiizme
//
//  Created by Amira on 2/11/17.
//  Copyright © 2017 Amira. All rights reserved.
//

#import "SeeThisViewController.h"
#import "Constant.h"
#import "MBProgressHUD.h"
#import "Validation.h"
#import "Animation.h"
#import "SideMenuViewController.h"
@interface SeeThisViewController ()
{
    UIImage *uploadedImg;
    double  userLatitude,userLogitude;
    WebServiceFunc *webService;
    int serviceType,privacyType;
    MBProgressHUD *hud;
    LPPopupListView *listView;
    NSMutableArray *memberList,*allMemberList,*groupList,*allGroupList;
    NSMutableArray *postPrivacies;
    NSMutableArray *categoryList,*selectedcategoryID;
    BOOL fromCategory;
    NSString* selectedCategoryValue;
}
@end

@implementation SeeThisViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [Animation borderView:category withBorderColor:[UIColor colorWithRed:40.0/255.0f green:76.0/255.0f blue:128.0/255.0f alpha:1.0f] andWidth:1.0f];
    mapView.showsUserLocation = YES;
    mapView.showsBuildings = YES;
    [mapView setDelegate:self];
    self.locatioManager = [CLLocationManager new];
    if ([self.locatioManager respondsToSelector:@selector(requestWhenInUseAuthorization)]) {
        [self.locatioManager requestWhenInUseAuthorization];
    }
    [self.locatioManager startUpdatingLocation];
    
    userName.text=self.userNameTXT;
    NSURL *imageUrl=[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",PhotosUrl,self.userImgID]];
    [userImg setImageURL:imageUrl];
    [Animation roundCornerForView:userImg withAngle:5.0f];
    
    [message setDelegate:self];
    
    [Animation roundCornerForView:send withAngle:15.0f];
    [Animation borderView:send withBorderColor:[UIColor colorWithRed:6.0/255.0f green:188.0/255.0f blue:228.0/255.0f alpha:1.0f] andWidth:1.0f];
    postPrivacies=[[NSMutableArray alloc]init];
    tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(StouchesBegan)];
    
    [self RegisterkeyboardObservers];
    
    webService=[[WebServiceFunc alloc]init];
    [webService setDelegate:self];
    
    if (self.isGroup) {
        [postPrivacies addObject:[NSDictionary dictionaryWithObjectsAndKeys:self.userId,@"groupId",@"",@"userId",@"00000000-0000-0000-0000-000000000000",@"postId", nil]];
    }
    
}
-(void)StouchesBegan {
    
    [message resignFirstResponder];
    [mapView resignFirstResponder];
    
}

- (IBAction)showMenu
{
    
    SideMenuViewController *sideMenu=[self.storyboard instantiateViewControllerWithIdentifier:@"SideMenu"];
    [self.navigationController presentViewController:sideMenu animated:YES completion:nil];
}

-(IBAction)uploadimg:(id)sender{
    UIImagePickerController *picker = [[UIImagePickerController alloc] init];
    picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    picker.delegate = self;
    [self presentViewController:picker animated:YES completion:nil];
    
}
-(IBAction)takePhoto:(id)sender{
    UIImagePickerController *picker = [[UIImagePickerController alloc] init];
    picker.delegate = self;
    picker.allowsEditing = YES;
    picker.sourceType = UIImagePickerControllerSourceTypeCamera;
    
    [self presentViewController:picker animated:YES completion:NULL];
}

-(IBAction)categoryList:(id)sender{
    hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    hud.mode = MBProgressHUDModeIndeterminate;
    NSString *urlString=CategoryList;
    serviceType=4;
    fromCategory=YES;
    [webService getData:urlString withflagLogin:YES];
}
-(IBAction)postPrivacy:(id)sender{
    fromCategory=NO;
    
    UIAlertController * alert=   [UIAlertController
                                  alertControllerWithTitle:nil
                                  message:nil
                                  preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* allGroup = [UIAlertAction
                               actionWithTitle:@"All Groups"
                               style:UIAlertActionStyleDefault
                               handler:^(UIAlertAction * action)
                               {
                                   privacyType=1;
                                   
                                   postPrivacies=@"";
                                   [alert dismissViewControllerAnimated:YES completion:nil];
                                   
                               }];
    
    alert.view.tintColor = [UIColor grayColor];
    
    [allGroup setValue:[[UIImage imageNamed:@"all-groups.png"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal] forKey:@"image"];
    
    UIAlertAction* selectGroup = [UIAlertAction
                                  actionWithTitle:@"Select Groups"
                                  style:UIAlertActionStyleDefault
                                  handler:^(UIAlertAction * action)
                                  {
                                      privacyType=2;
                                      NSString *urlString=ListMembersAndGroups;
                                      serviceType=1;
                                      [webService getData:urlString withflagLogin:YES];
                                      [alert dismissViewControllerAnimated:YES completion:nil];
                                      
                                  }];
    
    [selectGroup setValue:[[UIImage imageNamed:@"select-group.png"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal] forKey:@"image"];
    
    UIAlertAction* selectMember = [UIAlertAction
                                   actionWithTitle:@"Select Members"
                                   style:UIAlertActionStyleDefault
                                   handler:^(UIAlertAction * action)
                                   {
                                       privacyType=3;
                                       NSString *urlString=ListMembersAndGroups;
                                       serviceType=1;
                                       [webService getData:urlString withflagLogin:YES];
                                       [alert dismissViewControllerAnimated:YES completion:nil];
                                       
                                   }];
    
    [selectMember setValue:[[UIImage imageNamed:@"select-member.png"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal] forKey:@"image"];
    
    UIAlertAction* cancel = [UIAlertAction
                             actionWithTitle:@"Cancel"
                             style:UIAlertActionStyleDefault
                             handler:^(UIAlertAction * action)
                             {
                                 
                                 [alert dismissViewControllerAnimated:YES completion:nil];
                                 
                             }];
    
    [alert addAction:allGroup];
    [alert addAction:selectGroup];
    [alert addAction:selectMember];
    [alert addAction:cancel];
    
    [self presentViewController:alert animated:YES completion:nil];
    
    
}

-(void)groupAndMemberList{
    
    float paddingTopBottom = 20.0f;
    float paddingLeftRight = 20.0f;
    
    CGPoint point = CGPointMake(paddingLeftRight, (self.navigationController.navigationBar.frame.size.height + paddingTopBottom) + paddingTopBottom);
    CGSize size = CGSizeMake((self.view.frame.size.width - (paddingLeftRight * 2)), self.view.frame.size.height - ((self.navigationController.navigationBar.frame.size.height + paddingTopBottom) + (paddingTopBottom * 2)));
    
    self.selectedIndexes=nil;
    listView=[[LPPopupListView alloc]initWithTitle:@"" list:[self list] selectedIndexes:self.selectedIndexes point:point size:size multipleSelection:YES disableBackgroundInteraction:NO];
    
    [listView.layer setShadowColor:[[UIColor darkGrayColor] CGColor]];
    [listView.layer setShadowOffset:CGSizeMake(0, 0)];
    [listView.layer setShadowRadius:20.0];
    [listView.layer setShadowOpacity:1];
    listView.clipsToBounds = NO;
    listView.layer.masksToBounds = NO;
    
    if (fromCategory) {
        listView.searchBar.hidden=YES;
    }
    
    [listView.searchBar setDelegate:self];
    
    listView.delegate = self;
    
    [listView showInView:self.navigationController.view animated:YES];
}


-(IBAction)sendData:(id)sender{
    if (![self validateData]) {
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"" message:NSLocalizedString(@"Check you location and image",nil) delegate:nil cancelButtonTitle:@"ok" otherButtonTitles:nil];
        
        [alert show];
    }else{
        
        hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        hud.mode = MBProgressHUDModeIndeterminate;
        NSMutableArray *imageObject=[[NSMutableArray alloc]init];
        UserData *user=[[UserData alloc]init];
        
        user.userImage=[webService encodeToBase64String:uploadedImg];
        user.userImgId=PhotoPostId;
        
        NSMutableDictionary *imageData = [[NSMutableDictionary alloc] initWithObjectsAndKeys:user.userImgId,@"FileTypeId",user.userImage,@"Path",nil];
        [imageObject addObject:imageData];
        
        [webService updateUserImagewithimageobject:imageObject];
    }
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    
    if (buttonIndex==1) {
        privacyType=1;
        
        postPrivacies=@"";
    } else if(buttonIndex==2){
        privacyType=2;
        NSString *urlString=ListMembersAndGroups;
        serviceType=1;
        [webService getData:urlString withflagLogin:YES];
    }else if (buttonIndex==3){
        privacyType=3;
        NSString *urlString=ListMembersAndGroups;
        serviceType=1;
        [webService getData:urlString withflagLogin:YES];
        
    }
    
    NSLog(@"%li",buttonIndex);
    NSLog(@"%@",[alertView buttonTitleAtIndex:buttonIndex]);
}
- (void)imagePickerController:(UIImagePickerController *)picker
        didFinishPickingImage:(UIImage *)image
                  editingInfo:(NSDictionary *)editingInfo
{
    
    uploadedImg=image;
    
    [self dismissViewControllerAnimated:YES completion:nil];
    
}

-(void)mapView:(MKMapView *)mapView didUpdateUserLocation:(MKUserLocation *)userLocation {
    
    MKMapCamera *camera = [MKMapCamera cameraLookingAtCenterCoordinate:userLocation.coordinate fromEyeCoordinate:CLLocationCoordinate2DMake(userLocation.coordinate.latitude, userLocation.coordinate.longitude) eyeAltitude:1000];
    [mapView setCamera:camera animated:YES];
    
    userLatitude=[userLocation coordinate].latitude;
    userLogitude=[userLocation coordinate].longitude;
    
    
    
    
    
    NSLog(@"latitude:%f",userLatitude);
    NSLog(@"longitude:%f",userLogitude);
    
    
}


-(void)userImagesWebserviceCallDidSuccessWithRespone:(NSArray *)imageID{
    NSLog(@"%@",imageID);
    
    NSString *imageid=[imageID objectAtIndex:0];
    imageid=[imageid stringByReplacingOccurrencesOfString:@"\"" withString:@""];
    
    NSDateFormatter *dateFormatter=[[NSDateFormatter alloc] init];
    NSLocale *locale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US"];
    [dateFormatter setLocale:locale];
    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss a"];
    
    NSString *currentDate=[dateFormatter stringFromDate:[NSDate date]];
    
    NSMutableArray *postFile=[[NSMutableArray alloc]init];
    [postFile addObject:[NSDictionary dictionaryWithObjectsAndKeys:@"00000000-0000-0000-0000-000000000000",@"postId",imageid,@"fileId", nil]];
    
    NSString *postTypeId;
    NSMutableDictionary *userData;
    
    
    postTypeId=PostSeeThis;
    
    
    userData = [[NSMutableDictionary alloc] initWithObjectsAndKeys:@"00000000-0000-0000-0000-000000000000",@"originalPostId",
                postTypeId,@"postTypeId",
                postPrivacies,@"postPrivacies",
                @"00000000-0000-0000-0000-000000000000",@"userId",
                message.text,@"Text",
                currentDate,@"date",
                postFile,@"postFiles",
                [NSNumber numberWithDouble:userLatitude],@"latitude",
                [NSNumber numberWithDouble:userLogitude],@"longitude",
                selectedcategoryID,@"postTags",
                nil];
    
    serviceType=2;
    [webService postDataWithUrlString:UploadPost withData:userData withflagLogin:YES];
}
-(void)userWebserviceCallDidSuccessWithRespone:(NSMutableArray *)response{
    [hud setHidden:YES];
    if (serviceType==1) {
        groupList=[[NSMutableArray alloc]init];
        allGroupList=[[NSMutableArray alloc]init];
        memberList=[[NSMutableArray alloc]init];
        allMemberList=[[NSMutableArray alloc]init];
        postPrivacies=[[NSMutableArray alloc]init];
        
        for (int i=0; i<response.count; i++) {
            BOOL isuser=[[[response objectAtIndex:i]objectForKey:@"isUser"] boolValue];
            if (isuser) {
                [memberList addObject:[response objectAtIndex:i]];
                [allMemberList addObject:[response objectAtIndex:i]];
            }else{
                [groupList addObject:[response objectAtIndex:i]];
                [allGroupList addObject:[response objectAtIndex:i]];
                
            }
        }
        if(listView==nil){
            [self groupAndMemberList];
        }else{
            listView.searchBar.hidden=NO;
            listView.arrayList=[self list];
        }
        [self groupAndMemberList];
        
    }else if (serviceType==3){
        
    }else if (serviceType==4){
        categoryList=response;
        if(listView==nil){
            [self groupAndMemberList];
        }else{
            listView.searchBar.hidden=YES;
            listView.arrayList=[self list];
              [listView showInView:self.navigationController.view animated:YES];
        }
    }else{
        [self.navigationController popViewControllerAnimated:YES];
    }
    
    
}
-(void)userWebserviceCallDidFaildwithError:(NSError *)error andMessage:(NSString *)message{
    [hud setHidden:YES];
    NSLog(@"%@",error.localizedDescription);
    if ([message isEqualToString:@"401" ]) {
        UIStoryboard *storyboard=[UIStoryboard storyboardWithName:@"Main" bundle:nil];
        UIViewController* viewController=[storyboard instantiateViewControllerWithIdentifier:@"LoginController"];
        [self presentViewController:viewController animated:YES completion:nil];
    }
    
}
-(BOOL)validateData
{
    
    if ((![Validation validateusernamewithName:message.text])||([message.text isEqualToString:@"Write Your Post"])) {
        return false;
    }else if(userLatitude==0){
        return false;
    }else if (!uploadedImg){
        return false;
    }else{
        return true;
    }
}

#pragma mark - Array List

- (NSArray *)list
{
    NSMutableArray *activ=[[NSMutableArray alloc]init];
    if (fromCategory) {
        for (int i=0; i<categoryList.count;i++) {
            [activ addObject:[[categoryList objectAtIndex:i] objectForKey:@"value"]];
        }
    }else{
        if (privacyType==2) {
            for (int i=0; i<groupList.count;i++) {
                
                [activ addObject:[[groupList objectAtIndex:i] objectForKey:@"name"]];
            }
        }else if(privacyType==3){
            for (int i=0; i<memberList.count;i++) {
                
                [activ addObject:[[memberList objectAtIndex:i] objectForKey:@"name"]];
            }
        }
    }
    
    return activ;
}

#pragma mark - LPPopupListViewDelegate

- (void)popupListView:(LPPopupListView *)popUpListView didSelectIndex:(NSInteger)index
{
    NSLog(@"popUpListView - didSelectIndex: %d", index);
}

- (void)popupListViewDidHide:(LPPopupListView *)popUpListView selectedIndexes:(NSIndexSet *)selectedIndexes
{
        
    if ([selectedIndexes count]==0) {
        [category setTitle:@"" forState:UIControlStateNormal];
        [category setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    }else{
        
        NSLog(@"popupListViewDidHide - selectedIndexes: %@", selectedIndexes.description);
        self.selectedIndexes = [[NSMutableIndexSet alloc] initWithIndexSet:selectedIndexes];
        
        
        if (fromCategory) {
            selectedcategoryID=[[NSMutableArray alloc]init];
            //[selectedIndexes get]
            selectedCategoryValue = @"";
            [selectedIndexes enumerateIndexesUsingBlock:^(NSUInteger idx, BOOL *stop) {
                selectedCategoryValue =[selectedCategoryValue stringByAppendingFormat:@"%@,", [[self list] objectAtIndex:idx]];
                [selectedcategoryID addObject:[NSDictionary dictionaryWithObjectsAndKeys:[[categoryList objectAtIndex:idx]objectForKey:@"fieldId"],@"TagId",
                                               @"00000000-0000-0000-0000-000000000000",@"postId",@"00000000-0000-0000-0000-000000000000",@"Id",nil]];
                
            }];
            
            [category setTitle:selectedCategoryValue forState:UIControlStateNormal];
            [category setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
            
            
            
        }else{
            
            
            postPrivacies=[[NSMutableArray alloc]init];
            //[selectedIndexes get]
            [selectedIndexes enumerateIndexesUsingBlock:^(NSUInteger idx, BOOL *stop) {
                
                if (privacyType==2) {
                    [postPrivacies addObject:[NSDictionary dictionaryWithObjectsAndKeys:[[groupList objectAtIndex:idx]objectForKey:@"id"],@"groupId",@"",@"userId",@"00000000-0000-0000-0000-000000000000",@"postId", nil]];
                }else{
                    [postPrivacies addObject:[NSDictionary dictionaryWithObjectsAndKeys:[[memberList objectAtIndex:idx]objectForKey:@"id"],@"userId",@"",@"groupId",@"00000000-0000-0000-0000-000000000000",@"postId", nil]];
                }
                
            }];
        }
        
        
        
        
    }
    
}


#pragma Mark - Search in table
- (void)searchBarTextDidBeginEditing:(UISearchBar *)searchBar
{
    //  [self.view addGestureRecognizer:tap];
    [searchBar setShowsCancelButton:YES animated:YES];
}
- (void)searchBarTextDidEndEditing:(UISearchBar *)searchBar
{
    [searchBar setShowsCancelButton:NO animated:YES];
    
}
- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText
{
    
    if (searchText.length>3) {
        searchText = [searchText stringByReplacingOccurrencesOfString:@" " withString:@""];
        NSPredicate *resultPredicate = [NSPredicate predicateWithFormat:@"name contains[c] %@", searchText];
        if (privacyType==2) {
            groupList = [[groupList filteredArrayUsingPredicate:resultPredicate]mutableCopy];
        }else{
            memberList = [[memberList filteredArrayUsingPredicate:resultPredicate]mutableCopy];
        }
        listView.arrayList=[self list];
        [listView.tableView reloadData];
    }else{
        if (privacyType==2) {
            groupList=allGroupList;
        }else{
            memberList=allMemberList;
        }
        
        listView.arrayList=[self list];
        NSLog(@"%@",listView.arrayList);
        [listView.tableView reloadData];
    }
}

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar
{
    // [self.view removeGestureRecognizer:tap];
    [searchBar resignFirstResponder];
    
}

- (void)searchBarCancelButtonClicked:(UISearchBar *)searchBar
{
    [searchBar setShowsCancelButton:NO animated:YES];
    searchBar.text=@"";
    // fetcharr=locationArray;
    
}

#pragma End- Search in table

- (void)textViewDidBeginEditing:(UITextView *)textView{
    if ([textView.text isEqualToString:@"Write Your Post"]) {
        textView.text=@"";
    }
    textView.textColor=[UIColor blackColor];
    
}
-(void)textViewDidEndEditing:(UITextView *)textView{
    [textView resignFirstResponder];
}

-(void)RegisterkeyboardObservers{
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillShow:)
                                                 name:UIKeyboardWillShowNotification
                                               object:nil];
    
    // Register notification when the keyboard will be hide
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillHide:)
                                                 name:UIKeyboardWillHideNotification
                                               object:nil];
    
}

- (void)keyboardWillShow:(NSNotification *)notification {
    CGRect keyboardBounds;
    [scrollView addGestureRecognizer:tapGesture];
    [[notification.userInfo valueForKey:UIKeyboardFrameBeginUserInfoKey] getValue:&keyboardBounds];
    
    
    [scrollView setContentSize:CGSizeMake(scrollView.frame.size.width, (scrollView.contentSize.height+keyboardBounds.size.height))];
    [scrollView setContentOffset:CGPointMake(scrollView.contentOffset.x,scrollView.contentOffset.y+keyboardBounds.size.height) animated:YES];
    NSLog(@"scrollView.contentSize.height=%f",scrollView.contentSize.height);
    
    // Do something with keyboard height
}

- (void)keyboardWillHide:(NSNotification *)notification {
    CGRect keyboardBounds;
    [scrollView removeGestureRecognizer:tapGesture];
    [[notification.userInfo valueForKey:UIKeyboardFrameBeginUserInfoKey] getValue:&keyboardBounds];
    [scrollView setContentSize:CGSizeMake(scrollView.frame.size.width, (scrollView.contentSize.height-keyboardBounds.size.height))];
    NSLog(@"scrollView.contentSize.height=%f",scrollView.contentSize.height);
    // Do something with keyboard height
}

//If you want the keyboard to hide when you press the return
- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    return YES;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
