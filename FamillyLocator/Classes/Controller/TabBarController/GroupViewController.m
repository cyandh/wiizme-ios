//
//  GroupViewController.m
//  FamilyLocator
//
//  Created by Amira on 10/12/16.
//  Copyright © 2016 Amira. All rights reserved.
//

#import "GroupViewController.h"
#import "GroupTableViewCell.h"
#import "UIViewController+HeaderView.h"
#import "Animation.h"
#import "MBProgressHUD.h"
#import "Constant.h"
#import "HomeGroupViewController.h"
#import "SettingGroupViewController.h"
#import "UIViewController+RemoteNotificationData.h"
@interface GroupViewController ()
{
    NSMutableArray *tableData;
    WebServiceFunc *webService;
    MBProgressHUD *hud;
    int selectedRow;
}
@end

@implementation GroupViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.


}
-(void)viewWillAppear:(BOOL)animated{
    webService=[[WebServiceFunc alloc]init];
    [webService setDelegate:self];
    hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    hud.mode = MBProgressHUDModeIndeterminate;
    
    NSString *urlString=[NSString stringWithFormat:@"%@%@",MemberGroups,@"00000000-0000-0000-0000-000000000000"];
    [webService getData:urlString withflagLogin:YES];
}
-(void)viewDidDisappear:(BOOL)animated{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}


-(void)userWebserviceCallDidSuccessWithRespone:(NSMutableArray *)response{
    [hud setHidden:YES];
    tableData=response;
    [tableView reloadData];
    
}
-(void)userWebserviceCallDidFaildwithError:(NSError *)error andMessage:(NSString *)message{
    [hud setHidden:YES];
    NSLog(@"%@",error.localizedDescription);
    if ([message isEqualToString:@"401" ]) {
        UIStoryboard *storyboard=[UIStoryboard storyboardWithName:@"Main" bundle:nil];
        UIViewController* viewController=[storyboard instantiateViewControllerWithIdentifier:@"LoginController"];
        [self presentViewController:viewController animated:YES completion:nil];
    }
    
}


#pragma TableView DataSource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return tableData.count;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    GroupTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"groupCell"];
    
    NSString *security=[[tableData objectAtIndex:indexPath.row] objectForKey:@"groupTypeId"];
    cell.groupName.text=[[tableData objectAtIndex:indexPath.row] objectForKey:@"name"];
    if ([security isEqualToString:publicPrivacy]) {
        cell.groupsecur.text=@"Public";
    }else if ([security isEqualToString:privatePrivacy]){
        cell.groupsecur.text=@"Private";
    }else{
        cell.groupsecur.text=@"VIP";
    }
    int memNUM=[[[tableData objectAtIndex:indexPath.row] objectForKey:@"groupMembersNumber"] intValue];
    if (memNUM<=1) {
        cell.memberNum.text=[NSString stringWithFormat:@"%i %@",memNUM,@"Member"];
    }else{
    cell.memberNum.text=[NSString stringWithFormat:@"%i %@",memNUM,@"Members"];
    }
    
    [cell.image setImage:[UIImage imageNamed:@"menu-profile.png"]];
    NSURL *imageUrl=[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",PhotosUrl,[[tableData objectAtIndex:indexPath.row] objectForKey:@"logoId"]]];
    [cell.image setImageURL:imageUrl];
    NSLog(@"%f",cell.image.frame.size.width);
    [Animation roundCornerForView:cell.image withAngle:30.0f];

    
  //  [cell loadNewsImagewithLabelString:videotitle andDate:videoDateFormate];
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    // This will create a "invisible" footer
    return 0.01f;
}


- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    return [UIView new];
    
}



#pragma TableView Delegate

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    selectedRow=indexPath.row;
   bool member= [[[tableData objectAtIndex:selectedRow] objectForKey:@"isMember"] boolValue];
    bool pending= [[[tableData objectAtIndex:selectedRow] objectForKey:@"isPending"] boolValue];
    if ((member)&&(!pending)) {
        [self performSegueWithIdentifier:@"groupDetails" sender:nil];
    }else{
        SettingGroupViewController *settingView=[self.storyboard instantiateViewControllerWithIdentifier:@"settingGroupController"];
        settingView.groupData=[[GroupData alloc]init];
        settingView.groupData=[self segueData];
        [self.navigationController pushViewController:settingView animated:YES];
        
    }
    
    
}

-(GroupData*)segueData{
    GroupData *groupData=[[GroupData alloc]init];
    
    groupData.member=[[[tableData objectAtIndex:selectedRow] objectForKey:@"isMember"] boolValue];
    groupData.admin=[[[tableData objectAtIndex:selectedRow] objectForKey:@"isAdmin"] boolValue];
    groupData.onlyAdmin=[[[tableData objectAtIndex:selectedRow] objectForKey:@"isOnlyAdmin"] boolValue];
    groupData.pending=[[[tableData objectAtIndex:selectedRow] objectForKey:@"isPending"] boolValue];
   groupData.coverImg=[NSString stringWithFormat:@"%@%@",PhotosUrl,[[tableData objectAtIndex:selectedRow] objectForKey:@"coverPhotoId"]];
    groupData.coverID=[[tableData objectAtIndex:selectedRow] objectForKey:@"coverPhotoId"];
    groupData.profileImg=[NSString stringWithFormat:@"%@%@",PhotosUrl,[[tableData objectAtIndex:selectedRow] objectForKey:@"logoId"]];
    groupData.logoID=[[tableData objectAtIndex:selectedRow] objectForKey:@"logoId"];
    groupData.groupName=[[tableData objectAtIndex:selectedRow] objectForKey:@"name"];
    
    NSString *security=[[tableData objectAtIndex:selectedRow] objectForKey:@"groupTypeId"];
    groupData.privacyId=security;
    if ([security isEqualToString:publicPrivacy]) {
        groupData.privacy=@"Public";
    }else if ([security isEqualToString:privatePrivacy]){
        groupData.privacy=@"Private";
    }else{
        groupData.privacy=@"VIP";
    }
    
    groupData.activityType=[[tableData objectAtIndex:selectedRow] objectForKey:@"activityType"];
    groupData.activityTypeId=[[tableData objectAtIndex:selectedRow] objectForKey:@"activityTypeId"];
    groupData.activities=[[tableData objectAtIndex:selectedRow] objectForKey:@"tags"];
    groupData.desc=[[tableData objectAtIndex:selectedRow] objectForKey:@"description"];
    groupData.policy=[[tableData objectAtIndex:selectedRow] objectForKey:@"policies"];
    groupData.groupID=[[tableData objectAtIndex:selectedRow] objectForKey:@"id"];
    int memNUM=[[[tableData objectAtIndex:selectedRow] objectForKey:@"groupMembersNumber"] intValue];
    if (memNUM<=1) {
        groupData.groupNum=[NSString stringWithFormat:@"%i %@",memNUM,@"Member"];
    }else{
        groupData.groupNum=[NSString stringWithFormat:@"%i %@",memNUM,@"Members"];
    };
    groupData.unSeenFeeds=[[[tableData objectAtIndex:selectedRow]objectForKey:@"unseenFeedsNumber"] integerValue];
    groupData.unSeenChat=[[[tableData objectAtIndex:selectedRow]objectForKey:@"unseenMessagesNumber"] integerValue];
    groupData.groupObj=[tableData objectAtIndex:selectedRow];
    
    return groupData;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    
    if ([[segue identifier] isEqualToString:@"groupDetails"]) {
        HomeGroupViewController *homeGroup=[segue destinationViewController];
        homeGroup.groupData=[[GroupData alloc]init];
        homeGroup.groupData=[self segueData];
    }
}

@end
