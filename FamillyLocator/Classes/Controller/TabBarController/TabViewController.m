//
//  TabViewController.m
//  FamillyLocator
//
//  Created by Amira on 10/25/16.
//  Copyright © 2016 Amira. All rights reserved.
//

#import "TabViewController.h"
#import "SearchViewController.h"
#import "SideNotificationListViewController.h"
#import "SideMenuViewController.h"
#import "MKNumberBadgeView.h"
#import "Animation.h"
#import "Constant.h"
#import "ChatListViewController.h"
#import "UIViewController+RemoteNotificationData.h"

@interface TabViewController ()
{
    MKNumberBadgeView *number;
    WebServiceFunc *webService;
}
@end

@implementation TabViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [[UITabBar appearance] setTintColor:[UIColor darkGrayColor]];
    float ver = [[[UIDevice currentDevice] systemVersion] floatValue];
    if (ver >= 10.0) {
        [[UITabBar appearance] setUnselectedItemTintColor:[UIColor colorWithRed:40.0/255 green:158.0/255 blue:222.0/255 alpha:1.0]];// for unselected items that are gray
    }
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(unseenNotify:) name:@"UnseenNotification" object:nil];
    
    //UIImage *whiteBackground = [UIImage imageNamed:@"Rectangle"];
    
    // [[UITabBar appearance] setSelectionIndicatorImage:whiteBackground];
    
    number = [[MKNumberBadgeView alloc] initWithFrame:CGRectMake(28,10, 20,20)];
    number.font=[UIFont systemFontOfSize:9];
    [Animation roundView:number];
    number.value = 0;
    
    // Allocate UIButton
    UIButton *btn = [UIButton  buttonWithType:UIButtonTypeCustom];
    btn.frame = CGRectMake(0, 0, 50, 50);
    UIImage *btnImage = [UIImage imageNamed:@"notifications.png"];
    [btn setImage:btnImage forState:UIControlStateNormal];
    [btn addTarget:self action:@selector(notificationView:) forControlEvents:UIControlEventTouchUpInside];
    [btn addSubview:number]; //Add NKNumberBadgeView as a subview on UIButton
    
    // Initialize UIBarbuttonitem...
    UIBarButtonItem *proe = [[UIBarButtonItem alloc] initWithCustomView:btn];
    // self.navigationItem.leftBarButtonItem = proe;
    
    
    UISearchBar *searchBar = [[UISearchBar alloc]initWithFrame:CGRectMake(0,0, 210,44)];
    searchBar.placeholder = NSLocalizedString(@"Search",nil);
    searchBar.delegate=self;
    UIBarButtonItem *searchBarItem = [[UIBarButtonItem alloc]initWithCustomView:searchBar];
    searchBarItem.tag = 123;
    // searchBarItem.customView.hidden = YES;
    //searchBarItem.customView.alpha = 0.0f;
    //searchBarItem self.navigationItem.leftBarButtonItem = searchBarItem;
    
    UIButton *ProfileBt = [[UIButton alloc] init];
    ProfileBt.tag=-1;
    ProfileBt.frame=CGRectMake(0,0,35,35);
    ProfileBt.layer.cornerRadius = ProfileBt.frame.size.width/2;
    ProfileBt.layer.masksToBounds = YES;
    ProfileBt.layer.borderWidth = 1.0f;
    ProfileBt.layer.borderColor = [UIColor whiteColor].CGColor;
    [ProfileBt.imageView setContentMode:UIViewContentModeScaleAspectFit];
    
    NSString *userID=[[NSUserDefaults standardUserDefaults] objectForKey:@"UserId"];
    NSURL *imgurl=[NSURL URLWithString:[NSString stringWithFormat:CurrentUserImage,userID]];
    [ProfileBt setBackgroundImage:[UIImage imageWithData:[NSData dataWithContentsOfURL:imgurl]] forState:UIControlStateNormal];
    [ProfileBt addTarget:self action:@selector(ShowProfile) forControlEvents:UIControlEventTouchUpInside];
    self.navigationItem.leftBarButtonItems = @[proe,[[UIBarButtonItem alloc] initWithCustomView:ProfileBt],searchBarItem];
    webService =[[WebServiceFunc alloc]init];
    [webService setDelegate:self];
    // [webService getData:CurrentUserImage withflagLogin:YES];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(RemoteNotification:) name:@"RemoteNotifications" object:nil];
}

-(void)ShowProfile{



}
-(void)viewDidDisappear:(BOOL)animated
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}
-(void)RemoteNotification:(NSNotification*)notification{
    [self notificationData:notification.userInfo];
    
}
-(void)unseenNotify:(NSNotification*)notification{
    number.value=number.value+1;
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}
- (IBAction)showMenu
{
    
    SideMenuViewController *sideMenu=[self.storyboard instantiateViewControllerWithIdentifier:@"SideMenu"];
    [self.navigationController presentViewController:sideMenu animated:YES completion:nil];
}
-(IBAction)searchView:(id)sender{
    SearchViewController *searchView=[self.storyboard instantiateViewControllerWithIdentifier:@"SearchController"];
    [self.navigationController pushViewController:searchView animated:YES];
}
-(IBAction)notificationView:(id)sender{
    SideNotificationListViewController *notificationList=[self.storyboard instantiateViewControllerWithIdentifier:@"NotificationListController"];
    [self.navigationController pushViewController:notificationList animated:YES];
}

-(void)userWebserviceCallDidSuccessWithRespone:(NSMutableArray *)response{
    int value=[[(NSDictionary*)response objectForKey:@"count"] integerValue];
    number.value=value;
}
-(void)userWebserviceCallDidFaildwithError:(NSError *)error andMessage:(NSString *)message{
    
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
