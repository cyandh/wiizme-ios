//
//  WhereAreYouPOPUPViewController.h
//  wiizme
//
//  Created by Amira on 3/25/17.
//  Copyright © 2017 Amira. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AsyncImageView.h"
@protocol popShowLocation <NSObject>

@required

- (void)declineData;
-(void)acceptData:(NSDictionary*)userData;

@end

@interface WhereAreYouPOPUPViewController : UIViewController

@property(nonatomic)  IBOutlet UIView * popView;
@property(nonatomic)  IBOutlet AsyncImageView * userImg;
@property(nonatomic)  IBOutlet AsyncImageView * uploadedImg;
@property (nonatomic) IBOutlet UILabel *userName;
@property(nonatomic)  IBOutlet UITextView *Message;
@property(nonatomic)  IBOutlet UIButton * accept;
@property (nonatomic, weak) id <popShowLocation> delegate;
@property (nonatomic,strong) NSDictionary *userData;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *txtHeightConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *imgHeightConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *popviewHeightConstraint;

@end
