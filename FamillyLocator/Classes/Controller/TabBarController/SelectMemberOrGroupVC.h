//
//  SelectMemberOrGroupViewController.h
//  wiizme
//
//  Created by agamil on 5/12/17.
//  Copyright © 2017 Amira. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MBProgressHUD.h"
#import "WebServiceFunc.h"


@protocol SelectMemberOrGroupDelegate <NSObject>

@required
- (void)SelectMemberOrGroupD:(NSDictionary *)MemberOrGroup;



@end


@interface SelectMemberOrGroupVC : UIViewController{

    IBOutlet UITableView * tableView;
    WebServiceFunc *webService;
    MBProgressHUD *hud;
    NSMutableArray *members,*admins;
    int memberIndx,serviceNUM;
    NSIndexPath *selectedIndex;
    NSDictionary * selectedItem;
    IBOutlet UISearchBar * searchBar;
    NSMutableArray * allList;

}

@property(nonatomic,readwrite)BOOL isGroup;
@property(nonatomic,weak)id<SelectMemberOrGroupDelegate> deleget;

@end
