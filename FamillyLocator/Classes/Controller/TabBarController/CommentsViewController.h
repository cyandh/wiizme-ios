//
//  CommentsViewController.h
//  wiizme
//
//  Created by Amira on 12/24/16.
//  Copyright © 2016 Amira. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MBAutoGrowingTextView.h"
#import "WebServiceFunc.h"
@protocol PopviewDelegate <NSObject>

@required

- (void)CloseView;
-(void)likeView:(NSString*)postID;
-(void)sendComment:(NSString * )postID;

@end
@interface CommentsViewController : UIViewController<UITextViewDelegate,UITableViewDelegate,UITableViewDataSource,WebServiceProtocol>
{
     __weak IBOutlet UIView * likeup;
    __weak IBOutlet UILabel *likes;
    __weak IBOutlet UITableView *tableView;
    __weak IBOutlet UIImageView *likeImg;
    __weak IBOutlet UITextView *commentTxt;
    __weak IBOutlet UIView * popView;
    __weak IBOutlet UIButton *likeView;
    __weak IBOutlet UIView * CommentView;
}

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *tableHeightConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *CommentTXTHeightConstraint;
@property (nonatomic, weak) id <PopviewDelegate> delegate;
@property(nonatomic,strong) NSString * postId;
@property (nonatomic) int likeNum;
@end
