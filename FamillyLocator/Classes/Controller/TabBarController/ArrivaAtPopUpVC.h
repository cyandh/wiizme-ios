//
//  ArrivaAtPopUpVC.h
//  wiizme
//
//  Created by abdelrhman gamil on 7/5/17.
//  Copyright © 2017 Amira. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "WebServiceFunc.h"
#import "MBProgressHUD.h"





@interface ArrivaAtPopUpVC : UIViewController<UITextViewDelegate>{


    IBOutlet UITextView * MessageLbl;
    MBProgressHUD *hud;
    WebServiceFunc *webService;
    
    
}
@property(nonatomic,retain)NSString * GroupID;
@property(nonatomic,retain)NSString * UserID;
@property(nonatomic,readwrite)BOOL * IsGroup;
@end
