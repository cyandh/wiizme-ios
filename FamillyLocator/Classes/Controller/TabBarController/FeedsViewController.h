//
//  FeedsViewController.h
//  FamillyLocator
//
//  Created by Amira on 12/3/16.
//  Copyright © 2016 Amira. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FeedsTextTableViewCell.h"
#import "FeedsViewTableViewCell.h"
#import "FeedsViewTextTableViewCell.h"
#import "WebServiceFunc.h"
#import "LPPopupListView.h"
#import <MapKit/MapKit.h>
#import "TGRImageViewController.h"
#import "SSARefreshControl.h"
#import "RefreshTableview.h"
#import "WritePostViewController.h"

@interface FeedsViewController :UIViewController<UITableViewDataSource,UITableViewDelegate,FeedsTextCellDelegate,FeedsViewCellDelegate,FeedsViewTextCellDelegate,LPPopupListViewDelegate,UISearchBarDelegate,MKMapViewDelegate>
{
    __weak IBOutlet UITableView *tableView;
    
    BOOL RefreshFlag;
     RefreshTableview * RefreshTV;
    
    ///////////// Paging
    NSUInteger _totalPages;
    NSUInteger _totalRowsCount;
    NSUInteger _rowsPerPage;
    NSUInteger _currentPage;
    
    BOOL _isLoading;
    BOOL _shouldLoad;
    
    UIView *_backgroundView;
    UIView *_loadingView;
    UILabel *_loadingLabel;
    UIActivityIndicatorView *_activitiIndicator;
    //////////////
    BOOL ISRefresh;
    NSMutableArray * FeedsData;
    WritePostViewController * WritePostVC;
    TGRImageViewController* TGRImageVC;
    
}
@property (nonatomic,strong)NSString *groupId;
@property (nonatomic, strong) NSMutableIndexSet *selectedIndexes;
@property (nonatomic)BOOL isGroup;
@property (nonatomic ,strong)MKMapView *mapView;
@end
