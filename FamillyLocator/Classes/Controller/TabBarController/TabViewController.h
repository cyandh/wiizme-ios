//
//  TabViewController.h
//  FamillyLocator
//
//  Created by Amira on 10/25/16.
//  Copyright © 2016 Amira. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "WebServiceFunc.h"
@interface TabViewController : UITabBarController<WebServiceProtocol>

@end
