//
//  SearchViewController.h
//  wiizme
//
//  Created by Amira on 1/9/17.
//  Copyright © 2017 Amira. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "WebServiceFunc.h"
@interface SearchViewController : UIViewController<UISearchBarDelegate,UITextViewDelegate,UITableViewDataSource,WebServiceProtocol>
{
    __weak IBOutlet UISearchBar *searchBar;
    __weak IBOutlet UITableView *tableView;
    NSMutableDictionary *tableData;
}
@end
