//
//  CustomAlertViewController.m
//  wiizme
//
//  Created by Amira on 1/18/17.
//  Copyright © 2017 Amira. All rights reserved.
//

#import "CustomAlertViewController.h"
#import "Animation.h"
#import "UIViewController+RemoteNotificationData.h"
@interface CustomAlertViewController ()
{
    UIImage *uploadImg;
    UITapGestureRecognizer *tapGestureView;
}
@end
@implementation CustomAlertViewController
@synthesize delegate,popView,Message,userData,accept,userImg,decline,userName;

- (void)viewDidLoad {
    [super viewDidLoad];
    


    
//    [popView.layer setShadowColor:[[UIColor darkGrayColor] CGColor]];
//    [popView.layer setShadowOffset:CGSizeMake(0, 0)];
//    [popView.layer setShadowRadius:50.0];
//    [popView.layer setShadowOpacity:1];
//    popView.clipsToBounds = NO;
//    popView.layer.masksToBounds = NO;
    
    userName.text=userData.name;
    Message.text=userData.message;
    if (userData.addImg) {
        _sendMsg.hidden=NO;
        [userImg setUserInteractionEnabled:YES];
        [userImg setImage:[UIImage imageNamed:userData.userImage]];
        UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(uploadImg)];
        
        [userImg addGestureRecognizer:tapGesture];
        [_sendMsg setDelegate:self];
        [accept setTitle:@"Send" forState:UIControlStateNormal];
        [decline setTitle:@"Location Only" forState:UIControlStateNormal];
    }else{
        _sendMsg.hidden=YES;
        [userImg setUserInteractionEnabled:NO];
    NSURL *imageUrl=[NSURL URLWithString:userData.userImage];
    [userImg setImageURL:imageUrl];
    }
    [Animation roundCornerForView:popView withAngle:10.0f];
    [Animation roundCornerForView:userImg withAngle:25.0f];
    [userImg setClipsToBounds:YES];
    [userImg.layer setBorderColor:[UIColor lightGrayColor].CGColor];
    [userImg.layer setBorderWidth:1.0f];

    [Animation roundCornerForView:accept withAngle:5.0f];
    [Animation roundCornerForView:decline withAngle:5.0f];

}
-(void)uploadImg{
    [_sendMsg resignFirstResponder];
    UIActionSheet *actionsh=[[UIActionSheet alloc]initWithTitle:@"" delegate:self cancelButtonTitle:@"cancel" destructiveButtonTitle:nil otherButtonTitles:@"Camera",@"Gallery", nil];
    
    [actionsh showInView:self.view];
}

-(void)hideKeyBoard{
    [_sendMsg resignFirstResponder];
    [self.view removeGestureRecognizer:tapGestureView];
    
}

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex==0) {
        
        UIImagePickerController *picker = [[UIImagePickerController alloc] init];
        picker.delegate = self;
        picker.allowsEditing = YES;
        picker.sourceType = UIImagePickerControllerSourceTypeCamera;
        
        [self presentViewController:picker animated:YES completion:NULL];
    }else{
        UIImagePickerController *picker = [[UIImagePickerController alloc] init];
        picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
        picker.delegate = self;
        [self presentViewController:picker animated:YES completion:nil];
    }
}
- (void)imagePickerController:(UIImagePickerController *)picker
        didFinishPickingImage:(UIImage *)image
                  editingInfo:(NSDictionary *)editingInfo
{
    uploadImg=image;
    NSLog(@"%@",image);
    [userImg setImage:image];
    [self dismissViewControllerAnimated:YES completion:nil];
    
}





-(void)viewDidAppear:(BOOL)animated{
    
    [UIView beginAnimations: @"anim" context: nil];
    [UIView setAnimationBeginsFromCurrentState: YES];
    [UIView setAnimationDelegate:self];
    [UIView setAnimationDidStopSelector:@selector(OperHolderAnimation)];
    [UIView setAnimationDuration: .5];
    self.view.alpha =1;
    [UIView commitAnimations];
    [self.navigationController.navigationBar setHidden:YES];
    
}
-(void)viewWillDisappear:(BOOL)animated{
    [self.navigationController.navigationBar setHidden:NO];
}
-(void)OperHolderAnimation{
    
    [UIView beginAnimations: @"anim" context: nil];
    [UIView setAnimationBeginsFromCurrentState: YES];
    [UIView setAnimationDelegate:self];
    
    [UIView setAnimationDuration: .5];
    popView.alpha =1;
    [UIView commitAnimations];
    
    
}

-(IBAction)declineAction:(id)sender{
    // [self.delegate CloseView];
    
    [self.delegate decline:userData];
    
}
-(IBAction)acceptAction:(id)sender{
    // [self.delegate GotoDetails:DTO];
    if (userData.addImg) {
        if (uploadImg) {
            userData.uploadedImg=uploadImg;
        }else{
            userData.uploadedImg=nil;
        }
        if ([self.sendMsg.text isEqualToString:@"Type Your Message Here"]) {
            userData.message=@"";
        }else{
            userData.message=self.sendMsg.text;
        }
        
    }
    [self.delegate accept:userData];
}

- (void)textViewDidBeginEditing:(UITextView *)textView{
    tapGestureView = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(hideKeyBoard)];
    
    [self.view addGestureRecognizer:tapGestureView];
    if ([textView.text isEqualToString:@"Type Your Message Here"]) {
        textView.text=@"";
    }
    textView.textColor=[UIColor blackColor];
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
