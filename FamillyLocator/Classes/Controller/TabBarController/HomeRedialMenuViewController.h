//
//  HomeRedialMenuViewController.h
//  wiizme
//
//  Created by agamil on 4/30/17.
//  Copyright © 2017 Amira. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "ALRadialMenu.h"


@protocol RadialMenuDelegate <NSObject>

@required
- (void)selectedFunc:(NSDictionary* )SelectedItem;



@end


@interface HomeRedialMenuViewController : UIViewController<ALRadialMenuDelegate>{
    
    NSMutableArray * MenuArray;
    NSDictionary * NotificationObject;
   
    NSString * Centerimage;
    NSDictionary * SelectedItem;
    NSString * PageTitle;
    IBOutlet UILabel * labelView;
    int redialCount;
    BOOL isSOS;
    
    
}
@property (strong, nonatomic) ALRadialMenu *radialMenu;
@property (nonatomic, weak) id <RadialMenuDelegate> delegate;
@property(nonatomic,retain) IBOutlet  UIButton * CenterBt;
@property (nonatomic, readwrite)NSInteger PageId; ;
-(void)StartLoadRedialMenu:(NSDictionary *)Items setNotificationObj:(NSDictionary *)NotificationObj ;
@end
