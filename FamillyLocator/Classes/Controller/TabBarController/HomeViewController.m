//
//  HomeViewController.m
//  FamilyLocator
//
//  Created by Amira on 10/12/16.
//  Copyright © 2016 Amira. All rights reserved.
//

#import "HomeViewController.h"
#import "HomeRedialMenuViewController.h"
#import "UIViewController+HeaderView.h"
#import "UIViewController+ListenerWhereAreYou.h"
#import "UIViewController+ListenerWhereAreLocationPermi.h"
#import "UIViewController+ListenerLiveTracking.h"
#import "UIViewController+Permissionrequest.h"
#import "UIViewController+SendMsgChat.h"
#import "NSObject+CreateZoneNotify.h"
#import "AppDelegate.h"
#import "ALRadialButton.h"
#import "LastPosition.h"
#import "SelectMemberOrGroupVC.h"
#import "NeedHelpViewController.h"
#import "IamFine.h"
#import "MeetThereViewController.h"
#import "SeeThisViewController.h"
#import "whereareyou.h"
#import "ArrivaAtPopUpVC.h"
#import "SOSData.h"
#import "HomeNotifications.h"
#import "Constant.h"

@interface HomeViewController ()
{
    double latitudeNum,longitudeNum;
    NSString *address;
    LastPosition *lastLocation;
}
@end

@implementation HomeViewController

- (void)viewDidLoad {
    HomeNotification=[[HomeNotifications alloc] init];
    HomeNotification.deleget=self;
    [HomeNotification getMenuNotifications];
    
    
    [super viewDidLoad];
    SOS = [[SOSData alloc] init];
    [SOS GetSOSAPI:@""];
    SOS.deleget = self;
    FID=0;
    pageTitlebtArray= [[NSMutableArray alloc] init];
    UIStoryboard *storyboard=[UIStoryboard storyboardWithName:@"Main" bundle:nil];
    SelectIctionTypePopup=[storyboard instantiateViewControllerWithIdentifier:@"SelectIctionTypeViewController"];
    RedialMenuFlag = NO;
    SelectIctionTypePopup.delgate = self;
    // Do any additional setup after loading the view.
    //  [self searchBarView];
    
    
    //     [[self navigationItem] setLeftBarButtonItems:[NSArray arrayWithObjects:[[self navigationItem] backBarButtonItem], [[UIBarButtonItem alloc] initWithTitle:@"Custom" style:UIBarButtonItemStylePlain target:self action:@selector(action:)], nil]];
    
    //    ListenerWhereAreYouFunction *whereAreYouListener=[[ListenerWhereAreYouFunction alloc]init];
    //    [whereAreYouListener listenForRequest];
    if (((AppDelegate *)[[UIApplication sharedApplication] delegate]).lastLocation==nil) {
        ((AppDelegate *)[[UIApplication sharedApplication] delegate]).lastLocation=[[LastPosition alloc]init];
    }
    [self listenForRequest];
    
    [self listenForResponse];
    
    [self listenForRequestLiveTracking];
    
    [self listernForDeleteLiveTracking];
    
    [self listenForRequestPermissions];
    [self listenForMsgChat];
    [self ListenForcreateZone];
    
    scrollview.userInteractionEnabled = YES;
    scrollview.exclusiveTouch = YES;
    scrollview.canCancelContentTouches = YES;
    scrollview.delaysContentTouches = YES;
    
    
    
}

-(void)viewDidAppear:(BOOL)animated{
    
   }

-(void)goToPage:(id)sender{
    [CurrentPageBt setSelected:NO];
    CurrentPageBt = sender;
    [CurrentPageBt setSelected:YES];
    CurrentPageBt.tag;
    [scrollview setContentOffset:CGPointMake(self.view.frame.size.width*CurrentPageBt.tag, scrollview.contentOffset.y) animated:YES];
}

-(void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView{
    float totalWidth = self.view.frame.size.width*HomePages.count;
    float mm = (scrollview.contentOffset.x/totalWidth)*HomePages.count;
    NSInteger nearestNumber = floor(mm);
    if (nearestNumber>0 || nearestNumber <HomePages.count) {
        [CurrentPageBt setSelected:NO];
        CurrentPageBt= [pageTitlebtArray objectAtIndex:nearestNumber];
        [CurrentPageBt setSelected:YES];
    }
    
    
    // pageControl.currentPage = nearestNumber;
}

- (IBAction)buttonPressed:(id)sender{
    ALRadialButton * menuItem = sender;
    selectedMenuItemData = menuItem.ItemData;
    FID=[[selectedMenuItemData objectForKey:@"fid"] intValue];
    switch ([[selectedMenuItemData objectForKey:@"FunctionTypeID"] intValue]) {
        case 0:{
            
            break;
        }
        case 1:{
            [self ShowGreoupsMemberPopUp];
            break;
        }
        case 2:{
            [self SelectActiontype:1];
            break;
        }
        case 3:{
            [self SelectActiontype:0];
            break;
        }
            
        case 4:{
            [self SelectActiontype:4];
            break;
        }   case 5:{
            [self ZoonNotification];
            break;
        }
        default:
            break;
    }
    
}

-(void)ShowGreoupsMemberPopUp{
    
    [self.view addSubview:SelectIctionTypePopup.view];
}

- (void)SelectActiontype:(NSInteger )Type{
    
    if (Type==0) {
        
        isGroup =YES;
    }else{
        isGroup=NO;
    }
    switch ([[selectedMenuItemData objectForKey:@"fid"] intValue]) {
        case 0:{
            
            break;
        }
        case 1:{
            [self Chat];
            break;
        }
        case 2:{
            
            break;
        }
        case 3:{
            
            break;
        }
        case 4:{
            [self SelectMembersOrGroupsPopUp];
        
            break;
        }
        case 5:{
            [self SelectMembersOrGroupsPopUp];
            break;
        }
        case 6:{
            [self SelectMembersOrGroupsPopUp];
            break;
        }
        case 7:{
            [self SelectMembersOrGroupsPopUp];
            break;
        }
        case 8:{
            [self SelectMembersOrGroupsPopUp];
            break;
        }
        case 9:{
            [self SelectMembersOrGroupsPopUp];
            break;
        }
        case 10:{
            [self SelectMembersOrGroupsPopUp];
            break;
        }
        case 11:{
            [self News];
            break;
        }
        case 12:{
            [self SelectMembersOrGroupsPopUp];
            break;
        }
        case 13:{
            
            break;
        }
        case 14:{
            
            break;
        }
        case 20:{
            hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
            hud.mode = MBProgressHUDModeIndeterminate;
            [SOS SendPostList:[SOSResponse objectForKey:@"nearByUsersPostPrivacyList"]];
            break;
        }
        case 21:{
            hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
            hud.mode = MBProgressHUDModeIndeterminate;
             [SOS SendPostList:[SOSResponse objectForKey:@"nearByFriendsPostPrivacyList"]];
            break;
        }
        case 22:{
            hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
            hud.mode = MBProgressHUDModeIndeterminate;
             [SOS SendPostList:[SOSResponse objectForKey:@"postPrivacyList"]];
            break;
        }
        case 23:{
            
            if ([SOSResponse objectForKey:@"emergencyNumber"]!=nil) {
                [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[NSString stringWithFormat:@"tel:%@",[SOSResponse objectForKey:@"emergencyNumber"]]]];
            }
          
            break;  
        }
        default:
            break;
    }
    
}

-(void)SelectMembersOrGroupsPopUp{
    
    UIStoryboard *storyboard=[UIStoryboard storyboardWithName:@"Main" bundle:nil];
    SelectMemberOrGroupVC * SelectMemberOrGroupVC = [storyboard instantiateViewControllerWithIdentifier:@"SelectMemberOrGroupViewController"];
    SelectMemberOrGroupVC.deleget = self;
    
    SelectMemberOrGroupVC.isGroup = isGroup;
    //   dispatch_async(dispatch_get_main_queue(), ^{
    [SelectIctionTypePopup.view removeFromSuperview];
    [self.navigationController pushViewController:SelectMemberOrGroupVC animated:YES];
    //  });
    
}

- (void)SelectMemberOrGroupD:(NSDictionary *)MemberOrGroup{
    
    switch (FID) {
        case 0:{
            break;
        }
        case 1:{
            [self Chat];
            break;
        }
        case 2:{
            
            break;
        }
        case 3:{
            
            break;
        }
        case 4:{
            [self GotoMap:MemberOrGroup];
            
            break;
        }
        case 5:{
            
            ArrivaAtPopUp = [self.storyboard instantiateViewControllerWithIdentifier:@"ArrivaAtPopUpVC"];
            
            ArrivaAtPopUp.GroupID =[MemberOrGroup objectForKey:@"id"];
            ArrivaAtPopUp.IsGroup = isGroup;
            [self.view addSubview:ArrivaAtPopUp.view];
            [self.navigationController popViewControllerAnimated:NO];
            break;
        }
        case 6:{
            [self WhereAreYouFun:[MemberOrGroup objectForKey:@"id"]];
            break;
        }
        case 7:{
            [self GoBackHomeYou:[MemberOrGroup objectForKey:@"id"]];
            break;
        }
        case 8:{
            if (isGroup) {
                
                [self IamFine:[MemberOrGroup objectForKey:@"id"]];
            }else{
                
                [self IamFine:[MemberOrGroup objectForKey:@"id"]];
            }
            
            break;
        }
        case 9:{
            [self NeedHelp:MemberOrGroup];
            break;
        }
        case 10:{
            [self MeetYouThere:[MemberOrGroup objectForKey:@"id"] setGroupName:[MemberOrGroup objectForKey:@"name"]];
            break;
        }
        case 11:{
            
            break;
        }
        case 12:{
            [self seeThis:[MemberOrGroup objectForKey:@"id"] setlogoID:[MemberOrGroup objectForKey:@"logoid"]  SetGroupName:[MemberOrGroup objectForKey:@"name"] ];
            break;
        }
        case 13:{
            
            break;
        }
        case 14:{
            
            break;
        }
            
    
            
        default:
            break;
    }
}


-(void)ZoonNotification{
    
    ZoonNotificationList = [self.storyboard instantiateViewControllerWithIdentifier:@"ZoonNotificationListVC"];
    [self.navigationController popViewControllerAnimated:NO];
    [self.navigationController pushViewController:ZoonNotificationList animated:YES];
}

-(void)NeedHelp:(NSDictionary *)Data{
    
    NeedHelpViewController *needView=[self.storyboard instantiateViewControllerWithIdentifier:@"needController"];
    needView.inviterId=[Data objectForKey:@"id"];
    needView.inviterName=[Data objectForKey:@"name"];
    if (isGroup) {
        needView.isUser=NO;
    }else{
        needView.isUser=YES;
    }
    
    [self.navigationController popViewControllerAnimated:NO];
    [self.navigationController pushViewController:needView animated:YES];
    
}

-(void)IamFine:(NSString *)groupID{
    [self.navigationController popViewControllerAnimated:NO];
    hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    hud.mode = MBProgressHUDModeIndeterminate;
    iamfinfunction = [[IamFine alloc] init];
    
    iamfinfunction.deleget = self;
    [iamfinfunction finePost:groupID];
    
}

-(void)Chat{
    
    [self.tabBarController setSelectedIndex:2];
    
}

-(void)News{
    
    [self.tabBarController setSelectedIndex:4];
    
}

-(void)MeetYouThere:(NSString *)groupID setGroupName:(NSString *)groupName{
    
    
    MeetThereViewController *meetView=[self.storyboard instantiateViewControllerWithIdentifier:@"MeetThereViewController"];
    meetView.listID=groupID;
    meetView.listName=groupName;
    meetView.isUser=NO;
    [self.navigationController popViewControllerAnimated:NO];
    [self.navigationController pushViewController:meetView animated:YES];
}

-(void)seeThis:(NSString *)GroupID setlogoID:(NSString *)LogoID SetGroupName:(NSString *)GroupName{
    
    SeeThisViewController *writeView=[self.storyboard instantiateViewControllerWithIdentifier:@"SeeThisPostController"];
    writeView.userId=GroupID;
    writeView.userImgID=LogoID;
    writeView.userNameTXT=GroupName;
    writeView.isGroup=YES;
    [self.navigationController pushViewController:writeView animated:YES];
    
    
}

-(void)IamFinefunctionRespnse:(NSMutableArray *)response service:(NSInteger )Type{
    [hud setHidden:YES];
    
}

-(void)IamFinefunctionFail:(NSString *)message service:(NSInteger )Type{
    [hud setHidden:YES];
    
}

-(void)SOSDatafunctionRespnse:(NSMutableArray *)response service:(NSInteger )Type{
    if (response!=nil) {
          SOSResponse = response;
         [hud setHidden:YES];
    }else{
    
        hud.mode = MBProgressHUDModeCustomView;
        // Set an image view with a checkmark.
        UIImage *image = [[UIImage imageNamed:@"SCheckmark"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
        hud.customView = [[UIImageView alloc] initWithImage:image];
        // Looks a bit nicer if we make it square.
        hud.square = YES;
        hud.label.text = NSLocalizedString(@"Done", @"");
        [hud hideAnimated:YES afterDelay:1.f];
    
    }
    

   
    
}

-(void)SOSDatafunctionFail:(NSString *)message service:(NSInteger )Type{
    [hud setHidden:YES];
    
}

-(void)WhereAreYouFun:(NSString * )UserID{
    
    
    whereareyouP = [[whereareyou alloc] init];
    whereareyouP.deleget=self;
    [whereareyouP WhereAreYouPost:UserID];
    hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    hud.mode = MBProgressHUDModeIndeterminate;
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)GoBackHomeYou:(NSString * )UserID{
    
    
    GoBackHomeObj = [[GoBackHome alloc] init];
    GoBackHomeObj.deleget=self;
    [GoBackHomeObj GoBackHomePost:UserID];
    hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    hud.mode = MBProgressHUDModeIndeterminate;
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)whereAreYoufunctionRespnse:(NSMutableArray *)response service:(NSInteger )Type{
    
    [hud hideAnimated:YES];
    
}

-(void)whereAreYoufunctionFail:(NSString *)message service:(NSInteger )Type{
    
    [hud hideAnimated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)GoBackHomefunctionRespnse:(NSMutableArray *)response service:(NSInteger )Type{
    
    [hud hideAnimated:YES];
}

- (void)GoBackHomefunctionFail:(NSString *)message service:(NSInteger )Type{
    
    
    [hud hideAnimated:YES];
}

- (IBAction)test:(id)sender{
    NSLog(@"fsdfsdf");
    [HomeRedialMenu.radialMenu buttonsWillAnimateFromButton:HomeRedialMenu.CenterBt withFrame:HomeRedialMenu.CenterBt.frame inView:self.view];
}

- (void)selectedFunc:(NSDictionary* )SelectedItem{
    
    
    selectedMenuItemData = SelectedItem;
    FID=[[SelectedItem objectForKey:@"fid"] intValue];
    switch ([[selectedMenuItemData objectForKey:@"FunctionTypeID"] intValue]) {
        case 0:{
            
            break;
        }
        case 1:{
            [self ShowGreoupsMemberPopUp];
            break;
        }
        case 2:{
            [self SelectActiontype:1];
            break;
        }
        case 3:{
            [self SelectActiontype:0];
            break;
        }
        case 4:{
            [self SelectActiontype:4];
            break;
        }   case 5:{
            [self ZoonNotification];
            break;
        }
        default:
            break;
    }
    
    
}
/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

-(void)GotoMap:(NSDictionary * )userOfGroup{

    MapKitVC = [self.storyboard instantiateViewControllerWithIdentifier:@"MapController"];
    MapKitVC.userID = [userOfGroup objectForKey:@"id"];
    MapKitVC.memberNam=[userOfGroup objectForKey:@"name"];
    MapKitVC.memberImage=[NSString stringWithFormat:@"%@%@",PhotosUrl,[userOfGroup objectForKey:@"photoId"]];
    MapKitVC.isGroup = isGroup;
    [self.navigationController popViewControllerAnimated:YES];
    [self.navigationController pushViewController:MapKitVC animated:YES];
}
- (void)HomeNotificationsfunctionRespnse:(NSMutableArray *)response service:(NSInteger )Type{
    HomeNotificationDic = response;
    if (!RedialMenuFlag) {
        NSString* path = [[NSBundle mainBundle] pathForResource:@"HomeMenu"
                                                         ofType:@"plist"];
        HomePages = [[NSMutableArray alloc] initWithContentsOfFile:path];
        UIStoryboard *storyboard=[UIStoryboard storyboardWithName:@"Main" bundle:nil];
        float x =0;
        float labelsize =self.view.frame.size.width/HomePages.count;
        for (int i=0; i<HomePages.count; i++) {
            UIButton * pageTitlebt = [[UIButton alloc] initWithFrame:CGRectMake(i*labelsize, 0, labelsize, 40)];
            
            [pageTitlebtArray addObject:pageTitlebt];
            
            [pageTitlebt setBackgroundColor:[UIColor clearColor]];
            [pageTitlebt.titleLabel setTextAlignment:UITextAlignmentCenter];
            [pageTitlebt setTitle:[[HomePages objectAtIndex:i] objectForKey:@"Name"] forState:UIControlStateNormal ];
            [pageTitlebt.titleLabel setFont:[UIFont fontWithName:@"Arial" size:14]];
            [self.view addSubview:pageTitlebt];
            
            
            pageTitlebt.backgroundColor=[UIColor clearColor];
            pageTitlebt.tag=i;
            [pageTitlebt addTarget:self action:@selector(goToPage:) forControlEvents:UIControlEventTouchUpInside];
            [pageTitlebt setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
            [pageTitlebt setTitleColor:[UIColor blackColor] forState:UIControlStateSelected];
            if (i==0) {
                CurrentPageBt=pageTitlebt;
                [CurrentPageBt setSelected:YES];
            }
            
            HomeRedialMenu =[storyboard instantiateViewControllerWithIdentifier:@"HomeRedialMenuViewController"];
            HomeRedialMenu.delegate = self;
            HomeRedialMenu.PageId = i;
            [HomeRedialMenu StartLoadRedialMenu:[HomePages objectAtIndex:i]setNotificationObj:HomeNotificationDic];
            [HomeRedialMenu.view setFrame:CGRectMake(x, HomeRedialMenu.view.frame.origin.y, self.view.frame.size.width, self.view.frame.size.height)];
            x+=self.view.frame.size.width;
            [scrollview addSubview:HomeRedialMenu.view];
        }
        [scrollview setDelegate:self];
        [scrollview setPagingEnabled:YES];
        [scrollview setContentSize:CGSizeMake(x, self.view.frame.size.height)];
        RedialMenuFlag = YES;
    }

    
    
}

- (void)HomeNotificationsfunctionFail:(NSString *)message service:(NSInteger )Type{
    
    if (!RedialMenuFlag) {
        NSString* path = [[NSBundle mainBundle] pathForResource:@"HomeMenu"
                                                         ofType:@"plist"];
        HomePages = [[NSMutableArray alloc] initWithContentsOfFile:path];
        UIStoryboard *storyboard=[UIStoryboard storyboardWithName:@"Main" bundle:nil];
        float x =0;
        float labelsize =self.view.frame.size.width/HomePages.count;
        for (int i=0; i<HomePages.count; i++) {
            UIButton * pageTitlebt = [[UIButton alloc] initWithFrame:CGRectMake(i*labelsize, 0, labelsize, 40)];
            
            [pageTitlebtArray addObject:pageTitlebt];
            
            [pageTitlebt setBackgroundColor:[UIColor clearColor]];
            [pageTitlebt.titleLabel setTextAlignment:UITextAlignmentCenter];
            [pageTitlebt setTitle:[[HomePages objectAtIndex:i] objectForKey:@"Name"] forState:UIControlStateNormal ];
            [pageTitlebt.titleLabel setFont:[UIFont fontWithName:@"Arial" size:14]];
            [self.view addSubview:pageTitlebt];
            
            
            pageTitlebt.backgroundColor=[UIColor clearColor];
            pageTitlebt.tag=i;
            [pageTitlebt addTarget:self action:@selector(goToPage:) forControlEvents:UIControlEventTouchUpInside];
            [pageTitlebt setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
            [pageTitlebt setTitleColor:[UIColor blackColor] forState:UIControlStateSelected];
            if (i==0) {
                CurrentPageBt=pageTitlebt;
                [CurrentPageBt setSelected:YES];
            }
            
            HomeRedialMenu =[storyboard instantiateViewControllerWithIdentifier:@"HomeRedialMenuViewController"];
            HomeRedialMenu.delegate = self;
            HomeRedialMenu.PageId = i;
            [HomeRedialMenu StartLoadRedialMenu:[HomePages objectAtIndex:i] setNotificationObj:nil];
            [HomeRedialMenu.view setFrame:CGRectMake(x, HomeRedialMenu.view.frame.origin.y, self.view.frame.size.width, self.view.frame.size.height)];
            x+=self.view.frame.size.width;
            [scrollview addSubview:HomeRedialMenu.view];
        }
        [scrollview setDelegate:self];
        [scrollview setPagingEnabled:YES];
        [scrollview setContentSize:CGSizeMake(x, self.view.frame.size.height)];
        RedialMenuFlag = YES;
    }

    
}



@end
