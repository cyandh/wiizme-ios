//
//  SearchViewController.m
//  wiizme
//
//  Created by Amira on 1/9/17.
//  Copyright © 2017 Amira. All rights reserved.
//

#import "SearchViewController.h"
#import "GroupTableViewCell.h"
#import "MBProgressHUD.h"
#import "Constant.h"
#import "Animation.h"
#import "HomeGroupViewController.h"
#import "HomeMemberViewController.h"
#import "MemberInfoViewController.h"
#import "SettingGroupViewController.h"
#import "UIViewController+RemoteNotificationData.h"
@interface SearchViewController ()
{
    WebServiceFunc *webService;
    MBProgressHUD *hud;
}
@end

@implementation SearchViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    webService=[[WebServiceFunc alloc]init];
    [webService setDelegate:self];
    


}

-(void)viewDidDisappear:(BOOL)animated{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}



-(void)userWebserviceCallDidSuccessWithRespone:(NSMutableArray *)response{
    [hud setHidden:YES];
    tableData=(NSMutableDictionary*)response;
    [tableView reloadData];
    
}
-(void)userWebserviceCallDidFaildwithError:(NSError *)error andMessage:(NSString *)message{
    [hud setHidden:YES];
    NSLog(@"%@",error.localizedDescription);
    if ([message isEqualToString:@"401" ]) {
        UIStoryboard *storyboard=[UIStoryboard storyboardWithName:@"Main" bundle:nil];
        UIViewController* viewController=[storyboard instantiateViewControllerWithIdentifier:@"LoginController"];
        [self presentViewController:viewController animated:YES completion:nil];
    }
    
}


#pragma TableView DataSource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 2;
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UILabel *customLabel = [[UILabel alloc] init];
    customLabel.text = [self tableView:tableView titleForHeaderInSection:section];
    customLabel.textColor=[UIColor whiteColor];
    customLabel.font=[UIFont fontWithName:@"Cairo-SemiBold" size:16];
    customLabel.backgroundColor=[UIColor colorWithRed:47.0/255.0 green:61.0/255.0 blue:70.0/255.0 alpha:1.0f];
    return customLabel;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    if (([[tableData objectForKey:@"users"] count]>0)||([[tableData objectForKey:@"groups"]count])) {
        return 40;
    }else{
        return 0;
    }
    
    
}
-(NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section{
    NSString *title;
    if (section==0) {
       title=@"   People";
    }else{
       title=@"   Groups";
    }
    return title;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if (section==0) {
        return [[tableData objectForKey:@"users"] count];
    }else{
        return [[tableData objectForKey:@"groups"]count];
    }
    
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    GroupTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"groupCell"];
    NSString *name;
    NSURL *imageUrl;
    if (indexPath.section==0) {
        name=[NSString stringWithFormat:@"%@%@",[[[tableData objectForKey:@"users"]objectAtIndex:indexPath.row] objectForKey:@"firstName"],[[[tableData objectForKey:@"users"]objectAtIndex:indexPath.row] objectForKey:@"familyName"]];
        imageUrl=[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",PhotosUrl,[[[tableData objectForKey:@"users"]objectAtIndex:indexPath.row] objectForKey:@"photoId"]]];
        
        
    }else{
        name=[[[tableData objectForKey:@"groups"]objectAtIndex:indexPath.row] objectForKey:@"name"];
        imageUrl=[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",PhotosUrl,[[[tableData objectForKey:@"groups"]objectAtIndex:indexPath.row]objectForKey:@"logoId"]]];
    }

    cell.groupName.text=name;
    [cell.image setImage:nil];
    [cell.image setImageURL:imageUrl];
    NSLog(@"%f",cell.image.frame.size.width);
    [Animation roundCornerForView:cell.image withAngle:30.0f];
    //  [cell loadNewsImagewithLabelString:videotitle andDate:videoDateFormate];
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    // This will create a "invisible" footer
    return 0.01f;
}


- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    return [UIView new];
    
    
}



#pragma TableView Delegate

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
 
    if (indexPath.section==0) {
        BOOL relation=[[[[tableData objectForKey:@"users"] objectAtIndex:indexPath.row] objectForKey:@"hasRelation"] boolValue];
        if (relation) {
            HomeMemberViewController *memberView=[self.storyboard instantiateViewControllerWithIdentifier:@"HomeMemberController"];
            memberView.userData=[[UserData alloc]init];
            memberView.userData.name=[NSString stringWithFormat:@"%@ %@",[[[tableData objectForKey:@"users"] objectAtIndex:indexPath.row] objectForKey:@"firstName"],[[[tableData objectForKey:@"users"] objectAtIndex:indexPath.row] objectForKey:@"familyName"]];
            memberView.userData.userImage=[[[tableData objectForKey:@"users"] objectAtIndex:indexPath.row] objectForKey:@"photoId"];
            memberView.userData.fieldId=[[[tableData objectForKey:@"users"] objectAtIndex:indexPath.row] objectForKey:@"id"];
            memberView.fromSearch=YES;
            memberView.hasRelation=YES;
            [self.navigationController pushViewController:memberView animated:YES];
        }else{
//            MemberInfoViewController *info=[self.storyboard instantiateViewControllerWithIdentifier:@"MemberInfoController"];
//            info.userData=[[UserData alloc]init];
//            info.userData.name=[NSString stringWithFormat:@"%@ %@",[[[tableData objectForKey:@"users"] objectAtIndex:indexPath.row] objectForKey:@"firstName"],[[[tableData objectForKey:@"users"] objectAtIndex:indexPath.row] objectForKey:@"familyName"]];;
//            info.userData.userImage=[[[tableData objectForKey:@"users"] objectAtIndex:indexPath.row] objectForKey:@"photoId"];
//            info.userData.age=[[[[tableData objectForKey:@"users"] objectAtIndex:indexPath.row] objectForKey:@"age"]stringValue];
//            info.userData.city=[[[tableData objectForKey:@"users"] objectAtIndex:indexPath.row] objectForKey:@"currentCity"];
//            BOOL gender=[[[[tableData objectForKey:@"users"] objectAtIndex:indexPath.row] objectForKey:@"gender"] boolValue];
//            if (gender) {
//                info.userData.gender=@"Male";
//            }else{
//                info.userData.gender=@"Female";
//            }
//            info.userData.job=[[[tableData objectForKey:@"users"] objectAtIndex:indexPath.row] objectForKey:@"job"];
//            info.userData.friendsNum=[[[[tableData objectForKey:@"users"] objectAtIndex:indexPath.row] objectForKey:@"friendsNumber"] stringValue];
//            info.userData.groupsNum=[[[[tableData objectForKey:@"users"] objectAtIndex:indexPath.row] objectForKey:@"groupsNumber"] stringValue];
            HomeMemberViewController *memberView=[self.storyboard instantiateViewControllerWithIdentifier:@"HomeMemberController"];
            memberView.userData=[[UserData alloc]init];
            memberView.userData.name=[NSString stringWithFormat:@"%@ %@",[[[tableData objectForKey:@"users"] objectAtIndex:indexPath.row] objectForKey:@"firstName"],[[[tableData objectForKey:@"users"] objectAtIndex:indexPath.row] objectForKey:@"familyName"]];
            memberView.userData.userImage=[[[tableData objectForKey:@"users"] objectAtIndex:indexPath.row] objectForKey:@"photoId"];
            memberView.userData.fieldId=[[[tableData objectForKey:@"users"] objectAtIndex:indexPath.row] objectForKey:@"id"];
            memberView.fromSearch=YES;
            memberView.hasRelation=NO;
          //  NSLog(@"rr%@",info.userData);
            [self.navigationController pushViewController:memberView animated:YES];
        }
        
    }else{
        
        GroupData *groupData=[[GroupData alloc]init];
        groupData.member=[[[[tableData objectForKey:@"groups"] objectAtIndex:indexPath.row] objectForKey:@"isMember"] boolValue];
        groupData.admin=[[[[tableData objectForKey:@"groups"] objectAtIndex:indexPath.row] objectForKey:@"isAdmin"] boolValue];
        groupData.onlyAdmin=[[[[tableData objectForKey:@"groups"] objectAtIndex:indexPath.row] objectForKey:@"isOnlyAdmin"] boolValue];
        groupData.pending=[[[[tableData objectForKey:@"groups"] objectAtIndex:indexPath.row] objectForKey:@"isPending"] boolValue];
        groupData.coverImg=[NSString stringWithFormat:@"%@%@",PhotosUrl,[[[tableData objectForKey:@"groups"] objectAtIndex:indexPath.row] objectForKey:@"coverPhotoId"]];
        groupData.coverID=[[[tableData objectForKey:@"groups"] objectAtIndex:indexPath.row] objectForKey:@"coverPhotoId"];
        groupData.profileImg=[NSString stringWithFormat:@"%@%@",PhotosUrl,[[[tableData objectForKey:@"groups"] objectAtIndex:indexPath.row] objectForKey:@"logoId"]];

        groupData.logoID=[[[tableData objectForKey:@"groups"] objectAtIndex:indexPath.row] objectForKey:@"logoId"];
        groupData.groupName=[[[tableData objectForKey:@"groups"] objectAtIndex:indexPath.row] objectForKey:@"name"];
        
        NSString *security=[[[tableData objectForKey:@"groups"] objectAtIndex:indexPath.row] objectForKey:@"groupTypeId"];
        groupData.privacyId=security;
        if ([security isEqualToString:publicPrivacy]) {
            groupData.privacy=@"Public";
        }else if ([security isEqualToString:privatePrivacy]){
            groupData.privacy=@"Private";
        }else{
            groupData.privacy=@"VIP";
        }
        
        groupData.activityType=[[[tableData objectForKey:@"groups"] objectAtIndex:indexPath.row] objectForKey:@"activityType"];
        groupData.activityTypeId=[[[tableData objectForKey:@"groups"] objectAtIndex:indexPath.row] objectForKey:@"activityTypeId"];
        groupData.activities=[[[tableData objectForKey:@"groups"] objectAtIndex:indexPath.row] objectForKey:@"tags"];
        groupData.desc=[[[tableData objectForKey:@"groups"] objectAtIndex:indexPath.row] objectForKey:@"description"];
        groupData.policy=[[[tableData objectForKey:@"groups"] objectAtIndex:indexPath.row] objectForKey:@"policies"];
        groupData.groupID=[[[tableData objectForKey:@"groups"] objectAtIndex:indexPath.row] objectForKey:@"id"];
        int memNUM=[[[[tableData objectForKey:@"groups"] objectAtIndex:indexPath.row] objectForKey:@"groupMembersNumber"] intValue];
        if (memNUM<=1) {
            groupData.groupNum=[NSString stringWithFormat:@"%i %@",memNUM,@"Member"];
        }else{
            groupData.groupNum=[NSString stringWithFormat:@"%i %@",memNUM,@"Members"];
        }
        
        BOOL relation=[[[[tableData objectForKey:@"groups"] objectAtIndex:indexPath.row] objectForKey:@"isMember"] boolValue];
        
        
        if (relation) {
            HomeGroupViewController *homeGroup=[self.storyboard instantiateViewControllerWithIdentifier:@"HomeGroupController"];
            homeGroup.groupData=[[GroupData alloc]init];
            homeGroup.groupData=groupData;
            
            [self.navigationController pushViewController:homeGroup animated:YES];
            
        }else{
            SettingGroupViewController *settingView=[self.storyboard instantiateViewControllerWithIdentifier:@"settingGroupController"];
            settingView.groupData=[[GroupData alloc]init];
            settingView.groupData=groupData;
            [self.navigationController pushViewController:settingView animated:YES];
        }
}
}


#pragma mark UIsearchBar Delegates
- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar
{
    [searchBar resignFirstResponder];
    // Do the search...
}
-(void)searchBarTextDidBeginEditing:(UISearchBar *)searchBar
{
    [searchBar setShowsCancelButton:YES animated:YES];
     [self animateTextView:nil up: YES];
}
-(void)searchBarCancelButtonClicked:(UISearchBar *)searchBar
{
    [searchBar setShowsCancelButton:NO animated:YES];
    [searchBar resignFirstResponder];
    
    searchBar.text=@"";
}

- (void)searchBarTextDidEndEditing:(UISearchBar *)searchBar
{
    [searchBar setShowsCancelButton:NO animated:YES];
   // searchBar.text=@"";
       [self animateTextView:nil up: NO];
}
- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText
{
    if (searchText.length>0) {
        searchText = [searchText stringByReplacingOccurrencesOfString:@" " withString:@""];
        
//        hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
//        hud.mode = MBProgressHUDModeIndeterminate;
        NSString *urlString=[NSString stringWithFormat:@"%@%@",Search,searchText];
        [webService getData:urlString withflagLogin:YES];

    }
    else
    {
        [tableData removeAllObjects];
        [tableView reloadData];
    }
}


- (void)textViewDidBeginEditing:(UITextView *)textView{
    if ([textView.text isEqualToString:@"Message"]) {
        textView.text=@"";
    }
    [self animateTextView:textView up: YES];
}



- (void)textViewDidEndEditing:(UITextView *)textView
{
    [self animateTextView:textView up: NO];
}

- (void) animateTextView: (UITextView*) textView up: (BOOL) up
{
    
    
    const int movementDistance = 190; // tweak as needed
    const float movementDuration = 0.3f; // tweak as needed
    
    int movement = (up ? -movementDistance : movementDistance);
    
    [UIView beginAnimations: @"anim" context: nil];
    [UIView setAnimationBeginsFromCurrentState: YES];
    [UIView setAnimationDuration: movementDuration];
    tableView.frame = CGRectMake(tableView.frame.origin.x, tableView.frame.origin.y, tableView.frame.size.width, tableView.frame.size.height+movement);
    [UIView commitAnimations];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
