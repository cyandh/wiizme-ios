//
//  HomeRedialMenuViewController.m
//  wiizme
//
//  Created by agamil on 4/30/17.
//  Copyright © 2017 Amira. All rights reserved.
//

#import "HomeRedialMenuViewController.h"

@interface HomeRedialMenuViewController ()

@end

@implementation HomeRedialMenuViewController
@synthesize delegate,PageId,CenterBt;
- (void)viewDidLoad {
    [super viewDidLoad];
    
    isSOS = NO;
    //
    
    
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewDidAppear:(BOOL)animated{
    
    self.radialMenu = [[ALRadialMenu alloc] init];
    self.radialMenu.delegate = self;
    self.radialMenu.fadeItems = YES;
    
    [NSTimer scheduledTimerWithTimeInterval:0.5
                                     target:self
                                   selector:@selector(Tracehight)
                                   userInfo:nil
                                    repeats:NO];
    
}

-(void)StartLoadRedialMenu:(NSDictionary *)Items setNotificationObj:(NSDictionary *)NotificationObj {
    if (NotificationObj!=nil) {
        NotificationObject =NotificationObj;
    }
    
    MenuArray = [Items objectForKey:@"items"];
    PageTitle =[Items objectForKey:@"Name"];
    Centerimage =[Items objectForKey:@"Image"];
    NSLog(@"%@",[Items objectForKey:@"Image"]);
    
    
}


-(void)Tracehight{
    labelView.text = @"";
    
    [CenterBt setImage:[UIImage imageNamed:Centerimage] forState:UIControlStateNormal];
    [CenterBt addTarget:self action:@selector(test:) forControlEvents:UIControlEventTouchUpInside];
    [self.radialMenu buttonsWillAnimateFromButton:CenterBt withFrame:CenterBt.frame inView:self.view];
    
    labelView.frame=CGRectMake(labelView.frame.origin.x, 160.0, labelView.frame.size.width, labelView.frame.size.height);
    [labelView setHidden:YES];
    
}
- (IBAction)buttonPressed:(id)sender{
   
    
}

- (IBAction)test:(id)sender{
 
    [self.radialMenu buttonsWillAnimateFromButton:self.CenterBt withFrame:self.CenterBt.frame inView:self.view];
}

#pragma mark - radial menu delegate methods
- (NSInteger) numberOfItemsInRadialMenu:(ALRadialMenu *)radialMenu {
    
    return [MenuArray count];
}
-(void)MenuDidFinishAnimation{
    if (!isSOS) {
        
        
        isSOS=YES;
        NSString* path = [[NSBundle mainBundle] pathForResource:@"SOSMenu"
                                                         ofType:@"plist"];
        MenuArray = [[NSMutableArray alloc] initWithContentsOfFile:path];
        [self StartLoadRedialMenu:[MenuArray objectAtIndex:0]setNotificationObj:nil];
        [CenterBt setImage:[UIImage imageNamed:@"menu-picture"] forState:UIControlStateNormal];
    }else{
        isSOS=NO;
        
        NSString* path = [[NSBundle mainBundle] pathForResource:@"HomeMenu"
                                                         ofType:@"plist"];
        MenuArray = [[NSMutableArray alloc] initWithContentsOfFile:path];
        [self StartLoadRedialMenu:[MenuArray objectAtIndex:0]setNotificationObj:NotificationObject];
        [CenterBt setImage:[UIImage imageNamed:Centerimage] forState:UIControlStateNormal];
    }
    
    [self.radialMenu buttonsWillAnimateFromButton:self.CenterBt withFrame:self.CenterBt.frame inView:self.view];
    
}

- (NSInteger) arcSizeForRadialMenu:(ALRadialMenu *)radialMenu {
    
    return 360;
}


- (NSInteger) arcRadiusForRadialMenu:(ALRadialMenu *)radialMenu {
    
    return 130;
}


- (ALRadialButton *) radialMenu:(ALRadialMenu *)radialMenu buttonForIndex:(NSInteger)index {
    ALRadialButton *button = [[ALRadialButton alloc] init];
    button.ItemData = [MenuArray objectAtIndex:index-1];
    if (index == 1) {
        [button setImage:[UIImage imageNamed:@"menu-help"] forState:UIControlStateNormal];
    } else if (index == 2) {
        [button setImage:[UIImage imageNamed:@"menu-whereAreYou"] forState:UIControlStateNormal];
    } else if (index == 3) {
        [button setImage:[UIImage imageNamed:@"menu-meet"] forState:UIControlStateNormal];
    } else if (index == 4) {
        [button setImage:[UIImage imageNamed:@"menu-lastPosition"] forState:UIControlStateNormal];
    } else if (index == 5) {
        [button setImage:[UIImage imageNamed:@"menu-map"] forState:UIControlStateNormal];
    } else if (index == 6) {
        [button setImage:[UIImage imageNamed:@"menu-groups"] forState:UIControlStateNormal];
    } else if (index == 7) {
        [button setImage:[UIImage imageNamed:@"menu-chat"] forState:UIControlStateNormal];
    } else if (index == 8) {
        [button setImage:[UIImage imageNamed:@"menu-goHome"] forState:UIControlStateNormal];
    }
    if (index==9) {
        [button setImage:[UIImage imageNamed:@"menu-invite"] forState:UIControlStateNormal];
    }
    if (index==10) {
        [button setImage:[UIImage imageNamed:@"menu-invite"] forState:UIControlStateNormal];
    }
    if (index==11) {
        [button setImage:[UIImage imageNamed:@"menu-invite"] forState:UIControlStateNormal];
    }
    if (index==12) {
        [button setImage:[UIImage imageNamed:@"menu-invite"] forState:UIControlStateNormal];
    }
    if ([[MenuArray objectAtIndex:index-1] objectForKey:@"Image"]!=nil) {
        [button setImage:[UIImage imageNamed:[[MenuArray objectAtIndex:index-1] objectForKey:@"Image"]] forState:UIControlStateNormal];
    }
    if (button.imageView.image) {
        return button;
    }
    return button;
}


- (void) radialMenu:(ALRadialMenu *)radialMenu didSelectItemAtIndex:(NSInteger)index {
    
    [self.delegate selectedFunc:[MenuArray objectAtIndex:index-1]];
//    if (index==1) {
//        [self performSegueWithIdentifier:@"memberGroup" sender:nil];
//    }else{
//        //  [self.radialMenu itemsWillDisapearIntoButton:self.button];
//    }
    
}

- (BOOL)radialMenu:(ALRadialMenu *)radialMenu shouldRotateMenuButtonWhenItemsAppear:(UIButton *)button {
    if (radialMenu == self.radialMenu) {
        return YES;
    }
    
    return NO;
}

- (BOOL)radialMenu:(ALRadialMenu *)radialMenu shouldRotateMenuButtonWhenItemsDisappear:(UIButton *)button {
    if (radialMenu == self.radialMenu) {
        return NO;
    }
    
    return YES;
}
- (NSString *) GetTitleForRadialMenu:(NSInteger) index{
    NSString * Title = [[NSString alloc] init];
    
    
    Title =[[MenuArray objectAtIndex:index-1] objectForKey:@"Name"];
    
    return Title;
    
}
- (NSString *) radialMenu:(ALRadialMenu *)radialMenu BadgeForIndex:(NSInteger) index{
    if (NotificationObject!=nil) {
        
   
    NSString * ServerName = [[MenuArray objectAtIndex:index-1] objectForKey:@"ServerName"];
    return [[NotificationObject objectForKey:ServerName] stringValue];
    }else{
    
    return @"";
    }
    
}


/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
