//
//  GroupViewController.h
//  FamilyLocator
//
//  Created by Amira on 10/12/16.
//  Copyright © 2016 Amira. All rights reserved.

#import "WebServiceFunc.h"
#import <UIKit/UIKit.h>
@interface GroupViewController : UIViewController<UITableViewDelegate,UITableViewDataSource,WebServiceProtocol>
{
    __weak IBOutlet UITableView *tableView;
    
}
@end
