//
//  ShowListViewController.h
//  MIxFM
//
//  Created by agamil on 5/15/17.
//  Copyright © 2017 cyandh. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "WebServiceFunc.h"
#import "MBProgressHUD.h"

@interface NewsListViewController : UIViewController{
    
    IBOutlet UITableView * tableView;
    NSMutableArray * ListItems;
    NSMutableArray * OriginalListItems;
    WebServiceFunc * webService;
    MBProgressHUD *hud;
    UISearchController * NewsSearch;
    UISearchBar *searchBar;
    IBOutlet UILabel * NoResults;
    UITapGestureRecognizer *tapGesture;
}

@end
