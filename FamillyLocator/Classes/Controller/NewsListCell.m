//
//  ShowTableViewCell.m
//  MIxFM
//
//  Created by agamil on 5/15/17.
//  Copyright © 2017 cyandh. All rights reserved.
//

#import "NewsListCell.h"
#import "constant.h"

@implementation NewsListCell

- (void)awakeFromNib {
    [super awakeFromNib];
    //    Content.layer.cornerRadius = 8;
    //    Content.layer.masksToBounds = YES;
    //    CellImage.layer.cornerRadius = 8;
    //    CellImage.layer.masksToBounds = YES;
    //
    //    titleView.layer.cornerRadius = 8;
    //    titleView.layer.masksToBounds = YES;
    
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}
-(void)loadCellData:(NSDictionary* )CellData{
    [CellImage setImageURL:nil];
    if ([CellData objectForKey:@"urlToImage"] !=nil) {
        
        
        NSURL *imgurl=[NSURL URLWithString:[CellData objectForKey:@"urlToImage"]];
        
        
        
        
        
        [CellImage setImageURL:imgurl];
    }
    //"publishedAt":"2017-08-13T05:04:25Z"
    NSDateFormatter * dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss'Z'"];
    NSDate *articalDate =[dateFormatter dateFromString:[CellData objectForKey:@"publishedAt"]];
    NSTimeInterval timeInterval = [articalDate timeIntervalSinceNow];
    
    int seconds = -(int)timeInterval;
    int hours = seconds/3600;
    DescriptionLbl.text =[NSString stringWithFormat:@"%@ %i hours ago",[CellData objectForKey:@"author"],hours];
    titleLbl.text =[CellData objectForKey:@"title"];
    
}
@end
