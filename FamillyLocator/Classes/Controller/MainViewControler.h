//
//  MainViewControler.h
//  wiizme
//
//  Created by agamil on 5/18/17.
//  Copyright © 2017 Amira. All rights reserved.
//

#import "ViewController.h"

@interface MainViewControler : ViewController{

    UITableView * CurrentTableView;
    UIScrollView * CurrentScrollview;
        UITapGestureRecognizer *tapGesture;
    UITextView * currentTextView;
    UITextField * CurrentTextField;
    UIDatePicker * CurrentDatePicker;

}
-(void)RegisterkeyboardObservers;
- (void)keyboardWillHide:(NSNotification *)notification;
- (void)keyboardWillShow:(NSNotification *)notification;
-(void)StouchesBegan;
@end
