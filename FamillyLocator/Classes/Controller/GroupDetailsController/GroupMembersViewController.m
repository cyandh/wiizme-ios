//
//  GroupMembersViewController.m
//  FamillyLocator
//
//  Created by Amira on 11/1/16.
//  Copyright © 2016 Amira. All rights reserved.
//

#import "GroupMembersViewController.h"
#import "WebServiceFunc.h"
#import "GroupMembersTableViewCell.h"
#import "MBProgressHUD.h"
#import "Constant.h"
#import "GroupData.h"
#import "Animation.h"
#import "HomeMemberViewController.h"
#import "MemberInfoViewController.h"
#import "UIViewController+RemoteNotificationData.h"
#import "SideMenuViewController.h"
#import "SideMenuViewController.h"
@interface GroupMembersViewController ()
{
    WebServiceFunc *webService;
    MBProgressHUD *hud;
    NSMutableArray *members,*admins;
    int memberIndx,serviceNUM;
    NSIndexPath *selectedIndex;
}
@end

@implementation GroupMembersViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [Animation roundCornerForView:done withAngle:15.0F];
    [Animation roundCornerForView:groupImg withAngle:30.0f];


}
-(void)viewDidDisappear:(BOOL)animated{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

-(void)viewWillAppear:(BOOL)animated{
    if (self.forAdmins) {
        [done setHidden:NO];
    }else{
        [done setHidden:YES];
    }
    admins=[[NSMutableArray alloc]init];
    webService=[[WebServiceFunc alloc]init];
    serviceNUM=1;
    [webService setDelegate:self];
    hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    hud.mode = MBProgressHUDModeIndeterminate;
    [groupImg setImageURL:[NSURL URLWithString:self.groupData.profileImg]];
    groupName.text=self.groupData.groupName;
    NSString *urlString=[NSString stringWithFormat:@"%@%@",GroupMembers,self.groupData.groupID];
    [webService getData:urlString withflagLogin:YES];
}
- (IBAction)showMenu
{
    
    SideMenuViewController *sideMenu=[self.storyboard instantiateViewControllerWithIdentifier:@"SideMenu"];
    [self.navigationController presentViewController:sideMenu animated:YES completion:nil];
}

#pragma TableView DataSource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return members.count;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    GroupMembersTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"groupmembers"];
    cell.memberName.text=[NSString stringWithFormat:@"%@ %@",[[members objectAtIndex:indexPath.row] objectForKey:@"firstName"],[[members objectAtIndex:indexPath.row] objectForKey:@"familyName"]];
    NSURL *imgurl=[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",PhotosUrl,[[members objectAtIndex:indexPath.row] objectForKey:@"photoId"]]];
    [cell.memberImg setImageURL:imgurl];
    cell.memberImg.layer.cornerRadius = 30.0f;
    cell.memberImg.clipsToBounds = YES;
    
    BOOL admin =[[[members objectAtIndex:indexPath.row] objectForKey:@"isAdmin"] boolValue];
    if (self.forAdmins) {
        if (admin) {
          cell.Admin.hidden=YES;
        }else{
        cell.Admin.hidden=NO;
        
        [cell.Admin addTarget:self action:@selector(addAdmins:)  forControlEvents:UIControlEventTouchUpInside];
        [cell.Admin setTag:indexPath.row];
        }
    }else{
        cell.Admin.hidden=YES;
    }
    //  [cell loadNewsImagewithLabelString:videotitle andDate:videoDateFormate];
    
    return cell;
}
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    // This will create a "invisible" footer
    return 0.01f;
}


- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    return [UIView new];
    
}



#pragma TableView Delegate

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    selectedIndex=indexPath;
    BOOL relation=[[[members objectAtIndex:indexPath.row] objectForKey:@"hasRelation"] boolValue];
    if (relation) {
        [self performSegueWithIdentifier:@"homemember" sender:nil];
    }else{
        MemberInfoViewController *info=[self.storyboard instantiateViewControllerWithIdentifier:@"MemberInfoController"];
        info.userData=[[UserData alloc]init];
        info.userData.name=[NSString stringWithFormat:@"%@ %@",[[members objectAtIndex:indexPath.row] objectForKey:@"firstName"],[[members objectAtIndex:indexPath.row] objectForKey:@"familyName"]];;
        info.userData.userImage=[[members objectAtIndex:indexPath.row] objectForKey:@"photoId"];
        info.userData.age=[[[members objectAtIndex:indexPath.row] objectForKey:@"age"]stringValue];
        info.userData.city=[[members objectAtIndex:indexPath.row] objectForKey:@"currentCity"];
        BOOL gender=[[[members objectAtIndex:indexPath.row] objectForKey:@"gender"] boolValue];
        if (gender) {
            info.userData.gender=@"Male";
        }else{
            info.userData.gender=@"Female";
        }
        info.userData.job=[[members objectAtIndex:indexPath.row] objectForKey:@"job"];
        info.userData.friendsNum=[[[members objectAtIndex:indexPath.row] objectForKey:@"friendsNumber"] stringValue];
        info.userData.groupsNum=[[[members objectAtIndex:indexPath.row] objectForKey:@"groupsNumber"] stringValue];
        NSLog(@"rr%@",info.userData);
        [self.navigationController pushViewController:info animated:YES];
    }
    
}


-(void)userWebserviceCallDidSuccessWithRespone:(NSMutableArray *)response{
    [hud setHidden:YES];
    if (serviceNUM==1) {
        
        members=response;
        [tableView reloadData];
    }else if (serviceNUM==2) {
        [self.myDelegate addDiminControllerDismissed:NO];
       [self.navigationController popViewControllerAnimated:YES];
    }
    
    
}
-(void)userWebserviceCallDidFaildwithError:(NSError *)error andMessage:(NSString *)message{
    [hud setHidden:YES];
    NSLog(@"%@",error.localizedDescription);
    if ([message isEqualToString:@"401" ]) {
        UIStoryboard *storyboard=[UIStoryboard storyboardWithName:@"Main" bundle:nil];
        UIViewController* viewController=[storyboard instantiateViewControllerWithIdentifier:@"LoginController"];
        [self presentViewController:viewController animated:YES completion:nil];
    }
    
}

-(IBAction)addAdmins:(id)sender{
    UIButton *but=sender;
    memberIndx=but.tag;
    
        if (but.selected) {
            [but setSelected:NO];
            NSString *memid=[[members objectAtIndex:memberIndx] objectForKey:@"id"];
            for (int i=0; i<admins.count; i++) {
                NSString *insid=[[admins objectAtIndex:i] objectForKey:@"id"];
                if ([memid isEqualToString:insid]) {
                    [admins removeObjectAtIndex:i];
                    break;
                }
            }
            
        }else{
            [but setSelected:YES];
            [admins addObject:[[members objectAtIndex:memberIndx] objectForKey:@"id"]];
        }
    
    

}

-(IBAction)sendadmins:(id)sender{
    webService=[[WebServiceFunc alloc]init];
    [webService setDelegate:self];
    hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    hud.mode = MBProgressHUDModeIndeterminate;
    serviceNUM=2;
    
    NSMutableDictionary *userinfo = [[NSMutableDictionary alloc] initWithObjectsAndKeys:self.groupData.groupID,@"GroupId",
        admins,@"Users",nil];
    NSLog(@"%@",userinfo);
    
    [webService postDataWithUrlString:addadmin withData:userinfo withflagLogin:YES];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    HomeMemberViewController *home=[segue destinationViewController];
    home.userData=[[UserData alloc]init];

        home.userData.name=[NSString stringWithFormat:@"%@ %@",[[members objectAtIndex:selectedIndex.row] objectForKey:@"firstName"],[[members objectAtIndex:selectedIndex.row] objectForKey:@"familyName"]];
        home.userData.userImage=[[members objectAtIndex:selectedIndex.row] objectForKey:@"photoId"];
        home.userData.fieldId=[[members objectAtIndex:selectedIndex.row] objectForKey:@"id"];
    
}


@end
