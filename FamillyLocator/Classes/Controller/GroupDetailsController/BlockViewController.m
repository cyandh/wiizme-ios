//
//  BlockViewController.m
//  FamillyLocator
//
//  Created by Amira on 12/6/16.
//  Copyright © 2016 Amira. All rights reserved.
//

#import "BlockViewController.h"
#import "GroupMembersTableViewCell.h"
#import "MBProgressHUD.h"
#import "Constant.h"
#import "GroupData.h"
#import "Animation.h"
#import "HomeMemberViewController.h"
#import "UIViewController+RemoteNotificationData.h"
#import "SideMenuViewController.h"
@interface BlockViewController ()
{
    WebServiceFunc *webService;
    MBProgressHUD *hud;
    NSMutableArray *blockUsers,*unblockUsers;
    int memberIndx,serviceNUM;
    UITapGestureRecognizer *tapGesture;
    NSIndexPath *selectedIndex;
}
@end

@implementation BlockViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
  
    
    [Animation roundCornerForView:groupImg withAngle:30.0f];
    
    tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(memberProfile:)];
    
    
    [tapGesture setDelegate:self];
}

- (IBAction)showMenu
{
    
    SideMenuViewController *sideMenu=[self.storyboard instantiateViewControllerWithIdentifier:@"SideMenu"];
    [self.navigationController presentViewController:sideMenu animated:YES completion:nil];
}

-(void)viewDidDisappear:(BOOL)animated{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}


-(void)viewWillAppear:(BOOL)animated{
    webService=[[WebServiceFunc alloc]init];
    serviceNUM=1;
    [webService setDelegate:self];
    hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    hud.mode = MBProgressHUDModeIndeterminate;
    [groupImg setImageURL:[NSURL URLWithString:self.groupData.profileImg]];
    groupName.text=self.groupData.groupName;
    NSString *urlString=[NSString stringWithFormat:@"%@%@",GroupBlockMembers,self.groupData.groupID];
    [webService getData:urlString withflagLogin:YES];
    
    
}

#pragma TableView DataSource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
        return 2;
    
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{

        if (section==0) {
            return blockUsers.count;
        }else{
            return unblockUsers.count;
        }
    
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    GroupMembersTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"groupmembers"];
    NSURL *imgurl;
    if (indexPath.section==0) {
        cell.memberName.text=[NSString stringWithFormat:@"%@ %@",[[blockUsers objectAtIndex:indexPath.row] objectForKey:@"firstName"],[[blockUsers objectAtIndex:indexPath.row] objectForKey:@"familyName"]];
        imgurl=[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",PhotosUrl,[[blockUsers objectAtIndex:indexPath.row] objectForKey:@"photoId"]]];
        [cell.blockUser addTarget:self action:@selector(blockUser:)  forControlEvents:UIControlEventTouchUpInside];
        [cell.blockUser setTag:indexPath.row];
        [cell.blockUser setTitle:@"UnBlock" forState:UIControlStateNormal];
        [cell.blockUser setBackgroundColor:[UIColor colorWithRed:47.0/255.0 green:61.0/255.0 blue:70.0/255.0 alpha:1.0f]];
    }else{
        
    cell.memberName.text=[NSString stringWithFormat:@"%@ %@",[[unblockUsers objectAtIndex:indexPath.row] objectForKey:@"firstName"],[[unblockUsers objectAtIndex:indexPath.row] objectForKey:@"familyName"]];
    imgurl=[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",PhotosUrl,[[unblockUsers objectAtIndex:indexPath.row] objectForKey:@"photoId"]]];
         BOOL admin =[[[unblockUsers objectAtIndex:indexPath.row] objectForKey:@"isAdmin"] boolValue];
        if (admin) {
            [cell.blockUser setHidden:YES];
        }else{
            [cell.blockUser setHidden:NO];
        [cell.blockUser addTarget:self action:@selector(unBlockUser:)  forControlEvents:UIControlEventTouchUpInside];
        [cell.blockUser setTag:indexPath.row];
        [cell.blockUser setTitle:@"Block" forState:UIControlStateNormal];
        [cell.blockUser setBackgroundColor:[UIColor colorWithRed:6.0/255.0 green:188.0/255.0 blue:228.0/255.0 alpha:1.0f]];
        }
    }
    [cell.memberImg setImageURL:imgurl];
    [cell.memberImg setUserInteractionEnabled:YES];
   // [cell.memberImg addGestureRecognizer:tapGesture];
    cell.memberImg.layer.cornerRadius = 30.0f;
    cell.memberImg.clipsToBounds = YES;
    [cell.memberImg setTag:indexPath.row];
    [Animation roundCornerForView:cell.blockUser withAngle:25.0F];
    
    
    
    return cell;
}
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    // This will create a "invisible" footer
    return 0.01f;
}


- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    return [UIView new];
    
}


#pragma TableView Delegate
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    selectedIndex=indexPath;
    [self performSegueWithIdentifier:@"memberList" sender:nil];
}


-(void)userWebserviceCallDidSuccessWithRespone:(NSMutableArray *)response{
    [hud setHidden:YES];
    if (serviceNUM==1) {
        if ([response isKindOfClass:[NSDictionary class]]) {
            NSDictionary * Dictionary =(NSDictionary *)response;
            blockUsers=[Dictionary objectForKey:@"blockedUsers"];
            unblockUsers=[Dictionary objectForKey:@"unBlockedUsers"];
        }
        [tableView reloadData];
    }else if (serviceNUM==2) {
        
    }
    
    
}
-(void)userWebserviceCallDidFaildwithError:(NSError *)error andMessage:(NSString *)message{
    [hud setHidden:YES];
    NSLog(@"%@",error.localizedDescription);
    if ([message isEqualToString:@"401" ]) {
        UIStoryboard *storyboard=[UIStoryboard storyboardWithName:@"Main" bundle:nil];
        UIViewController* viewController=[storyboard instantiateViewControllerWithIdentifier:@"LoginController"];
        [self presentViewController:viewController animated:YES completion:nil];
    }
    
}


-(IBAction)blockUser:(id)sender{
    UIButton *but=sender;
    memberIndx=but.tag;
    NSString *memID=[[blockUsers objectAtIndex:memberIndx]objectForKey:@"id"];
    
    webService=[[WebServiceFunc alloc]init];
    serviceNUM=3;
    [webService setDelegate:self];
    hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    hud.mode = MBProgressHUDModeIndeterminate;
    [groupImg setImageURL:[NSURL URLWithString:self.groupData.profileImg]];
    groupName.text=self.groupData.groupName;
    NSString *urlString=[NSString stringWithFormat:@"%@/group/usergroup/BlockUser?id=%@&groupId=%@",MainAPi,memID,self.groupData.groupID];
    [webService getData:urlString withflagLogin:YES];
}

-(IBAction)unBlockUser:(id)sender{
    UIButton *but=sender;
    memberIndx=but.tag;
    NSString *memID=[[unblockUsers objectAtIndex:memberIndx]objectForKey:@"id"];
    
    webService=[[WebServiceFunc alloc]init];
    serviceNUM=3;
    [webService setDelegate:self];
    hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    hud.mode = MBProgressHUDModeIndeterminate;
    [groupImg setImageURL:[NSURL URLWithString:self.groupData.profileImg]];
    groupName.text=self.groupData.groupName;
    NSString *urlString=[NSString stringWithFormat:@"%@/group/usergroup/BlockUser?id=%@&groupId=%@",MainAPi,memID,self.groupData.groupID];
    [webService getData:urlString withflagLogin:YES];
}

-(IBAction)memberProfile:(id)sender{
    [self performSegueWithIdentifier:@"memberList" sender:nil];
}



 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
     HomeMemberViewController *home=[segue destinationViewController];
     home.userData=[[UserData alloc]init];
     if (selectedIndex.section==0) {
         home.userData.name=[NSString stringWithFormat:@"%@ %@",[[blockUsers objectAtIndex:selectedIndex.row] objectForKey:@"firstName"],[[blockUsers objectAtIndex:selectedIndex.row] objectForKey:@"familyName"]];
         home.userData.userImage=[[blockUsers objectAtIndex:selectedIndex.row] objectForKey:@"photoId"];
         home.userData.fieldId=[[blockUsers objectAtIndex:selectedIndex.row] objectForKey:@"id"];
         
     }else{
         home.userData.name=[NSString stringWithFormat:@"%@ %@",[[unblockUsers objectAtIndex:selectedIndex.row] objectForKey:@"firstName"],[[unblockUsers objectAtIndex:selectedIndex.row] objectForKey:@"familyName"]];
         home.userData.userImage=[[unblockUsers objectAtIndex:selectedIndex.row] objectForKey:@"photoId"];
         home.userData.fieldId=[[unblockUsers objectAtIndex:selectedIndex.row] objectForKey:@"id"];
     }
     
     
 }


@end
