//
//  GroupMembersViewController.h
//  FamillyLocator
//
//  Created by Amira on 11/1/16.
//  Copyright © 2016 Amira. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "WebServiceFunc.h"
#import "AsyncImageView.h"
#import "GroupData.h"
@protocol AddAdminDelegate <NSObject>
-(void) addDiminControllerDismissed:(BOOL) onlyadmin;
@end


@interface GroupMembersViewController : UIViewController<UITableViewDelegate,UITableViewDataSource,WebServiceProtocol>
{
    __weak IBOutlet UITableView *tableView;
    __weak IBOutlet AsyncImageView *groupImg;
    __weak IBOutlet UILabel *groupName;
    __weak IBOutlet UIButton *done;
}
@property (nonatomic, assign) id<AddAdminDelegate> myDelegate;
@property(nonatomic,strong)GroupData *groupData;
@property(nonatomic) BOOL forAdmins;
@end
