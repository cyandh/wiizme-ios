//
//  MeetThereViewController.h
//  FamillyLocator
//
//  Created by Amira on 10/19/16.
//  Copyright © 2016 Amira. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "WebServiceFunc.h"
@interface MeetThereViewController : UIViewController<UITableViewDelegate,UITableViewDataSource,UIScrollViewDelegate,WebServiceProtocol>
{
    __weak IBOutlet UITableView *tableView;
}
@property(nonatomic,strong)NSString *listID;
@property(nonatomic,strong)NSString *listName;
@property(nonatomic)BOOL isUser;
@end
