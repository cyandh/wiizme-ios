//
//  PolicyViewController.h
//  FamillyLocator
//
//  Created by Amira on 12/7/16.
//  Copyright © 2016 Amira. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GroupData.h"

@protocol PopviewDelegate <NSObject>

@required

- (void)CloseView;
-(void)accept:(GroupData * )groupData;

@end

@interface PolicyViewController : UIViewController

@property(nonatomic)  IBOutlet UITextView * policyTXT;
@property(nonatomic)  IBOutlet UIView * popView;
@property(nonatomic)  IBOutlet UIButton * accept;
@property (nonatomic, weak) id <PopviewDelegate> delegate;
@property(nonatomic,strong) GroupData * groupData;
@end
