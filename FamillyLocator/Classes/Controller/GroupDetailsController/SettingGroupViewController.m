//
//  SettingGroupViewController.m
//  FamilyLocator
//
//  Created by Amira on 10/15/16.
//  Copyright © 2016 Amira. All rights reserved.
//

#import "SettingGroupViewController.h"
#import "UIViewController+HeaderView.h"
#import "Animation.h"
#import "MBProgressHUD.h"
#import "Constant.h"
#import "GroupMembersViewController.h"
#import "RequestsViewController.h"
#import "AddNewGroupViewController.h"
#import "BlockViewController.h"
#import "PolicyViewController.h"
#import "UIViewController+RemoteNotificationData.h"
#import "SideMenuViewController.h"
#import "SearchViewController.h"
@interface SettingGroupViewController ()
{
    NSArray *tableData;
    WebServiceFunc *webService;
    MBProgressHUD *hud;
    BOOL forAdmin,forRequest;
    int joinGroup;
    PolicyViewController * policyView;
    NSDictionary *groupdata;
    
}
@end

@implementation SettingGroupViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self updateLayout];

    
    if(self.fromNotification){
        joinGroup=10;
        webService=[[WebServiceFunc alloc]init];
        [webService setDelegate:self];
        hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        hud.mode = MBProgressHUDModeIndeterminate;
        NSString *urlString=[NSString stringWithFormat:@"%@%@",GroupDataUrl,self.groupData.groupID];
        [webService getData:urlString withflagLogin:YES];

    }else{
        [self loadviewData];
    }
    
}

-(void)loadviewData{
    
    if (self.groupData.member) {
        if (self.groupData.pending) {
            [self popupview];
        }else{
            settingBtn.hidden=NO;
            joinBtn.hidden=YES;
        }
        
        
    }else if (self.groupData.pending){
        joinGroup=1;//request
        joinBtn.hidden=NO;
        settingBtn.hidden=YES;
        [joinBtn setTitle:@"cancel Request" forState:UIControlStateNormal];
        
    }else{
        joinGroup=2;//join
        settingBtn.hidden=YES;
        joinBtn.hidden=NO;
        
        [joinBtn setTitle:@"Join Group" forState:UIControlStateNormal];
    }
    
    [coverImg setImageURL:[NSURL URLWithString:self.groupData.coverImg]];
    [grooupImg setImageURL:[NSURL URLWithString:self.groupData.profileImg]];
    groupName.text=self.groupData.groupName;

        if (![self.groupData.activityType isKindOfClass:[NSNull class]]) {
           groupType.text=self.groupData.activityType;
        }

    groupPrivacy.text=self.groupData.privacy;
    descTXT.text=self.groupData.desc;
    [memberNum setTitle:self.groupData.groupNum forState:UIControlStateNormal];
}
-(void)viewDidDisappear:(BOOL)animated{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

-(void)viewWillAppear:(BOOL)animated{
    self.table.hidden=YES;
}
-(void)updateLayout{
    
    [Animation roundCornerForView:grooupImg withAngle:5.0F];
    [Animation roundCornerForView:memberNum withAngle:15.0f];
    [Animation roundCornerForView:joinBtn withAngle:15.0f];
    
    self.table.hidden=YES;
    
    [self.table.layer setShadowColor:[[UIColor darkGrayColor] CGColor]];
    [self.table.layer setShadowOffset:CGSizeMake(0, 0)];
    [self.table.layer setShadowRadius:200.0];
    [self.table.layer setShadowOpacity:1];
    
    
    self.table.clipsToBounds = NO;
    self.table.layer.masksToBounds = NO;
    
}

-(void)popupview{
    
    if (self.groupData.pending) {
        
        policyView =[self.storyboard instantiateViewControllerWithIdentifier:@"PolicyView"];
        
        //  policyView.view.frame = CGRectMake(0, 80, policyView.popView.frame.size.width, policyView.popView.frame.size.height);
        
        //  [self.view addSubview:policyView.view];
        
        policyView.delegate=self;
        policyView.policyTXT.text =self.groupData.policy;
        
        policyView.groupData =self.groupData;
        
        policyView.popView.layer.shadowColor = [UIColor blackColor].CGColor;
        policyView.popView.layer.shadowOffset = CGSizeMake(0,0);
        [policyView.popView.layer setShadowRadius:50.0];
        
        policyView.popView.layer.shadowOpacity =1;
        policyView.popView.clipsToBounds = NO;
        policyView.popView.layer.masksToBounds = NO;
        
        
        
        policyView.modalPresentationStyle = UIModalPresentationOverCurrentContext;
        [self.navigationController presentViewController:policyView animated:NO completion:nil];
    }
    
}

- (IBAction)showMenu
{
    
    SideMenuViewController *sideMenu=[self.storyboard instantiateViewControllerWithIdentifier:@"SideMenu"];
    [self.navigationController presentViewController:sideMenu animated:YES completion:nil];
}


- (IBAction)settingBTN:(id)sender {
    if ([self.table isHidden]) {
        self.table.hidden=NO;
        [self.table reloadData];
    }else{
       self.table.hidden=YES ;
    }
    
    
}
- (IBAction)memberNum:(id)sender {
    forAdmin=NO;
    [self performSegueWithIdentifier:@"groupmembers" sender:nil];
}




- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)CloseView{
    joinGroup=5;
    [self dismissViewControllerAnimated:YES completion:nil];
    webService=[[WebServiceFunc alloc]init];
    [webService setDelegate:self];
    hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    hud.mode = MBProgressHUDModeIndeterminate;
    NSString *urlString=[NSString stringWithFormat:@"%@%@",LeaveGroup,self.groupData.groupID];
    [webService getData:urlString withflagLogin:YES];
}
-(void)accept:(GroupData *)groupData{
    joinGroup=4;
    [self dismissViewControllerAnimated:YES completion:nil];
    webService=[[WebServiceFunc alloc]init];
    [webService setDelegate:self];
    hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    hud.mode = MBProgressHUDModeIndeterminate;
    NSString *urlString=[NSString stringWithFormat:@"%@%@",acceptpolicy,self.groupData.groupID];
    [webService getData:urlString withflagLogin:YES];
    
    
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {

    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (self.groupData.admin) {
        if (!self.groupData.onlyAdmin) {
           tableData=@[@"Block User",@"Roles",@"Requests",@"Leave Group",@"Set A Member As Admin"];
        }else{
           tableData=@[@"Block User",@"Roles",@"Requests",@"Delete Group",@"Set A Member As Admin"];
        }
        
        return 5;

    }else{
         tableData=@[@"Leave Group",@"Mute"];
       return 2;
    }
    
    
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath

{

    return 30;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    
    static NSString *simpleTableIdentifier = @"TableView";
    NSLog(@"leftpanel call==================================================================================");
    UITableViewCell *cell=[tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    if (cell == nil)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:simpleTableIdentifier];
    }
    cell.textLabel.text=[tableData objectAtIndex:indexPath.row];
    [cell.textLabel setFont:[UIFont fontWithName:@"Cairo-Regular" size:11.0f]];

    cell.layer.shadowColor = [[UIColor clearColor] CGColor];
    cell.layer.shadowOpacity = 0.0;
    cell.layer.shadowOffset = CGSizeMake(0, 0);
    
    if (indexPath.row ==tableData.count-1) {
      //  cell.textLabel.textColor=[UIColor colorWithRed:6.0/255.0 green:188.0/255.0 blue:228.0/255.0 alpha:1.0f];
        cell.layer.shadowColor = [[UIColor blackColor] CGColor];
        cell.layer.shadowOpacity = 0.9;
        cell.layer.shadowOffset = CGSizeMake(0,2);
    }
    
    
        return cell;
    
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    // This will create a "invisible" footer
    return 0.01f;
}







#pragma mark - Table view delegate

// In a xib-based application, navigation from a table can be handled in -tableView:didSelectRowAtIndexPath:
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (self.groupData.admin) {
        if (!self.groupData.onlyAdmin) {
          //  tableData=@[@"Block User",@"Roles",@"Requests",@"Leave Group"];
            if (indexPath.row==0) {
                [self blockUser];
            }else if (indexPath.row==1){
                [self roles];
            }else if (indexPath.row==2){
                [self requstes];
            }else if (indexPath.row==3){
                [self leaveGroup];
            }else{
                [self addAdmin];
            }
        }else{
         //   tableData=@[@"Block User",@"Roles",@"Requests",@"Delete Group"];
            if (indexPath.row==0) {
                [self blockUser];
            }else if (indexPath.row==1){
                [self roles];
            }else if (indexPath.row==2){
                [self requstes];
            }else if (indexPath.row==3){
                [self deleteGroup];
            }else{
                [self addAdmin];
            }
        }
        
        
    }else{
    //    tableData=@[@"Leave Group",@"Mute"];
        if (indexPath.row==0) {
            [self leaveGroup];
        }else{
            [self muteGroup];
        }
    }
    
    self.table.hidden=YES;
   
}

-(void)blockUser{
    forRequest=NO;
    [self performSegueWithIdentifier:@"blockSegue" sender:nil];
}

-(void)roles{
    [self performSegueWithIdentifier:@"editGroup" sender:nil];
}

-(void)requstes{
    forRequest=YES;
     [self performSegueWithIdentifier:@"requestSegue" sender:nil];
}

-(void)addAdmin{
    forAdmin=YES;
     [self performSegueWithIdentifier:@"groupmembers" sender:nil];
    
}
- (IBAction)joinGroup:(id)sender {
    // 1 request    2 join
    if (joinGroup==1) {
        [self leaveGroup];
    }else if (joinGroup==2){
        webService=[[WebServiceFunc alloc]init];
        [webService setDelegate:self];
        hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        hud.mode = MBProgressHUDModeIndeterminate;
        NSString *urlString=[NSString stringWithFormat:@"%@%@",JoinGroup,self.groupData.groupID];
        [webService getData:urlString withflagLogin:YES];
    }
    
}

-(void)leaveGroup{
    joinGroup=5;
    webService=[[WebServiceFunc alloc]init];
    [webService setDelegate:self];
    hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    hud.mode = MBProgressHUDModeIndeterminate;
    
    NSString *urlString=[NSString stringWithFormat:@"%@%@",LeaveGroup,self.groupData.groupID];
    [webService getData:urlString withflagLogin:YES];
}

-(void)deleteGroup{
    joinGroup=5;
    webService=[[WebServiceFunc alloc]init];
    [webService setDelegate:self];
    hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    hud.mode = MBProgressHUDModeIndeterminate;
    
    NSString *urlString=[NSString stringWithFormat:@"%@%@",DeleteGroup,self.groupData.groupID];
    [webService getData:urlString withflagLogin:YES];
}

-(void)muteGroup{
    joinGroup=4;
    webService=[[WebServiceFunc alloc]init];
    [webService setDelegate:self];
    hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    hud.mode = MBProgressHUDModeIndeterminate;
    NSString *urlString=[NSString stringWithFormat:@"%@%@",MuteGroup,self.groupData.groupID];
    [webService getData:urlString withflagLogin:YES];
}



-(void)segueData{
    self.groupData=[[GroupData alloc]init];
    
    self.groupData.member=[[groupdata objectForKey:@"isMember"] boolValue];
    self.groupData.admin=[[groupdata objectForKey:@"isAdmin"] boolValue];
    self.groupData.onlyAdmin=[[groupdata objectForKey:@"onlyAdmin"] boolValue];
    self.groupData.pending=[[groupdata objectForKey:@"isPending"] boolValue];
    self.groupData.coverImg=[NSString stringWithFormat:@"%@%@",PhotosUrl,[groupdata objectForKey:@"coverPhotoId"]];
    self.groupData.coverID=[groupdata objectForKey:@"coverPhotoId"];
    self.groupData.profileImg=[NSString stringWithFormat:@"%@%@",PhotosUrl,[groupdata objectForKey:@"logoId"]];
    self.groupData.logoID=[groupdata objectForKey:@"logoId"];
    self.groupData.groupName=[groupdata objectForKey:@"name"];
    
    NSString *security=[groupdata objectForKey:@"groupTypeId"];
    self.groupData.privacyId=security;
    if ([security isEqualToString:publicPrivacy]) {
        self.groupData.privacy=@"Public";
    }else if ([security isEqualToString:privatePrivacy]){
        self.groupData.privacy=@"Private";
    }else{
        self.groupData.privacy=@"VIP";
    }
    
    self.groupData.activityType=[groupdata objectForKey:@"activityType"];
    self.groupData.activityTypeId=[groupdata objectForKey:@"activityTypeId"];
    self.groupData.activities=[groupdata objectForKey:@"tags"];
    self.groupData.desc=[groupdata objectForKey:@"description"];
    self.groupData.policy=[groupdata objectForKey:@"policies"];
    self.groupData.groupID=[groupdata objectForKey:@"id"];
    int memNUM=[[groupdata objectForKey:@"groupMembersNumber"] intValue];
    if (memNUM<=1) {
        self.groupData.groupNum=[NSString stringWithFormat:@"%i %@",memNUM,@"Member"];
    }else{
        self.groupData.groupNum=[NSString stringWithFormat:@"%i %@",memNUM,@"Members"];
    };
    self.groupData.unSeenFeeds=[[groupdata objectForKey:@"unseenFeedsNumber"] integerValue];
    self.groupData.unSeenChat=[[groupdata objectForKey:@"unseenMessagesNumber"] integerValue];

    

}
-(void)addDiminControllerDismissed:(BOOL)onlyadmin{
    
    [self.groupData setValue:[NSNumber numberWithBool:onlyadmin] forKey:@"onlyAdmin"];
}

-(void)userWebserviceCallDidSuccessWithRespone:(NSMutableArray *)response{
    [hud setHidden:YES];
    if (joinGroup==2) {
        [joinBtn setTitle:@"cancel Request" forState:UIControlStateNormal];
        joinGroup=1;
    }else if (joinGroup==3){
        [self.navigationController popoverPresentationController];
    }else if(joinGroup==4){
        
    }else if(joinGroup==10){
        groupdata=(NSDictionary*)response;
        [self segueData];
        [self loadviewData];
    }else{
    [self.navigationController popToRootViewControllerAnimated:YES];
    }
    
}
-(void)userWebserviceCallDidFaildwithError:(NSError *)error andMessage:(NSString *)message{
    [hud setHidden:YES];
    NSLog(@"%@",error.localizedDescription);
    if ([message isEqualToString:@"401" ]) {
        UIStoryboard *storyboard=[UIStoryboard storyboardWithName:@"Main" bundle:nil];
        UIViewController* viewController=[storyboard instantiateViewControllerWithIdentifier:@"LoginController"];
        [self presentViewController:viewController animated:YES completion:nil];
    }
    
}



#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    if ([[segue identifier] isEqualToString:@"groupmembers"]) {
        GroupMembersViewController *memberView=[segue destinationViewController];
        [memberView setMyDelegate:self];
        memberView.groupData=[[GroupData alloc]init];
        memberView.groupData=self.groupData;
        if (forAdmin) {
            memberView.forAdmins=YES;

        }else{
            memberView.forAdmins=NO;

        }
        
    }else if([[segue identifier] isEqualToString:@"requestSegue"]){
        RequestsViewController *requestView=[segue destinationViewController];
        requestView.groupData=[[GroupData alloc]init];
        requestView.groupData=self.groupData;
    }else if ([[segue identifier] isEqualToString:@"editGroup"]){
        AddNewGroupViewController *addView=[segue destinationViewController];
        addView.groupData=[[GroupData alloc]init];
        addView.groupData=self.groupData;
        addView.editGroup=YES;
    }else if ([[segue identifier] isEqualToString:@"blockSegue"]){
        BlockViewController *blockView=[segue destinationViewController];
        blockView.groupData=[[GroupData alloc]init];
        blockView.groupData=self.groupData;
    }
}


@end
