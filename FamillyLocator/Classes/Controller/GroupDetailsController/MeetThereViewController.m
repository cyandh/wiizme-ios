//
//  MeetThereViewController.m
//  FamillyLocator
//
//  Created by Amira on 10/19/16.
//  Copyright © 2016 Amira. All rights reserved.
//

#import "MeetThereViewController.h"
#import "MeetThereTableViewCell.h"
#import "MBProgressHUD.h"
#import "Constant.h"
#import "AddMeetEventViewController.h"
#import "UIViewController+HeaderView.h"
#import "UIViewController+RemoteNotificationData.h"
#import "SideMenuViewController.h"
@interface MeetThereViewController ()
{
    NSMutableArray *tableData;
    WebServiceFunc *webService;
    MBProgressHUD *hud;
    int selectedRow,editView;
    
}
@end

@implementation MeetThereViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.


}

-(void)viewDidDisappear:(BOOL)animated{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}


-(void)viewDidAppear:(BOOL)animated{
 //   [tableView setContentOffset:CGPointMake(0,-64) animated:YES];
   // tableView.contentInset = UIEdgeInsetsMake(-45, 0, 0, 0);
    editView=0;
    webService=[[WebServiceFunc alloc]init];
    [webService setDelegate:self];
    hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    hud.mode = MBProgressHUDModeIndeterminate;
    NSString *urlString;
    if(self.isUser){
        urlString=[NSString stringWithFormat:@"%@%@",ListOfUserMeets,self.listID];
    }else{
       urlString=[NSString stringWithFormat:@"%@%@",ListOfGroupMeets,self.listID];
    }
    [webService getData:urlString withflagLogin:YES];
}


- (IBAction)showMenu
{
    
    SideMenuViewController *sideMenu=[self.storyboard instantiateViewControllerWithIdentifier:@"SideMenu"];
    [self.navigationController presentViewController:sideMenu animated:YES completion:nil];
}

-(void)userWebserviceCallDidSuccessWithRespone:(NSMutableArray *)response{
    [hud setHidden:YES];
    tableData=response;
    [tableView reloadData];
    
}
-(void)userWebserviceCallDidFaildwithError:(NSError *)error andMessage:(NSString *)message{
    [hud setHidden:YES];
    NSLog(@"%@",error.localizedDescription);
    if ([message isEqualToString:@"401" ]) {
        UIStoryboard *storyboard=[UIStoryboard storyboardWithName:@"Main" bundle:nil];
        UIViewController* viewController=[storyboard instantiateViewControllerWithIdentifier:@"LoginController"];
        [self presentViewController:viewController animated:YES completion:nil];
    }
    
}

#pragma TableView DataSource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return tableData.count;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    MeetThereTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"eventCell"];
    cell.eventName.text=[[tableData objectAtIndex:indexPath.row] objectForKey:@"title"];
    NSArray *arr=[[tableData objectAtIndex:indexPath.row]objectForKey:@"invitations"];
    cell.enviters.text=[self invitations:arr];
    NSDictionary *dic=[self editDateAndTime:[[tableData objectAtIndex:indexPath.row] objectForKey:@"from"]];
    cell.dateLBL.attributedText=[self attribut:@"event-calendar.png" andtxtName:[dic objectForKey:@"date"]];
    cell.timeLBL.attributedText=[self attribut:@"event-time.png" andtxtName:[dic objectForKey:@"time"]];

    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    // This will create a "invisible" footer
    return 0.01f;
}


- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    return [UIView new];
    
}


-(NSString*)invitations:(NSArray*)servArr{
    NSString *lblTXT=@"";
    for (int i=0; i<servArr.count; i++) {
        lblTXT=[lblTXT stringByAppendingString:[NSString stringWithFormat:@"%@,",[servArr objectAtIndex:i]]];
    }
    return lblTXT;
}

-(NSMutableAttributedString *)attribut:(NSString*)imgname andtxtName:(NSString*)txtName{
    NSTextAttachment *attachment = [[NSTextAttachment alloc] init];
    attachment.image = [UIImage imageNamed:imgname];
    
    NSAttributedString *attachmentString = [NSAttributedString attributedStringWithAttachment:attachment];
    NSAttributedString *mystring = [[NSAttributedString alloc]initWithString:[NSString stringWithFormat:@"  %@  ",txtName]];
    NSMutableAttributedString *myStringimg= [[NSMutableAttributedString alloc] initWithAttributedString:attachmentString];

    [myStringimg appendAttributedString:mystring];

    return myStringimg;
}

#pragma TableView Delegate

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    selectedRow=indexPath.row;
    editView=1;
    [self performSegueWithIdentifier:@"editEvent" sender:nil];
}

-(void)scrollViewDidScroll:(UIScrollView *)scrollView{
    
    if (scrollView.contentOffset.y < 45) {
        tableView.contentInset = UIEdgeInsetsMake(0, 0, 0, 0);
    }
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    AddMeetEventViewController *addEvent=[segue destinationViewController];
    if (editView==1) {
        addEvent.editView=YES;
        addEvent.eventData=[[NSDictionary alloc]init];
        addEvent.eventData=[tableData objectAtIndex:selectedRow];
    }else{
        addEvent.editView=NO;
        addEvent.inviterId=self.listID;
        addEvent.inviterName=self.listName;
        addEvent.isUser=self.isUser;
    }
    
}


@end
