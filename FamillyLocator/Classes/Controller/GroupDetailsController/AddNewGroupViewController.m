//
//  AddNewGroupViewController.m
//  FamilyLocator
//
//  Created by Amira on 10/13/16.
//  Copyright © 2016 Amira. All rights reserved.
//

#import "AddNewGroupViewController.h"
#import "Animation.h"
#import "GroupData.h"
#import "MBProgressHUD.h"
#import "Constant.h"
#import "Validation.h"
#import "UIViewController+RemoteNotificationData.h"
#import "SideMenuViewController.h"
#import "SearchViewController.h"
@interface AddNewGroupViewController ()
{
    UIImage *logoImg,*coverImg;
    int img;
    WebServiceFunc *webSevice;
    NSString *coverID,*logoID;
    NSMutableArray *imageObject;
    NSMutableArray *activityArr,*selectedactivityID;
    WebServiceFunc *service;
    MBProgressHUD *hud;
    NSString *selectedTypeId,*selectedTypeValue,*selectedActivityValue,*privacyId;
    int typeservice;
    int typepicker;
    BOOL updateimgs;
    
}
@end

@implementation AddNewGroupViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    
    typeservice=0;
    [self updateLayout];
    pickrView=[[UIPickerView alloc]init];
    
    if (self.editGroup) {
        typeservice=2;
        viewAction.text=@"Edit Your Group";
        groupName.text=self.groupData.groupName;
        [groupPrivacy setTitle:self.groupData.privacy forState:UIControlStateNormal];
        [groupPrivacy setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        privacyId=self.groupData.privacyId;
        if (![self.groupData.activityType isKindOfClass:[NSNull class]]) {
            [groupType setTitle:self.groupData.activityType forState:UIControlStateNormal];
        }
        
        [groupType setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        selectedTypeId=self.groupData.activityTypeId;
        DescTXT.text=self.groupData.desc;
        DescTXT.textColor=[UIColor blackColor];
        coverID=self.groupData.coverID;
        logoID=self.groupData.logoID;
        policyTXT.text=self.groupData.policy;
        policyTXT.textColor=[UIColor blackColor];
        selectedactivityID=[[NSMutableArray alloc]init];
        selectedActivityValue = @"";
        for (int i=0; i<self.groupData.activities.count; i++) {
            selectedActivityValue = [selectedActivityValue stringByAppendingFormat:@"%@,",[[self.groupData.activities objectAtIndex:i] objectForKey:@"value"]];
            [selectedactivityID addObject:[NSDictionary dictionaryWithObject:[[self.groupData.activities objectAtIndex:i]objectForKey:@"fieldId"] forKey:@"id"]];
        }
        [groupActivity setTitle:selectedActivityValue forState:UIControlStateNormal];
        [groupActivity setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    }else{
        viewAction.text=@"Add New Group";
    }
    
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(StouchesBegan)];
    
    [scrollview addGestureRecognizer:tapGesture];
}

-(void)viewDidDisappear:(BOOL)animated{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

-(void)RemoteNotification:(NSNotification*)notification{
    [self notificationData:notification.userInfo];
}

-(void)updateLayout{
    
    [Animation roundCornerForView:DescTXT withAngle:15.0f];
    [Animation roundCornerForView:policyTXT withAngle:15.0F];
    [Animation roundCornerForView:done withAngle:15.0F];
    [Animation roundCornerForView:groupType withAngle:15.0f];
    
    [self updateText:groupName];
    [self updateButton:groupPrivacy];
    [self updateButton:groupType];
    [self updateButton:groupActivity];
    
    [[DescTXT layer] setBorderWidth:1.0f];
    [[DescTXT layer] setBorderColor:[UIColor colorWithRed:128.0/255.0f green:130.0/255.0f blue:129.0/255.0f alpha:1.0f].CGColor];;
    [[policyTXT layer] setBorderWidth:1.0f];
    [[policyTXT layer] setBorderColor:[UIColor colorWithRed:128.0/255.0f green:130.0/255.0f blue:129.0/255.0f alpha:1.0f].CGColor];;
    
}

-(void)updateText:(UITextField*)textField{
    [Animation roundCornerForView:textField withAngle:15.0F];
    [[textField layer] setBorderWidth:1.0f];
    [[textField layer] setBorderColor:[UIColor colorWithRed:128.0/255.0f green:130.0/255.0f blue:129.0/255.0f alpha:1.0f].CGColor];
    
    UIView *paddingView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 5, 20)];
    textField.leftView = paddingView;
    textField.leftViewMode = UITextFieldViewModeAlways;
}

-(void)updateButton:(UIButton*)button{
    [Animation roundCornerForView:button withAngle:15.0f];
    [[button layer] setBorderWidth:1.0f];
    [[button layer] setBorderColor:[UIColor colorWithRed:128.0/255.0f green:130.0/255.0f blue:129.0/255.0f alpha:1.0f].CGColor];
    button.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
}

- (IBAction)showMenu
{
    
    SideMenuViewController *sideMenu=[self.storyboard instantiateViewControllerWithIdentifier:@"SideMenu"];
    [self.navigationController presentViewController:sideMenu animated:YES completion:nil];
}


-(IBAction)selectPrivacy:(id)sender{
    pickerData=@[@"Public",@"Private",@"VIP"];
    typepicker=1;
    [self addPicker];
    
}
-(IBAction)selectType:(id)sender{
    [groupActivity setTitle:@"" forState:UIControlStateNormal];
    [selectedactivityID removeAllObjects];
    typepicker=2;
    typeservice=1;
    hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    hud.mode = MBProgressHUDModeIndeterminate;
    
    service =[[WebServiceFunc alloc]init];
    [service setDelegate:self];
    NSString *urlString=[NSString stringWithFormat:@"%@/Group/ActivityType/list",MainAPi];
    [service getData:urlString withflagLogin:YES];
    
    
}


-(IBAction)selectActivity:(id)sender{
    if (typeservice==0) {
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Attention" message:@"Please Select Group Type First" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        
        [alert show];
    }else{
        hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        hud.mode = MBProgressHUDModeIndeterminate;
        
        service =[[WebServiceFunc alloc]init];
        [service setDelegate:self];
        NSString *urlString=[NSString stringWithFormat:@"%@/group/tag/list?id=%@",MainAPi,selectedTypeId];
        [service getData:urlString withflagLogin:YES];
        
    }
    
    
}

-(void)StouchesBegan {
    
    [groupName resignFirstResponder];
    [groupActivity resignFirstResponder];
    [groupType resignFirstResponder];
    [groupPrivacy resignFirstResponder];
    [DescTXT resignFirstResponder];
    [policyTXT resignFirstResponder];
    
}
-(void)addPicker{
    [self StouchesBegan];
    
    [pickrView setDelegate:self];
    [pickrView setDataSource:self];
    pickrView.frame=CGRectMake(0, self.view.frame.size.height-pickrView.frame.size.height, self.view.frame.size.width,pickrView.frame.size.height);
    pickrView.backgroundColor = [UIColor darkGrayColor];
    [pickrView setValue:[UIColor whiteColor] forKey:@"textColor"];
    [pickrView setBackgroundColor:[UIColor darkGrayColor]];
    
    
    
    pickrView.clipsToBounds = NO;
    pickrView.layer.masksToBounds = NO;
    [pickrView setUserInteractionEnabled:YES];
    UITapGestureRecognizer* gestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(pickerViewTapGestureRecognized:)];
    
    gestureRecognizer.delegate = self;
    gestureRecognizer.cancelsTouchesInView = NO;
    [pickrView addGestureRecognizer:gestureRecognizer];
    
    [self.view addSubview:pickrView];
    
    
}

#pragma mark - Array List

- (NSArray *)list
{
    NSMutableArray *activ=[[NSMutableArray alloc]init];
    for (int i=0; i<activityArr.count;i++) {
        [activ addObject:[[activityArr objectAtIndex:i] objectForKey:@"value"]];
    }
    return activ;
}

#pragma mark - LPPopupListViewDelegate

- (void)popupListView:(LPPopupListView *)popUpListView didSelectIndex:(NSInteger)index
{
    NSLog(@"popUpListView - didSelectIndex: %d", index);
}

- (void)popupListViewDidHide:(LPPopupListView *)popUpListView selectedIndexes:(NSIndexSet *)selectedIndexes
{
    
    if ([selectedIndexes count]==0) {
        
    }else{
        
        selectedactivityID=[[NSMutableArray alloc]init];
        self.selectedIndexes = [[NSMutableIndexSet alloc] initWithIndexSet:selectedIndexes];
        //[selectedIndexes get]
        selectedActivityValue = @"";
        [selectedIndexes enumerateIndexesUsingBlock:^(NSUInteger idx, BOOL *stop) {
            selectedActivityValue = [selectedActivityValue stringByAppendingFormat:@"%@,", [[self list] objectAtIndex:idx]];
            [selectedactivityID addObject:[NSDictionary dictionaryWithObject:[[activityArr objectAtIndex:idx]objectForKey:@"fieldId"] forKey:@"id"]];
            
        }];
        
        [groupActivity setTitle:selectedActivityValue forState:UIControlStateNormal];
        [groupActivity setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    }
    
}



- (IBAction)uploadCoverImg:(id)sender {
    img=1;
    UIImagePickerController *picker = [[UIImagePickerController alloc] init];
    picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    picker.delegate = self;
    [self presentViewController:picker animated:YES completion:nil];
}
- (IBAction)uploadLogoImg:(id)sender {
    img=2;
    UIImagePickerController *picker = [[UIImagePickerController alloc] init];
    picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    picker.delegate = self;
    [self presentViewController:picker animated:YES completion:nil];
}

- (void)imagePickerController:(UIImagePickerController *)picker
        didFinishPickingImage:(UIImage *)image
                  editingInfo:(NSDictionary *)editingInfo
{
    if (self.editGroup) {
        updateimgs=YES;
    }
    if (img==1) {
        coverImg=image;
        coverImgView.image=image;
        NSLog(@"%@",image);
    }else if (img==2){
        logoImg=image;
        logoImgView.image=image;
    }
    
    [self dismissViewControllerAnimated:YES completion:nil];
    
}

- (IBAction)sendData:(id)sender {
    if(![self validateData]){
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"" message:NSLocalizedString(@"Please Check Your Data Again and make sure to upload image",nil) delegate:nil cancelButtonTitle:@"ok" otherButtonTitles:nil];
        
        [alert show];
    }else{
        webSevice=[[WebServiceFunc alloc]init];
        [webSevice setDelegate:self];
        hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        hud.mode = MBProgressHUDModeIndeterminate;
        
        if ((updateimgs)||(!self.editGroup)) {
            
            imageObject=[[NSMutableArray alloc]init];
            GroupData *data=[[GroupData alloc]init];
            data.coverImg=[webSevice encodeToBase64String:coverImg];
            
            NSMutableDictionary *imageData = [[NSMutableDictionary alloc] initWithObjectsAndKeys:GroupCoverID,@"FileTypeId",data.coverImg,@"Path",nil];
            [imageObject addObject:imageData];
            
            data.profileImg=[webSevice encodeToBase64String:logoImg];
            
            imageData = [[NSMutableDictionary alloc] initWithObjectsAndKeys:GroupLogoID,@"FileTypeId",data.profileImg,@"Path",nil];
            [imageObject addObject:imageData];
            
            [webSevice updateUserImagewithimageobject:imageObject];
        }else{
            typeservice=3;
            if(selectedactivityID.count==0){
                selectedactivityID=[[NSMutableArray alloc]init];
            }
            [self.groupData.groupObj setValue:groupName.text forKey:@"name"];
            [self.groupData.groupObj setValue:privacyId forKey:@"groupTypeId"];
            [self.groupData.groupObj setValue:selectedTypeId forKey:@"activityTypeId"];
            [self.groupData.groupObj setValue:selectedactivityID forKey:@"tags"];
            [self.groupData.groupObj setValue:DescTXT.text forKey:@"description"];
            [self.groupData.groupObj setValue:policyTXT.text forKey:@"policies"];
            [self.groupData.groupObj setValue:coverID forKey:@"coverPhotoId"];
            [self.groupData.groupObj setValue:logoID forKey:@"logoId"];
            
            
            [webSevice postDataWithUrlString:EditGroup withData:self.groupData.groupObj withflagLogin:YES];
        }
        
    }
}


-(void)userWebserviceCallDidSuccessWithRespone:(NSMutableArray *)response{
    
    [hud setHidden:YES];
    if (typeservice==1) {
        pickerData=response;
        [self addPicker];
    }else if(typeservice==2){
        activityArr=response;
        float paddingTopBottom = 20.0f;
        float paddingLeftRight = 20.0f;
        
        CGPoint point = CGPointMake(paddingLeftRight, (self.navigationController.navigationBar.frame.size.height + paddingTopBottom) + paddingTopBottom);
        CGSize size = CGSizeMake((self.view.frame.size.width - (paddingLeftRight * 2)), self.view.frame.size.height - ((self.navigationController.navigationBar.frame.size.height + paddingTopBottom) + (paddingTopBottom * 2)));
        
        // LPPopupListView *listView = [[LPPopupListView alloc] initWithTitle:@"List View" list:[self list] selectedIndexes:self.selectedIndexes point:point size:size multipleSelection:YES];
        self.selectedIndexes=nil;
        LPPopupListView *listView=[[LPPopupListView alloc]initWithTitle:selectedTypeValue list:[self list] selectedIndexes:self.selectedIndexes point:point size:size multipleSelection:YES disableBackgroundInteraction:NO];
        listView.searchBar.hidden=YES;
        listView.delegate = self;
        
        [listView showInView:self.navigationController.view animated:YES];
        
    }else{
        [self.navigationController popToRootViewControllerAnimated:YES];
    }
    
}

-(void)userWebserviceCallDidFaildwithError:(NSError *)error andMessage:(NSString *)message{
    [hud setHidden:YES];
    NSLog(@"%@",error.localizedDescription);
    if ([message isEqualToString:@"401" ]) {
        UIStoryboard *storyboard=[UIStoryboard storyboardWithName:@"Main" bundle:nil];
        UIViewController* viewController=[storyboard instantiateViewControllerWithIdentifier:@"LoginController"];
        [self presentViewController:viewController animated:YES completion:nil];
    }
    
}

-(void)userImagesWebserviceCallDidSuccessWithRespone:(NSArray *)imageID{
    coverID=[imageID objectAtIndex:0];
    logoID=[imageID objectAtIndex:1];
    webSevice=[[WebServiceFunc alloc]init];
    [webSevice setDelegate:self];
    if(selectedactivityID.count==0){
        selectedactivityID=[[NSMutableArray alloc]init];
    }
    NSMutableDictionary *userData = [[NSMutableDictionary alloc] initWithObjectsAndKeys:groupName.text,@"name",
                                     privacyId,@"groupTypeId",
                                     selectedTypeId,@"activityTypeId",
                                     DescTXT.text,@"description",
                                     policyTXT.text,@"policies",
                                     coverID,@"coverPhotoId",
                                     logoID,@"logoId",
                                     selectedactivityID,@"tags",
                                     nil];
    typeservice=3;
    if (self.editGroup) {
        [self.groupData.groupObj setValue:groupName.text forKey:@"name"];
        [self.groupData.groupObj setValue:privacyId forKey:@"groupTypeId"];
        [self.groupData.groupObj setValue:selectedTypeId forKey:@"activityTypeId"];
        [self.groupData.groupObj setValue:selectedactivityID forKey:@"tags"];
        [self.groupData.groupObj setValue:DescTXT.text forKey:@"description"];
        [self.groupData.groupObj setValue:policyTXT.text forKey:@"policies"];
        [self.groupData.groupObj setValue:coverID forKey:@"coverPhotoId"];
        [self.groupData.groupObj setValue:logoID forKey:@"logoId"];
        
         [webSevice postDataWithUrlString:EditGroup withData:self.groupData.groupObj withflagLogin:YES];
    }else{
    [webSevice postDataWithUrlString:AddGroup withData:userData withflagLogin:YES];
    }
    
}
-(void)userImagesWebserviceCallDidFaildwithError:(NSError *)error andMessage:(NSString *)message{
    NSLog(@"%@",error.localizedDescription);
}




#pragma mark - Picker View delegate

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return 1;
}
// returns the # of rows in each component..
//the number of rows of data
- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    return  pickerData.count;
}

// these methods return either a plain NSString, a NSAttributedString, or a view (e.g UILabel) to display the row for the component.
// for the view versions, we cache any hidden and thus unused views and pass them back for reuse.
// If you return back a different object, the old one will be released. the view will be centered in the row rect

// The data to return for the row and component (column) that's being passed in
//- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
//{
//    return tableData[row];
//}

- (NSAttributedString *)pickerView:(UIPickerView *)pickerView attributedTitleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    NSAttributedString *attString;
    if (typepicker==1) {
        attString =
        [[NSAttributedString alloc] initWithString:pickerData[row] attributes:@{NSForegroundColorAttributeName:[UIColor whiteColor]}];
    }else if (typepicker==2){
        attString =
        [[NSAttributedString alloc] initWithString:[[pickerData objectAtIndex:row ]objectForKey:@"name"] attributes:@{NSForegroundColorAttributeName:[UIColor whiteColor]}];
    }
    
    
    return attString;
}
- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer{
    return YES;
}

-(void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
    
}

- (void)pickerViewTapGestureRecognized:(UITapGestureRecognizer*)gestureRecognizer
{
    CGPoint touchPoint = [gestureRecognizer locationInView:gestureRecognizer.view.superview];
    
    CGRect frame = pickrView.frame;
    CGRect selectorFrame = CGRectInset( frame, 0.0, pickrView.bounds.size.height * 0.85 / 2.0 );
    
    if( CGRectContainsPoint( selectorFrame, touchPoint) )
    {
        if (typepicker==1)
        {
            int row=(int)[pickrView selectedRowInComponent:0];
            if (row==0) {
                privacyId=publicPrivacy;
            }else if(row==1){
                privacyId=privatePrivacy;
            }else{
                privacyId=VIPPrivacy;
            }
            
            
            
            [ groupPrivacy setTitle:[pickerData objectAtIndex:row ]forState:UIControlStateNormal];
            [groupPrivacy setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
            
            
        }else if(typepicker==2){
            int row=(int)[pickrView selectedRowInComponent:0];
            typeservice=2;
            selectedTypeId=[[pickerData objectAtIndex:row ]objectForKey:@"id"];
            selectedTypeValue=[[pickerData objectAtIndex:row ]objectForKey:@"name"];
            [groupType setTitle:selectedTypeValue forState:UIControlStateNormal];
            [groupType setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
            
            
            
        }
        [pickrView removeFromSuperview];
        // [self HidePicker];
        
        
    }
    
}

-(BOOL)validateData
{
    if (![Validation validateusernamewithName:groupName.text]) {
        return false;
    }else if ((![Validation validateusernamewithName:DescTXT.text])||([DescTXT.text isEqualToString:@"Description"])){
        return false;
    }else if((![Validation validateusernamewithName:policyTXT.text])||([policyTXT.text isEqualToString:@"Group Policy"]))
    {
        return false;
    }else if(![Validation validateusernamewithName:privacyId]){
        return false;
    }else if (![Validation validateusernamewithName:selectedTypeId]){
        return false;
    }else if(!logoImg && !EditGroup){
        return false;
    }else if (!coverImg && !EditGroup){
        return false;
    }else{
        //    }else if(selectedactivityID.count==0){
        //        return false;
        //    }else{
        return true;
    }
    
}

-(void)textFieldDidBeginEditing:(UITextField *)textField{
    if (!pickrView.hidden) {
        [pickrView removeFromSuperview];
    }
}
- (void)textViewDidBeginEditing:(UITextView *)textView{
    if (!pickrView.hidden) {
        [pickrView removeFromSuperview];
    }
    if ([textView.text isEqualToString:@"Description"]) {
        textView.text=@"";
    }else if ([textView.text isEqualToString:@"Group Policy"]){
        textView.text=@"";
    }
    textView.textColor=[UIColor blackColor];
    [self animateTextView:textView up: YES];
}



- (void)textViewDidEndEditing:(UITextView *)textView
{
    [self animateTextView:textView up: NO];
}

- (void) animateTextView: (UITextView*) textView up: (BOOL) up
{
    
    
    const int movementDistance = 80; // tweak as needed
    const float movementDuration = 0.3f; // tweak as needed
    
    int movement = (up ? -movementDistance : movementDistance);
    
    [UIView beginAnimations: @"anim" context: nil];
    [UIView setAnimationBeginsFromCurrentState: YES];
    [UIView setAnimationDuration: movementDuration];
    self.view.frame = CGRectOffset(self.view.frame, 0, movement);
    [UIView commitAnimations];
}
//If you want the keyboard to hide when you press the return
- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    return YES;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
