//
//  HomeGroupViewController.m
//  FamilyLocator
//
//  Created by Amira on 10/15/16.
//  Copyright © 2016 Amira. All rights reserved.
//

#import "HomeGroupViewController.h"
#import "Animation.h"
#import "SettingGroupViewController.h"
#import "Constant.h"
#import "InviteViewController.h"
#import "GroupMembersViewController.h"
#import "MBProgressHUD.h"
#import "MeetThereViewController.h"
#import "NeedHelpViewController.h"
#import "FeedsViewController.h"
#import "WritePostViewController.h"
#import "MapKitViewController.h"
#import "MKNumberBadgeView.h"
#import "UIViewController+RemoteNotificationData.h"
#import "LiveTrackingViewController.h"
#import "UIViewController+HeaderView.h"
#import "SeeThisViewController.h"
#import "IamFine.h"
#import "ConversationViewController.h"
#import "ConversationModel.h"
#import "UserModel.h"
#import "IamFine.h"


@interface HomeGroupViewController (){
    PolicyViewController * policyView;
    WebServiceFunc *webService;
    MBProgressHUD *hud;
    int webservice;
    MKNumberBadgeView *number;
}
@end

@implementation HomeGroupViewController

- (void)viewDidLoad {
    [super viewDidLoad];
self.title=@"";
    
   [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(RemoveBadgeNum:) name:@"RemoveBadgeFeedsNumber" object:nil];
    
    groupName.text=self.groupData.groupName;
    
    NSURL *imageUrl=[NSURL URLWithString:self.groupData.profileImg];
    
    [groupImg setImageURL:imageUrl];
    
    [Animation roundCornerForView:groupImg withAngle:75.0f];

    self.radialMenu = [[ALRadialMenu alloc] init];
    
    self.radialMenu.delegate = self;
    
    [NSTimer scheduledTimerWithTimeInterval:0.5
                                     target:self
                                   selector:@selector(Tracehight)
                                   userInfo:nil
                                    repeats:NO];
}

-(void)RemoveBadgeNum:(NSNotification*)notification{
    
    number.value=0;
    
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    
}
-(void)viewDidDisappear:(BOOL)animated{
    
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    
}

-(void)viewWillAppear:(BOOL)animated{
    
    [self.navigationController setNavigationBarHidden:YES];
    
}
-(void)popupview{
    
    if (self.groupData.pending) {
        
        policyView =[self.storyboard instantiateViewControllerWithIdentifier:@"PolicyView"];
        
        //  policyView.view.frame = CGRectMake(0, 80, policyView.popView.frame.size.width, policyView.popView.frame.size.height);
        
        //  [self.view addSubview:policyView.view];
        
        policyView.delegate=self;
        
        policyView.policyTXT.text =self.groupData.policy;
        
        policyView.groupData =self.groupData;
        
        policyView.popView.layer.shadowColor = [UIColor blackColor].CGColor;
        
        policyView.popView.layer.shadowOffset = CGSizeMake(0,0);
        
        [policyView.popView.layer setShadowRadius:200.0];
        
        policyView.popView.layer.shadowOpacity =1;
        
        policyView.popView.clipsToBounds = NO;
        
        policyView.popView.layer.masksToBounds = NO;
        
        policyView.modalPresentationStyle = UIModalPresentationOverCurrentContext;
        
        [self.navigationController presentViewController:policyView animated:NO completion:nil];
    }
    
}
-(void)viewWillDisappear:(BOOL)animated{
    
    [self.navigationController setNavigationBarHidden:NO];
    
    
}
-(IBAction)closeView:(id)sender{
    
    [self.navigationController popViewControllerAnimated:YES];
    
}

-(IBAction)settingBtn:(id)sender{
    
    [self performSegueWithIdentifier:@"settingView" sender:nil];
}


-(void)Tracehight{
    [self popupview];
    [self.radialMenu buttonsWillAnimateFromButton:groupImg withFrame:groupImg.frame inView:self.view];
    
}


#pragma mark - radial menu delegate methods

- (NSInteger) numberOfItemsInRadialMenu:(ALRadialMenu *)radialMenu {
    
    return 9;
}


- (NSInteger) arcSizeForRadialMenu:(ALRadialMenu *)radialMenu {
    
    return 360;
}


- (NSInteger) arcRadiusForRadialMenu:(ALRadialMenu *)radialMenu {
    
    return 130;
}


- (ALRadialButton *) radialMenu:(ALRadialMenu *)radialMenu buttonForIndex:(NSInteger)index {
    ALRadialButton *button = [[ALRadialButton alloc] init];
    
    if (index == 1) {
        [button setImage:[UIImage imageNamed:@"menu-help"] forState:UIControlStateNormal];
    } else if (index == 2) {
        [button setImage:[UIImage imageNamed:@"menu-fine"] forState:UIControlStateNormal];
    } else if (index == 3) {
        [button setImage:[UIImage imageNamed:@"menu-seeThis"] forState:UIControlStateNormal];
    } else if (index == 4) {
        [button setImage:[UIImage imageNamed:@"menu-meet"] forState:UIControlStateNormal];
    } else if (index == 5) {
        [button setImage:[UIImage imageNamed:@"menu-map"] forState:UIControlStateNormal];
    } else if (index == 6) {
        [button setImage:[UIImage imageNamed:@"menu-invite"] forState:UIControlStateNormal];
    } else if (index == 7) {
        [button setImage:[UIImage imageNamed:@"menu-member"] forState:UIControlStateNormal];
    } else if (index == 8) {
        [button setImage:[UIImage imageNamed:@"menu-chat"] forState:UIControlStateNormal];
    }else if (index == 9) {
        // Initialize NKNumberBadgeView...
      
        [button setImage:[UIImage imageNamed:@"menu-feeds"] forState:UIControlStateNormal];
         [button addSubview:number];
        
        
    }
    
    
    if (button.imageView.image) {
        return button;
    }
    
    return nil;
}

- (NSString *) GetTitleForRadialMenu:(NSInteger) index{
    NSString * Title = [[NSString alloc] init];
    
    if (index == 1) {
        Title=@"Help";
    } else if (index == 2) {
        Title=@"I am fine";
    } else if (index == 3) {
        Title=@"See This";
    } else if (index == 4) {
        Title=@"Meet You There";
    } else if (index == 5) {
        Title=@"Map";
    } else if (index == 6) {
        Title=@"Invite";
    } else if (index == 7) {
        Title=@"Members";
    } else if (index == 8) {
        Title=@"Chat";
    }else if (index == 9) {
        Title=@"Feeds";
    }
    return Title;
    
}
- (void) radialMenu:(ALRadialMenu *)radialMenu didSelectItemAtIndex:(NSInteger)index {
    
    if (index==1) {
        [self performSegueWithIdentifier:@"NeedView" sender:nil];
    }else if (index==2){
        //[self finePost];
        hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        hud.mode = MBProgressHUDModeIndeterminate;
        iamfinfunction = [[IamFine alloc] init];
        
        iamfinfunction.deleget = self;
        [iamfinfunction finePost:self.groupData.groupID];
        
        
    }else if (index==3){
        SeeThisViewController *writeView=[self.storyboard instantiateViewControllerWithIdentifier:@"SeeThisPostController"];
        writeView.userId=self.groupData.groupID;
        writeView.userImgID=self.groupData.logoID;
        writeView.userNameTXT=self.groupData.groupName;
        writeView.isGroup=YES;
        [self.navigationController pushViewController:writeView animated:YES];
    }else if (index==4){
        [self performSegueWithIdentifier:@"MeetThere" sender:nil];
    }else if (index==5){
        MapKitViewController *mapView=[self.storyboard instantiateViewControllerWithIdentifier:@"MapController"];
        mapView.memberImage=self.groupData.profileImg;
        mapView.memberNam=self.groupData.groupName;
        mapView.userID=self.groupData.groupID;
        mapView.groupData =self.groupData;
        mapView.isGroup=YES;
        [self.navigationController pushViewController:mapView animated:YES];
    }else if(index==6){
        [self performSegueWithIdentifier:@"inviteSegue" sender:nil];
               
    }else if(index==7){
        [self performSegueWithIdentifier:@"memberView" sender:nil];
    }else if(index==8){
        
        webService=[[WebServiceFunc alloc]init];
        [webService setDelegate:self];
        hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        hud.mode = MBProgressHUDModeIndeterminate;
        webservice=7;
        NSString *urlString=[NSString stringWithFormat:@"%@%@",CheckConversation,self.groupData.groupID];
        [webService getData:urlString withflagLogin:YES];

    }else if (index==9){
        [self performSegueWithIdentifier:@"feedsSegue" sender:nil];
    }
    
}

- (BOOL)radialMenu:(ALRadialMenu *)radialMenu shouldRotateMenuButtonWhenItemsAppear:(UIButton *)button {
    if (radialMenu == self.radialMenu) {
        return YES;
    }
    
    return NO;
}

- (BOOL)radialMenu:(ALRadialMenu *)radialMenu shouldRotateMenuButtonWhenItemsDisappear:(UIButton *)button {
    if (radialMenu == self.radialMenu) {
        return NO;
    }
    
    return YES;
}
- (NSString *) radialMenu:(ALRadialMenu *)radialMenu BadgeForIndex:(NSInteger) index{
    
    if (index==9) {
        return [ NSString stringWithFormat:@"%i",self.groupData.unSeenFeeds];
    }else{
    return 0;
    }
}


- (void)IamFinefunctionRespnse:(NSMutableArray *)response service:(NSInteger )Type{
    hud.mode = MBProgressHUDModeCustomView;
    // Set an image view with a checkmark.
    UIImage *image = [[UIImage imageNamed:@"SCheckmark"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    hud.customView = [[UIImageView alloc] initWithImage:image];
    // Looks a bit nicer if we make it square.
    hud.square = YES;
    hud.label.text = NSLocalizedString(@"Done", @"");
    [hud hideAnimated:YES afterDelay:1.f];

}

- (void)IamFinefunctionFail:(NSString *)message service:(NSInteger )Type{
   [hud setHidden:YES];

}

//
//-(void)finePost {
//    
//    webService=[[WebServiceFunc alloc]init];
//    
//    [webService setDelegate:self];
//    
//    hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
//    
//    hud.mode = MBProgressHUDModeIndeterminate;
//    
//    NSDateFormatter *dateFormatter=[[NSDateFormatter alloc] init];
//    
//    NSLocale *locale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US"];
//    
//    [dateFormatter setLocale:locale];
//    
//    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss a"];
//    
//    NSMutableArray *postPrivacies=[[NSMutableArray alloc]init];
//    
//     [postPrivacies addObject:[NSDictionary dictionaryWithObjectsAndKeys:self.groupData.groupID,@"groupId",@"",@"userId",@"00000000-0000-0000-0000-000000000000",@"postId", nil]];
//    
//    NSString *currentDate=[dateFormatter stringFromDate:[NSDate date]];
//    
//    NSString *postTypeId=PostWithNoFiles;
//    
//    NSMutableDictionary *userData = [[NSMutableDictionary alloc] initWithObjectsAndKeys:@"00000000-0000-0000-0000-000000000000",@"originalPostId",
//                                     postTypeId,@"postTypeId",
//                                     postPrivacies,@"postPrivacies",
//                                     @"00000000-0000-0000-0000-000000000000",@"userId",
//                                     @" I'm Fine ",@"Text",
//                                     currentDate,@"date",
//                                     nil];
//    webservice=3;
//    
//    [webService postDataWithUrlString:UploadPost withData:userData withflagLogin:YES];
//    
//}

- (void)CloseView{
    webservice=2;
    webService=[[WebServiceFunc alloc]init];
    [webService setDelegate:self];
    hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    hud.mode = MBProgressHUDModeIndeterminate;
    NSString *urlString=[NSString stringWithFormat:@"%@%@",LeaveGroup,self.groupData.groupID];
    [webService getData:urlString withflagLogin:YES];
}
-(void)accept:(GroupData *)groupData{
    webservice=1;
    webService=[[WebServiceFunc alloc]init];
    [webService setDelegate:self];
    hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    hud.mode = MBProgressHUDModeIndeterminate;
    NSString *urlString=[NSString stringWithFormat:@"%@%@",acceptpolicy,self.groupData.groupID];
    [webService getData:urlString withflagLogin:YES];
    
    
}

-(void)userWebserviceCallDidSuccessWithRespone:(NSMutableArray *)response{
    [hud setHidden:YES];

    if (webservice==1) {
        [self.navigationController.visibleViewController dismissViewControllerAnimated:YES completion:nil];
        self.groupData.pending=NO;
    }else if(webservice==2){
        [self.navigationController.visibleViewController dismissViewControllerAnimated:YES completion:nil];
        [self closeView:nil];
        
    }else if(webservice==6){
        LiveTrackingViewController *liveTrack=[self.storyboard instantiateViewControllerWithIdentifier:@"liveTrackingController"];
        liveTrack.groupData=[[GroupData alloc]init];
        liveTrack.groupData.groupID=self.groupData.groupID;
        liveTrack.groupData.profileImg=self.groupData.profileImg;
        liveTrack.groupData.groupName=self.groupData.groupName;
        liveTrack.isGroup=YES;
        
        liveTrack.locations=[[NSMutableArray alloc]init];
        liveTrack.usersId=[[NSMutableArray alloc]init];
        
        NSDictionary *dic;
        for (int i=0; i<response.count; i++)
        {
            NSString *firstName=[[[response objectAtIndex:i] objectForKey:@"user"] objectForKey:@"firstName"];
            NSString *lastName=[[[response objectAtIndex:i] objectForKey:@"user"] objectForKey:@"familyName"];
            NSString *name=[NSString stringWithFormat:@"%@ %@",firstName,lastName];
            NSDictionary *dic=[self editDateAndTime:[[response objectAtIndex:i] objectForKey:@"lastPostionDate"] ];
            NSString *date=[NSString stringWithFormat:@"%@  %@",[dic objectForKey:@"date"],[dic objectForKey:@"time"]];
            NSString *battery;
            if ([[response objectAtIndex:i] objectForKey:@"batteryStatue"]==[NSNull null]) {
                battery=@"%0";
            }else{
                battery=[NSString stringWithFormat:@"%@ %@",@"%",[[response objectAtIndex:i] objectForKey:@"batteryStatue"]];
            }
            NSString *lastKnowingLatitude =[[response objectAtIndex:i]objectForKey:@"lastKnowingLatitude"];
            NSString *lastKnowingLangitude = [[response objectAtIndex:i]objectForKey:@"lastKnowingLangitude"];
            
            if (![lastKnowingLatitude isKindOfClass:[NSNull class]]) {
            dic=[NSDictionary dictionaryWithObjectsAndKeys:[NSNumber numberWithDouble:[lastKnowingLatitude doubleValue]],@"lat",[NSNumber numberWithDouble:[lastKnowingLangitude doubleValue]],@"lng",name,@"name",battery,@"battery",date,@"date",[[[response objectAtIndex:i] objectForKey:@"user"] objectForKey:@"id"],@"userid",[NSNumber numberWithBool:NO],@"fromSeeThis", nil];
            [liveTrack.locations addObject:dic];
            [liveTrack.usersId addObject:[[[response objectAtIndex:i] objectForKey:@"user"] objectForKey:@"id"]];
            }
        }
        [self.navigationController pushViewController:liveTrack animated:YES];

    }else if(webservice==7){
        NSDictionary *dic=(NSDictionary *)response;
        NSString *userID=[[NSUserDefaults standardUserDefaults] objectForKey:@"UserId"];
        ConversationModel *convModel=[[ConversationModel alloc]init];
        RLMRealm *realm = [RLMRealm defaultRealm];
        [realm beginWriteTransaction];
        
        NSDateFormatter *format=[[NSDateFormatter alloc]init];
        NSLocale *locale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US"];
        [format setLocale:locale];
        [format setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSSS"];
        
        convModel.Id=[dic objectForKey:@"id"];
        convModel.ChatName=[dic objectForKey:@"chatName"];
        convModel.LogoId=[dic objectForKey:@"logoId"];
        convModel.UserId=[dic objectForKey:@"userId"];
        convModel.LastUpdateTime=[format dateFromString:[dic objectForKey:@"lastUpdateTime"]];
        convModel.UnseenMsgNo=0;
        convModel.CreatorId=[dic objectForKey:@"creatorId"];
        convModel.LastText=[dic objectForKey:@"lastText"];
        convModel.Mute=NO;
        convModel.CreationDate=[format dateFromString:[dic objectForKey:@"creationDate"]];
        [realm addOrUpdateObject:convModel];
        // now commit & end this transaction
        [realm commitWriteTransaction];
        
        
        UserModel *userModl=[[UserModel alloc]init];
        [realm beginWriteTransaction];
        
        userModl.Id=self.groupData.groupID;
        userModl.UserName=self.groupData.groupName;
        
        [realm addOrUpdateObject:userModl];
        // now commit & end this transaction
        [realm commitWriteTransaction];
        
        UIStoryboard *chatStoryBoard=[UIStoryboard storyboardWithName:@"Chat" bundle:nil];
        conversationView=[chatStoryBoard instantiateViewControllerWithIdentifier:@"ConversationController"];
        conversationView.isGroup=YES;
        conversationView.conversationData=convModel;
        [self.navigationController pushViewController:conversationView animated:YES];
    }else{
    
    }
    
}
-(void)userWebserviceCallDidFaildwithError:(NSError *)error andMessage:(NSString *)message{
    [hud setHidden:YES];
    NSLog(@"%@",error.localizedDescription);
    if ([message isEqualToString:@"401" ]) {
        UIStoryboard *storyboard=[UIStoryboard storyboardWithName:@"Main" bundle:nil];
        UIViewController* viewController=[storyboard instantiateViewControllerWithIdentifier:@"LoginController"];
        [self presentViewController:viewController animated:YES completion:nil];
    }else if ([message isEqualToString:@"404"]){
        if (webservice==7) {
                 NSString *userID=[[NSUserDefaults standardUserDefaults] objectForKey:@"UserId"];
            
            NSString *uuid = [[NSUUID UUID] UUIDString];
            uuid = [uuid lowercaseString];
            
            UIStoryboard *chatStoryBoard=[UIStoryboard storyboardWithName:@"Chat" bundle:nil];
            ConversationViewController *conversationView=[chatStoryBoard instantiateViewControllerWithIdentifier:@"ConversationController"];
            conversationView.isGroup=YES;
            conversationView.firstconversationData=[NSDictionary dictionaryWithObjectsAndKeys:
                                                    uuid,@"ConversationId",
                                                    self.groupData.groupID , @"GroupID",
                                                     userID , @"UserId",
                                                    self.groupData.groupID,@"fieldId",
                                                    self.groupData.groupName,@"ChatName",
                                                    self.groupData.profileImg,@"ChatImage", nil];
            
            RLMRealm *realm = [RLMRealm defaultRealm];
            UserModel *userModl=[[UserModel alloc]init];
            [realm beginWriteTransaction];
            
            userModl.Id=self.groupData.groupID;
            userModl.UserName=self.groupData.groupName;
            
            [realm addOrUpdateObject:userModl];
            // now commit & end this transaction
            [realm commitWriteTransaction];
            
            [self.navigationController pushViewController:conversationView animated:YES];
        }
    }
    
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    if ([[segue identifier] isEqualToString:@"settingView"]) {
        
        SettingGroupViewController *settingView=[segue destinationViewController];
        settingView.groupData=[[GroupData alloc]init];
        settingView.groupData=self.groupData;
        
        NSLog(@"%@",settingView.groupData);
        
    }else if([[segue identifier] isEqualToString:@"inviteSegue"]){
        InviteViewController *invite=[segue destinationViewController];
        invite.groupID=self.groupData.groupID;
    }else if([[segue identifier] isEqualToString:@"memberView"]){
        GroupMembersViewController *memView=[segue destinationViewController];
        memView.groupData=[[GroupData alloc]init];
        memView.groupData=self.groupData;
    }else if([[segue identifier] isEqualToString:@"MeetThere"]){
        MeetThereViewController *meetView=[segue destinationViewController];
        meetView.listID=self.groupData.groupID;
        meetView.listName=self.groupData.groupName;
        meetView.isUser=NO;
    }else if ([[segue identifier] isEqualToString:@"NeedView"]){
        NeedHelpViewController *needView=[segue destinationViewController];
        needView.inviterId=self.groupData.groupID;
        needView.inviterName=self.groupData.groupName;
        needView.isUser=NO;
        
    }else if ([[segue identifier] isEqualToString:@"feedsSegue"]){
        FeedsViewController *feedsView=[segue destinationViewController];
        feedsView.isGroup=YES;
        feedsView.groupId=self.groupData.groupID;
    }
}



@end
