//
//  RequestsViewController.m
//  FamillyLocator
//
//  Created by Amira on 12/4/16.
//  Copyright © 2016 Amira. All rights reserved.
//

#import "RequestsViewController.h"
#import "GroupMembersTableViewCell.h"
#import "MBProgressHUD.h"
#import "Constant.h"
#import "GroupData.h"
#import "Animation.h"
#import "UIViewController+RemoteNotificationData.h"
#import "SideMenuViewController.h"
@interface RequestsViewController ()
{
    WebServiceFunc *webService;
    MBProgressHUD *hud;
    NSMutableArray *members,*admins;
    int memberIndx,serviceNUM;
}
@end

@implementation RequestsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [Animation roundCornerForView:groupImg withAngle:30.0f];
    


}

-(void)viewDidDisappear:(BOOL)animated{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}


-(void)viewWillAppear:(BOOL)animated{
    admins=[[NSMutableArray alloc]init];
    webService=[[WebServiceFunc alloc]init];
    serviceNUM=1;
    [webService setDelegate:self];
    hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    hud.mode = MBProgressHUDModeIndeterminate;
    [groupImg setImageURL:[NSURL URLWithString:self.groupData.profileImg]];
    groupName.text=self.groupData.groupName;
    NSString *urlString=[NSString stringWithFormat:@"%@%@",GroupRequests,self.groupData.groupID];
    [webService getData:urlString withflagLogin:YES];
    
}

- (IBAction)showMenu
{
    
    SideMenuViewController *sideMenu=[self.storyboard instantiateViewControllerWithIdentifier:@"SideMenu"];
    [self.navigationController presentViewController:sideMenu animated:YES completion:nil];
}

#pragma TableView DataSource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
       return 1;

}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
        return members.count;
    
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    GroupMembersTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"groupmembers"];
    cell.memberName.text=[NSString stringWithFormat:@"%@ %@",[[members objectAtIndex:indexPath.row] objectForKey:@"firstName"],[[members objectAtIndex:indexPath.row] objectForKey:@"familyName"]];
    NSURL *imgurl=[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",PhotosUrl,[[members objectAtIndex:indexPath.row] objectForKey:@"photoId"]]];
    [cell.memberImg setImageURL:imgurl];
    cell.memberImg.layer.cornerRadius = 30.0f;
    cell.memberImg.clipsToBounds = YES;
    
        [Animation roundCornerForView:cell.confRequest withAngle:5.0F];
        [Animation roundCornerForView:cell.delRequest withAngle:5.0F];
        
        [cell.confRequest addTarget:self action:@selector(confirmreq:)  forControlEvents:UIControlEventTouchUpInside];
        [cell.confRequest setTag:indexPath.row];
        [cell.delRequest addTarget:self action:@selector(deletereq:)  forControlEvents:UIControlEventTouchUpInside];
        [cell.delRequest setTag:indexPath.row];
    
    return cell;
}
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    // This will create a "invisible" footer
    return 0.01f;
}


- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    return [UIView new];
    
}



#pragma TableView Delegate



-(void)userWebserviceCallDidSuccessWithRespone:(NSMutableArray *)response{
    [hud setHidden:YES];
    if (serviceNUM==1) {
          members=response;
        [tableView reloadData];
    }else if (serviceNUM==2) {
        [members removeObjectAtIndex:memberIndx];
        [tableView reloadData];
        
    }
    
    
}
-(void)userWebserviceCallDidFaildwithError:(NSError *)error andMessage:(NSString *)message{
    [hud setHidden:YES];
    NSLog(@"%@",error.localizedDescription);
    if ([message isEqualToString:@"401" ]) {
        UIStoryboard *storyboard=[UIStoryboard storyboardWithName:@"Main" bundle:nil];
        UIViewController* viewController=[storyboard instantiateViewControllerWithIdentifier:@"LoginController"];
        [self presentViewController:viewController animated:YES completion:nil];
    }
    
}

-(IBAction)confirmreq:(id)sender{
    UIButton *but=sender;
    memberIndx=but.tag;
    NSString *memID=[[members objectAtIndex:memberIndx]objectForKey:@"id"];
    webService=[[WebServiceFunc alloc]init];
    serviceNUM=2;
    [webService setDelegate:self];
    hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    hud.mode = MBProgressHUDModeIndeterminate;
    NSString *urlString=[NSString stringWithFormat:@"%@/group/usergroup/RespondRequest?id=%@&groupId=%@&accept=true",MainAPi,memID,self.groupData.groupID];
    [webService getData:urlString withflagLogin:YES];

}
-(IBAction)deletereq:(id)sender{
    UIButton *but=sender;
    memberIndx=but.tag;
    NSString *memID=[[members objectAtIndex:memberIndx]objectForKey:@"id"];
    
    webService=[[WebServiceFunc alloc]init];
    serviceNUM=2;
    [webService setDelegate:self];
    BOOL accept=NO;
    hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    hud.mode = MBProgressHUDModeIndeterminate;
    [groupImg setImageURL:[NSURL URLWithString:self.groupData.profileImg]];
    groupName.text=self.groupData.groupName;
    NSString *urlString=[NSString stringWithFormat:@"%@/group/usergroup/RespondRequest?id=%@&groupId=%@&accept=%@",MainAPi,memID,self.groupData.groupID,accept];
    [webService getData:urlString withflagLogin:YES];
    
}

-(IBAction)blockUser:(id)sender{
    UIButton *but=sender;
    memberIndx=but.tag;
    NSString *memID=[[members objectAtIndex:memberIndx]objectForKey:@"id"];
    
    webService=[[WebServiceFunc alloc]init];
    serviceNUM=3;
    [webService setDelegate:self];
    hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    hud.mode = MBProgressHUDModeIndeterminate;
    [groupImg setImageURL:[NSURL URLWithString:self.groupData.profileImg]];
    groupName.text=self.groupData.groupName;
    NSString *urlString=[NSString stringWithFormat:@"%@/group/usergroup/BlockUser?id=%@&groupId=%@",MainAPi,memID,self.groupData.groupID];
    [webService getData:urlString withflagLogin:YES];
}

@end
