//
//  HomeGroupViewController.h
//  FamilyLocator
//
//  Created by Amira on 10/15/16.
//  Copyright © 2016 Amira. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ALRadialMenu.h"
#import "AsyncImageView.h"
#import "GroupData.h"
#import "PolicyViewController.h"
#import "WebServiceFunc.h"
#import "IamFine.h"
#import "ConversationViewController.h"
@interface HomeGroupViewController : UIViewController<ALRadialMenuDelegate,UIGestureRecognizerDelegate,PopviewDelegate,WebServiceProtocol>
{
    __weak IBOutlet AsyncImageView *groupImg;
    __weak IBOutlet UILabel *groupName;
    IamFine * iamfinfunction;
    NSString * GroupID;
    ConversationViewController *conversationView;
}
@property (strong, nonatomic) ALRadialMenu *radialMenu;
@property (strong, nonatomic) NSArray *popups;
@property(strong,nonatomic)GroupData *groupData;

@end
