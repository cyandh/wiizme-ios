//
//  SettingGroupViewController.h
//  FamilyLocator
//
//  Created by Amira on 10/15/16.
//  Copyright © 2016 Amira. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GroupData.h"
#import "WebServiceFunc.h"
#import "AsyncImageView.h"
#import "GroupMembersViewController.h"
@interface SettingGroupViewController : UIViewController<UITableViewDelegate,UITableViewDataSource,WebServiceProtocol,AddAdminDelegate>{
    
    __weak IBOutlet AsyncImageView *coverImg;
    __weak IBOutlet AsyncImageView *grooupImg;
    __weak IBOutlet UILabel *groupName;
    __weak IBOutlet UILabel *groupPrivacy;
    __weak IBOutlet UILabel *groupType;
    __weak IBOutlet UITextView *descTXT;
    __weak IBOutlet UIButton *memberNum;
    __weak IBOutlet UIButton *settingBtn;
    __weak IBOutlet UIButton *joinBtn;
}

@property(nonatomic,strong)IBOutlet UITableView *table;
@property(nonatomic,strong)GroupData *groupData;
@property(nonatomic)BOOL fromNotification;

@end
