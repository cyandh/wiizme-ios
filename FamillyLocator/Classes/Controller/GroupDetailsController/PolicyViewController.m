//
//  PolicyViewController.m
//  FamillyLocator
//
//  Created by Amira on 12/7/16.
//  Copyright © 2016 Amira. All rights reserved.
//

#import "PolicyViewController.h"
#import "Animation.h"
#import "UIViewController+RemoteNotificationData.h"

@interface PolicyViewController ()

@end

@implementation PolicyViewController

@synthesize delegate,popView,policyTXT,groupData,accept;

- (void)viewDidLoad {

    
    [popView.layer setShadowColor:[[UIColor darkGrayColor] CGColor]];
    [popView.layer setShadowOffset:CGSizeMake(0, 0)];
    [popView.layer setShadowRadius:200.0];
    [popView.layer setShadowOpacity:1];
    popView.clipsToBounds = NO;
    popView.layer.masksToBounds = NO;
    
    policyTXT.text=groupData.policy;
    
    [Animation roundCornerForView:popView withAngle:10.0f];
    [Animation roundCornerForView:accept withAngle:15.0f];
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

-(void)viewDidDisappear:(BOOL)animated{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}


-(void)viewDidAppear:(BOOL)animated{
    
    [UIView beginAnimations: @"anim" context: nil];
    [UIView setAnimationBeginsFromCurrentState: YES];
    [UIView setAnimationDelegate:self];
    [UIView setAnimationDidStopSelector:@selector(OperHolderAnimation)];
    [UIView setAnimationDuration: .5];
    self.view.alpha =1;
    [UIView commitAnimations];
    
    
}

-(void)OperHolderAnimation{
    
    [UIView beginAnimations: @"anim" context: nil];
    [UIView setAnimationBeginsFromCurrentState: YES];
    [UIView setAnimationDelegate:self];
    
    [UIView setAnimationDuration: .5];
    popView.alpha =1;
    [UIView commitAnimations];
    
    
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */



-(IBAction)CloseViewBt:(id)sender{
    // [self.delegate CloseView];
    
    [self.delegate CloseView];
    
}
-(IBAction)Details:(id)sender{
    // [self.delegate GotoDetails:DTO];
    
    [self.delegate accept:self.groupData];
}

@end
