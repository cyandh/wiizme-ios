//
//  RequestsViewController.h
//  FamillyLocator
//
//  Created by Amira on 12/4/16.
//  Copyright © 2016 Amira. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GroupData.h"
#import "AsyncImageView.h"
#import "WebServiceFunc.h"
@interface RequestsViewController : UIViewController<UITableViewDelegate,UITableViewDataSource,WebServiceProtocol>
{
    __weak IBOutlet UITableView *tableView;
    __weak IBOutlet AsyncImageView *groupImg;
    __weak IBOutlet UILabel *groupName;
}

@property(nonatomic,strong)GroupData *groupData;

@end
