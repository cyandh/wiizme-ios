//
//  AddNewGroupViewController.h
//  FamilyLocator
//
//  Created by Amira on 10/13/16.
//  Copyright © 2016 Amira. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "WebServiceFunc.h"
#import "UiCustomButton.h"
#import "WebServiceFunc.h"
#import "LPPopupListView.h"
#import "GroupData.h"
@interface AddNewGroupViewController : UIViewController<WebServiceProtocol,UIPickerViewDelegate,UIPickerViewDataSource,UIGestureRecognizerDelegate,WebServiceProtocol,LPPopupListViewDelegate,UITextViewDelegate,UITextFieldDelegate>
{
    __weak IBOutlet UIScrollView *scrollview;
    __weak IBOutlet UITextField *groupName;
    __weak IBOutlet  UiCustomButton*groupPrivacy;
    __weak IBOutlet  UiCustomButton*groupType;
    __weak IBOutlet UiCustomButton *groupActivity;
    __weak IBOutlet UITextView *DescTXT;
    __weak IBOutlet UITextView *policyTXT;
    __weak IBOutlet UILabel *coverImgName;
    __weak IBOutlet UILabel *logoImgName;
    __weak IBOutlet UILabel *viewAction;
    __weak IBOutlet UIButton *done;
    __weak IBOutlet UIImageView *logoImgView;
    __weak IBOutlet UIImageView *coverImgView;

    
    
    NSArray *pickerData;
    UIPickerView* pickrView;

}
@property (nonatomic, strong) NSMutableIndexSet *selectedIndexes;
@property (nonatomic, strong) GroupData *groupData;
@property (nonatomic) BOOL editGroup;
@end
