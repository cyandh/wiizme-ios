//
//  ZoonNotificationListVC.h
//  wiizme
//
//  Created by abdelrhman gamil on 7/6/17.
//  Copyright © 2017 Amira. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DKPaginatedTableViewController.h"
@interface ZoonNotificationListVC : UIViewController{
    __weak IBOutlet UITableView *tableView;
    BOOL RefreshFlag;
    
    ///////////// Paging
    NSUInteger _totalPages;
    NSUInteger _totalRowsCount;
    NSUInteger _rowsPerPage;
    NSUInteger _currentPage;
    
    BOOL _isLoading;
    BOOL _shouldLoad;
    
    UIView *_backgroundView;
    UIView *_loadingView;
    UILabel *_loadingLabel;
    UIActivityIndicatorView *_activitiIndicator;
    //////////////
    
    NSMutableArray * NotficationData;
}

@end
