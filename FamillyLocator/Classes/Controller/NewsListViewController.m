//
//  ShowListViewController.m
//  MIxFM
//
//  Created by agamil on 5/15/17.
//  Copyright © 2017 cyandh. All rights reserved.
//

#import "NewsListViewController.h"
#import "NewsListCell.h"
#import "WebServiceFunc.h"
#import "constant.h"
#import <SafariServices/SafariServices.h>

@interface NewsListViewController ()

@end

@implementation NewsListViewController

- (void)viewDidLoad {
    
    tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(StouchesBegan)];
    
    
    
    [super viewDidLoad];
    ListItems = [[NSMutableArray alloc] init];
    [self LoadViewData];
    tableView.opaque = NO;
    tableView.backgroundColor = [UIColor clearColor];
    
    NewsSearch = [[UISearchController alloc] init];
    
    searchBar = [[UISearchBar alloc]initWithFrame:CGRectZero];
    searchBar.delegate =self;
    searchBar.placeholder = NSLocalizedString(@"Search",nil);
    UIBarButtonItem *searchBarItem = [[UIBarButtonItem alloc]initWithCustomView:searchBar];
    searchBarItem.tag = 123;
    searchBarItem.customView.hidden = YES;
    searchBarItem.customView.alpha = 0.0f;
    self.navigationItem.leftBarButtonItem = searchBarItem;
    UIButton *button1 = [[UIButton alloc] init];
    button1.frame=CGRectMake(0,0,30,30);
    [button1.imageView setContentMode:UIViewContentModeScaleAspectFit];
    [button1 setBackgroundImage:[UIImage imageNamed: @"search.png"] forState:UIControlStateNormal];
    [button1 addTarget:self action:@selector(whenSearchClicked:) forControlEvents:UIControlEventTouchUpInside];
    
    self.navigationItem.leftBarButtonItems = [NSArray arrayWithObjects:[[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"menu.png"] style:UIBarButtonItemStylePlain target:self action:@selector(showMenu)],searchBarItem,nil];
    
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc]initWithCustomView:button1];
    
    
    
    
    // Do any additional setup after loading the view.
}


- (void)whenSearchClicked:(id)sender
{
    NSArray *buttonsArray = self.navigationController.navigationBar.topItem.leftBarButtonItems;
    for(UIBarButtonItem *item in buttonsArray)
    {
        if(item.tag == 123 && item.customView.hidden)
        {
            [self.view addGestureRecognizer:tapGesture];
            item.customView.hidden = NO;
            item.customView.frame = CGRectMake(0,0, 250,44);
            if([item.customView isKindOfClass:[UISearchBar class]])
                [item.customView becomeFirstResponder];
            UIBarButtonItem *rightItem = [[UIBarButtonItem alloc] initWithTitle:@"X" style:UIBarButtonItemStylePlain target:self action:@selector(whenSearchClicked:)];//single rite item for this example
            self.navigationItem.rightBarButtonItem = rightItem;
            self.title = @"";
        }
        else
        {
            [searchBar resignFirstResponder];
            [self.view removeGestureRecognizer:tapGesture];
            item.customView.hidden = YES;
            self.title = NSLocalizedString(@"News",nil);
            UIButton *button1 = [[UIButton alloc] init];
            item.customView.frame = CGRectZero;
            button1.frame=CGRectMake(0,0,30,30);
            [button1 setBackgroundImage:[UIImage imageNamed: @"search.png"] forState:UIControlStateNormal];
            [button1.imageView setContentMode:UIViewContentModeScaleAspectFit];
            [button1 addTarget:self action:@selector(whenSearchClicked:) forControlEvents:UIControlEventTouchUpInside];
            self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc]initWithCustomView:button1];
            
        }
    }
    
}

-(void)viewDidAppear:(BOOL)animated{
    self.title = NSLocalizedString(@"News",nil);
    [super viewDidAppear:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



#pragma mark UITableView Delegate

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    //    NSInteger num= indexPath.row;
    //    if (num % 2){
    //        cell.backgroundColor = [UIColor colorWithRed:248.0/255 green:105.0/255.0 blue:123.0/255.0 alpha:1];
    //    }else{
    //
    //        cell.backgroundColor = [UIColor colorWithRed:240.0/255 green:62.0/255.0 blue:120.0/255.0 alpha:1];
    //    }
    //    [cell.textLabel setTextColor:[UIColor whiteColor]];
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)sectionIndex
{
    if (sectionIndex == 0)
        return nil;
    
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, tableView.frame.size.width, 34)];
    view.backgroundColor = [UIColor colorWithRed:167/255.0f green:167/255.0f blue:167/255.0f alpha:0.6f];
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(10, 8, 0, 0)];
    label.text = @"Friends Online";
    label.font = [UIFont systemFontOfSize:15];
    label.textColor = [UIColor whiteColor];
    label.backgroundColor = [UIColor clearColor];
    [label sizeToFit];
    [view addSubview:label];
    return view;
}



- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    
    //    NewsDetailsViewController *NewsDetailsVC = [self.storyboard instantiateViewControllerWithIdentifier:@"NewsDetailsViewController"];
    //    NewsDetailsVC.NewsData =ListItems[indexPath.row];
    //    [self.navigationController pushViewController:NewsDetailsVC animated:YES ];
    
    
    NSURL *URL = [NSURL URLWithString:[[ListItems objectAtIndex:indexPath.row] objectForKey:@"url"]];
    if (URL) {
        if ([SFSafariViewController class] != nil) {
            SFSafariViewController *sfvc = [[SFSafariViewController alloc] initWithURL:URL];
            sfvc.delegate = self;
            [self presentViewController:sfvc animated:YES completion:nil];
        } else {
            if (![[UIApplication sharedApplication] openURL:URL]) {
            }
        }
    } else {
        // will have a nice alert displaying soon.
    }

    
    
    
}

#pragma mark -
#pragma mark UITableView Datasource



- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)sectionIndex
{
    return ListItems.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableview cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier = @"NewsListCell";
    
    NewsListCell * cell = [tableview dequeueReusableCellWithIdentifier:cellIdentifier];
    
    if (cell == nil) {
        cell = [[NewsListCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
    }
    
    
    
    cell.textLabel.text = [ListItems[indexPath.row] objectForKey:@"Name"];
    [cell loadCellData:ListItems[indexPath.row]];
    return cell;
}





-(void)LoadViewData {
    
    webService=[[WebServiceFunc alloc]init];
    
    [webService setDelegate:self];
    hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    hud.mode = MBProgressHUDModeIndeterminate;
    
    [webService getData:GoogleNews withflagLogin:NO];
    
}

-(void)userWebserviceCallDidSuccessWithRespone:(NSMutableArray *)response{
    

    NSDictionary * Data;
    if ([response isKindOfClass:[NSDictionary class]]) {
        Data =(NSDictionary *)response;
        ListItems = [Data objectForKey:@"articles"];
        OriginalListItems =[Data objectForKey:@"articles"];
        [tableView reloadData];
    }
    [hud hideAnimated:YES];
    
    
}
-(void)userWebserviceCallDidFaildwithError:(NSError *)error andMessage:(NSString *)message{
    
    [hud hideAnimated:YES];
    
    
}



/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

#pragma Mark - Search in table
- (void)searchBarTextDidBeginEditing:(UISearchBar *)searchBar
{
    //  [self.view addGestureRecognizer:tap];
    // [searchBar setShowsCancelButton:YES animated:YES];
}
- (void)searchBarTextDidEndEditing:(UISearchBar *)searchBar
{
    
    // [searchBar setShowsCancelButton:NO animated:YES];
    
}
- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText
{
    
    if (searchText.length>3) {
        searchText = [searchText stringByReplacingOccurrencesOfString:@" " withString:@""];
        NSPredicate *resultPredicate = [NSPredicate predicateWithFormat:@"title contains[c] %@", searchText];
        ListItems = [[ListItems filteredArrayUsingPredicate:resultPredicate]mutableCopy];
        
        
        
        
        [tableView reloadData];
    }else{
        
        ListItems=OriginalListItems;
        
        
        [tableView reloadData];
    }
    if ([ListItems count]==0) {
        [NoResults setHidden:NO];
    }else{
        [NoResults setHidden:YES];
        
    }
}

- (void)searchBarSearchButtonClicked:(UISearchBar *)SearchBar
{
    // [self.view removeGestureRecognizer:tap];
    [searchBar resignFirstResponder];
    
}

- (void)searchBarCancelButtonClicked:(UISearchBar *)SearchBar
{
    [searchBar setShowsCancelButton:NO animated:YES];
    searchBar.text=@"";
    // fetcharr=locationArray;
    
}

-(void)StouchesBegan {
    NSArray *buttonsArray = self.navigationController.navigationBar.topItem.leftBarButtonItems;
    for(UIBarButtonItem *item in buttonsArray)
    {
        if(item.tag == 123 && !item.customView.hidden)
        {
            [self.view removeGestureRecognizer:tapGesture];
            [searchBar resignFirstResponder];
            [searchBar setText:@""];
            item.customView.hidden = YES;
            self.title = NSLocalizedString(@"News",nil);
            UIButton *button1 = [[UIButton alloc] init];
            item.customView.frame = CGRectZero;
            button1.frame=CGRectMake(0,0,30,30);
            [button1 setBackgroundImage:[UIImage imageNamed: @"search.png"] forState:UIControlStateNormal];
            [button1.imageView setContentMode:UIViewContentModeScaleAspectFit];
            [button1 addTarget:self action:@selector(whenSearchClicked:) forControlEvents:UIControlEventTouchUpInside];
            self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc]initWithCustomView:button1];
        }
    }
}

@end
