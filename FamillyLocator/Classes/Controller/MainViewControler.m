//
//  MainViewControler.m
//  wiizme
//
//  Created by agamil on 5/18/17.
//  Copyright © 2017 Amira. All rights reserved.
//

#import "MainViewControler.h"

@interface MainViewControler ()

@end

@implementation MainViewControler

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)RegisterkeyboardObservers{
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillShow:)
                                                 name:UIKeyboardWillShowNotification
                                               object:nil];
    
    // Register notification when the keyboard will be hide
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillHide:)
                                                 name:UIKeyboardWillHideNotification
                                               object:nil];
    tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(StouchesBegan)];
    [tapGesture setDelegate:self];
    [self.view addGestureRecognizer:tapGesture];
    
    
}

- (void)keyboardWillShow:(NSNotification *)notification {
    CGRect keyboardBounds;
    if (CurrentScrollview!=nil) {
        
    
    
    [[notification.userInfo valueForKey:UIKeyboardFrameBeginUserInfoKey] getValue:&keyboardBounds];
    
    
    [CurrentScrollview setContentSize:CGSizeMake(CurrentScrollview.frame.size.width, (CurrentScrollview.contentSize.height+keyboardBounds.size.height))];
    [CurrentScrollview setContentOffset:CGPointMake(CurrentScrollview.contentOffset.x,CurrentScrollview.contentOffset.y+keyboardBounds.size.height) animated:YES];
    NSLog(@"CurrentScrollview.contentSize.height=%f",CurrentScrollview.contentSize.height);
    }
    // Do something with keyboard height
}

- (void)keyboardWillHide:(NSNotification *)notification {
    CGRect keyboardBounds;
    
    [[notification.userInfo valueForKey:UIKeyboardFrameBeginUserInfoKey] getValue:&keyboardBounds];
    [CurrentScrollview setContentSize:CGSizeMake(CurrentScrollview.frame.size.width, (CurrentScrollview.contentSize.height-keyboardBounds.size.height))];
    NSLog(@"CurrentScrollview.contentSize.height=%f",CurrentScrollview.contentSize.height);
    // Do something with keyboard height
}


-(void)StouchesBegan {

    [currentTextView resignFirstResponder];
    [CurrentTextField resignFirstResponder];

   
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/


-(void)viewDidDisappear:(BOOL)animated{
    [[NSNotificationCenter defaultCenter] removeObserver:UIKeyboardWillShowNotification];
        [[NSNotificationCenter defaultCenter] removeObserver:UIKeyboardWillHideNotification];

}


@end
