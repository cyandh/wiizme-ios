//
//  ChatListViewController.m
//  wiizme
//
//  Created by Amira on 3/7/17.
//  Copyright © 2017 Amira. All rights reserved.
//

#import "ChatListViewController.h"
#import "ChatListTableViewCell.h"
#import "ConversationModel.h"
#import "Constant.h"
#import "Helper.h"
#import "Animation.h"
#import "ConversationViewController.h"
#import "MBProgressHUD.h"
#define ROWS_PER_PAGE 10
#define TOTAL_ROWS 300

@interface ChatListViewController ()
{
    WebServiceFunc *webService;
    RLMResults<ConversationModel *> *artists;
    RLMResults<ConversationModel *> *Allartists;
    id sucessObj;
    MBProgressHUD *hud;
    NSMutableArray *chatListData;
}
@end

@implementation ChatListViewController

- (void)viewDidLoad {
    ChatMessage = [[SaveChatMessage alloc] init];
    
    
    [super viewDidLoad];
    // Do any additional setup after loading the view.
//    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(receiveMsg:) name:@"ReceiveMsg" object:nil];
      [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(NewMessgae:) name:@"ReloadMessageData" object:nil];
    
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(showChatMsg:) name:@"ShowChatMsg" object:nil];
    
}





-(void)showChatMsg:(NSNotification *)notification{
    NSDictionary *dic=notification.userInfo;
    
    RLMResults<ConversationModel *> *convData = [ConversationModel objectsWhere:@"Id == %@",[dic objectForKey:@"ConversationId"]];
    ConversationModel *convdic=[convData objectAtIndex:0];
    UIStoryboard *chatStoryBoard=[UIStoryboard storyboardWithName:@"Chat" bundle:nil];
    ConversationViewController *conversationView=[chatStoryBoard instantiateViewControllerWithIdentifier:@"ConversationController"];
    conversationView.conversationData=convdic;
    
    [self.navigationController pushViewController:conversationView animated:YES];
    
    
}

-(void)NewMessgae:(NSNotification *)notification{
    artists = [ChatMessage GetAllConvesations];
        [self.tableView reloadData];
}

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    artists = [ChatMessage GetAllConvesations];
    
    
    
    if (artists.count==0) {
        webService=[[WebServiceFunc alloc]init];
        [webService setDelegate:self];
        hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        hud.mode = MBProgressHUDModeIndeterminate;
        [super loadDataWithRowsPerPage:ROWS_PER_PAGE success:^{
            NSLog(@"First data was loaded...");
        } failure:^(NSError *error) {
            
        }];
    }else{
        [self.tableView reloadData];
    }
}

- (void)loadingData:(void (^)(NSUInteger totalRowsCount, NSArray *rows))success failure:(void (^)(NSError *error))failure{
    [super loadingData:success failure:failure];
    NSLog(@"");
    NSDateFormatter *dateFormatter=[[NSDateFormatter alloc] init];
    NSLocale *locale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US"];
    [dateFormatter setLocale:locale];
    [dateFormatter setDateFormat:@"yyyy-MM-dd"];
    NSString *currentDate=[dateFormatter stringFromDate:[NSDate date]];
    
    NSString *urlString=[NSString stringWithFormat:@"%@%@&index=%lu&size=10",ChatList,currentDate,(unsigned long)[self currentPage]];
    
   [webService getData:urlString withflagLogin:YES];
    sucessObj=success;
    
}


- (void) wait:(void (^)(NSUInteger totalRowsCount, NSArray *rows))success{
    
    
    if(chatListData.count > 0){
        
        success(TOTAL_ROWS, [chatListData copy]);
    }
}
-(void)userWebserviceCallDidSuccessWithRespone:(NSMutableArray *)response{
    
    [hud setHidden:YES];
    if ([response isKindOfClass:[NSDictionary class]]) {
        chatListData=[[NSMutableArray alloc]init];
        chatListData=[(NSDictionary*)response objectForKey:@"conversations"];
       
        for (int i=0; i<chatListData.count; i++) {

            [ChatMessage SaveMessage:[chatListData objectAtIndex:i]];
        }
  
     
        [self.tableView reloadData];

        
        [self performSelector:@selector(wait:) withObject:sucessObj afterDelay:0];
    }
    
    
    
}


-(void)userWebserviceCallDidFaildwithError:(NSError *)error andMessage:(NSString *)message{
    [hud setHidden:YES];
    NSLog(@"%@",error.localizedDescription);
    if ([message isEqualToString:@"401" ]) {
        UIStoryboard *storyboard=[UIStoryboard storyboardWithName:@"Main" bundle:nil];
        UIViewController* viewController=[storyboard instantiateViewControllerWithIdentifier:@"LoginController"];
        [self presentViewController:viewController animated:YES completion:nil];
    }
    
}

#pragma TableView DataSource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

//-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
//
//}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    return (unsigned long)artists.count;
    
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    ChatListTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"ChatCell"];
    [cell.image setImage:nil];
        
        ConversationModel *dic=[artists objectAtIndex:indexPath.row];
        cell.chatName.text=dic.ChatName;
        NSURL *imageUrl=[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",PhotosUrl,dic.LogoId]];
        
        [cell.image setImageURL:imageUrl];
        [Animation roundCornerForView:cell.image withAngle:30.0f];
        [Animation roundCornerForView:cell.notificationNum withAngle:10.0f];
        if (dic.UnseenMsgNo==0) {
            cell.notificationNum.hidden=YES;
        }else{
            cell.notificationNum.hidden=NO;
            [cell.notificationNum setTitle:[NSString stringWithFormat:@"%d",dic.UnseenMsgNo] forState:UIControlStateNormal];
        }
        
        cell.lastMsg.text=dic.LastText;
        cell.lastMsgDate.attributedText=[Helper timeTXT:[Helper timeLeftSinceDate:[NSString stringWithFormat:@"%@",dic.LastUpdateTime]]];
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    // This will create a "invisible" footer
    return 0.01f;
}


- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    return [UIView new];
    
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    ConversationModel *dic=[artists objectAtIndex:indexPath.row];
    UIStoryboard *chatStoryBoard=[UIStoryboard storyboardWithName:@"Chat" bundle:nil];
    ConversationViewController *conversationView=[chatStoryBoard instantiateViewControllerWithIdentifier:@"ConversationController"];
    conversationView.conversationData=dic;
    [self.navigationController pushViewController:conversationView animated:YES];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
