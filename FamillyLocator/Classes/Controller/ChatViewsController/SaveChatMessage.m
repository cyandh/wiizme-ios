//
//  SaveChatMessage.m
//  wiizme
//
//  Created by abdelrhman gamil on 7/7/17.
//  Copyright © 2017 Amira. All rights reserved.
//

#import "SaveChatMessage.h"

@implementation SaveChatMessage


-(instancetype)init{

  
    return self;

}

-(void)AddObserver{

  [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(receiveMsg:) name:@"ReceiveMsg" object:nil];

}
-(void)receiveMsg:(NSNotification *)notification{
    
    NSDictionary *dic=notification.userInfo;
    NSString *convId=[dic objectForKey:@"ConversationId"];
    NSLog(@"%@",self.conversationData.Id);
     RLMResults<MessageModel *> *MessageData = [MessageModel objectsWhere:@"ConversationId == %@",[dic objectForKey:@"ConversationId"]];
    if (MessageData.count==0) {
        
        //7acbbe90-3a9c-4ba9-b341-de295099d4f9
        NSString *userID=[[NSUserDefaults standardUserDefaults] objectForKey:@"UserId"];
        ConversationModel *convModel=[[ConversationModel alloc]init];
        RLMRealm *realm = [RLMRealm defaultRealm];
     RLMResults<MessageModel *> *convData = [MessageModel objectsWhere:@"Id == %@",[dic objectForKey:@"ConversationId"]];
    

        [realm beginWriteTransaction];
        convModel.Id=[dic objectForKey:@"ConversationId"];
        convModel.ChatName=[dic objectForKey:@"ChatName"];
        convModel.LogoId=[[dic objectForKey:@"Conversation"] objectForKey:@"LogoId"];
        convModel.UserId=[dic objectForKey:@"UserId"];
        convModel.LastUpdateTime=[NSDate date];
        convModel.UnseenMsgNo=0;
        convModel.CreatorId=[dic objectForKey:@"CreatorId"];
        convModel.LastText=[[dic objectForKey:@"Conversation"] objectForKey:@"LastText"];
        convModel.Mute=[[dic objectForKey:@"Conversation"] objectForKey:@"Mute"];
        convModel.CreationDate=[NSDate date];
        [realm addOrUpdateObject:convModel];
        // now commit & end this transaction
        [realm commitWriteTransaction];
        self.conversationData=convModel;
    
    
    

        MessageModel *msgModel=[[MessageModel alloc]init];
        [realm beginWriteTransaction];
        msgModel.Id=[dic objectForKey:@"Id"];
        msgModel.ConversationId=self.conversationData.Id;
        
        msgModel.Text=[dic objectForKey:@"Text"];
        msgModel.Date=[NSDate date];
        msgModel.UserId=userID;
        msgModel.MessageType=@"Text";
        
        
        [realm addObject:msgModel];
        // now commit & end this transaction
        [realm commitWriteTransaction];
        
    
//        msgDBData = [[MessageModel objectsWhere:@"ConversationId == %@",self.conversationData.Id ]sortedResultsUsingKeyPath:@"Date" ascending:YES];
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"ReloadMessageData"
                                                        object:nil
                                                      userInfo:dic];
    
    
    UIApplicationState state = [[UIApplication sharedApplication] applicationState];
        if (state == UIApplicationStateBackground || state == UIApplicationStateInactive)
        {
            dispatch_async(dispatch_get_main_queue(), ^{
                UILocalNotification* localNotification = [[UILocalNotification alloc] init];
                localNotification.fireDate = [NSDate dateWithTimeIntervalSinceNow:0];
                localNotification.alertBody = [NSString stringWithFormat:@"%@ send message",[[dic objectForKey:@"Conversation"] objectForKey:@"ChatName"]];
                localNotification.alertAction = @"Show me";
                localNotification.repeatInterval=NO;
                localNotification.timeZone = [NSTimeZone defaultTimeZone];
                localNotification.userInfo=dic;
                [[UIApplication sharedApplication]scheduleLocalNotification:localNotification];
            });
        }
        
    }
    
}
-(void)SaveMessage:(NSDictionary*)Message{
    RLMRealm *realm = [RLMRealm defaultRealm];
    [realm beginWriteTransaction];
    
    ConversationModel *convModel=[[ConversationModel alloc]init];
    NSDateFormatter *format=[[NSDateFormatter alloc]init];
    NSLocale *locale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US"];
    [format setLocale:locale];
    [format setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSSS"];
    convModel.Id=[Message objectForKey:@"id"];
    convModel.ChatName=[Message objectForKey:@"chatName"];
    convModel.LogoId=[Message objectForKey:@"logoId"];
    convModel.UserId=[Message objectForKey:@"userId"];
    convModel.LastUpdateTime=[format dateFromString:[Message objectForKey:@"lastUpdateTime"]];
    convModel.UnseenMsgNo=0;
    convModel.CreatorId=[Message objectForKey:@"creatorId"];
    convModel.LastText=[Message objectForKey:@"lastText"];
    convModel.Mute=NO;
    convModel.CreationDate=[format dateFromString:[Message objectForKey:@"creationDate"]];
    [realm addOrUpdateObject:convModel];
    
    // now commit & end this transaction
    [realm commitWriteTransaction];

    
    
}

-(RLMResults<MessageModel *> *)AddMsg:(ConversationModel *)convModel setMessageModel:(MessageModel *)msgModel{
    
  
    RLMRealm *realm = [RLMRealm defaultRealm];
    // RLMResults<ConversationModel *> *convData = [ConversationModel objectsWhere:@"Id == %@",[self.conversationData objectForKey:@"ConversationId"]];
    
    [realm beginWriteTransaction];
  
    [realm addOrUpdateObject:convModel];
    // now commit & end this transaction
    [realm commitWriteTransaction];
    self.conversationData=convModel;
    

    [realm beginWriteTransaction];
  
    
    
    [realm addObject:msgModel];
    // now commit & end this transaction
    [realm commitWriteTransaction];
    
    
    return  [[MessageModel objectsWhere:@"ConversationId == %@",self.conversationData.Id ]sortedResultsUsingKeyPath:@"Date" ascending:YES];
    
 
    
    
    
}

-(RLMResults<MessageModel *> *) GetAllConvesations{
 return   [[ConversationModel allObjects] sortedResultsUsingKeyPath:@"LastUpdateTime" ascending:YES];

}
-(RLMResults<MessageModel *> *)GetConvesationByID:(NSString*)ID{
return  [[MessageModel objectsWhere:@"ConversationId == %@",ID ]sortedResultsUsingKeyPath:@"Date" ascending:YES];

}

@end
