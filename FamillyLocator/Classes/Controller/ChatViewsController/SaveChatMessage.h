//
//  SaveChatMessage.h
//  wiizme
//
//  Created by abdelrhman gamil on 7/7/17.
//  Copyright © 2017 Amira. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ConversationModel.h"
#import "MessageModel.h"

@interface SaveChatMessage : NSObject{
RLMResults<MessageModel *> *msgDBData;
    int index;

}
@property (strong,nonatomic) ConversationModel *conversationData;

-(RLMResults<MessageModel *> *) GetAllConvesations;
-(RLMResults<MessageModel *> *)GetConvesationByID:(NSString*)ID;

-(void)AddObserver;
-(void)SaveMessage:(NSDictionary*)Message;
@end
