//
//  ChatListViewController.h
//  wiizme
//
//  Created by Amira on 3/7/17.
//  Copyright © 2017 Amira. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "WebServiceFunc.h"
#import "DKPaginatedTableViewController.h"
#import "SaveChatMessage.h"
@interface ChatListViewController : DKPaginatedTableViewController<UITableViewDelegate,UITableViewDataSource,WebServiceProtocol>
{
    __weak IBOutlet UITableView *tableView;
    SaveChatMessage * ChatMessage;
    
}

@end
