//
//  ConversationViewController.m
//  wiizme
//
//  Created by Amira on 3/8/17.
//  Copyright © 2017 Amira. All rights reserved.
//

#import "ConversationViewController.h"
#import "Animation.h"
#import "Constant.h"
#import "MBProgressHUD.h"
#import "UIViewController+RemoteNotificationData.h"
#import "SideMenuViewController.h"
#import "ConsersationRecieverTableViewCell.h"
#import "ConversationSenderTableViewCell.h"
#import "Helper.h"
#import "MessageModel.h"
#import "ConversationModel.h"

#define ROWS_PER_PAGE 10
#define TOTAL_ROWS 300

@interface ConversationViewController ()
{
    WebServiceFunc *webService;
    MBProgressHUD *hud;
    int serviceType,keyboardHeight;
    RLMResults<MessageModel *> *msgDBData;
    BOOL firstTimedb;
    id sucessObj;
    NSMutableArray *msgserverData;
    
}
@end

@implementation ConversationViewController


- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    ChatMessage = [[SaveChatMessage alloc] init];
    
    
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(NewMessgae:) name:@"ReloadMessageData" object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(showChatMsg:) name:@"ShowChatMsg" object:nil];
    
    
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWasShown:)
                                                 name:UIKeyboardDidShowNotification
                                               object:nil];
    [self updateLayout];
    if (self.firstconversationData==nil) {
        chatName.text=self.conversationData.ChatName;
    }else{
        chatName.text=[self.firstconversationData objectForKey:@"ChatName"];
        
    }
}

-(void)viewDidAppear:(BOOL)animated{
    
    [super viewDidAppear:animated];
    
    if (self.firstconversationData==nil) {
        
        msgDBData = [ChatMessage GetConvesationByID:self.conversationData.Id ];
        
        [self.tableView reloadData];
        int Section = 0;
        long lastRow = [self.tableView numberOfRowsInSection:Section] - 1;
        if (lastRow>0) {
            
        
        [self.tableView scrollToRowAtIndexPath:[NSIndexPath indexPathForRow: lastRow inSection:Section] atScrollPosition:UITableViewScrollPositionBottom animated:NO];
        }
        if (msgDBData.count==0) {
            
            webService=[[WebServiceFunc alloc]init];
            [webService setDelegate:self];
            hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
            hud.mode = MBProgressHUDModeIndeterminate;
            [super loadDataWithRowsPerPage:ROWS_PER_PAGE success:^{
                NSLog(@"First data was loaded...");
            } failure:^(NSError *error) {
                
            }];
        }
        
    }else{
        firstTimedb=YES;
    }
    
    
}

-(void)NewMessgae:(NSNotification *)notification{
    NSDictionary *dic=notification.userInfo;
    msgDBData = [ChatMessage GetConvesationByID:[dic objectForKey:@"ConversationId"]];
    
    
    [self.tableView reloadData];
    int Section = 0;
    long lastRow = [self.tableView numberOfRowsInSection:Section] - 1;
    [self.tableView scrollToRowAtIndexPath:[NSIndexPath indexPathForRow: lastRow inSection:Section] atScrollPosition:UITableViewScrollPositionBottom animated:YES];
}

-(void)showChatMsg:(NSNotification *)notification{
    
    NSDictionary *dic=notification.userInfo;
    chatName.text=[[dic objectForKey:@"Conversation"] objectForKey:@"ChatName"];
    
    msgDBData = [ChatMessage GetConvesationByID:[dic objectForKey:@"ConversationId"]];
    
    [self.tableView reloadData];
    
}

- (void)loadingData:(void (^)(NSUInteger totalRowsCount, NSArray *rows))success failure:(void (^)(NSError *error))failure{
    [super loadingData:success failure:failure];
    
    serviceType=1;
    NSDateFormatter *dateFormatter=[[NSDateFormatter alloc] init];
    NSLocale *locale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US"];
    [dateFormatter setLocale:locale];
    
    [dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSSS"];
    NSString *currentDate=[dateFormatter stringFromDate:[NSDate date]];
    NSString *urlString=[NSString stringWithFormat:@"%@%@&startDate=%@&index=%lu&size=10",ConversationMessages,self.conversationData.Id,currentDate,(unsigned long)[self currentPage]];
    [webService getData:urlString withflagLogin:YES];
    sucessObj=success;
    
}


- (void) wait:(void (^)(NSUInteger totalRowsCount, NSArray *rows))success{
    
    
    if(msgserverData.count > 0){
        
        success(TOTAL_ROWS, [msgserverData copy]);
    }
}
-(void)updateLayout{
    
    [[msgView layer] setBorderWidth:1.5f];
    [[msgView layer] setBorderColor:[UIColor colorWithRed:224.0/255.0f green:224.0/255.0f blue:224.0/255.0f alpha:1.0f].CGColor];
    [Animation roundCornerForView:msgView withAngle:5.0f];
    
    [popView.layer setShadowColor:[[UIColor darkGrayColor] CGColor]];
    [popView.layer setShadowOffset:CGSizeMake(0, 0)];
    [popView.layer setShadowRadius:20.0];
    [popView.layer setShadowOpacity:1];
    popView.clipsToBounds = NO;
    popView.layer.masksToBounds = NO;
    
    
    [Animation roundCornerForView:popView withAngle:10.0f];
}
-(void)viewDidDisappear:(BOOL)animated{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

-(void)OperHolderAnimation{
    
    [UIView beginAnimations: @"anim" context: nil];
    [UIView setAnimationBeginsFromCurrentState: YES];
    [UIView setAnimationDelegate:self];
    
    [UIView setAnimationDuration: .5];
    popView.alpha =1;
    [UIView commitAnimations];
    
    
}
- (IBAction)showMenu
{
    UIStoryboard *storyBoard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    SideMenuViewController *sideMenu=[storyBoard instantiateViewControllerWithIdentifier:@"SideMenu"];
    [self.navigationController presentViewController:sideMenu animated:YES completion:nil];
}

-(IBAction)writecomment:(id)sender{
    
    CGRect newFrame = msgTxt.frame;
    newFrame.size.height=80.0f;
    msgTxt.frame = newFrame;
    self.msgTXTHeightConstraint.constant=40;
    [self.view needsUpdateConstraints];
    [self.tableView reloadData];
    
    NSString *trimmedString;
    if (msgTxt.text.length>40) {
        trimmedString=[msgTxt.text substringWithRange:NSMakeRange(msgTxt.text.length-40,msgTxt.text.length)];
    }else{
        trimmedString=msgTxt.text;
    }
    NSString *uuid = [[NSUUID UUID] UUIDString];
    uuid = [uuid lowercaseString];
    NSDictionary *msgDic=[[NSDictionary alloc]initWithObjectsAndKeys:trimmedString,@"ShortTXT",uuid,@"MsgId",msgTxt.text,@"MsgTXT", nil];
    msgTxt.text=@"";
    [msgTxt resignFirstResponder];
    [self addMessage:msgDic];
    [self sendMsgService:msgDic];

   // NSIndexPath* ipath = [NSIndexPath indexPathForRow: msgDBData.count-1 inSection: 1];
   // [msgTxt scrollToRowAtIndexPath: ipath atScrollPosition: UITableViewScrollPositionTop animated: YES];
    
    int Section = 0;
    long lastRow = [self.tableView numberOfRowsInSection:Section] - 1;
     if (lastRow>0) {
         
    [self.tableView scrollToRowAtIndexPath:[NSIndexPath indexPathForRow: lastRow inSection:Section] atScrollPosition:UITableViewScrollPositionBottom animated:YES];
     }
}

-(void)addMessage:(NSDictionary*)convDic{
    NSString *userID=self.conversationData.UserId;
    
    
    ConversationModel *convModel=[[ConversationModel alloc]init];
    RLMRealm *realm = [RLMRealm defaultRealm];
    // RLMResults<ConversationModel *> *convData = [ConversationModel objectsWhere:@"Id == %@",[self.conversationData objectForKey:@"ConversationId"]];
    if(firstTimedb){
        
        [realm beginWriteTransaction];
        if (self.firstconversationData==nil) {
            convModel.Id=self.conversationData.Id;
            convModel.ChatName=self.conversationData.ChatName;
            convModel.LogoId=self.conversationData.LogoId;
            convModel.UserId=self.conversationData.UserId;
            
        }else{
            convModel.Id=[self.firstconversationData objectForKey:@"ConversationId"];
            convModel.ChatName=[self.firstconversationData objectForKey:@"ChatName"];
            convModel.LogoId=[self.firstconversationData objectForKey:@"ChatImage"];;
            convModel.UserId=[self.firstconversationData objectForKey:@"UserId"];
            convModel.GroupId=[self.firstconversationData objectForKey:@"GroupID"];
            
        }
        
        convModel.LastUpdateTime=[NSDate date];
        convModel.UnseenMsgNo=0;
        convModel.CreatorId=@"00000000-0000-0000-0000-000000000000";
        convModel.LastText=[convDic objectForKey:@"ShortTXT"];
        convModel.Mute=NO;
        convModel.CreationDate=[NSDate date];
        [realm addOrUpdateObject:convModel];
        // now commit & end this transaction
        [realm commitWriteTransaction];
        self.conversationData=convModel;
        firstTimedb=NO;
        
    }else{
        [realm beginWriteTransaction];
        
        //        NSDateFormatter *dateFormatter=[[NSDateFormatter alloc] init];
        //
        //        [dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ssZ"];
        //
        //        NSString *currentDate=[dateFormatter stringFromDate:[NSDate date]];
        
        
        convModel.Id=self.conversationData.Id;
        convModel.UserId =self.conversationData.UserId;
        convModel.GroupId =self.conversationData.GroupId;
        convModel.LastUpdateTime=[NSDate date];
        convModel.LastText=[convDic objectForKey:@"ShortTXT"];
        convModel.CreatorId=self.conversationData.CreatorId;
        convModel.CreationDate =self.conversationData.CreationDate;
        convModel.ChatName = chatName.text;
        convModel.LogoId =self.conversationData.LogoId;
        [realm addOrUpdateObject:convModel];
        // now commit & end this transaction
        [realm commitWriteTransaction];
    }
    
    MessageModel *msgModel=[[MessageModel alloc]init];
    [realm beginWriteTransaction];
    msgModel.Id=[convDic objectForKey:@"MsgId"];
    msgModel.ConversationId=self.conversationData.Id;
    msgModel.Text=[convDic objectForKey:@"MsgTXT"];
    msgModel.Date=[NSDate date];
    msgModel.UserId=self.conversationData.UserId;
    msgModel.MessageType=@"Text";
    
    
    [realm addObject:msgModel];
    // now commit & end this transaction
    [realm commitWriteTransaction];
    
    dispatch_async(dispatch_get_main_queue(), ^{
        msgDBData = [[MessageModel objectsWhere:@"ConversationId == %@",self.conversationData.Id ]sortedResultsUsingKeyPath:@"Date" ascending:YES];
        [self.tableView reloadData];
        int Section = 0;
        long lastRow = [self.tableView numberOfRowsInSection:Section] - 1;
        [self.tableView scrollToRowAtIndexPath:[NSIndexPath indexPathForRow: lastRow inSection:Section] atScrollPosition:UITableViewScrollPositionBottom animated:YES];
    });
    
    
}
-(void)sendMsgService:(NSDictionary*)dic{
    NSDateFormatter *dateFormatter=[[NSDateFormatter alloc] init];
    NSLocale *locale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US"];
    [dateFormatter setLocale:locale];
    
    [dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ssZ"];
    
    NSString *currentDate=[dateFormatter stringFromDate:[NSDate date]];
    NSString *creationDate=[dateFormatter stringFromDate:self.conversationData.CreationDate];
    
    if (webService==nil) {
        webService=[[WebServiceFunc alloc]init];
        [webService setDelegate:self];
    }
    serviceType=2;
    NSString *userID=[[NSUserDefaults standardUserDefaults] objectForKey:@"UserId"];
    NSMutableDictionary *convdic;
    
     
    
    
    NSString * logoID =[self.conversationData.LogoId stringByReplacingOccurrencesOfString:PhotosUrl withString:@""];
    
    convdic=[NSMutableDictionary dictionaryWithObjectsAndKeys:self.conversationData.UserId ,@"UserId",
             self.conversationData.GroupId,@"GroupId",
             logoID,@"LogoId",
             @"00000000-0000-0000-0000-000000000000",@"CreatorId",
             [dic objectForKey:@"ShortTXT"],@"LastText",
             currentDate,@"LastUpdateTime",
             creationDate,@"CreationDate",
             [NSNumber numberWithBool:NO],@"Mute",
             self.conversationData.ChatName,@"ChatName",
             nil];
    
    
    NSMutableDictionary *dicData=[NSMutableDictionary dictionaryWithObjectsAndKeys:
                                  [dic objectForKey:@"MsgId"],@"Id",
                                  @"00000000-0000-0000-0000-000000000000",@"UserId",
                                  self.conversationData.Id,@"ConversationId",
                                  @"00000000-0000-0000-0000-000000000000",@"FileId",
                                  @"00000000-0000-0000-0000-000000000000",@"FileTypeId",
                                  currentDate,@"Date",
                                  [dic objectForKey:@"MsgTXT"],@"Text",
                                  @"Text",@"MessageType",
                                  [NSNumber numberWithBool:false],@"IsSent",
                                  convdic,@"Conversation", nil];
    
    hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    hud.mode = MBProgressHUDModeIndeterminate;
    
    [webService postDataWithUrlString:SendMsgChat withData:dicData withflagLogin:YES];
}
-(void)userWebserviceCallDidSuccessWithRespone:(NSMutableArray *)response{
    [hud setHidden:YES];
    if (serviceType==1) {
        
        RLMRealm *realm = [RLMRealm defaultRealm];
        [realm beginWriteTransaction];
        for (int i=0; i<response.count; i++) {
            NSDateFormatter *dateFormatter=[[NSDateFormatter alloc] init];
            NSLocale *locale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US"];
            [dateFormatter setLocale:locale];
            
            [dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSSS"];
            
            MessageModel *msgModel=[[MessageModel alloc]init];
            
            msgModel.Id=[[response objectAtIndex:i] objectForKey:@"id"];
            msgModel.ConversationId=self.conversationData.Id;
            msgModel.Text=[[response objectAtIndex:i] objectForKey:@"text"];
            msgModel.Date=[dateFormatter dateFromString:[[response objectAtIndex:i] objectForKey:@"date"]];
            msgModel.UserId=[[response objectAtIndex:i] objectForKey:@"userId"];
            msgModel.MessageType=[[response objectAtIndex:i] objectForKey:@"messageType"];
            
            [realm addObject:msgModel];
            // now commit & end this transaction
            
        }
        [realm commitWriteTransaction];
        dispatch_async(dispatch_get_main_queue(), ^{
            msgDBData = [[MessageModel objectsWhere:@"ConversationId == %@",self.conversationData.Id] sortedResultsUsingKeyPath:@"Date" ascending:YES];
            [self.tableView reloadData];

            int Section = 0;
            long lastRow = [self.tableView numberOfRowsInSection:Section] - 1;
            [self.tableView scrollToRowAtIndexPath:[NSIndexPath indexPathForRow: lastRow inSection:Section] atScrollPosition:UITableViewScrollPositionBottom animated:YES];
            
        });
    }else if (serviceType==2){
        
    }else{
        //  [self.navigationController popViewControllerAnimated:YES];
    }
    
    
}
-(void)userWebserviceCallDidFaildwithError:(NSError *)error andMessage:(NSString *)message{
    [hud setHidden:YES];
    NSLog(@"%@",error.localizedDescription);
    if ([message isEqualToString:@"401" ]) {
        UIStoryboard *storyboard=[UIStoryboard storyboardWithName:@"Main" bundle:nil];
        UIViewController* viewController=[storyboard instantiateViewControllerWithIdentifier:@"LoginController"];
        [self presentViewController:viewController animated:YES completion:nil];
    }else if ([message isEqualToString:@"404"]){
        
    }
    
}


#pragma TableView DataSource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    NSLog(@"%lu",(unsigned long)msgDBData.count);
    if (indexPath.row<msgDBData.count) {
        UILabel *lbl=[[UILabel alloc]initWithFrame:CGRectMake(83, 37, self.view.frame.size.width-83-6,21)];
        
        MessageModel *dic=[msgDBData objectAtIndex:indexPath.row];
        lbl=[self customizeLBLwithString:lbl andtxt:dic.Text andfintsize:13];
        
        CGFloat height=lbl.frame.size.height+30;
        return height;
    }else{
        return 0;
    }
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    NSLog(@"%lu",(unsigned long)msgDBData.count);
    return (unsigned long)msgDBData.count;
    
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    NSLog(@"%lu",indexPath.row);
    
    
    MessageModel *dic=[msgDBData objectAtIndex:indexPath.row];
    NSString *userID=[[NSUserDefaults standardUserDefaults] objectForKey:@"UserId"];
    NSString *msgUserId=dic.UserId;
    if ([userID isEqualToString:msgUserId]) {
        ConversationSenderTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"senderCell"];
        cell.sendDate.attributedText=[Helper timeTXT:[Helper timeLeftSinceDate:[NSString stringWithFormat:@"%@",dic.Date]]];
        cell.senderMsg.text=dic.Text;
        cell.senderMsg=[self customizeLBLwithString:cell.senderMsg andtxt:dic.Text andfintsize:13];
        cell.viewSenderHeightConstraint.constant=cell.senderMsg.frame.size.height;
        [Animation roundCornerForView:cell.senderView withAngle:5.0f];
        
        return cell;
    }else{
        ConsersationRecieverTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"recieverCell"];
        cell.recieveDate.attributedText=[Helper timeTXT:[Helper timeLeftSinceDate:[NSString stringWithFormat:@"%@",dic.Date]]];
        cell.recieverMsg.text=dic.Text;
        cell.recieverMsg=[self customizeLBLwithString:cell.recieverMsg andtxt:dic.Text andfintsize:13];
        cell.viewRecieverHeightConstraint.constant=cell.recieverMsg.frame.size.height;
        [Animation roundCornerForView:cell.recieverView withAngle:5.0f];
        if (self.isGroup) {
            cell.senderNameHeightConstraint.constant=20.0f;
        }else{
            cell.senderNameHeightConstraint.constant=0.0f;
        }
        return cell;
    }
    
    
    
    
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    // This will create a "invisible" footer
    return 0.01f;
}


- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    return [UIView new];
    
}


-(UILabel*)customizeLBLwithString:(UILabel*)lbl andtxt:(NSString*)lblstr andfintsize:(int)fontSize{
    //UILabel *lbl=[[UILabel alloc]initWithFrame:frame];
    lbl.numberOfLines = 0; //will wrap text in new line
    CGSize maximumLabelSize = CGSizeMake(lbl.frame.size.width, FLT_MAX);
    CGSize expectedLabelSize = [lblstr sizeWithFont:[UIFont fontWithName:@"Cairo-SemiBold" size:fontSize] constrainedToSize:
                                maximumLabelSize lineBreakMode:lbl.lineBreakMode];
    //adjust the label the the new height.
    CGRect newFrame = lbl.frame;
    newFrame.size.height = expectedLabelSize.height;
    lbl.frame = newFrame;
    [lbl setFont:[UIFont fontWithName:@"Cairo-SemiBold" size:fontSize]];
    lbl.textAlignment=NSTextAlignmentLeft;
    // lbl.backgroundColor=[UIColor whiteColor];
    [lbl setNeedsUpdateConstraints];
    //  [scrollView setContentSize:CGSizeMake(self.view.frame.size.width, ContainerView.frame.origin.y+details.frame.origin.y+details.frame.size.height+60)];
    return lbl;
}


- (void)moveBannerOffScreen {
    [self.view layoutIfNeeded];
    _tableHeightConstraint.constant =_tableHeightConstraint.constant-10;
    self.msgTXTHeightConstraint.constant=self.msgTXTHeightConstraint.constant+10;
    [UIView animateWithDuration:5
                     animations:^{
                         [self.view layoutIfNeeded]; // Called on parent view
                     }];
    [self animateTextView:msgTxt up:YES];
}

- (void)textViewDidBeginEditing:(UITextView *)textView{
    if ([textView.text isEqualToString:@"Write Comment"]) {
        textView.text=@"";
    }
    textView.textColor=[UIColor blackColor];
    [self animateTextView:textView up:YES];
}
-(void)textViewDidEndEditing:(UITextView *)textView{
    [self animateTextView:textView up:NO];
}

- (void) animateTextView: (UITextView*) textView up: (BOOL) up
{
    
    const int movementDistance =  keyboardHeight; // tweak as needed
    const float movementDuration = 0.1f; // tweak as needed
    
    int movement = (up ? -movementDistance : movementDistance);
    
    [UIView beginAnimations: @"anim" context: nil];
    [UIView setAnimationBeginsFromCurrentState: YES];
    [UIView setAnimationDuration: movementDuration];
    msgView.frame = CGRectOffset(msgView.frame, 0, movement);
    [UIView commitAnimations];
}

- (void)keyboardWasShown:(NSNotification *)notification
{
    
    // Get the size of the keyboard.
    CGSize keyboardSize = [[[notification userInfo] objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    if(keyboardHeight==0){
        //Given size may not account for screen rotation
        keyboardHeight = MIN(keyboardSize.height,keyboardSize.width);
        
        const int movementDistance =  keyboardHeight; // tweak as needed
        const float movementDuration = 0.1f; // tweak as needed
        
        int movement = (/* DISABLES CODE */ (YES) ? -movementDistance : movementDistance);
        
        [UIView beginAnimations: @"anim" context: nil];
        [UIView setAnimationBeginsFromCurrentState: YES];
        [UIView setAnimationDuration: movementDuration];
        msgView.frame = CGRectOffset(msgView.frame, 0, movement);
        [UIView commitAnimations];
    }
    
    //your other code here..........
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text {
    return YES;
}

- (void)textViewDidChange:(UITextView *)textView{
    
    CGFloat maxHeight = 312.0f;
    CGFloat fixedWidth = msgTxt.frame.size.width;
    CGFloat fixedHight = msgTxt.frame.size.height;
    CGSize newSize = [msgTxt sizeThatFits:CGSizeMake(fixedWidth, MAXFLOAT)];
    CGRect newFrame = msgTxt.frame;
    newFrame.size = CGSizeMake(fmaxf(newSize.width, fixedWidth), fminf(newSize.height, maxHeight));
    [UIView beginAnimations:@"" context:nil];
    [UIView setAnimationDuration:0.1];
    
    
    
    
    
    CGFloat Def =newFrame.size.height-fixedHight;
    if (Def>0&&msgTxt.frame.size.height<80) {
        msgTxt.frame = newFrame;
        [self moveBannerOffScreen];
        // commentBar.frame = CGRectMake(commentBar.frame.origin.x, commentBar.frame.origin.y-Def, commentBar.frame.size.width, commentBar.frame.size.height+Def);
        //   tableview.frame = CGRectMake(tableview.frame.origin.x, tableview.frame.origin.y, tableview.frame.size.width, tableview.frame.size.height-Def);
    }
    if (Def<0&&msgTxt.frame.size.height) {
        msgTxt.frame = newFrame;
        // commentBar.frame = CGRectMake(commentBar.frame.origin.x, commentBar.frame.origin.y-Def, commentBar.frame.size.width, commentBar.frame.size.height+Def);
        //  tableview.frame = CGRectMake(tableview.frame.origin.x, tableview.frame.origin.y, tableview.frame.size.width, tableview.frame.size.height-Def);
    }
    [UIView commitAnimations];
    
}



/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
