//
//  ConversationViewController.h
//  wiizme
//
//  Created by Amira on 3/8/17.
//  Copyright © 2017 Amira. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "WebServiceFunc.h"
#import "PagingChatTableView.h"
#import "ConversationModel.h"
#import "SaveChatMessage.h"
@interface ConversationViewController : PagingChatTableView<WebServiceProtocol,UITextViewDelegate,UITableViewDelegate,UITableViewDataSource>
{
    __weak IBOutlet UILabel * chatName;
    __weak IBOutlet UITextView *msgTxt;
    __weak IBOutlet UIView * popView;
    __weak IBOutlet UIButton *settingBut;
    __weak IBOutlet UIView * msgView;
    __weak IBOutlet UIButton *sendMsgBut;
    __weak IBOutlet UIButton *sendImgBut;
    __weak IBOutlet UIButton *sendAudioBut;
     SaveChatMessage * ChatMessage;
    NSString * GroupID ;
}
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *tableHeightConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *msgTXTHeightConstraint;
@property (strong,nonatomic) ConversationModel *conversationData;
@property (strong,nonatomic) NSDictionary *firstconversationData;

@property (nonatomic) BOOL isGroup;
@end
