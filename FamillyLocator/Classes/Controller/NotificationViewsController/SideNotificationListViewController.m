//
//  SideNotificationListViewController.m
//  wiizme
//
//  Created by Amira on 2/6/17.
//  Copyright © 2017 Amira. All rights reserved.
//

#import "SideNotificationListViewController.h"
#import "GroupTableViewCell.h"
#import "UIViewController+HeaderView.h"
#import "Animation.h"
#import "MBProgressHUD.h"
#import "Constant.h"
#import "HomeGroupViewController.h"
#import "SettingGroupViewController.h"
#import "UIViewController+RemoteNotificationData.h"
#import "Helper.h"
#import "SideMenuViewController.h"
#import "ConversationModel.h"
#import "ConversationViewController.h"
#define ROWS_PER_PAGE 10
#define TOTALTablerows 43
#define HEIGHT_LOADING_VIEW 44.0


@interface SideNotificationListViewController ()
{
    NSMutableArray *tableData;
    WebServiceFunc *webService;
    MBProgressHUD *hud;
    int selectedRow;
    int serviceType;
    id sucessObj;
}
@end

@implementation SideNotificationListViewController

- (void)viewDidLoad {
    NotficationData = [[NSMutableArray alloc] init];
    [super viewDidLoad];
    webService=[[WebServiceFunc alloc]init];
    [webService setDelegate:self];
    _currentPage=1;
    hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    
    hud.mode = MBProgressHUDModeIndeterminate;
   
       [self GetTableContent:1];
    // Do any additional setup after loading the view.
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(receiveMsg:) name:@"ReceiveMsg" object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(showChatMsg:) name:@"ShowChatMsg" object:nil];
    
}

-(void)receiveMsg:(NSNotification *)notification{
    
    NSDictionary *dic=notification.userInfo;
    NSString *convId=[dic objectForKey:@"ConversationId"];
    [self viewDidAppear:YES];
    [tableView reloadData];
    UIApplicationState state = [[UIApplication sharedApplication] applicationState];
    if (state == UIApplicationStateBackground || state == UIApplicationStateInactive)
    {
        dispatch_async(dispatch_get_main_queue(), ^{
            UILocalNotification* localNotification = [[UILocalNotification alloc] init];
            localNotification.fireDate = [NSDate dateWithTimeIntervalSinceNow:0];
            localNotification.alertBody = [NSString stringWithFormat:@"%@ send message",[[dic objectForKey:@"Conversation"] objectForKey:@"ChatName"]];
            localNotification.alertAction = @"Show me";
            localNotification.repeatInterval=NO;
            localNotification.timeZone = [NSTimeZone defaultTimeZone];
            localNotification.userInfo=dic;
            [[UIApplication sharedApplication]scheduleLocalNotification:localNotification];
        });
    }
    
    
}

-(void)showChatMsg:(NSNotification *)notification{
    NSDictionary *dic=notification.userInfo;
    
    RLMResults<ConversationModel *> *convData = [ConversationModel objectsWhere:@"Id == %@",[dic objectForKey:@"ConversationId"]];
    ConversationModel *convdic=[convData objectAtIndex:0];
    UIStoryboard *chatStoryBoard=[UIStoryboard storyboardWithName:@"Chat" bundle:nil];
    ConversationViewController *conversationView=[chatStoryBoard instantiateViewControllerWithIdentifier:@"ConversationController"];
    conversationView.conversationData=convdic;
    
    [self.navigationController pushViewController:conversationView animated:YES];
    
    
}

- (IBAction)showMenu
{
    
    SideMenuViewController *sideMenu=[self.storyboard instantiateViewControllerWithIdentifier:@"SideMenu"];
    [self.navigationController presentViewController:sideMenu animated:YES completion:nil];
}

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:YES];
}

-(void)viewDidDisappear:(BOOL)animated{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}



#pragma TableView DataSource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return NotficationData.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    GroupTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"groupCell"];
    
    
    cell.groupName.text=[[NotficationData objectAtIndex:indexPath.row] objectForKey:@"body"];
    cell.groupsecur.attributedText=[Helper timeTXT:[Helper timeLeftSinceDate:[[NotficationData objectAtIndex:indexPath.row] objectForKey:@"date"]]];
    
    [cell.image setImage:[UIImage imageNamed:@"menu-profile.png"]];
    NSURL *imageUrl=[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",PhotosUrl,[[NotficationData objectAtIndex:indexPath.row] objectForKey:@"iconId"]]];
    [cell.image setImageURL:imageUrl];
    NSLog(@"%f",cell.image.frame.size.width);
    [Animation roundCornerForView:cell.image withAngle:30.0f];
    BOOL visited=[[[NotficationData objectAtIndex:indexPath.row] objectForKey:@"isVisited"] boolValue];
    if (visited) {
        cell.backgroundColor=[UIColor whiteColor];
    }else{
        cell.backgroundColor=[UIColor colorWithRed:135/255.0f green:185/255.0f blue:169/255.0f alpha:1.0f];
    }
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    // This will create a "invisible" footer
    return 0.01f;
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section {
    
    if (section==0) {
        return nil;
    }else{
        UIView *headerView = [[UIView alloc] init];
        
        [headerView setBackgroundColor:[UIColor clearColor]];
        
        UIButton *button = [UIButton buttonWithType:UIButtonTypeRoundedRect];
        [button addTarget:self
                   action:@selector(aMethod:)
         forControlEvents:UIControlEventTouchDown];
        [button setTitle:@"Load Data" forState:UIControlStateNormal];
        button.frame = CGRectMake(10.0, 210.0, 160.0, 40.0);
        
        
        [headerView addSubview:button];
        return headerView;
    }
    
}

- (void) tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    if(indexPath.row == NotficationData.count - 1 && NotficationData.count>10){
        if((_isLoading == NO && _totalPages >= _currentPage && _totalPages > 1) || (_totalPages == 0 && _isLoading == NO) )
        {
            if(_totalRowsCount > 0){
                //_loadingLabel.text = [NSString stringWithFormat:@" %d / %d", _rows.count, NotficationData];
            }
            if (_currentPage>=6) {
                
            }else{
                [self showLoadingView:YES];
            }
        }
    }
}

#pragma TableView Delegate

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    selectedRow=indexPath.row;
    [self notificationData:[NotficationData objectAtIndex:indexPath.row]];
}


- (void) initialization{
    
    _backgroundView = [[UIView alloc] initWithFrame:tableView.frame];
    tableView.backgroundView = _backgroundView;
    UIColor *backgroundColor = tableView.backgroundColor;
    if(backgroundColor){
        _backgroundView.backgroundColor = backgroundColor;
    }
    
    _loadingView = [[UIView alloc] initWithFrame:CGRectMake(0, _backgroundView.frame.size.height - HEIGHT_LOADING_VIEW, _backgroundView.frame.size.width, HEIGHT_LOADING_VIEW)];
    _loadingView.backgroundColor = [UIColor clearColor];
    [_backgroundView addSubview:_loadingView];
    [_loadingView setHidden:YES];
}

- (void) initLoadingView{
    _activitiIndicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    [_activitiIndicator setFrame:CGRectMake(_backgroundView.frame.size.width/2, 11, 30, 30)];
    [_activitiIndicator startAnimating];
    [_loadingView addSubview:_activitiIndicator];
    
    _loadingLabel = [[UILabel alloc] initWithFrame:CGRectMake(_activitiIndicator.frame.origin.x- _activitiIndicator.frame.size.width-20, 14, 220, 20)];
    [_loadingView addSubview:_loadingLabel];
    _loadingLabel.text = @"";
    // _loadingLabel.textAlignment=NSTextAlignmentCenter;
    [_loadingView setHidden:YES];
}


- (void) showLoadingView:(BOOL)isAnimated{
    
    if(_activitiIndicator == nil || _loadingLabel == nil) {
        [self initLoadingView];
    }
    
    [_loadingView setHidden:NO];

    _currentPage++;
    [self GetTableContent:_currentPage];
    _isLoading = YES;
   // [tableView setContentOffset:CGPointMake(0, tableView.contentSize.height - tableView.bounds.size.height + HEIGHT_LOADING_VIEW -10) animated:isAnimated];
}

-(void)GetTableContent:(NSInteger )currentPage{
    
    
  
    
    NSString *urlString=[NSString stringWithFormat:@"%@%lu&size=20",ListNotification,(unsigned long) _currentPage];
    
    [webService getData:urlString withflagLogin:YES];
  
    
}


- (void) hideLoadingView{
    [_loadingView setHidden:YES];
    [tableView setContentOffset:CGPointMake(0, tableView.contentOffset.y) animated:YES];
}






- (void) scrollViewDidScroll:(UIScrollView *)scrollView{
    if(_shouldLoad){
        [self showLoadingView:YES];
    }
    
 
    
}


- (void) scrollViewWillEndDragging:(UIScrollView *)scrollView withVelocity:(CGPoint)velocity targetContentOffset:(inout CGPoint *)targetContentOffset{
    if (tableView.contentOffset.y == tableView.contentSize.height - tableView.bounds.size.height){
        _shouldLoad = YES;
        
    }
    else{
        _shouldLoad = NO;
    }

}


-(void)userWebserviceCallDidSuccessWithRespone:(NSMutableArray *)response{
    dispatch_async(dispatch_get_main_queue(), ^{
        [hud setHidden:YES];
    });
    tableData=[[NSMutableArray alloc]init];
    tableData=response;
    
    //    if (RefreshFlag) {
    //        RefreshFlag=NO;
    //        [Tablerows removeAllObjects];
    //    }
    //
    if ([response isKindOfClass:[NSArray class]]) {
        
        
        if (NotficationData.count==0) {
            NotficationData = response;
        }else{
            
            //  [Tablerows arrayByAddingObjectsFromArray:response];
            [NotficationData addObjectsFromArray:response];
        }
        
        
        [tableView reloadData];
    }
    //  [self.refreshControl endRefreshing];
    if (_isLoading) {
        _isLoading = NO;
        [self hideLoadingView];
    }
    
    
    
    
    //    [super loadDataWithRowsPerPage:ROWS_PER_PAGE success:^{
    //        NSLog(@"First data was loaded...");
    //    } failure:^(NSError *error) {
    //
    //    }];
    
    
    //   [self performSelector:@selector(wait:) withObject:sucessObj afterDelay:0];
    
    
}

-(void)userWebserviceCallDidFaildwithError:(NSError *)error andMessage:(NSString *)message{
    dispatch_async(dispatch_get_main_queue(), ^{
        [hud setHidden:YES];
    });
    NSLog(@"%@",error.localizedDescription);
    if ([message isEqualToString:@"401" ]) {
        UIStoryboard *storyboard=[UIStoryboard storyboardWithName:@"Main" bundle:nil];
        UIViewController* viewController=[storyboard instantiateViewControllerWithIdentifier:@"LoginController"];
        [self presentViewController:viewController animated:YES completion:nil];
    }
    
}


/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
