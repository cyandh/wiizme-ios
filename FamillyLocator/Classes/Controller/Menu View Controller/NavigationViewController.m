//
//  NavigationViewController.m
//  InteractiveReporting
//
//  Created by Amira on 5/18/16.
//  Copyright © 2016 Cyan Digital House. All rights reserved.
//

#import "NavigationViewController.h"

@interface NavigationViewController ()

@end

@implementation NavigationViewController


- (void)viewDidLoad
{
    [super viewDidLoad];
    //[self.view addGestureRecognizer:[[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(panGestureRecognized:)]];
    // Do any additional setup after loading the view.
    [[UINavigationBar appearance] setBarTintColor:[UIColor colorWithRed:40.0/255.0 green:158.0/255.0 blue:222.0/255.0 alpha:1.0f]];
    [[UINavigationBar appearance] setTintColor:[UIColor whiteColor]];
    
    [[UINavigationBar appearance] setTranslucent:NO];
    
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStylePlain target:nil action:nil];
    self.navigationItem.backBarButtonItem.tintColor = [UIColor whiteColor];
    
}

-(NSString *)title{
    return @"";
    
}

#pragma mark -
#pragma mark Gesture recognizer

- (void)panGestureRecognized:(UIPanGestureRecognizer *)sender
{
    // Dismiss keyboard (optional)
    //    //
    //    [self.view endEditing:YES];
    //    [self.frostedViewController.view endEditing:YES];
    //
    //    // Present the view controller
    //    //
    //    [self.frostedViewController panGestureRecognized:sender];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
