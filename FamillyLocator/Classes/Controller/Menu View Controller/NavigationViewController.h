//
//  NavigationViewController.h
//  InteractiveReporting
//
//  Created by Amira on 5/18/16.
//  Copyright © 2016 Cyan Digital House. All rights reserved.
//

#import <UIKit/UIKit.h>
@interface NavigationViewController : UINavigationController<UINavigationBarDelegate,UINavigationControllerDelegate>
- (void)panGestureRecognized:(UIPanGestureRecognizer *)sender;
@end
