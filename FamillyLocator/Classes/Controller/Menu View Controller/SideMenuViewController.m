//
//  SideMenuViewController.m
//  wiizme
//
//  Created by Amira on 2/11/17.
//  Copyright © 2017 Amira. All rights reserved.
//

#import "SideMenuViewController.h"
#import "RegistrationViewController.h"
@interface SideMenuViewController ()

@end

@implementation SideMenuViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

-(IBAction)closeBTN:(id)sender{
    [self dismissViewControllerAnimated:YES completion:nil];
}
-(IBAction)termsBTN:(id)sender{
    
}
-(IBAction)profileBTN:(id)sender{
    RegistrationViewController *registView=[self.storyboard instantiateViewControllerWithIdentifier:@"RegisterController"];
    registView.editView=YES;
    [self presentViewController:registView animated:YES completion:nil];
}
-(IBAction)logoutBTN:(id)sender{
    [[NSNotificationCenter defaultCenter] postNotificationName:@"Logout"
                                                        object:nil
                                                      userInfo:nil];
}
-(IBAction)settingBTN:(id)sender{
    
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
