//
//  MenuTableViewController.m
//  InteractiveReporting
//
//  Created by Amira on 5/18/16.
//  Copyright © 2016 Cyan Digital House. All rights reserved.
//


#import "MenuTableViewController.h"
#import "UIViewController+REFrostedViewController.h"
#import "MenuTableViewCell.h"
#import "NavigationViewController.h"
@interface MenuTableViewController ()
{
    NSArray *images, *titles;
}
@end

@implementation MenuTableViewController

-(void)viewDidLoad{
    
    [super viewDidLoad];
    // self.tableView.separatorColor = [UIColor colorWithRed:150/255.0f green:161/255.0f blue:177/255.0f alpha:1.0f];
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    self.tableView.backgroundColor =[UIColor colorWithRed:41.0/255.0 green:41.0/255.0 blue:41.0/255.0 alpha:1.0f];
    
    self.tableView.tableHeaderView = ({
        UIView *view=[[UIView alloc] initWithFrame:CGRectMake(0,0,self.view.frame.size.width,100)];
        view.backgroundColor=[UIColor whiteColor];
        UIImage*image=[UIImage imageNamed:@"logo-menu.png"];
        UIImageView *logomenu=[[UIImageView alloc]initWithFrame:CGRectMake((view.frame.size.width/2+50.0)/2-image.size.width/2,(view.frame.size.height/2)-image.size.height/2, image.size.width,image.size.height)];
        
//        UIImageView *logomenu=[[UIImageView alloc]initWithFrame:CGRectMake((view.frame.size.width/2+50.0)/2-view.frame.size.width/4,0,view.frame.size.width/2,100)];
        
        logomenu.image=image;
        [view addSubview:logomenu];
        view;
    });
    
    
    
    
}



//- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
//    return [NSString stringWithFormat:@"Section %d", section];
//}
-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 0;
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    NSString *admin=[[NSUserDefaults standardUserDefaults]objectForKey:@"admin"];
    if ([admin isEqualToString:@"YES"]) {
        images = @[@"menu-about.png",@"menu-policy.png",@"menu-logout.png"];
        titles = @[NSLocalizedString(@"About",nil),NSLocalizedString(@"Terms & Conditions",nil),NSLocalizedString(@"Log out",nil)];
        return 3;
    }else{
        images = @[@"menu-agenda.png",@"menu-about.png",@"menu-policy.png",@"menu-logout.png"];
        titles = @[NSLocalizedString(@"Agenda",nil),NSLocalizedString(@"About",nil),NSLocalizedString(@"Terms & Conditions",nil),NSLocalizedString(@"Log out",nil)];
       return 4;
    }
    
    
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    return 40;
    
}
- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    cell.backgroundColor = [UIColor colorWithRed:41.0/255.0 green:41.0/255.0 blue:41.0/255.0 alpha:1.0f];
    cell.textLabel.textColor = [UIColor whiteColor];

}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    // This will create a "invisible" footer
    return 0.01f;
}


- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    return [UIView new];
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {

    
    static NSString *CustomCellIdentifier3 = @"MenuCell";
    MenuTableViewCell *cell = (MenuTableViewCell *)[tableView dequeueReusableCellWithIdentifier:CustomCellIdentifier3];
    if(cell==nil)
    {
        NSArray *nib =[[NSBundle mainBundle] loadNibNamed:@"MenuTableViewCell" owner:self options:nil];
        cell = [nib objectAtIndex:0];
        
    
        [tableView setSeparatorStyle:UITableViewCellSeparatorStyleSingleLine];
        [tableView setSeparatorColor:[UIColor blackColor]];
    
        
    [cell loadCellwithImageName:[images objectAtIndex:indexPath.row] andTitle:[titles objectAtIndex:indexPath.row]];
    }
    return cell;
    
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];

    NSString *admin=[[NSUserDefaults standardUserDefaults]objectForKey:@"admin"];
    int row;
    if ([admin isEqualToString:@"YES"]) {
        row=indexPath.row+1;
    }else{
        row=indexPath.row;
    }
    if (indexPath.section == 0 && row == 3) {
        
        
        
        [[NSNotificationCenter defaultCenter] postNotificationName:@"Logout"
                                                            object:nil
                                                          userInfo:nil];
        

    } else if (indexPath.section == 0 &&row == 1) {


        
      
        
        
    } else if (indexPath.section == 0 && row == 2) {
        // navigationController.viewControllers = @[DescWebviewVC];
        

  
        
    }else if (indexPath.section == 0 && row == 0){

//        navigationController.viewControllers = @[agenda];
//        self.frostedViewController.contentViewController = navigationController;

    }
 
    
    /// self.frostedViewController.contentViewController = navigationController;
  //  [self.frostedViewController hideMenuViewController];

}
- (void)CloseAbout{
    
    [contentViewController dismissViewControllerAnimated:YES completion:nil];
    UIViewController *nav=self.navigationController.visibleViewController;
      NSLog(@"dljfd%@",[self class]);
    
}



@end
