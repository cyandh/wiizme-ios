//
//  WebServiceProtocol.h
//  FamillyLocator
//
//  Created by Amira on 10/26/16.
//  Copyright © 2016 Amira. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol WebServiceProtocol <NSObject>

-(void)userWebserviceCallDidSuccessWithRespone:(NSMutableArray*)response ;
- (void)userWebserviceCallDidFaildwithError:(NSError *)error andMessage:(NSString*)message;
@optional
-(void)userImagesWebserviceCallDidSuccessWithRespone:(NSArray*)imageID ;
- (void)userImagesWebserviceCallDidFaildwithError:(NSError *)error andMessage:(NSString*)message;
@end
