//
//  IamFine.h
//  wiizme
//
//  Created by agamil on 5/13/17.
//  Copyright © 2017 Amira. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "WebServiceFunc.h"

@protocol IamFineDelegate <NSObject>

@required
- (void)IamFinefunctionRespnse:(NSMutableArray *)response service:(NSInteger )Type;
- (void)IamFinefunctionFail:(NSString *)message service:(NSInteger )Type;


@end


@interface IamFine : NSObject<WebServiceProtocol>{

  WebServiceFunc *webService;
}

-(void)finePost:(NSString * )groupID;

@property(nonatomic,readwrite)NSInteger FunctionType;
@property(nonatomic,weak)id<IamFineDelegate>deleget;
@end
