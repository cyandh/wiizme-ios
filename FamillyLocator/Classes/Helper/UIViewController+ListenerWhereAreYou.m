//
//  UIViewController+ListenerWhereAreYou.m
//  wiizme
//
//  Created by Amira on 1/18/17.
//  Copyright © 2017 Amira. All rights reserved.
//

#import "UIViewController+ListenerWhereAreYou.h"
#import "CustomalertViewController.h"
#import "Constant.h"
#import "AppDelegate.h"
@implementation UIViewController (ListenerWhereAreYou)
-(void)listenForRequest{
    dispatch_async(dispatch_get_main_queue(), ^{
        self.client = ((AppDelegate *)[[UIApplication sharedApplication] delegate]).client;
        
        NSString *userID=[[NSUserDefaults standardUserDefaults] valueForKey:@"UserId"];
        
        
        PTPusherChannel *channel = [self.client subscribeToChannelNamed:userID];
        
        [channel bindToEventNamed:@"where_are_you" handleWithBlock:^(PTPusherEvent *channelEvent) {
            

            // channelEvent.data is a NSDictianary of the JSON object received
            NSDictionary *message=[[NSDictionary alloc]init];
            if ([channelEvent.data isKindOfClass:[NSString class]]) {
                
                
                NSError *jsonError;
                
                NSData *objectData = [channelEvent.data dataUsingEncoding:NSUTF8StringEncoding];
                
                NSDictionary *json = [NSJSONSerialization JSONObjectWithData:objectData
                                                                     options:0
                                                                       error:&jsonError];
 
                message = json ;
            }else{
                message = channelEvent.data ;
                
            }
            
            CustomAlertViewController *alertView =[self.storyboard instantiateViewControllerWithIdentifier:@"alertView"];
            
            
            alertView.delegate=self;
            
            NSString *firstName=[message  objectForKey:@"FirstName"];
            NSString *lastName=[message  objectForKey:@"FamilyName"];
            

            alertView.userData=[[UserData alloc]init];
            alertView.userData.fieldId=[message objectForKey:@"Id"];
            alertView.userData.message=@"Want Permission To send you location";
            alertView.userData.name=[NSString stringWithFormat:@"%@ %@",firstName,lastName];
            alertView.userData.userImage=[NSString stringWithFormat:@"%@%@",PhotosUrl,[message objectForKey:@"PhotoId"]];
            alertView.userData.addImg=NO;

            
            alertView.popView.layer.shadowColor = [UIColor blackColor].CGColor;
            alertView.popView.layer.shadowOffset = CGSizeMake(0,0);
            [alertView.popView.layer setShadowRadius:200.0];
            
            alertView.popView.layer.shadowOpacity =1;
            alertView.popView.clipsToBounds = NO;
            alertView.popView.layer.masksToBounds = NO;
            
            
            
            [self.navigationController pushViewController:alertView animated:NO];
        }];
        
        [self.client connect];
    });
    
}


-(void)accept:(UserData *)userData{
    if (userData.addImg) {
       
        WebServiceFunc *webService=[[WebServiceFunc alloc]init];
        [webService setDelegate:self];
       
        UserData *user=[[UserData alloc]init];
        if (userData.uploadedImg==nil) {
            
            NSString *urlString=[NSString stringWithFormat:@"%@%@&flag=true&imageId=""&text=%@",whereAreYouResponse,userData.fieldId,userData.message];
            [webService getData:urlString withflagLogin:YES];

        }else{
        NSMutableArray *imageObject=[[NSMutableArray alloc]init];
        user.userImage=[webService encodeToBase64String:userData.uploadedImg];
        user.userImgId=ProfileImageID;
        
        NSMutableDictionary *imageData = [[NSMutableDictionary alloc] initWithObjectsAndKeys:user.userImgId,@"FileTypeId",user.userImage,@"Path",nil];
        [imageObject addObject:imageData];
        NSDictionary *userobj=[NSDictionary dictionaryWithObjectsAndKeys:userData.fieldId,@"userID",userData.message,@"userMsg", nil];
        [[NSUserDefaults standardUserDefaults] setObject:userobj forKey:@"UserWhereareYou"];
        [webService updateUserImagewithimageobject:imageObject];

        }
        
        
    }else{
         [self.navigationController popViewControllerAnimated:YES ];
        CustomAlertViewController *alertView =[self.storyboard instantiateViewControllerWithIdentifier:@"alertView"];
        
        
        alertView.delegate=self;
        
        
        alertView.userData=[[UserData alloc]init];
        alertView.userData.fieldId=userData.fieldId;
        alertView.Message.hidden=YES;
        alertView.userData.name=@"upload Image";
        alertView.userData.userImage=@"take-photo.png";
        alertView.userData.addImg=YES;
        
        
        alertView.popView.layer.shadowColor = [UIColor blackColor].CGColor;
        alertView.popView.layer.shadowOffset = CGSizeMake(0,0);
        [alertView.popView.layer setShadowRadius:20.0];
        
        alertView.popView.layer.shadowOpacity =1;
        alertView.popView.clipsToBounds = NO;
        alertView.popView.layer.masksToBounds = NO;
        
        
        
        [self.navigationController pushViewController:alertView animated:NO];
    }
    
}
-(void)decline:(UserData *)userData{
    WebServiceFunc *webService=[[WebServiceFunc alloc]init];
    [webService setDelegate:self];
    NSLog(@"%@",userData.name);
    NSString *urlString;
    if (userData.addImg) {
        [self.navigationController popViewControllerAnimated:YES ];
         urlString=[NSString stringWithFormat:@"%@%@&flag=true&imageId=""&text=""",whereAreYouResponse,userData.fieldId];
    }else{
       urlString=[NSString stringWithFormat:@"%@%@&flag=false&imageId=""&text=""",whereAreYouResponse,userData.fieldId];

    }
        [webService getData:urlString withflagLogin:YES];
    
   
    
}

-(void)userImagesWebserviceCallDidSuccessWithRespone:(NSArray *)imageID{
    NSLog(@"%@",imageID);
    
    NSString *imageid=[imageID objectAtIndex:0];
    imageid=[imageid stringByReplacingOccurrencesOfString:@"\"" withString:@""];
    
    WebServiceFunc *webService=[[WebServiceFunc alloc]init];
    [webService setDelegate:self];
    
    NSDictionary *userobj=[[NSUserDefaults standardUserDefaults] objectForKey:@"UserWhereareYou"];
    NSString *urlString=[NSString stringWithFormat:@"%@%@&flag=true&imageId=%@&text=%@",whereAreYouResponse,[userobj objectForKey:@"userID"],imageid,[userobj objectForKey:@"userMsg"]];
   
    [webService getData:urlString withflagLogin:YES];
}

-(void)userWebserviceCallDidSuccessWithRespone:(NSMutableArray *)response{
    
    [self.navigationController popViewControllerAnimated:YES ];
    
    
    
}
-(void)userWebserviceCallDidFaildwithError:(NSError *)error andMessage:(NSString *)message{
    
    NSLog(@"%@",error.localizedDescription);
    if ([message isEqualToString:@"401" ]) {
        UIStoryboard *storyboard=[UIStoryboard storyboardWithName:@"Main" bundle:nil];
        UIViewController* viewController=[storyboard instantiateViewControllerWithIdentifier:@"LoginController"];
        [self presentViewController:viewController animated:YES completion:nil];
    }
    
}
@end
