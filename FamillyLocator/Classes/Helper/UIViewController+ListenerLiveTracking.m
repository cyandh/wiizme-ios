//
//  UIViewController+ListenerLiveTracking.m
//  wiizme
//
//  Created by Amira on 1/30/17.
//  Copyright © 2017 Amira. All rights reserved.
//

#import "UIViewController+ListenerLiveTracking.h"
#import "Constant.h"
#import "AppDelegate.h"

@implementation UIViewController (ListenerLiveTracking)

-(void)listenForRequestLiveTracking{
    dispatch_async(dispatch_get_main_queue(), ^{
        self.client = ((AppDelegate *)[[UIApplication sharedApplication] delegate]).client;
        
        NSString *userID=[[NSUserDefaults standardUserDefaults] valueForKey:@"UserId"];
        
        PTPusherChannel *channel = [self.client subscribeToChannelNamed:userID];
        
        [channel bindToEventNamed:@"new_live_tracker" handleWithBlock:^(PTPusherEvent *channelEvent) {
            // channelEvent.data is a NSDictianary of the JSON object received
            NSDictionary *message=[[NSDictionary alloc]init];
            message = channelEvent.data ;
            [self storeTrackerIds:message];
            
        }];
        
        [self.client connect];
    });
    
}

-(void)storeTrackerIds:(NSDictionary*)userIds{
    

    
    
    NSMutableArray *storedTrackers=[[[NSUserDefaults standardUserDefaults] objectForKey:@"TrackersId"] mutableCopy];

    [storedTrackers addObject:[userIds objectForKey:@"Id"]];
    [[NSUserDefaults standardUserDefaults] setObject:storedTrackers forKey:@"TrackersId"];
    
    
    
}


-(void)listernForDeleteLiveTracking{
    dispatch_async(dispatch_get_main_queue(), ^{
        self.client = ((AppDelegate *)[[UIApplication sharedApplication] delegate]).client;
        
        NSString *userID=[[NSUserDefaults standardUserDefaults] valueForKey:@"UserId"];
        
        PTPusherChannel *channel = [self.client subscribeToChannelNamed:userID];
        
        [channel bindToEventNamed:@"delete_live_tracker" handleWithBlock:^(PTPusherEvent *channelEvent) {
            // channelEvent.data is a NSDictianary of the JSON object received
            //   NSDictionary *message=[[NSMutableArray alloc]init];
            NSString *  message = channelEvent.data ;
            [self deleteTrackerId:message];
            
        }];
        
        [self.client connect];
    });
    
}

-(void)deleteTrackerId:(NSString*)userId{
    
    userId=[userId
            stringByReplacingOccurrencesOfString:@"\"" withString:@""];
    NSMutableArray *storedTrackers=[[[NSUserDefaults standardUserDefaults] objectForKey:@"TrackersId"] mutableCopy];
    
    for (int i=0; i<storedTrackers.count; i++) {
        NSString *storeID=[storedTrackers objectAtIndex:i] ;
        if ([storeID isEqualToString:userId]) {
            [storedTrackers removeObjectAtIndex:i];
            break;
        }
        
    }
    [[NSUserDefaults standardUserDefaults] setObject:storedTrackers forKey:@"TrackersId"];
    
}

@end
