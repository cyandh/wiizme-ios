//
//  NSObject+CreateZoneNotify.m
//  wiizme
//
//  Created by Amira El Samahy on 3/29/17.
//  Copyright © 2017 Amira. All rights reserved.
//

#import "NSObject+CreateZoneNotify.h"
#import "AppDelegate.h"
#import "ZonesModel.h"
@implementation UIViewController (CreateZoneNotify)

-(void)ListenForcreateZone{
    
    dispatch_async(dispatch_get_main_queue(), ^{
        
        self.client = ((AppDelegate *)[[UIApplication sharedApplication] delegate]).client;
        
        NSString *userID=[[NSUserDefaults standardUserDefaults] valueForKey:@"UserId"];
        
        PTPusherChannel *channel = [self.client subscribeToChannelNamed:userID];
        
        [channel bindToEventNamed:@"AreaNotification" handleWithBlock:^(PTPusherEvent *channelEvent) {
            
            NSDictionary *message=[[NSDictionary alloc]init];
            message = channelEvent.data ;

            
        }];
        
        [self.client connect];
    });
}


-(void)saveMsgIndb:(NSDictionary*)zonData{
    
    ZonesModel *zonModel=[[ZonesModel alloc]init];
    RLMRealm *realm = [RLMRealm defaultRealm];
    [realm beginWriteTransaction];
    zonModel.Id=[zonData objectForKey:@"AreaId"];
    if (![[zonData objectForKey:@"Latitude"] isKindOfClass:[NSNull class]]) {
        
    
    zonModel.Latitude=[[zonData objectForKey:@"Latitude"] doubleValue];
    }
     if (![[zonData objectForKey:@"Langitude"] isKindOfClass:[NSNull class]]) {
    zonModel.Longitude=[[zonData objectForKey:@"Langitude"] doubleValue];
     }
    
   // zonModel.Radius=[[zonData objectForKey:@"Radius"] doubleValue];
    [realm addOrUpdateObject:zonModel];
    // now commit & end this transaction
    [realm commitWriteTransaction];
    
    
}

@end
