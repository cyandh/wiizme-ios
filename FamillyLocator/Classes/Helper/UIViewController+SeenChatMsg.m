//
//  UIViewController+SeenChatMsg.m
//  wiizme
//
//  Created by Amira on 3/22/17.
//  Copyright © 2017 Amira. All rights reserved.
//

#import "UIViewController+SeenChatMsg.h"
#import "AppDelegate.h"
#import "ConversationViewController.h"
#import "ChatListViewController.h"
#import "MessageModel.h"
#import "ConversationModel.h"
#import "UserModel.h"

@implementation UIViewController (SeenChatMsg)

-(void)listenForMsgChat{
    
    dispatch_async(dispatch_get_main_queue(), ^{
        self.client = ((AppDelegate *)[[UIApplication sharedApplication] delegate]).client;
        
        NSString *userID=[[NSUserDefaults standardUserDefaults] valueForKey:@"UserId"];
        
        PTPusherChannel *channel = [self.client subscribeToChannelNamed:userID];
        
        [channel bindToEventNamed:@"seen_message" handleWithBlock:^(PTPusherEvent *channelEvent) {
            
            NSDictionary *message=[[NSDictionary alloc]init];
            message = channelEvent.data ;
            
            
            
            
        }];
        
        [self.client connect];
    });
}
@end
