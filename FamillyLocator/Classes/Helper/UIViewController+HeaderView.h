//
//  UIViewController+HeaderView.h
//  FamilyLocator
//
//  Created by Amira on 10/12/16.
//  Copyright © 2016 Amira. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIViewController (HeaderView)<UISearchBarDelegate>
- (void)titleViewWithFlag:(int)flag andString:(NSString*)title;
-(NSDictionary*)editDateAndTime:(NSString *)date;
-(void)searchBarView;
@end
