//
//  UIViewController+Permissionrequest.h
//  wiizme
//
//  Created by Amira on 2/13/17.
//  Copyright © 2017 Amira. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Pusher/Pusher.h>
#import "WebServiceFunc.h"
@interface UIViewController (Permissionrequest)<WebServiceProtocol>
@property(nonatomic,strong)PTPusher *client;

-(void)listenForRequestPermissions;
@end
