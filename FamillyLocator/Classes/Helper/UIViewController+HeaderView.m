//
//  UIViewController+HeaderView.m
//  FamilyLocator
//
//  Created by Amira on 10/12/16.
//  Copyright © 2016 Amira. All rights reserved.
//

#import "UIViewController+HeaderView.h"

@implementation UIViewController (HeaderView)

- (void)titleViewWithFlag:(int)flag andString:(NSString*)title{
    
    //    // Initialization code
    //    self.backgroundColor = [UIColor whiteColor];
    //    self.layer.borderColor = [UIColor grayColor].CGColor;
    //    self.layer.borderWidth = 3.0f;
    UIView *titleView;
    if (flag==1){
        //tableview
        titleView = [[UIView alloc] initWithFrame:CGRectMake(0,0,[[UIScreen mainScreen]bounds].size.width,50)];
    }else{
        titleView = [[UIView alloc] initWithFrame:CGRectMake(0,64,[[UIScreen mainScreen]bounds].size.width,25)];
    }
    
    titleView.backgroundColor=[UIColor colorWithRed:47.0/255.0 green:61.0/255.0 blue:70.0/255.0 alpha:1.0];
    
    UILabel * titleLabel= [[UILabel alloc] initWithFrame:CGRectMake(0,20, [[UIScreen mainScreen]bounds].size.width,26)];
    

    
    UIButton *backButton  = [[UIButton alloc] initWithFrame:CGRectMake(self.view.frame.size.width-35, 2, 25, 25)];
    [backButton setImage:[UIImage imageNamed:@"addthis500"] forState:UIControlStateNormal];
    [backButton addTarget:self action:@selector(backView:) forControlEvents:UIControlEventTouchUpInside];

    
    
    
  //  titleLabel.backgroundColor=[UIColor colorWithRed:108.0/255.0 green:104.0/255.0 blue:105.0/255.0 alpha:1.0];
    titleLabel.textColor=[UIColor whiteColor];
    
    NSTextAttachment *attachment ;
    if ( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad )
    {
        attachment= [[NSTextAttachment alloc] init];
        attachment.image = [UIImage imageNamed:@""];
    }else{
        attachment= [[NSTextAttachment alloc] init];
        attachment.image = [UIImage imageNamed:@""];
    }
    NSAttributedString *attachmentString = [NSAttributedString attributedStringWithAttachment:attachment];
    NSAttributedString *mystring = [[NSAttributedString alloc]initWithString:[NSString stringWithFormat:@"  %@  ",title]];
    NSMutableAttributedString *myStringimg= [[NSMutableAttributedString alloc] initWithAttributedString:attachmentString];
    
    NSAttributedString *space = [[NSAttributedString alloc]initWithString:@"   "];
    NSMutableAttributedString *finalstring= [[NSMutableAttributedString alloc] initWithAttributedString:space];
    [myStringimg appendAttributedString:mystring];
    [finalstring appendAttributedString:myStringimg];
    
    titleLabel.attributedText = finalstring;
    titleLabel.textAlignment=NSTextAlignmentRight;
    titleLabel.font = [UIFont fontWithName:@"Droid Arabic Kufi" size:12];
    if ( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad )
    {
        CGRect frame=titleView.frame;
        frame.size.height=60;
        frame.origin.y=94;
        titleView.frame=frame;
        frame=titleLabel.frame;
        frame.size.height=61;
        titleLabel.frame=frame;
        titleLabel.font = [UIFont fontWithName:@"Droid Arabic Kufi" size:28];
    }

    [titleView addSubview:titleLabel];
    [titleView addSubview:backButton];
    [self.view addSubview:titleView];
    
}


-(NSDictionary*)editDateAndTime:(NSString *)date{
    NSDateFormatter *format=[[NSDateFormatter alloc]init];
    NSLocale *locale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US"];
    [format setLocale:locale];
    NSCharacterSet *cset = [NSCharacterSet characterSetWithCharactersInString:@"."];
    if (![date isEqual:[NSNull class]]) {
        
    
    NSRange range = [date rangeOfCharacterFromSet:cset];
    if (range.location == NSNotFound) {
        [format setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss"];
    } else {
        [format setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSSS"];
    }
    cset = [NSCharacterSet characterSetWithCharactersInString:@"Z"];

     range = [date rangeOfCharacterFromSet:cset];
    if (range.location == NSNotFound) {
    } else {
        [format setDateFormat:@"yyyy-MM-dd'T'HH:mm:ssZ"];
    }
    NSDate *dat=[format dateFromString:date];
    [format  setDateFormat:@"yyyy-MM-dd"];
    
    NSString *dateString=[format stringFromDate:dat];
    [format setDateFormat:@"hh:mm a"];
    NSString *timeString=[format stringFromDate:dat];
    
    NSDictionary *dic=[[NSDictionary alloc]initWithObjectsAndKeys:dateString,@"date",timeString,@"time", nil];
    return dic;
    }else{
    
        return nil;
    }
}


-(void)searchBarView{
    
    UIView *titleView = [[UIView alloc] initWithFrame:CGRectMake(0,0,[[UIScreen mainScreen]bounds].size.width,50)];
     titleView.backgroundColor=[UIColor colorWithRed:47.0/255.0 green:61.0/255.0 blue:70.0/255.0 alpha:1.0];
        
    UISearchBar *searchBar=[[UISearchBar alloc]initWithFrame:CGRectMake(40,titleView.frame.size.height-36,self.view.frame.size.width-90, 25)];
    //searchProduct.searchBarStyle = UISearchBarStyleMinimal;
    searchBar.translucent = false;
    searchBar.backgroundImage = [[UIImage alloc]init];
    
    searchBar.delegate=self;
    [titleView addSubview:searchBar];
    [self.view addSubview:titleView];
}

-(IBAction)backView:(id)sender{
    [self dismissViewControllerAnimated:YES completion:nil];
}



@end
