//
//  UIViewController+RemoteNotificationData.m
//  wiizme
//
//  Created by Amira on 1/29/17.
//  Copyright © 2017 Amira. All rights reserved.
//

#import "UIViewController+RemoteNotificationData.h"
#import "HomeMemberViewController.h"
#import "NotificationPostViewController.h"
#import "RequestsViewController.h"
#import "SettingGroupViewController.h"
#import "AddMeetEventViewController.h"
#import "Constant.h"
@implementation UIViewController (RemoteNotificationData)

-(void)notificationData:(NSDictionary*)contentData{
    
   
    NSError *jsonError;
    
    NSData *objectData = [[contentData objectForKey:@"json" ] dataUsingEncoding:NSUTF8StringEncoding];
    
    NSDictionary *json = [NSJSONSerialization JSONObjectWithData:objectData
                                                         options:NSJSONReadingMutableContainers
                                                           error:&jsonError];
    WebServiceFunc *webService=[[WebServiceFunc alloc]init];
    [webService setDelegate:self];
    NSString *urlString=[NSString stringWithFormat:@"%@%@",VisitNotification,[contentData objectForKey:@"id"]];
    [webService getData:urlString withflagLogin:YES];
    
     NSString *notificationType=[contentData objectForKey:@"type"];
    
    if ([notificationType isEqualToString:@"GoBackHome"]) {
       
        [self goBackHome:json];
    }else if([notificationType isEqualToString:@"NewPost"]){
        [self newPostNotify:json];
    }else if ([notificationType isEqualToString:@"IamFine"]){
        [self goBackHome:json];
    }else if ([notificationType isEqualToString:@"SharePost"]){
        [self newPostNotify:json];
    }else if ([notificationType isEqualToString:@"LikePost"]){
        [self newPostNotify:json];
    }else if ([notificationType isEqualToString:@"CommentPost"]){
        [self newPostNotify:json];
    }else if ([notificationType isEqualToString:@"CallHelp"]){
        [self goBackHome:json];
    }else if ([notificationType isEqualToString:@"GroupInvitationAdmin"]){
        [self RequestGroup:json];
    }else if ([notificationType isEqualToString:@"NewGroupAdmin"]){
        [self groupSetting:json];
    }else if ([notificationType isEqualToString:@"GroupInvitation"]){
        [self groupSetting:json];
    }else if ([notificationType isEqualToString:@"AcceptJoinRequest"]){
        [self groupSetting:json];
    }else if ([notificationType isEqualToString:@"RejectJoinRequest"]){
       // [self goBackHome:json];
    }else if ([notificationType isEqualToString:@"NewMeetYouThere"]){
        [self meetYouThereEvent:json];
    }else if ([notificationType isEqualToString:@"UpdateMeetYouThere"]){
        [self meetYouThereEvent:json];
    }else if ([notificationType isEqualToString:@"RequestPermission"]){
      //  [self meetYouThereEvent:json];
       // [self goBackHome:json];
    }else if ([notificationType isEqualToString:@"AcceptGroupPolicies"]){
      //  [self meetYouThereEvent:json];
        [self goBackHome:json];
    }else if ([notificationType isEqualToString:@"EnterArea"]){
      //  [self meetYouThereEvent:json];
       // [self goBackHome:json];
    }
    else if ([notificationType isEqualToString:@"RejectGroupPolicies"]){
        //  [self meetYouThereEvent:json];
        // [self goBackHome:json];
    }
}


-(void)goBackHome:(NSDictionary*)jsonData{
    HomeMemberViewController *homecontroller=[self.storyboard instantiateViewControllerWithIdentifier:@"HomeMemberController"];
    

    UIViewController *nav=self.navigationController.visibleViewController;
    //  NSLog(@"dljfd%@",[nav class]);
    if ([nav class]==[homecontroller class]) {
        
    }else{
        homecontroller.userData=[[UserData alloc]init];
        homecontroller.userData.name=[NSString stringWithFormat:@"%@ %@",[jsonData objectForKey:@"firstName"],[jsonData objectForKey:@"familyName"]];
        homecontroller.userData.userImage=[jsonData  objectForKey:@"photoId"];
        homecontroller.userData.fieldId=[jsonData  objectForKey:@"id"];
        [self.navigationController pushViewController:homecontroller animated:YES];
    }

}

-(void)newPostNotify:(NSDictionary*)jsonData{
    
    NotificationPostViewController *notifyView=[self.storyboard instantiateViewControllerWithIdentifier:@"NotificationPostController"];
    
    UINavigationController *navigationController = [self.storyboard instantiateViewControllerWithIdentifier:@"HomeController"];
    UIViewController *nav=navigationController.visibleViewController;
    //  NSLog(@"dljfd%@",[nav class]);
    if ([nav class]==[notifyView class]) {
        notifyView.postId=[jsonData objectForKey:@"id"];
        [notifyView reloadInputViews];
    }else{
        notifyView.timeLinePost=[[NSDictionary alloc]init];
        notifyView.postId=[jsonData objectForKey:@"id"];
        [self.navigationController pushViewController:notifyView animated:YES];
    }

}

-(void)RequestGroup:(NSDictionary*)jsonData{
    RequestsViewController *requestView=[self.storyboard instantiateViewControllerWithIdentifier:@"RequestController"];
    
    UINavigationController *navigationController = [self.storyboard instantiateViewControllerWithIdentifier:@"HomeController"];
    UIViewController *nav=navigationController.visibleViewController;
    //  NSLog(@"dljfd%@",[nav class]);
    requestView.groupData=[[GroupData alloc]init];
    requestView.groupData.groupName=[jsonData objectForKey:@"groupName"];
    requestView.groupData.profileImg=[NSString stringWithFormat:@"%@%@",PhotosUrl,[jsonData objectForKey:@"photoId"]];
    requestView.groupData.groupID=[jsonData objectForKey:@"id"];
    if ([nav class]==[requestView class]) {

        [requestView reloadInputViews];
    }else{
        
        [self.navigationController pushViewController:requestView animated:YES];
    }
}
-(void)groupSetting:(NSDictionary*)jsonData{
    SettingGroupViewController *settingView=[self.storyboard instantiateViewControllerWithIdentifier:@"settingGroupController"];
    
    UINavigationController *navigationController = [self.storyboard instantiateViewControllerWithIdentifier:@"HomeController"];
    UIViewController *nav=navigationController.visibleViewController;
    //  NSLog(@"dljfd%@",[nav class]);
    settingView.groupData=[[GroupData alloc]init];
    settingView.groupData.groupID=[jsonData objectForKey:@"id"];
    settingView.fromNotification=YES;
    if ([nav class]==[settingView class]) {
        
        [settingView reloadInputViews];
    }else{
        
        [self.navigationController pushViewController:settingView animated:YES];
    }

}
-(void)meetYouThereEvent:(NSDictionary*)jsonData{
    AddMeetEventViewController *addEvent=[self.storyboard instantiateViewControllerWithIdentifier:@"addMeetController"];
    
    UINavigationController *navigationController = [self.storyboard instantiateViewControllerWithIdentifier:@"HomeController"];
    UIViewController *nav=navigationController.visibleViewController;
    //  NSLog(@"dljfd%@",[nav class]);
    addEvent.editView=YES;
    addEvent.eventData=[[NSDictionary alloc]init];
    addEvent.eventData=jsonData;
    if ([nav class]==[addEvent class]) {
        
        [addEvent reloadInputViews];
    }else{
        
        [self.navigationController pushViewController:addEvent animated:YES];
    }
}
-(void)userWebserviceCallDidSuccessWithRespone:(NSMutableArray *)response{
    
}
-(void)userWebserviceCallDidFaildwithError:(NSError *)error andMessage:(NSString *)message{
    
}
@end
