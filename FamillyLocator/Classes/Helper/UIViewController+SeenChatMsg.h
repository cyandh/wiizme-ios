//
//  UIViewController+SeenChatMsg.h
//  wiizme
//
//  Created by Amira on 3/22/17.
//  Copyright © 2017 Amira. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Pusher/Pusher.h>
@interface UIViewController (SeenChatMsg)

@property(nonatomic,strong)PTPusher *client;
-(void)listenForSeenMsgChat;
@end
