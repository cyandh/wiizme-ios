//
//  IamFine.h
//  wiizme
//
//  Created by agamil on 5/13/17.
//  Copyright © 2017 Amira. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "WebServiceFunc.h"

@protocol SOSDataDelegate <NSObject>

@required
- (void)SOSDatafunctionRespnse:(NSMutableArray *)response service:(NSInteger )Type;
- (void)SOSDatafunctionFail:(NSString *)message service:(NSInteger )Type;


@end


@interface SOSData : NSObject<WebServiceProtocol>{

  WebServiceFunc *webService;
}
-(void)SendPost:(NSDictionary * )postPrivay;
-(void)GetSOSAPI:(NSString * )groupID ;
-(void)SendPostList:(NSMutableArray * )postPrivay;

@property(nonatomic,readwrite)NSInteger FunctionType;
@property(nonatomic,weak)id<SOSDataDelegate>deleget;
@end
