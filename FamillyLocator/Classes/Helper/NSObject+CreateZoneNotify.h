//
//  NSObject+CreateZoneNotify.h
//  wiizme
//
//  Created by Amira El Samahy on 3/29/17.
//  Copyright © 2017 Amira. All rights reserved.
//
#import <UIKit/UIKit.h>
#import <Foundation/Foundation.h>
#import <Pusher/Pusher.h>

@interface UIViewController (CreateZoneNotify)
@property(nonatomic,strong)PTPusher *client;
-(void)ListenForcreateZone;
@end
