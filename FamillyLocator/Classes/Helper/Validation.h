//
//  Validation.h
//  T3alm
//
//  Created by Amira on 12/22/15.
//  Copyright © 2015 Amira. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Validation : NSObject
+(BOOL)validateusernamewithName:(NSString *)username;
+(BOOL)validatepasswordwithpass:(NSString *)password;
+(BOOL)validatephonewithPhone:(NSString *)Phone;
+(BOOL)validateemailwithEmail:(NSString *)email;
@end
