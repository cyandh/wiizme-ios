//
//  UIViewController+SendMsgChat.m
//  wiizme
//
//  Created by Amira on 3/15/17.
//  Copyright © 2017 Amira. All rights reserved.
//

#import "UIViewController+SendMsgChat.h"
#import "AppDelegate.h"
#import "ConversationViewController.h"
#import "ChatListViewController.h"
#import "MessageModel.h"
#import "ConversationModel.h"
#import "UserModel.h"
#include <AudioToolbox/AudioToolbox.h>

@implementation UIViewController (SendMsgChat)

-(void)listenForMsgChat{
    
    dispatch_async(dispatch_get_main_queue(), ^{
        
        self.client = ((AppDelegate *)[[UIApplication sharedApplication] delegate]).client;
        
        NSString *userID=[[NSUserDefaults standardUserDefaults] valueForKey:@"UserId"];
        
        PTPusherChannel *channel = [self.client subscribeToChannelNamed:userID];
        
        [channel bindToEventNamed:@"send_message" handleWithBlock:^(PTPusherEvent *channelEvent) {
            
            NSDictionary *message=[[NSDictionary alloc]init];
            message = channelEvent.data ;
            
            [self saveMsgIndb:message];
            AudioServicesPlaySystemSound(1007);
            AudioServicesPlaySystemSound(kSystemSoundID_Vibrate);
            
            [[NSNotificationCenter defaultCenter] postNotificationName:@"ReceiveMsg"
                                                                object:nil
                                                              userInfo:message];
            
            
        }];
        
        [self.client connect];
    });
}
-(void)saveMsgIndb:(NSDictionary*)msgData{
    
    
    
    ConversationModel *convModel=[[ConversationModel alloc]init];
    RLMRealm *realm = [RLMRealm defaultRealm];
    [realm beginWriteTransaction];
    convModel.Id=[msgData objectForKey:@"ConversationId"];
    
    if([[[msgData objectForKey:@"Conversation"] objectForKey:@"GroupId"] isEqualToString:@"00000000-0000-0000-0000-000000000000"]){
        convModel.UserId=[[msgData objectForKey:@"User"] objectForKey:@"Id"];
        convModel.ChatName=[[msgData objectForKey:@"User"] objectForKey:@"FirstName"];
        
    }else {
        
        convModel.UserId=[[msgData objectForKey:@"Conversation"] objectForKey:@"UserId"];
        convModel.ChatName=[[msgData objectForKey:@"Conversation"] objectForKey:@"ChatName"];
    }
    
    convModel.LogoId=[[msgData objectForKey:@"Conversation"] objectForKey:@"LogoId"];
    convModel.LastUpdateTime=[NSDate date];
    convModel.UnseenMsgNo=convModel.UnseenMsgNo+1;
    convModel.CreatorId=[[msgData objectForKey:@"Conversation"] objectForKey:@"CreatorId"];
    convModel.LastText=[[msgData objectForKey:@"Conversation"] objectForKey:@"LastText"];
    convModel.Mute=NO;
    convModel.CreationDate=[NSDate date];
    [realm addOrUpdateObject:convModel];
    // now commit & end this transaction
    [realm commitWriteTransaction];
    
    MessageModel *msgModel=[[MessageModel alloc]init];
    [realm beginWriteTransaction];
    msgModel.Id=[msgData objectForKey:@"Id"];
    msgModel.ConversationId=[msgData objectForKey:@"ConversationId"];
    msgModel.Text=[msgData objectForKey:@"Text"];
    msgModel.Date=[NSDate date];
    msgModel.UserId=[msgData objectForKey:@"UserId"];
    msgModel.MessageType=[msgData objectForKey:@"MessageType"];
    
    [realm addObject:msgModel];
    // now commit & end this transaction
    [realm commitWriteTransaction];
    
    UserModel *userModl=[[UserModel alloc]init];
    [realm beginWriteTransaction];
    
    userModl.Id=[[msgData objectForKey:@"User"] objectForKey:@"Id"];
    userModl.UserName=[[msgData objectForKey:@"User"] objectForKey:@"FirstName"];
    
    [realm addOrUpdateObject:userModl];
    // now commit & end this transaction
    [realm commitWriteTransaction];
    
    
    
}




@end
