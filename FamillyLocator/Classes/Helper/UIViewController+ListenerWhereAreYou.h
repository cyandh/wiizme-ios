//
//  UIViewController+ListenerWhereAreYou.h
//  wiizme
//
//  Created by Amira on 1/18/17.
//  Copyright © 2017 Amira. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Pusher/Pusher.h>
#import "CustomAlertViewController.h"
#import "WebServiceFunc.h"
@interface UIViewController (ListenerWhereAreYou)<PopviewDelegate,WebServiceProtocol>

@property(nonatomic,strong)PTPusher *client;
-(void)listenForRequest;

@end
