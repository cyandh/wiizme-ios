//
//  Constant.h
//  FamillyLocator
//
//  Created by Amira on 11/5/16.
//  Copyright © 2016 Amira. All rights reserved.
//

#ifndef Constant_h
#define Constant_h

#define ProfileImageID @"69e8ca91-6cac-4932-a70b-828afa6d83b0"
#define GroupCoverID @"69e8ca91-6cac-4932-a70b-828afa6d83b2"
#define GroupLogoID @"69e8ca91-6cac-4932-a70b-828afa6d83b1"


#define publicPrivacy @"0b07c214-3faa-4438-b209-b4b8bbc55147"
#define privatePrivacy @"8df69a3a-9f3e-4529-8bad-9a252c819778"
#define VIPPrivacy @"b7450528-1090-41ee-83ea-f25787cb7794"

#define ActionTypeLastPostion @"0b07c214-3faa-4438-b209-b4b8bbc55147"

#define PhotosUrl @"http://testapi.wiizme.com/home/DisplayImage?id="

#define MainAPi @"http://testapi.wiizme.com/api"
#define SignIn (MainAPi @"/auth/login/getToken")
#define CheckUser (MainAPi @"/user/user/checkuser?email=")
#define validateCodeforgetPass (MainAPi @"/user/user/ValidateCode?email=")
#define ChangePass (MainAPi @"/user/user/ChangePassword?email=")
#define ForgetPassword (MainAPi @"/user/user/ForgetPassword?email=")
#define SignUP (MainAPi @"/user/user/add")
#define CurrentUserImage @"http://www.testapi.wiizme.com/home/DisplayUserImage?UserId=%@"


#define Search (MainAPi @"/user/user/Search?text=")

#define AddGroup (MainAPi @"/group/group/Add")
#define JoinGroup (MainAPi @"/group/usergroup/joingroup?groupId=")
#define EditGroup (MainAPi @"/group/group/Edit")
#define DeleteGroup (MainAPi @"/group/group/delete?id=")
#define LeaveGroup (MainAPi @"/group/usergroup/LeaveGroup?id=")
#define GroupMembers (MainAPi @"/group/usergroup/ListGroupMembers?id=")
#define GroupBlockMembers (MainAPi @"/group/usergroup/ListBlockMembersList?id=")
#define GroupRequests (MainAPi @"/group/usergroup/GetGroupRequests?groupId=")
#define MuteGroup (MainAPi @"/group/usergroup/MuteGroup?groupId=")


#define membernotingroup (MainAPi @"/user/user/GetUserConnections?groupId=")
#define addadmin (MainAPi @"/group/usergroup/AdminUser")
#define adminforgroup (MainAPi @"/group/usergroup/GetUserToBeAdmin?id=")
#define acceptpolicy (MainAPi @"/group/userGroup/AcceptGroupPolicies?groupId=")
#define HomeMember (MainAPi @"/user/user/GetCurrentUser?id=")
#define MemberGroups (MainAPi @"/group/usergroup/ListMemberGroups?id=")

/*Meet You There*/
#define ListMembersAndGroups (MainAPi @"/group/event/SendFullUserConnection")
#define AddEvent (MainAPi @"/group/Event/Add")
#define EditEvent (MainAPi @"/group/Event/updateevent")
#define LeaveEvent (MainAPi @"/group/Event/LeaveEvent?eventId=")
#define CancelEvent (MainAPi @"/group/event/DeleteEvent?Id=")
#define AcceptEvent (MainAPi @"/api/group/event/ResponseEvent?eventId=%@&accept=%@")
#define invits (MainAPi @"/group/event/SendEventInvitation")
/* For Group */
#define ListOfGroupMeets (MainAPi @"/group/event/GetGroupEvents?groupId=")
#define GroupDataUrl (MainAPi @"/group/usergroup/GetGroupRequest?groupid=")

/* For User */
#define ListOfUserMeets (MainAPi @"/group/event/GetUserEvents?userId=")
#define EditUser (MainAPi @"/user/user/edit")
#define HistoryPosition (MainAPi @"/tracking/History/History?id=")
#define InviteUser (MainAPi @"/group/usergroup/InviteUserToGroup?id=")

/*Need Help*/
#define ActionList (MainAPi @"/tracking/HelpType/list")
#define CallHelp (MainAPi @"/tracking/UserHelpAction/CallHelpCustomized")

/* Last Position*/
#define  LastLocation (MainAPi @"/tracking/LastPosition/ChangePosition")

#define LastLocationGroup (MainAPi @"/tracking/LastPosition/GroupLastPosition?id=")

#define LastlocationUser (MainAPi @"/tracking/LastPosition/UserLastPosition?id=")

#define TimeFrequency (MainAPi @"/common/SiteSetting/GetFrequencyUpdate")

/* Where are You */

#define WhereAreYou (MainAPi @"/tracking/LastPosition/WhereAreYou?id=")

#define whereAreYouResponse (MainAPi @"/tracking/lastposition/UserSendLastPosition?id=")

/* Go Home */

#define GoHome (MainAPi @"/social/post/gobackhome?userId=")


/* Permission*/
#define GetPermission (MainAPi @"/permission/permission/GetPermissionMatrix?userId=")
#define Sendpermission (MainAPi @"/permission/permission/RequestPermissions")
#define RespondRequestPermission (MainAPi@"/permission/permission/RespondPermissionsRequest")

/* Map Live Tracking */

#define RequestLiveTracking (MainAPi @"/tracking/lastposition/LiveTracking?id=")
#define RequestLiveTrackingGroup (MainAPi @"/tracking/lastposition/LiveTrackGroup?id=")

#define sendLiveTrackers (MainAPi @"/tracking/lastposition/SendToLiveTrackers")
#define ExitLiveTracking (MainAPi @"/tracking/lastposition/ExitTracking")


/* ZoneNotification */
#define ZoneNotification (MainAPi @"/api/Notification/Notification/GetZoneNotificationList?index=&size=")


/* Map SeeThis*/

#define seeThisMapGroup (MainAPi @"/social/post/LoadSeeThisMap?groupId=")
#define seethisMapUser (MainAPi @"/social/post/LoadSeeThisUserMap?id=")
/*Write Post*/
#define PostWithNoFiles @"021b579f-95e5-47cb-9b4d-3e7450c9f5c9"
#define PostWithtxt @"f96a32bc-1374-47f9-9393-61fe65a50aaa"
#define PostGoBackHome @"021b579f-95e5-47cb-9b4d-3e7450c9f5ca"
#define PostWithImage @"021b579f-95e5-47cb-9b4d-3e7450c9f5c8"
#define PostWithLocationAndImage @"021b579f-95e5-47cb-9b4d-3e7450c9f5c6"
#define PostWithLocationAndTxt @"021b579f-95e5-47cb-9b4d-3e7450c9f5c7"


#define PostNeedHep @"f96a32bc-1374-47f9-9393-61fe65a50a4d"
#define PostSeeThis @"f96a32bc-1374-47f9-9393-61fe65a50a4c"
#define PostIamFine @"f96a32bc-1374-47f9-9393-61fe65a50a4a"
#define SharePostID  @"25892e17-80f6-415f-9c65-7395632f0224"
#define PhotoPostId @"723264c1-370c-47ea-9a09-b99e87752fc6"
#define PhotoLocationScreenShot @"723264c1-370c-47ea-9a09-b99e87752fc5"


#define UploadPost (MainAPi @"/social/Post/Add")
#define EditPost (MainAPi @"/social/post/Edit")
#define DeletePost (MainAPi @"/social/post/Delete?id=%@")

#define CategoryList (MainAPi @"/group/tag/ListSeeThisTags")
/*load TimeLine*/
#define LoadTimeLine (MainAPi @"/social/post/LoadTimeLine?groupId=")
#define LikePostID @"0b07c214-3faa-4438-b209-b4b8bbc55147"
#define CommentPostID @"0b07c214-3faa-4438-b209-b4b8bbc55148"
#define MakeReaction (MainAPi @"/Social/PostReactions/Add")
#define GetReactions (MainAPi @"/social/PostReactions/RetrievePostReactions?postId=")

#define PostDataNotify (MainAPi @"/social/post/getcurrentpost?postid=")
#define SharePost (MainAPi @"/social/Post/share")

/* Notification*/
#define ListNotification (MainAPi @"/Notification/Notification/GetNotificationList?index=")
#define UnReadNotification (MainAPi @"/notification/notification/GetUnreadNumber")
#define VisitNotification (MainAPi @"/Notification/Notification/VisitNotification?notificationId=")

#define SendMsgChat (MainAPi @"/chat/Conversation/SendMessage")

#define CheckConversation (MainAPi @"/chat/Conversation/GetUserConversationId?id=")

#define ChatList (MainAPi @"/chat/Conversation/GetConversationList?startDate=")

#define ConversationMessages (MainAPi @"/chat/conversation/GetConversationMessageList?conversationId=")
#define SeenMessage (MainAPi @"/chat/Conversation/SeeMessage?messageId=")

#define SendOutZone (MainAPi @"/tracking/area/NotificationOnArea?areaId=")

#define GoogleNews @"https://newsapi.org/v1/articles?source=google-news&sortBy=top&apiKey=309bb828179c4d5ca2f0452fbdd40f20"
#define SOSAPI (MainAPi @"/tracking/UserHelpAction/CallHelp")

#define Notifications (MainAPi @"/notification/notification/GetHomeNumbers?id=%@")


#endif /* Constant_h */
