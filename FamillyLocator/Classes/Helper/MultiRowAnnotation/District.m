//
//  DemoMapAnnotation.m
//  Created by Gregory Combs on 11/30/11.
//
//  based on work at https://github.com/grgcombs/MultiRowCalloutAnnotationView
//
//  This work is licensed under the Creative Commons Attribution 3.0 Unported License. 
//  To view a copy of this license, visit http://creativecommons.org/licenses/by/3.0/
//  or send a letter to Creative Commons, 444 Castro Street, Suite 900, Mountain View, California, 94041, USA.
//
//

#import "District.h"
#import "Representative.h"

@implementation District

    NSDictionary *userdat;

#pragma mark For Demonstration Purposes

/* Naturally, you should set up your annotation objects as usual, but this demo factory helps distance the cell data from the view controller. */
+ (instancetype)demoAnnotationFactory:(NSDictionary*)userData
{
    userdat=[[NSDictionary alloc]init];
    userdat=userData;
    if ([[userData objectForKey:@"fromSeeThis"] boolValue]) {
        Representative *battery = [Representative representativeWithName:@"Show Post" party:@"" image:[UIImage imageNamed:@""] representativeID:[userData objectForKey:@"userid"]];
//        Representative *date = [Representative representativeWithName:[userData objectForKey:@"date"] party:@"" image:[UIImage imageNamed:@""] representativeID:[userData objectForKey:@"userID"]];
        double latitude=[[userData objectForKey:@"lat"] doubleValue];
        double longitude =[[userData objectForKey:@"lng"] doubleValue];
        return [District districtWithCoordinate:CLLocationCoordinate2DMake(latitude, longitude) title:[userData objectForKey:@"name"] representatives:@[battery]];
    }else{
    Representative *battery = [Representative representativeWithName:[userData objectForKey:@"battery"] party:@"" image:[UIImage imageNamed:@"lable-battery.png"] representativeID:@"TXL1"];
    Representative *date = [Representative representativeWithName:[userData objectForKey:@"date"] party:@"" image:[UIImage imageNamed:@""] representativeID:@"TXL2"];
        double latitude=[[userData objectForKey:@"lat"] doubleValue];
        double longitude =[[userData objectForKey:@"lng"] doubleValue];
        return [District districtWithCoordinate:CLLocationCoordinate2DMake(latitude, longitude) title:[userData objectForKey:@"name"] representatives:@[battery, date]];
    }
    
}

#pragma mark - The Good Stuff

+ (instancetype)districtWithCoordinate:(CLLocationCoordinate2D)coordinate title:(NSString *)title representatives:(NSArray *)representatives
{
    return [[District alloc] initWithCoordinate:coordinate title:title representatives:representatives];
}

- (instancetype)initWithCoordinate:(CLLocationCoordinate2D)coordinate title:(NSString *)title representatives:(NSArray *)representatives
{
    self = [super init];
    if (self)
    {
        _coordinate = coordinate;
        _title = title;
        _representatives = representatives;
        _userID=[userdat objectForKey:@"userid"];
        
    }
    return self;
}

-(NSString *)userID{
    return _userID;
}
- (NSArray *)calloutCells
{
    if (!_representatives || [_representatives count] == 0)
        return nil;
    return [self valueForKeyPath:@"representatives.calloutCell"];
}

@end
