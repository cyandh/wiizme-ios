//
//  CustomCollectionViewFlowLayout.m
//  AlMirbad
//
//  Created by Amira on 4/5/16.
//  Copyright © 2016 Amira. All rights reserved.
//

#import "CustomCollectionViewFlowLayout.h"

@implementation CustomCollectionViewFlowLayout

- (instancetype)init
{
    self = [super init];
    if (self)
    {
        self.minimumLineSpacing = 5.00;
        self.minimumInteritemSpacing = 5.00;
        self.scrollDirection = UICollectionViewScrollDirectionHorizontal;
    }
    return self;
}

- (CGSize)itemSize
{
    NSInteger numberOfColumns = 2;
    
    CGFloat itemWidth = (CGRectGetWidth(self.collectionView.frame)-60) / numberOfColumns;
    return CGSizeMake(90,90);
}

@end
