//
//  UIViewController+Permissionrequest.m
//  wiizme
//
//  Created by Amira on 2/13/17.
//  Copyright © 2017 Amira. All rights reserved.
//

#import "UIViewController+Permissionrequest.h"
#import "RequestPermissionsViewController.h"
#import "Constant.h"
#import "AppDelegate.h"
@implementation UIViewController (Permissionrequest)

-(void)listenForRequestPermissions{
    dispatch_async(dispatch_get_main_queue(), ^{
        self.client = ((AppDelegate *)[[UIApplication sharedApplication] delegate]).client;
        
        NSString *userID=[[NSUserDefaults standardUserDefaults] valueForKey:@"UserId"];
        
        
        PTPusherChannel *channel = [self.client subscribeToChannelNamed:userID];
        
        [channel bindToEventNamed:@"request_permissions" handleWithBlock:^(PTPusherEvent *channelEvent) {
            
            NSMutableDictionary *message=[[NSMutableDictionary alloc]initWithDictionary:channelEvent.data ];
   
            
            
            RequestPermissionsViewController *alertView =[self.storyboard instantiateViewControllerWithIdentifier:@"RequestPermi"];
 //           alertView.pusherData=[[NSDictionary alloc]init];
            alertView.pusherData=message;
            [self.navigationController pushViewController:alertView animated:YES];
        }];
        
        [self.client connect];
    });
    
}



@end
