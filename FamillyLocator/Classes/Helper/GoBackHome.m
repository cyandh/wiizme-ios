//
//  GoBackHome.m
//  wiizme
//
//  Created by agamil on 5/15/17.
//  Copyright © 2017 Amira. All rights reserved.
//

#import "GoBackHome.h"
#import "Constant.h"
#import "WebServiceFunc.h"


@implementation GoBackHome
@synthesize deleget;


-(void)GoBackHomePost:(NSString * )UserID {
    
    webService=[[WebServiceFunc alloc]init];
    
    [webService setDelegate:self];
    
    NSString *urlString=[NSString stringWithFormat:@"%@%@",GoHome,UserID];
    [webService getData:urlString withflagLogin:YES];
    
}

-(void)userWebserviceCallDidSuccessWithRespone:(NSMutableArray *)response{
    
    [self.deleget GoBackHomefunctionRespnse:response service:0];
    
}
-(void)userWebserviceCallDidFaildwithError:(NSError *)error andMessage:(NSString *)message{
    
    [self.deleget GoBackHomefunctionFail:message service:0];
    NSLog(@"%@",error.localizedDescription);
    if ([message isEqualToString:@"401" ]) {
        UIStoryboard *storyboard=[UIStoryboard storyboardWithName:@"Main" bundle:nil];
        UIViewController* viewController=[storyboard instantiateViewControllerWithIdentifier:@"LoginController"];
        
    }
    
}

@end
