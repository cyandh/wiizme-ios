//
//  UIViewController+RemoteNotificationData.h
//  wiizme
//
//  Created by Amira on 1/29/17.
//  Copyright © 2017 Amira. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "WebServiceFunc.h"
@interface UIViewController (RemoteNotificationData)<WebServiceProtocol>

-(void)notificationData:(NSDictionary*)contentData;
@end
