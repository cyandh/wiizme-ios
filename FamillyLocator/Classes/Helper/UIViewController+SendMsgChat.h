//
//  UIViewController+SendMsgChat.h
//  wiizme
//
//  Created by Amira on 3/15/17.
//  Copyright © 2017 Amira. All rights reserved.
//

#import <UIKit/UIKit.h>

#import <Pusher/Pusher.h>
@interface UIViewController (SendMsgChat)

@property(nonatomic,strong)PTPusher *client;
-(void)listenForMsgChat;
@end
