//
//  Helper.m
//  wiizme
//
//  Created by Amira on 1/3/17.
//  Copyright © 2017 Amira. All rights reserved.
//

#import "Helper.h"

@implementation Helper

+(NSString *)calculateDate:(NSDictionary*)dic{
    NSString *dateTime=@"";
    NSDateFormatter *dateFormatter=[[NSDateFormatter alloc] init];
    NSLocale *locale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US"];
    [dateFormatter setLocale:locale];
    [dateFormatter setDateFormat:@"yyyy-MM-dd"];
    NSString *currentDate=[dateFormatter stringFromDate:[NSDate date]];
    NSDate *date1 = [dateFormatter dateFromString:[dic objectForKey:@"date"]];
    NSDate *date2 = [dateFormatter dateFromString:currentDate];
    
    NSTimeInterval secondsBetween = [date2 timeIntervalSinceDate:date1];
    
    int numberOfDays = secondsBetween / 86400;
    
    if (numberOfDays==1) {
        
    }else if (numberOfDays>=360){
        
    }else{
        
    }
    
    return dateTime;
}


+(NSDictionary*)editDateAndTime:(NSString *)date{
    NSDateFormatter *format=[[NSDateFormatter alloc]init];
    NSLocale *locale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US"];
    [format setLocale:locale];
    NSCharacterSet *cset = [NSCharacterSet characterSetWithCharactersInString:@"Z"];
    NSRange range = [date rangeOfCharacterFromSet:cset];
    
    if (range.location == NSNotFound) {
        NSCharacterSet *csetplus = [NSCharacterSet characterSetWithCharactersInString:@"+"];
        NSRange rangeplus = [date rangeOfCharacterFromSet:csetplus];
        if (rangeplus.location == NSNotFound) {
            
            NSCharacterSet *cset = [NSCharacterSet characterSetWithCharactersInString:@"."];
            NSRange range = [date rangeOfCharacterFromSet:cset];
            if (range.location == NSNotFound) {
                [format setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss"];
            } else {
                [format setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSSS"];
            }
            
        } else {
            [format setDateFormat:@"yyyy-MM-dd HH:mm:ss +0000"];
        }

   
    } else {
        [format setDateFormat:@"yyyy-MM-dd'T'HH:mm:ssZ"];
    }
    NSDate *dat=[format dateFromString:date];
    [format  setDateFormat:@"yyyy-MM-dd"];
    
    NSString *dateString=[format stringFromDate:dat];
    [format setDateFormat:@"hh:mm a"];
    NSString *timeString=[format stringFromDate:dat];
    
    NSDictionary *dic=[[NSDictionary alloc]initWithObjectsAndKeys:dateString,@"date",timeString,@"time", nil];
    return dic;
}


+(NSMutableString*) timeLeftSinceDate: (NSString *) dic {
    
    NSDateFormatter *format=[[NSDateFormatter alloc]init];
    NSLocale *locale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US"];
    [format setLocale:locale];
    NSCharacterSet *csetplus = [NSCharacterSet characterSetWithCharactersInString:@"+"];
    NSRange rangeplus = [dic rangeOfCharacterFromSet:csetplus];
    if (rangeplus.location == NSNotFound) {
        
        NSCharacterSet *cset = [NSCharacterSet characterSetWithCharactersInString:@"."];
        NSRange range = [dic rangeOfCharacterFromSet:cset];
        if (range.location == NSNotFound) {
            [format setDateFormat:@"yyyy-MM-dd'T'HH:mm:ssZ"];
        } else {
            [format setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSSSZ"];
        }
        
    } else {
        [format setDateFormat:@"yyyy-MM-dd HH:mm:ss +0000"];
    }
    
    
    NSDate *dat=[format dateFromString:dic];
    [format setDateFormat:@"yyyy-MM-dd HH:mm:ss a"];
    
    NSMutableString *timeLeft = [[NSMutableString alloc]init];
    
    NSDate *today10am =[NSDate date];
    
    NSInteger seconds = [today10am timeIntervalSinceDate:dat];
    
    NSInteger days = (int) (floor(seconds / (3600 * 24)));
    if(days) seconds -= days * 3600 * 24;
    
    NSInteger hours = (int) (floor(seconds / 3600));
    if(hours) seconds -= hours * 3600;
    
    NSInteger minutes = (int) (floor(seconds / 60));
    if(minutes) seconds -= minutes * 60;
    
    if(days) {
        //[timeLeft appendString:[NSString stringWithFormat:@"%ld Days", (long)days]];
        if (days==1) {
            [format setDateFormat:@"HH:mm a"];
            NSString *time=[format stringFromDate:dat];
            [timeLeft appendString:[NSString stringWithFormat:@"Yesterday at %@",time]];
        }else if (days<365){
            [format setDateFormat:@"dd MMMM"];
            NSString *dates=[format stringFromDate:dat];
            [format setDateFormat:@"HH:mm a"];
            NSString *time=[format stringFromDate:dat];
            [timeLeft appendString:[NSString stringWithFormat:@"%@ at %@",dates,time]];
        }
    }else if(hours) {
       // [timeLeft appendString:[NSString stringWithFormat: @"%ld H", (long)hours]];
        [timeLeft appendString:[NSString stringWithFormat:@"%ld hour",(long)hours]];
    }else if(minutes) {
        [timeLeft appendString: [NSString stringWithFormat: @"%ld ago",(long)minutes]];
    } else if(seconds) {
        [timeLeft appendString:[NSString stringWithFormat: @"Just Now"]];
    }
    
    return timeLeft;
}
+ (UIImage*)imageWithImage:(UIImage*)image scaledToSize:(CGSize)newSize
{
    UIGraphicsBeginImageContext( newSize );
    [image drawInRect:CGRectMake(0,0,newSize.width,newSize.height)];
    UIImage* newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return newImage;
}
+(NSAttributedString*)timeTXT:(NSString*)time{
    NSTextAttachment *attachment= [[NSTextAttachment alloc] init];
    attachment.image = [UIImage imageNamed:@"time.png"];
    NSAttributedString *attachmentString = [NSAttributedString attributedStringWithAttachment:attachment];
    NSAttributedString *mystring = [[NSAttributedString alloc]initWithString:[NSString stringWithFormat:@" %@ ",time]];
    NSMutableAttributedString *myStringimg= [[NSMutableAttributedString alloc] initWithAttributedString:attachmentString];
    [myStringimg appendAttributedString:mystring];
    
    NSMutableAttributedString *finalstring= [[NSMutableAttributedString alloc] initWithAttributedString:myStringimg];
    
    
    
    return finalstring;
    
}




@end
