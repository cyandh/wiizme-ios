//
//  IamFine.m
//  wiizme
//
//  Created by agamil on 5/13/17.
//  Copyright © 2017 Amira. All rights reserved.
//

#import "IamFine.h"
#import "MBProgressHUD.h"
#import "Constant.h"
#import "WebServiceFunc.h"


@implementation IamFine
@synthesize deleget;


-(void)finePost:(NSString * )groupID {
    
    webService=[[WebServiceFunc alloc]init];
    
    [webService setDelegate:self];
    
 
    
    NSDateFormatter *dateFormatter=[[NSDateFormatter alloc] init];
    
    NSLocale *locale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US"];
    
    [dateFormatter setLocale:locale];
    
    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss a"];
    
    NSMutableArray *postPrivacies=[[NSMutableArray alloc]init];
    
    [postPrivacies addObject:[NSDictionary dictionaryWithObjectsAndKeys:groupID,@"groupId",@"",@"userId",@"00000000-0000-0000-0000-000000000000",@"postId", nil]];
    
    NSString *currentDate=[dateFormatter stringFromDate:[NSDate date]];
    
    NSString *postTypeId=PostWithNoFiles;
    
    NSMutableDictionary *userData = [[NSMutableDictionary alloc] initWithObjectsAndKeys:@"00000000-0000-0000-0000-000000000000",@"originalPostId",
                                     postTypeId,@"postTypeId",
                                     postPrivacies,@"postPrivacies",
                                     @"00000000-0000-0000-0000-000000000000",@"userId",
                                     @" I'm Fine ",@"Text",
                                     currentDate,@"date",
                                     nil];

    [webService postDataWithUrlString:UploadPost withData:userData withflagLogin:YES];
    
}

-(void)userWebserviceCallDidSuccessWithRespone:(NSMutableArray *)response{

    [self.deleget IamFinefunctionRespnse:response service:0];
    
}
-(void)userWebserviceCallDidFaildwithError:(NSError *)error andMessage:(NSString *)message{

    [self.deleget IamFinefunctionFail:message service:0];
    NSLog(@"%@",error.localizedDescription);
    if ([message isEqualToString:@"401" ]) {
        UIStoryboard *storyboard=[UIStoryboard storyboardWithName:@"Main" bundle:nil];
        UIViewController* viewController=[storyboard instantiateViewControllerWithIdentifier:@"LoginController"];
     
    }
    
}


@end
