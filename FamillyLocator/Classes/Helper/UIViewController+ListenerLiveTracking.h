//
//  UIViewController+ListenerLiveTracking.h
//  wiizme
//
//  Created by Amira on 1/30/17.
//  Copyright © 2017 Amira. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Pusher/Pusher.h>
#import "WebServiceFunc.h"
@interface UIViewController (ListenerLiveTracking)<WebServiceProtocol>
@property(nonatomic,strong)PTPusher *client;

-(void)listenForRequestLiveTracking;

-(void)listernForDeleteLiveTracking;
@end
