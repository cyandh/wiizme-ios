//
//  GeofenceMonitor.m
//  Geofening
//
//  Created by KH1386 on 10/8/13.
//  Copyright (c) 2013 KH1386. All rights reserved.
//

#import "GeofenceMonitor.h"
#import "ATPlaceModal.h"
#import <MapKit/MapKit.h>
@implementation GeofenceMonitor
@synthesize locationManager;
+(GeofenceMonitor *) sharedObj
{
    
    static GeofenceMonitor * shared =nil;
    
    static dispatch_once_t onceTocken;
    dispatch_once(&onceTocken, ^{
        shared = [[GeofenceMonitor alloc] init];
        
    });
    return shared;
}

- (CLRegion*)dictToRegion:(ATPlaceModal*)placeModel
{
    NSString *identifier = placeModel.title;
    CLLocationDegrees latitude = placeModel.latitude.doubleValue;
    CLLocationDegrees longitude =placeModel.longitude.doubleValue;
    CLLocationCoordinate2D centerCoordinate = CLLocationCoordinate2DMake(latitude, longitude);
    CLLocationDistance regionRadius = placeModel.radius.doubleValue;
    
    
    if(regionRadius > locationManager.maximumRegionMonitoringDistance)
    {
        regionRadius = locationManager.maximumRegionMonitoringDistance;
    }
    
    NSString *version = [[UIDevice currentDevice] systemVersion];
    CLRegion * region =nil;
    
    if([version floatValue] >= 7.0f) //for iOS7
    {
        region =  [[CLCircularRegion alloc] initWithCenter:centerCoordinate
                                                    radius:regionRadius
                                                identifier:identifier];
    }
    else // iOS 7 below
    {
        region = [[CLRegion alloc] initCircularRegionWithCenter:centerCoordinate
                                                         radius:regionRadius
                                                     identifier:identifier];
    }
    return  region;
}

-(id) init
{
    self = [super init];
    if(self)
    {
        self.locationManager = [[CLLocationManager alloc]init];
        self.locationManager.delegate = self;
        //        self.locationManager.activityType = CLActivityTypeFitness;
        
        self.locationManager.desiredAccuracy = kCLLocationAccuracyHundredMeters;
        self.locationManager.distanceFilter = 10;
        
        if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0)
        {
            
        }
        //        [locationManager requestAlwaysAuthorization];
        
    }
    return self;
}
-(void) showMessage:(NSString *) message
{
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Geofence"
                                                        message:message
                                                       delegate:self
                                              cancelButtonTitle:@"Cancel"
                                              otherButtonTitles:Nil, nil];
    
    alertView.alertViewStyle = UIAlertViewStyleDefault;
    
    [alertView show];
    
    
}
-(BOOL) checkLocationManager
{
    if(![CLLocationManager locationServicesEnabled])
    {
        //        [self showMessage:@"You need to enable Location Services"];
        return  FALSE;
    }
    if(![CLLocationManager isMonitoringAvailableForClass:[CLRegion class]])
    {
        [self showMessage:@"Region monitoring is not available for this Class"];
        return  FALSE;
    }
    if([CLLocationManager authorizationStatus] == kCLAuthorizationStatusDenied ||
       [CLLocationManager authorizationStatus] == kCLAuthorizationStatusRestricted  )
    {
        //        [self showMessage:@"You need to authorize Location Services for the APP"];
        return  FALSE;
    }
    return TRUE;
}
-(void) addGeofence:(ATPlaceModal*) placeModel
{
    
    CLRegion * region = [self dictToRegion:placeModel];
    [locationManager startMonitoringForRegion:region];
    NSLog(@"%@",locationManager.monitoredRegions);
}
-(void) findCurrentFence
{
    NSString *version = [[UIDevice currentDevice] systemVersion];
    
    if([version floatValue] >= 7.0f) //for iOS7
    {
        NSArray * monitoredRegions = [locationManager.monitoredRegions allObjects];
        for(CLRegion *region in monitoredRegions)
        {
            [locationManager requestStateForRegion:region];
        }
    }
    else
    {
        [locationManager startUpdatingLocation];
    }
    
}
-(void) removeGeofence:(NSDictionary*) dict
{
    CLRegion * region = [self dictToRegion:dict];
    [locationManager stopMonitoringForRegion:region];
    
}
-(void) clearGeofences
{
    NSArray * monitoredRegions = [locationManager.monitoredRegions allObjects];
    for(CLRegion *region in monitoredRegions) {
        [locationManager stopMonitoringForRegion:region];
    }
}



/*
 Delegate Methods
 */

- (void)locationManager:(CLLocationManager *)manager
      didDetermineState:(CLRegionState)state forRegion:(CLRegion *)region
{
    if(state == CLRegionStateInside)
    {
        //        UIAlertView* alertView=[[UIAlertView alloc]initWithTitle:@"entered Region" message:[NSString stringWithFormat:@"u have entered the region %@",region.identifier] delegate:nil cancelButtonTitle:@"Cancel" otherButtonTitles: nil];
        //        [alertView show];
        //        [self Notification:region status:@"exited"];
        //        NSLog(@"##Entered Region - %@", region.identifier);
    }
    else if(state == CLRegionStateOutside)
    {
        //        UIAlertView* alertView=[[UIAlertView alloc]initWithTitle:@"Exit Region" message:[NSString stringWithFormat:@"u have exited the region %@",region.identifier] delegate:nil cancelButtonTitle:@"Cancel" otherButtonTitles: nil];
        //        [alertView show];
        //        NSLog(@"##Exited Region - %@", region.identifier);
    }
    else{
        //        UIAlertView* alertView=[[UIAlertView alloc]initWithTitle:@"Unknown Region" message:[NSString stringWithFormat:@"Unknown %@",region.identifier] delegate:nil cancelButtonTitle:@"Cancel" otherButtonTitles: nil];
        //        [alertView show];
        //        NSLog(@"##Unknown state  Region - %@", region.identifier);
    }
}
- (void)locationManager:(CLLocationManager *)manager didStartMonitoringForRegion:(CLRegion *)region
{
    //    UIAlertView* alertView=[[UIAlertView alloc]initWithTitle:@"Started Monitoring" message:[NSString stringWithFormat:@"Started Monitoring %@",region.identifier] delegate:nil cancelButtonTitle:@"Cancel" otherButtonTitles: nil];
    //    [alertView show];
    //
    //    NSLog(@"Started monitoring %@ region", region.identifier);
}

- (void)locationManager:(CLLocationManager *)manager didEnterRegion:(CLRegion *)region
{
    [self Notification:region status:@"entered"];
    
    UIAlertView* alertView=[[UIAlertView alloc]initWithTitle:@"enter Region" message:[NSString stringWithFormat:@"engtered Region %@",region.identifier] delegate:nil cancelButtonTitle:@"Cancel" otherButtonTitles: nil];
    [alertView show];
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        NSNumber *lat = [NSNumber numberWithDouble:region.center.latitude];
        NSNumber *lon = [NSNumber numberWithDouble:region.center.longitude];
        NSDictionary *userInfo;
        NSMutableArray *allStation=[[NSUserDefaults standardUserDefaults] objectForKey:@"AllStation"];
        for (int i=0; i<allStation.count; i++) {
            NSNumber *latitute=[NSNumber numberWithDouble:[[[allStation objectAtIndex:i] objectForKey:@"latitude"]doubleValue ]];
            NSNumber *langitude=[NSNumber numberWithDouble:[[[allStation objectAtIndex:i] objectForKey:@"langitude"]doubleValue ]];
            if (([lat doubleValue] == [latitute doubleValue])&&([lon doubleValue] == [langitude doubleValue])) {
                NSString *stationID=[[allStation objectAtIndex:i] objectForKey:@"id"];
                NSDictionary *data=[[NSDictionary alloc]init];
                data=[allStation objectAtIndex:i];
                NSString *stationPhones=[self changeCallString:data];
                NSString *brief=[[allStation objectAtIndex:i] objectForKey:@"brief"];
                NSString *coverimg=[[allStation objectAtIndex:i]objectForKey:@"coverImage"];
                userInfo=@{@"identifier":region.identifier,@"status":@"entered",@"lat":[lat stringValue],@"lon":[lon stringValue],@"sationID":stationID,@"stationPhones":stationPhones,@"stationbrief":brief,@"stationImg":coverimg};
                break;
            }
        }
        dispatch_sync(dispatch_get_main_queue(), ^{
            
            [[NSNotificationCenter defaultCenter] postNotificationName:@"EnterStation"
                                                                object:nil
                                                              userInfo:@{@"Data":userInfo}];
            
            
            
        });
    });
    //    NSLog(@"Entered Region - %@", region.identifier);
}

-(NSString *)changeCallString:(NSDictionary*)stationData{
    NSString *phone1=[stationData  objectForKey:@"phoneNumber1"];
    NSString *phone2=[stationData  objectForKey:@"phoneNumber2"];
    NSString *phone3=[stationData  objectForKey:@"phoneNumber3"];
    NSString *callnum;
    if ([phone1 isEqualToString:@""]) {
        callnum=@"";
    }else{
        callnum=phone1;
       
        
    }
    
    return callnum;
}

- (void)locationManager:(CLLocationManager *)manager didExitRegion:(CLRegion *)region
{
    [self Notification:region status:@"Exited"];
    
    UIAlertView* alertView=[[UIAlertView alloc]initWithTitle:@"Exite Region" message:[NSString stringWithFormat:@"Exite Region %@",region.identifier] delegate:nil cancelButtonTitle:@"Cancel" otherButtonTitles: nil];
    [alertView show];
    //    NSLog(@"Exited Region - %@", region.identifier);
}

- (void)locationManager:(CLLocationManager *)manager didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation
{
    static BOOL firstTime=TRUE;
    if(firstTime)
    {
        firstTime = FALSE;
        NSSet * monitoredRegions = locationManager.monitoredRegions;
        if(monitoredRegions)
        {
            [monitoredRegions enumerateObjectsUsingBlock:^(CLRegion *region,BOOL *stop)
             {
                 NSString *identifer = region.identifier;
                 CLLocationCoordinate2D centerCoords =region.center;
                 CLLocationCoordinate2D currentCoords= CLLocationCoordinate2DMake(newLocation.coordinate.latitude,newLocation.coordinate.longitude);
                 CLLocationDistance radius = region.radius;
                 
                 NSNumber * currentLocationDistance =[self calculateDistanceInMetersBetweenCoord:currentCoords coord:centerCoords];
                 if([currentLocationDistance floatValue] < radius)
                 {
                     NSLog(@"Invoking didEnterRegion Manually for region: %@",identifer);
                     
                     //stop Monitoring Region temporarily
                     [locationManager stopMonitoringForRegion:region];
                     
                     [self locationManager:locationManager didEnterRegion:region];
                     //start Monitoing Region
                     [locationManager startMonitoringForRegion:region];
                 }
             }];
        }
        //Stop Location Updation, we dont need it now.
        [locationManager stopUpdatingLocation];
        
    }
}

//Helper Functions.
- (NSNumber*)calculateDistanceInMetersBetweenCoord:(CLLocationCoordinate2D)coord1 coord:(CLLocationCoordinate2D)coord2 {
    NSInteger nRadius = 6371; // Earth's radius in Kilometers
    double latDiff = (coord2.latitude - coord1.latitude) * (M_PI/180);
    double lonDiff = (coord2.longitude - coord1.longitude) * (M_PI/180);
    double lat1InRadians = coord1.latitude * (M_PI/180);
    double lat2InRadians = coord2.latitude * (M_PI/180);
    double nA = pow ( sin(latDiff/2), 2 ) + cos(lat1InRadians) * cos(lat2InRadians) * pow ( sin(lonDiff/2), 2 );
    double nC = 2 * atan2( sqrt(nA), sqrt( 1 - nA ));
    double nD = nRadius * nC;
    // convert to meters
    return @(nD*1000);
}

-(void)Notification:(CLRegion*)region status:(NSString*)status
{
    UILocalNotification * notification=[[UILocalNotification alloc]init];
    notification.soundName = UILocalNotificationDefaultSoundName;
    //    NSLog(@"Ta3ala hena ya%@",region);
    notification.fireDate=[NSDate date];
    //    NSMutableDictionary* dic=[[NSMutableDictionary alloc]init];
    
    //    id containedRegion=region;
    //    [dic setObject:region forKey:region.identifier];
    NSNumber *lat = [NSNumber numberWithDouble:region.center.latitude];
    NSNumber *lon = [NSNumber numberWithDouble:region.center.longitude];
    notification.userInfo=@{@"identifier":region.identifier,@"status":status,@"lat":[lat stringValue],@"lon":[lon stringValue]};
    notification.category=@"UILocalNotification";
    //    notification.userInfo=@{[NSString stringWithFormat:@"%f",region.center.latitude]:@"latitude",[NSString stringWithFormat:@"%f",region.center.longitude]:@"longitude",[NSString stringWithFormat:@"%@",region.identifier]:@"identifier"};
    notification.alertBody= [NSString stringWithFormat:@"%@ Location %@",status,[region identifier]];
    
    [[UIApplication sharedApplication]scheduleLocalNotification:notification];
    
}

@end
