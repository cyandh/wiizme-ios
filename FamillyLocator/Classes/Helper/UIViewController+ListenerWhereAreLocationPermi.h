//
//  UIViewController+ListenerWhereAreLocationPermi.h
//  wiizme
//
//  Created by Amira on 1/19/17.
//  Copyright © 2017 Amira. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Pusher/Pusher.h>
#import "WhereAreYouPOPUPViewController.h"
@interface UIViewController (ListenerWhereAreLocationPermi)<popShowLocation>
@property(nonatomic,strong)PTPusher *client;

-(void)listenForResponse;
@end
