//
//  Helper.h
//  wiizme
//
//  Created by Amira on 1/3/17.
//  Copyright © 2017 Amira. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface Helper : NSObject


+(NSString *)calculateDate:(NSDictionary*)dic;
+(NSMutableString*) timeLeftSinceDate: (NSString *) dic;
+ (UIImage*)imageWithImage:(UIImage*)image scaledToSize:(CGSize)newSize;
+(NSAttributedString*)timeTXT:(NSString*)time;
+(NSDictionary*)editDateAndTime:(NSString *)date;
@end
