//
//  whereareyou.h
//  wiizme
//
//  Created by agamil on 5/14/17.
//  Copyright © 2017 Amira. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "WebServiceFunc.h"



@protocol whereAreYouDelegate <NSObject>

@required
- (void)whereAreYoufunctionRespnse:(NSMutableArray *)response service:(NSInteger )Type;
- (void)whereAreYoufunctionFail:(NSString *)message service:(NSInteger )Type;


@end


@interface whereareyou : NSObject{
    
    
    WebServiceFunc *webService;
}

-(void)WhereAreYouPost:(NSString * )UserID;

@property(nonatomic,readwrite)NSInteger FunctionType;
@property(nonatomic,weak)id<whereAreYouDelegate>deleget;
@end


