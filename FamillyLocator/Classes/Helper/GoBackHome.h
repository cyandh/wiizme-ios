//
//  GoBackHome.h
//  wiizme
//
//  Created by agamil on 5/15/17.
//  Copyright © 2017 Amira. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "WebServiceFunc.h"



@protocol GoBackHomeDelegate <NSObject>

@required
- (void)GoBackHomefunctionRespnse:(NSMutableArray *)response service:(NSInteger )Type;
- (void)GoBackHomefunctionFail:(NSString *)message service:(NSInteger )Type;


@end


@interface GoBackHome : NSObject{
    
    
    WebServiceFunc *webService;
}

-(void)GoBackHomePost:(NSString * )UserID;

@property(nonatomic,readwrite)NSInteger FunctionType;
@property(nonatomic,weak)id<GoBackHomeDelegate>deleget;
@end

