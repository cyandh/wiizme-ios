//
//  IamFine.h
//  wiizme
//
//  Created by agamil on 5/13/17.
//  Copyright © 2017 Amira. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "WebServiceFunc.h"

@protocol HomeNotificationsDelegate <NSObject>

@required
- (void)HomeNotificationsfunctionRespnse:(NSMutableArray *)response service:(NSInteger )Type;
- (void)HomeNotificationsfunctionFail:(NSString *)message service:(NSInteger )Type;


@end


@interface HomeNotifications : NSObject<WebServiceProtocol>{

  WebServiceFunc *webService;
}

-(void)getMenuNotifications;

@property(nonatomic,readwrite)NSInteger FunctionType;
@property(nonatomic,weak)id<HomeNotificationsDelegate>deleget;
@end
