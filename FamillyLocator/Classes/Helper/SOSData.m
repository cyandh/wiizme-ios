//
//  IamFine.m
//  wiizme
//
//  Created by agamil on 5/13/17.
//  Copyright © 2017 Amira. All rights reserved.
//

#import "SOSData.h"
#import "MBProgressHUD.h"
#import "Constant.h"
#import "WebServiceFunc.h"


@implementation SOSData
@synthesize deleget;


-(void)GetSOSAPI:(NSString * )groupID {
    
    webService=[[WebServiceFunc alloc]init];
    
    [webService setDelegate:self];


    [webService getData:SOSAPI withflagLogin:YES];
    
}
-(void)SendPost:(NSDictionary * )postPrivay {
    
    webService=[[WebServiceFunc alloc]init];
    
    [webService setDelegate:self];

    NSDateFormatter *dateFormatter=[[NSDateFormatter alloc] init];
    
    NSLocale *locale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US"];
    
    [dateFormatter setLocale:locale];
    
    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss a"];
    
    NSMutableArray *postPrivacies=[[NSMutableArray alloc]init];
    
  
    
    NSString *currentDate=[dateFormatter stringFromDate:[NSDate date]];
    
    NSString *postTypeId=PostWithNoFiles;
    
    NSMutableDictionary *userData = [[NSMutableDictionary alloc] initWithObjectsAndKeys:@"00000000-0000-0000-0000-000000000000",@"originalPostId",
                                     postTypeId,@"postTypeId",
                                     postPrivay,@"postPrivacies",
                                     @"00000000-0000-0000-0000-000000000000",@"userId",
                                     @" I'm Fine ",@"Text",
                                     currentDate,@"date",
                                     nil];
    
    [webService postDataWithUrlString:UploadPost withData:userData withflagLogin:YES];
    
}

-(void)SendPostList:(NSMutableArray * )postPrivay {
    
    webService=[[WebServiceFunc alloc]init];
    
    [webService setDelegate:self];
    
    NSDateFormatter *dateFormatter=[[NSDateFormatter alloc] init];
    
    NSLocale *locale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US"];
    
    [dateFormatter setLocale:locale];
    
    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss a"];
    
    NSMutableArray *postPrivacies=[[NSMutableArray alloc]init];
    
    
    
    NSString *currentDate=[dateFormatter stringFromDate:[NSDate date]];
    
    NSString *postTypeId=PostWithNoFiles;
    
    NSMutableDictionary *userData = [[NSMutableDictionary alloc] initWithObjectsAndKeys:@"00000000-0000-0000-0000-000000000000",@"originalPostId",
                                     postTypeId,@"postTypeId",
                                     postPrivay,@"postPrivacies",
                                     @"00000000-0000-0000-0000-000000000000",@"userId",
                                     @" I'm Fine ",@"Text",
                                     currentDate,@"date",
                                     nil];
    
    [webService postDataWithUrlString:UploadPost withData:userData withflagLogin:YES];
    
}



-(void)userWebserviceCallDidSuccessWithRespone:(NSMutableArray *)response{
    if ([response isKindOfClass:[NSDictionary class]]) {
        
  
        NSDictionary * Resp = (NSDictionary *)response;

        NSDictionary * SOS = [[NSDictionary alloc] initWithObjectsAndKeys:@"emergencyNumber",[Resp objectForKey:@"emergencyNumber"], nil];
        
        
        

      }
    [self.deleget SOSDatafunctionRespnse:response service:0];
    
}
-(void)userWebserviceCallDidFaildwithError:(NSError *)error andMessage:(NSString *)message{

    [self.deleget SOSDatafunctionFail:message service:0];
    NSLog(@"%@",error.localizedDescription);
    if ([message isEqualToString:@"401" ]) {
        UIStoryboard *storyboard=[UIStoryboard storyboardWithName:@"Main" bundle:nil];
        UIViewController* viewController=[storyboard instantiateViewControllerWithIdentifier:@"LoginController"];
     
    }
    
}


@end
