//
//  Validation.m
//  T3alm
//
//  Created by Amira on 12/22/15.
//  Copyright © 2015 Amira. All rights reserved.
//

#import "Validation.h"
#import <UIKit/UIKit.h>
@implementation Validation

+(BOOL)validateusernamewithName:(NSString *)username
{
    if([username stringByReplacingOccurrencesOfString:@" " withString:@""].length==0)
    {
        
        return false;
    }else{
        return true;
    }
}
+(BOOL)validatepasswordwithpass:(NSString *)password
{
    if([password stringByReplacingOccurrencesOfString:@" " withString:@""].length==0)
    {
        
        return false;
    }else{
        return true;
    }
}
+(BOOL)validatephonewithPhone:(NSString *)Phone
{
    Phone=[Phone stringByReplacingOccurrencesOfString:@" " withString:@""];
//    NSString *phoneRegex = @"^((01))[0-9]{9}$";
//    NSPredicate *phoneTest2 = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", phoneRegex];
    if (![Phone isEqualToString:@""]) {
        return YES;
    }else{
     return NO;
    
    }
   // return [phoneTest2 evaluateWithObject:Phone];
}
+(BOOL)validateemailwithEmail:(NSString *)email
{
    email=[email stringByReplacingOccurrencesOfString:@" " withString:@""];
    NSString *emailRegex = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,6}";
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:email];
    
}
@end
