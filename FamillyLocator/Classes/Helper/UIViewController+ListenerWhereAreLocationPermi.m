//
//  UIViewController+ListenerWhereAreLocationPermi.m
//  wiizme
//
//  Created by Amira on 1/19/17.
//  Copyright © 2017 Amira. All rights reserved.
//

#import "UIViewController+ListenerWhereAreLocationPermi.h"
#import "AppDelegate.h"
#import "MapKitViewController.h"
#import "WhereAreYouPOPUPViewController.h"
#import "Constant.h"
@implementation UIViewController (ListenerWhereAreLocationPermi)

-(void)listenForResponse{
    dispatch_async(dispatch_get_main_queue(), ^{
        
        self.client = ((AppDelegate *)[[UIApplication sharedApplication] delegate]).client;
        
        NSString *userID=[[NSUserDefaults standardUserDefaults] valueForKey:@"UserId"];
        
        PTPusherChannel *channel = [self.client subscribeToChannelNamed:userID];
        
        [channel bindToEventNamed:@"receive_last_position" handleWithBlock:^(PTPusherEvent *channelEvent) {
            // channelEvent.data is a NSDictianary of the JSON object received
            
            NSDictionary *message=[[NSDictionary alloc]init];
            message = channelEvent.data ;
            
            if ([message isEqual:@"null"]) {
                UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Attention" message:@"Can not shown his/her location" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
                
                [alert show];
            }else{
                WhereAreYouPOPUPViewController *alertView =[self.storyboard instantiateViewControllerWithIdentifier:@"wherePOPUP"];
                
                
                alertView.delegate=self;
                
                NSString *firstName=[[message  objectForKey:@"User"]objectForKey:@"FirstName"];
                NSString *lastName=[[message  objectForKey:@"User"]objectForKey:@"FamilyName"];
                
                NSString *imageUrl=[NSString stringWithFormat:@"%@%@",PhotosUrl,[[message  objectForKey:@"User"] objectForKey:@"PhotoId"]];
                NSString *uploadedImg=[NSString stringWithFormat:@"%@%@",PhotosUrl,[message objectForKey:@"ImageId"]];
                
                alertView.userData=[[NSDictionary alloc]initWithObjectsAndKeys:[NSString stringWithFormat:@"%@ %@",firstName,lastName],@"Name",imageUrl,@"UserImageUrl",uploadedImg,@"UploadedImgUrl",[message objectForKey:@"Text"],@"message",[message objectForKey:@"LastKnowingLatitude"],@"Latitude",[message objectForKey:@"LastKnowingLangitude"],@"Longitude", nil];
                [alertView.userImg setUserInteractionEnabled:NO];
                
                
                [self.navigationController pushViewController:alertView animated:NO];
                
                
            }
                  }];
        
        [self.client connect];
    });
    
}
- (void)declineData{
    
}
-(void)acceptData:(NSDictionary*)userData{
    [self.navigationController popViewControllerAnimated:NO];
    double latitude=[[userData objectForKey:@"Latitude"] doubleValue];
    double longitude=[[userData objectForKey:@"Longitude"] doubleValue];
    MapKitViewController  *mapView=[self.storyboard instantiateViewControllerWithIdentifier:@"MapController"];
    NSDictionary *dic=[NSDictionary dictionaryWithObjectsAndKeys:[NSNumber numberWithDouble:latitude],@"lat",[NSNumber numberWithDouble:longitude],@"lng", nil];
    mapView.locations=[[NSMutableArray alloc]init];
    [mapView.locations addObject:dic];
    mapView.fromPost=YES;
    
    [self.navigationController pushViewController:mapView animated:YES];
}
@end
