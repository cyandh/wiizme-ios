//
//  MemberAboutTableViewCell.h
//  FamillyLocator
//
//  Created by Amira on 10/22/16.
//  Copyright © 2016 Amira. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MemberAboutTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UITextView *about;

@end
