//
//  FeedsTextTableViewCell.m
//  FamillyLocator
//
//  Created by Amira on 12/3/16.
//  Copyright © 2016 Amira. All rights reserved.
//

#import "FeedsTextTableViewCell.h"
#import "Helper.h"
#import "Animation.h"
#import "Constant.h"
#import "BSDropDown.h"

@implementation FeedsTextTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

-(IBAction)appearSocialView:(id)sender{
    if (self.socialView.hidden) {
        [sender setImage:[UIImage imageNamed:@"social-active.png"] forState:UIControlStateSelected];
        [sender setSelected:YES];
        
        self.socialView.hidden=NO;
    }else{
        [sender setImage:[UIImage imageNamed:@"social.png"] forState:UIControlStateNormal];
        [sender setSelected:NO];
        
        self.socialView.hidden=YES;
    }
    
}


-(void)LoadCell:(NSDictionary *)CellDictionary setTag:(int )tag{
    UserData =CellDictionary;
    dispatch_async(dispatch_get_main_queue(), ^{
        
        BOOL isLike=[[CellDictionary objectForKey:@"isLike"] boolValue];
        if (isLike) {
            [_likeBtn setSelected:YES];
        }else{
            [_likeBtn setSelected:NO];
        }
        [self.socialView.layer setShadowColor:[[UIColor darkGrayColor] CGColor]];
        [self.socialView.layer setShadowOffset:CGSizeMake(0, 0)];
        [self.socialView.layer setShadowRadius:15.0];
        [self.socialView.layer setShadowOpacity:1];
        self.socialView.clipsToBounds = NO;
        self.socialView.layer.masksToBounds = NO;
        
        
        NSString *firstName=[[CellDictionary  objectForKey:@"user"] objectForKey:@"firstName"];
        NSString *lastName=[[CellDictionary  objectForKey:@"user"] objectForKey:@"familyName"];
        _name.text=[NSString stringWithFormat:@"%@ %@",firstName,lastName];
        NSString * UserID =[[CellDictionary  objectForKey:@"user"] objectForKey:@"id"];
        
        
        
        _namedetails.text=[[CellDictionary  objectForKey:@"user"] objectForKey:@"currentCity"];
        NSDictionary *dic=[Helper editDateAndTime:[[CellDictionary  objectForKey:@"post"] objectForKey:@"date"]];
        
        NSURL *imageUrl=[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",PhotosUrl,[[CellDictionary  objectForKey:@"user"] objectForKey:@"photoId"]]];
        [_image setImage:nil];
        [_image setImageURL:imageUrl];
        [Animation roundCornerForView: _image withAngle:22.0f];
        _likeNum.text=[NSString stringWithFormat:@"%li Likes",[[CellDictionary  objectForKey:@"likesNumber"] integerValue]];
        
        _commentNum.text=[NSString stringWithFormat:@"%li Comments",[[CellDictionary  objectForKey:@"commentsNumber"] integerValue]];
        
        if ([[CellDictionary  objectForKey:@"post"] objectForKey:@"originalPost"]!=[NSNull null]) {
            _contentTxt.text=[[[CellDictionary  objectForKey:@"post"] objectForKey:@"originalPost"] objectForKey:@"text"];
        }else{
            
            _contentTxt.text=[[CellDictionary  objectForKey:@"post"] objectForKey:@"text"];
        }
        [ _socialView setHidden:YES];
        [ _likeBtn setTag:tag];
        [ _commentBTN setTag:tag];
        [ _securityBTN setTag:tag];
        [ _shareBTN setTag:tag];
        
    });
}

-(IBAction)likeAction:(id)sender{
    NSLog(@" tag %li",(long)[sender tag]);
    int num;
    if (_likeBtn.selected) {
        // [sender setImage:[UIImage imageNamed:@""] forState:UIControlStateSelected];
        num=[_likeNum.text integerValue];
        num=num-1;
        [sender setSelected:NO];
        [self.delegate actionButtonsDelegateWith:1 andTagNumber:(int)[sender tag] andLikeSelect:NO andsocialNum:0];
    }else{
        num=[_likeNum.text integerValue];
        num=num+1;
        [sender setSelected:YES];
        [self.delegate actionButtonsDelegateWith:1 andTagNumber:(int)[sender tag] andLikeSelect:YES andsocialNum:0];
        // [sender setImage:[UIImage imageNamed:@""] forState:UIControlStateSelected];
    }
    
    _likeNum.text=[NSString stringWithFormat:@"%i",num];
}

-(IBAction)commentAction:(id)sender{
    NSLog(@" tag %li",(long)[sender tag]);
    [self.delegate actionButtonsDelegateWith:2 andTagNumber:(int)[sender tag] andLikeSelect:NO andsocialNum:0];
}

-(IBAction)shareInApp:(id)sender{
    [self.delegate actionButtonsDelegateWith:3 andTagNumber:(int)[sender tag] andLikeSelect:NO andsocialNum:0];
}
-(IBAction)ShareSocial:(id)sender{
    [self.delegate actionButtonsDelegateWith:4 andTagNumber:(int)self.shareBTN.tag andLikeSelect:NO andsocialNum:(int)[sender tag]];
}
- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}
-(IBAction)GotoMember:(id)sender{
    
    [self.delegate GotoMemeberView:[UserData  objectForKey:@"user"]];
    
    
}

-(IBAction)PostAction:(id)sender{
    
    
    UIButton *btn=(UIButton*)sender;
    
    BSDropDown *ddView=[[BSDropDown alloc] initWithWidth:220 withHeightForEachRow:50 originPoint:CGPointMake(btn.frame.origin.x-100, btn.frame.origin.y+20) withOptions:@[@"Edit",@"Delete"]];
    ddView.delegate=self;
    //    ddView.dropDownBGColor=[UIColor yellowColor];
    //    ddView.dropDownTextColor=[UIColor greenColor];
    //    ddView.dropDownFont=[UIFont systemFontOfSize:13];
    [self addSubview:ddView];
    
}



#pragma mark - DropDown Delegate
-(void)dropDownView:(UIView *)ddView AtIndex:(NSInteger)selectedIndex{
    
    NSLog(@"selectedIndex: %li",(long)selectedIndex);
    if (selectedIndex==0) {
        [self.delegate EditPostAction:UserData];
        
        
    }else{
    
        [self.delegate DeletePostAction:UserData forIndex:self.index];
    
    }
    // _displayLabel.text=[NSString stringWithFormat:@"options %li",(long)selectedIndex+1];
    
}


@end
