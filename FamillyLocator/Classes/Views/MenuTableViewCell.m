//
//  MenuTableViewCell.m
//  IndoorNav
//
//  Created by Amira on 8/23/16.
//  Copyright © 2016 abdelrhman gamil. All rights reserved.
//

#import "MenuTableViewCell.h"

@implementation MenuTableViewCell
- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        
        NSArray *arrayofnibs=[[NSBundle mainBundle]loadNibNamed:@"MenuTableViewCell" owner:self options:0];
        UIView * view=[arrayofnibs objectAtIndex:0];
        [self addSubview:view];
            }
    return self;
}
- (void)loadCellwithImageName:(NSString *)imgName andTitle:(NSString *)titleTXT{
    // Initialization code
    
[CellImage setImage:[UIImage imageNamed:imgName]];
    
    
    NSMutableParagraphStyle *style =  [[NSParagraphStyle defaultParagraphStyle] mutableCopy];
    style.alignment = NSTextAlignmentRight;
    
    self.TitleLabel.text = titleTXT;
    
    
    
    
    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
