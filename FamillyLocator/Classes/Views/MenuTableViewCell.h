//
//  MenuTableViewCell.h
//  IndoorNav
//
//  Created by Amira on 8/23/16.
//  Copyright © 2016 abdelrhman gamil. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MenuTableViewCell : UITableViewCell{
    
    IBOutlet UIImageView * CellImage;
    
}
@property (weak, nonatomic) IBOutlet UILabel *TitleLabel;

- (void)loadCellwithImageName:(NSString *)imgName andTitle:(NSString *)titleTXT;

@end
