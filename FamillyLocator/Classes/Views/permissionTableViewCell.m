//
//  permissionTableViewCell.m
//  wiizme
//
//  Created by Amira on 2/12/17.
//  Copyright © 2017 Amira. All rights reserved.
//

#import "permissionTableViewCell.h"

@implementation permissionTableViewCell
@synthesize CellData,cellid,sectionid,CheckAllFlag,CheckAllChecked;

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}


- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
    
    
}
-(void)loadCellData{
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(CheckAll:) name:@"CheckAll" object:nil];
    
    self.permiLBL.text=[[CellData objectForKey:@"actionType"] objectForKey:@"name"];
    
    BOOL userInteract=[[CellData  objectForKey:@"cancelable"] boolValue];
    
  //  [self.cancelableSwitch setUserInteractionEnabled:NO];
    
//    if (userInteract) {
//        [self.permiSwitch setUserInteractionEnabled:YES];
//    }else{
//        [self.permiSwitch setUserInteractionEnabled:NO];
//    }
    
    [self.permiSwitch setTag:cellid];
    
    [self.cancelableSwitch setTag:cellid];
    
    
    BOOL switchValue=[[CellData objectForKey:@"isConfirmed"] boolValue];
    
    if (switchValue) {
        [self.permiSwitch setOn:YES animated:YES];
        
    }else{
        [self.permiSwitch setOn:NO animated:YES];
    }
    if (userInteract) {
        
        [self.cancelableSwitch setOn:YES animated:YES];
        
        self.permiSwitch.tintColor = [UIColor colorWithRed:239.0/255.0f green:239.0/255.0f blue:244.0/255.0f alpha:1.0f]; // the "off" color
        self.permiSwitch.onTintColor = [UIColor colorWithRed:41.0/255.0f green:71.0/255.0f blue:95.0/255.0f alpha:1.0f]; // the "on" color
        //  [cell.permiSwitch setThumbTintColor:[UIColor whiteColor]];
        
    }else {
        [self.cancelableSwitch setOn:NO animated:YES];
        self.permiSwitch.tintColor = [UIColor colorWithRed:239.0/255.0f green:239.0/255.0f blue:244.0/255.0f alpha:1.0f]; // the "off" color
        self.permiSwitch.onTintColor = [UIColor colorWithRed:239.0/255.0f green:239.0/255.0f blue:244.0/255.0f alpha:1.0f];
        // the "on" color
        //   [cell.permiSwitch setThumbTintColor:[UIColor colorWithRed:239.0/255.0f green:239.0/255.0f blue:244.0/255.0f alpha:1.0f]];
    }
    
    
    if (switchValue) {
        [self.permiSwitch setOn:YES animated:YES];
        
        
    }else{
        
        [self.permiSwitch setOn:NO animated:YES];
        
    }
    if (CheckAllChecked) {
        
        
        if (CheckAllFlag) {
            
            [self.permiSwitch setOn:YES animated:YES];
            [CellData setValue:[NSNumber numberWithBool:YES] forKey:@"isConfirmed"];
            [self.Delegate permiswitchStatusChange:sectionid setindex:cellid setcelldata:CellData ];
            
        }else{
            [CellData setValue:[NSNumber numberWithBool:NO] forKey:@"isConfirmed"];
            [self.Delegate permiswitchStatusChange:sectionid setindex:cellid setcelldata:CellData ];
            [self.permiSwitch setOn:NO animated:YES];
        }
    }
    
    
}
-(void)CheckAll:(NSNotification*)notification{
    
    
    NSDictionary* userInfo = notification.userInfo;
    CheckAllFlag = [[userInfo objectForKey:@"CheckAllFlag"] boolValue];
    if (sectionid==2) {
        
        
        if (CheckAllFlag)
        {
            [self.permiSwitch setOn:YES animated:NO];
            [CellData setValue:[NSNumber numberWithBool:YES] forKey:@"isConfirmed"];
            [self.Delegate permiswitchStatusChange:sectionid setindex:cellid setcelldata:CellData ];
        }else{
            
            [self.permiSwitch setOn:NO animated:NO];
            [CellData setValue:[NSNumber numberWithBool:NO] forKey:@"isConfirmed"];
            [self.Delegate permiswitchStatusChange:sectionid setindex:cellid setcelldata:CellData ];
        }
    }
    
    
    
    
    
}
- (IBAction)permiswitchStatusChange:(UISwitch *)sender
{
    // [send setHidden:NO];
    int i=sender.tag;
    BOOL value;
    if (sender.isOn) {
        value=true;
    }else{
        value=false;
    }
    [self.Delegate permiswitchStatusChange:sectionid setindex:cellid setcelldata:CellData];
    
    
    [CellData setValue:[NSNumber numberWithBool:value] forKey:@"isConfirmed"];
    
    
    [self.Delegate permiswitchStatusChange:sectionid setindex:cellid setcelldata:CellData ];
    
}

- (IBAction)cancelableswitchStatusChange:(UISwitch *)sender
{
    //[send setHidden:NO];
    int i=sender.tag;
    BOOL value;
    if (sender.isOn) {
        value=true;
    }else{
        value=false;
    }
    
    [CellData setValue:[NSNumber numberWithBool:value] forKey:@"cancelable"];
    [self.Delegate permiswitchStatusChange:sectionid setindex:cellid setcelldata:CellData ];
    
}



@end
