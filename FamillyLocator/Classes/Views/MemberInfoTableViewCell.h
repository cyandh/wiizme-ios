//
//  MemberInfoTableViewCell.h
//  FamillyLocator
//
//  Created by Amira on 10/22/16.
//  Copyright © 2016 Amira. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MemberInfoTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *infoName;
@property (weak, nonatomic) IBOutlet UILabel *data;

@end
