//
//  GroupTableViewCell.h
//  FamilyLocator
//
//  Created by Amira on 10/13/16.
//  Copyright © 2016 Amira. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AsyncImageView.h"
@interface GroupTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet AsyncImageView *image;
@property (weak, nonatomic) IBOutlet UILabel *groupName;
@property (weak, nonatomic) IBOutlet UILabel *groupsecur;
@property (weak, nonatomic) IBOutlet UILabel *memberNum;
@end
