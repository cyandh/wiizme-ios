//
//  MemberGroupsTableViewCell.h
//  FamilyLocator
//
//  Created by Amira on 10/17/16.
//  Copyright © 2016 Amira. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AsyncImageView.h"
@interface MemberGroupsTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet AsyncImageView *groupImg;
@property (weak, nonatomic) IBOutlet UILabel *groupName;
@property (weak, nonatomic) IBOutlet UIButton *joinBTN;

@end
