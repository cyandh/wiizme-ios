//
//  ChatListTableViewCell.h
//  wiizme
//
//  Created by Amira on 3/7/17.
//  Copyright © 2017 Amira. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AsyncImageView.h"
@interface ChatListTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet AsyncImageView *image;
@property (weak, nonatomic) IBOutlet UILabel *chatName;
@property (weak, nonatomic) IBOutlet UILabel *lastMsg;
@property (weak, nonatomic) IBOutlet UILabel *lastMsgDate;
@property (weak, nonatomic) IBOutlet UIButton *notificationNum;
@end
