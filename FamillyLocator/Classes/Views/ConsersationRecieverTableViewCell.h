//
//  ConsersationRecieverTableViewCell.h
//  wiizme
//
//  Created by Amira on 3/9/17.
//  Copyright © 2017 Amira. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ConsersationRecieverTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *recieverimage;
@property (weak, nonatomic) IBOutlet UILabel *recieverMsg;
@property (weak, nonatomic) IBOutlet UILabel *recieveDate;
@property (weak, nonatomic) IBOutlet UILabel *recieverName;
@property (weak, nonatomic) IBOutlet UIView *recieverView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *viewRecieverHeightConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *senderNameHeightConstraint;
@end
