//
//  GroupMembersTableViewCell.h
//  FamillyLocator
//
//  Created by Amira on 11/1/16.
//  Copyright © 2016 Amira. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AsyncImageView.h"
@interface GroupMembersTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet AsyncImageView *memberImg;
@property (weak, nonatomic) IBOutlet UILabel *memberName;
@property (weak, nonatomic) IBOutlet UIButton *Admin;
@property (weak, nonatomic) IBOutlet UIButton *confRequest;
@property (weak, nonatomic) IBOutlet UIButton *delRequest;
@property (weak, nonatomic) IBOutlet UIButton *blockUser;
@end
