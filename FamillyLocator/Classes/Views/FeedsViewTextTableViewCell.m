//
//  FeedsViewTextTableViewCell.m
//  FamillyLocator
//
//  Created by Amira on 12/3/16.
//  Copyright © 2016 Amira. All rights reserved.
//

#import "FeedsViewTextTableViewCell.h"
#import "Helper.h"
#import "Animation.h"
#import "Constant.h"
#import <UIImageView+WebCache.h>
#import "BSDropDown.h"


@implementation FeedsViewTextTableViewCell
{
    AsyncImageView *iamgeview;
    int gestureTag;
}
- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

-(void)LoadCell:(NSDictionary *)CellDictionary setTag:(int )tag{
        UserData =CellDictionary;
    dispatch_async(dispatch_get_main_queue(), ^(void){
        
        
        BOOL isLike=[[CellDictionary objectForKey:@"isLike"] boolValue];
        if (isLike) {
            
            [self.likeBtn setSelected:YES];
            
        }else{
            [self.likeBtn setSelected:NO];
            
        }
        [self.socialView.layer setShadowColor:[[UIColor darkGrayColor] CGColor]];
        [self.socialView.layer setShadowOffset:CGSizeMake(0, 0)];
        [self.socialView.layer setShadowRadius:15.0];
        [self.socialView.layer setShadowOpacity:1];
        self.socialView.clipsToBounds = NO;
        self.socialView.layer.masksToBounds = NO;
        
        [self prepareForReuse];
        NSString *firstName=[[CellDictionary objectForKey:@"user"] objectForKey:@"firstName"];
        NSString *lastName=[[CellDictionary objectForKey:@"user"] objectForKey:@"familyName"];
        NSDictionary *dic=[Helper editDateAndTime:[[CellDictionary objectForKey:@"post"] objectForKey:@"date"]];
        
        
        self.name.text=[NSString stringWithFormat:@"%@ %@",firstName,lastName];
        
        self.namedetails.text=[[CellDictionary objectForKey:@"user"] objectForKey:@"currentCity"];
        
        
        
      //  self.time.attributedText=[Helper timeTXT:[Helper timeLeftSinceDate:[[CellDictionary objectForKey:@"post"] objectForKey:@"date"]]];
        
        
        self.likeNum.text=[NSString stringWithFormat:@"%li Likes",[[CellDictionary objectForKey:@"likesNumber"] integerValue]];
        
        self.commentNum.text=[NSString stringWithFormat:@"%li Comments",[[CellDictionary objectForKey:@"commentsNumber"] integerValue]];

        
        NSURL *imageUrl=[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",PhotosUrl,[[CellDictionary objectForKey:@"user"] objectForKey:@"photoId"]]];
        
        [self.image setImageURL:imageUrl];
        [Animation roundCornerForView:self.image withAngle:22.0f];
        if (iamgeview==nil) {
            iamgeview=[[AsyncImageView alloc]initWithFrame:CGRectMake(0, _contentTxt.frame.origin.y+_contentTxt.frame.size.height, self.frame.size.width,self.contentsView.frame.size.height)];
            iamgeview.contentMode = UIViewContentModeScaleAspectFill;
            [self.contentsView addSubview:iamgeview];
     
        }
        [iamgeview cancel];
        [iamgeview setImage:nil];
        NSString *postType;
        double latitude = 0.0,longitude = 0.0;
        if ([[CellDictionary  objectForKey:@"post"] objectForKey:@"originalPost"]!=[NSNull null]) {
            _contentTxt.text=[[[CellDictionary  objectForKey:@"post"] objectForKey:@"originalPost"] objectForKey:@"text"];
            postType=[[[CellDictionary objectForKey:@"post"] objectForKey:@"originalPost"] objectForKey:@"postTypeId"];
            if (([postType isEqualToString:PostWithLocationAndTxt])||([postType isEqualToString:PostNeedHep])) {
                latitude=[[[[CellDictionary objectForKey:@"post"] objectForKey:@"originalPost"]objectForKey:@"latitude"] doubleValue];
                longitude= [[[[CellDictionary objectForKey:@"post"] objectForKey:@"originalPost"]objectForKey:@"longitude"]doubleValue];
            }
        }else{
            _contentTxt.text=[[CellDictionary  objectForKey:@"post"] objectForKey:@"text"];
            postType=[[CellDictionary objectForKey:@"post"] objectForKey:@"postTypeId"];
            if (([postType isEqualToString:PostWithLocationAndTxt])||([postType isEqualToString:PostNeedHep])) {
                latitude=[[[CellDictionary objectForKey:@"post"] objectForKey:@"latitude"] doubleValue];
                longitude= [[[CellDictionary objectForKey:@"post"] objectForKey:@"longitude"]doubleValue];
            }
        }
        if (([postType isEqualToString:PostWithLocationAndTxt])||([postType isEqualToString:PostNeedHep])) {
            
            NSString *staticMapUrl = [NSString stringWithFormat:@"http://maps.google.com/maps/api/staticmap?markers=color:red|%f,%f&zoom=13&size=%ix165&sensor=true,markers=color:blue%7@%f,%f",latitude,longitude,(int)self.frame.size.width,@"C",latitude,longitude];
            NSURL *mapUrl = [NSURL URLWithString:[staticMapUrl stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
            [iamgeview setImageURL:mapUrl];
        }else{
            
            if ([CellDictionary objectForKey:@"postFiles"] == nil || [[CellDictionary objectForKey:@"postFiles"] count] == 0) {
                
            }else{
                NSURL *postImgUrl=[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",PhotosUrl,[[CellDictionary objectForKey:@"postFiles"] objectAtIndex:0]]];
                
                [iamgeview setImageURL:postImgUrl];
                
                ImgButton = [[UIButton alloc] initWithFrame:iamgeview.frame];
                ImgButton.backgroundColor = [UIColor clearColor];
                [ImgButton setTitle:@"" forState:UIControlStateNormal];
                
                
                [ImgButton addTarget:self action:@selector(PopUpLargeImg) forControlEvents:UIControlEventTouchUpInside];
                [self addSubview:ImgButton];
                
                
            }
        }
        
        
        if (([postType isEqualToString:PostWithLocationAndTxt])||([postType isEqualToString:PostNeedHep])||([postType isEqualToString:PostSeeThis])||([postType isEqualToString:PostWithLocationAndImage])) {
            
            if ([postType isEqualToString:PostSeeThis]) {
                NSArray*tags=[CellDictionary objectForKey:@"postTags"];
                NSString *tagsStr=@"#";
                if (!(tags.count==0)||!(tags==nil)||([tags isKindOfClass:[NSNull class]])) {
                    
                    for (int i=0; i<tags.count; i++) {
                        tagsStr = [tagsStr stringByAppendingFormat:@"%@,",[tags objectAtIndex:i]];
                    }
                }
                
                _contentTxt.text=[NSString stringWithFormat:@"%@\n%@",tagsStr,[[CellDictionary  objectForKey:@"post"] objectForKey:@"text"]];
            }
            self.locationBTN.hidden=NO;
            
            UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]
                                           initWithTarget:self
                                           action:@selector(imgLocationMapView:)];
            gestureTag=tag;
            [iamgeview setUserInteractionEnabled:YES];
            [iamgeview addGestureRecognizer:tap];
        }else{
            self.locationBTN.hidden=YES;
        }
        
        [self.socialView setHidden:YES];
        [self.likeBtn setTag:tag];
        [self.commentBTN setTag:tag];
        [self.securityBTN setTag:tag];
        [self.shareBTN setTag:tag];
    });
}

-(void)prepareForReuse
{
    [self.image cancel];
    [self.image setImage:nil];
}
-(IBAction)appearSocialView:(id)sender{
    if (self.socialView.hidden) {
        [sender setImage:[UIImage imageNamed:@"social-active.png"] forState:UIControlStateSelected];
        [sender setSelected:YES];
        
        self.socialView.hidden=NO;
    }else{
        [sender setImage:[UIImage imageNamed:@"social.png"] forState:UIControlStateNormal];
        [sender setSelected:NO];
        
        self.socialView.hidden=YES;
    }
    
}

-(IBAction)likeAction:(id)sender{
    NSLog(@" tag %li",(long)[sender tag]);
    int num;
    if (_likeBtn.selected) {
        // [sender setImage:[UIImage imageNamed:@""] forState:UIControlStateSelected];
        num=[_likeNum.text integerValue];
        num=num-1;
        [sender setSelected:NO];
        [self.delegate actionButtonsDelegateWith:1 andTagNumber:(int)[sender tag] andLikeSelect:NO andsocialNum:0];
    }else{
        num=[_likeNum.text integerValue];
        num=num+1;
        [sender setSelected:YES];
        [self.delegate actionButtonsDelegateWith:1 andTagNumber:(int)[sender tag] andLikeSelect:YES andsocialNum:0];
        // [sender setImage:[UIImage imageNamed:@""] forState:UIControlStateSelected];
    }
    
    _likeNum.text=[NSString stringWithFormat:@"%i",num];
}

-(IBAction)commentAction:(id)sender{
    NSLog(@" tag %li",(long)[sender tag]);
    [self.delegate actionButtonsDelegateWith:2 andTagNumber:(int)[sender tag] andLikeSelect:NO andsocialNum:0];
}
-(IBAction)shareInApp:(id)sender{
    [self.delegate actionButtonsDelegateWith:3 andTagNumber:(int)[sender tag] andLikeSelect:NO andsocialNum:0];
}
-(IBAction)ShareSocial:(id)sender{
    [self.delegate actionButtonsDelegateWith:4 andTagNumber:(int)self.shareBTN.tag andLikeSelect:NO andsocialNum:(int)[sender tag]];
}
-(IBAction)imgLocationMapView:(id)sender{
    [self.delegate actionButtonsDelegateWith:5 andTagNumber:gestureTag andLikeSelect:NO andsocialNum:0];
}
- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}
-(IBAction)GotoMember:(id)sender{
    
    [self.delegate GotoMemeberView:[UserData  objectForKey:@"user"]];
    
    
}

-(IBAction)PostAction:(id)sender{
    
    UIButton *btn=(UIButton*)sender;
    
    BSDropDown *ddView=[[BSDropDown alloc] initWithWidth:220 withHeightForEachRow:50 originPoint:CGPointMake(btn.frame.origin.x-100, btn.frame.origin.y+20) withOptions:@[@"Edit",@"Delete"]];
    ddView.delegate=self;
    //    ddView.dropDownBGColor=[UIColor yellowColor];
    //    ddView.dropDownTextColor=[UIColor greenColor];
    //    ddView.dropDownFont=[UIFont systemFontOfSize:13];
    [self addSubview:ddView];
    
}



#pragma mark - DropDown Delegate
-(void)dropDownView:(UIView *)ddView AtIndex:(NSInteger)selectedIndex{
    
    NSLog(@"selectedIndex: %li",(long)selectedIndex);
    if (selectedIndex==0) {
        [self.delegate EditPostAction:UserData];
        
        
    }else{
        
        [self.delegate DeletePostAction:UserData forIndex:self.index];
        
    }
    // _displayLabel.text=[NSString stringWithFormat:@"options %li",(long)selectedIndex+1];
    
}
-(void)PopUpLargeImg{
    
    [self.delegate PopUpLargeImg:[NSString stringWithFormat:@"%@%@",PhotosUrl,[[UserData objectForKey:@"postFiles"] objectAtIndex:0]]];
    
}
@end
