//
//  permissionTableViewCell.h
//  wiizme
//
//  Created by Amira on 2/12/17.
//  Copyright © 2017 Amira. All rights reserved.
//

#import <UIKit/UIKit.h>
@protocol PermitionCellDelegate <NSObject>
-(void) permiswitchStatusChange:(NSInteger) section setindex:(NSInteger)index setcelldata:(NSMutableDictionary *)celldata;
@end


@interface permissionTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UISwitch *permiSwitch;
@property (weak, nonatomic) IBOutlet UISwitch *cancelableSwitch;
@property (weak, nonatomic) IBOutlet UILabel *permiLBL;
@property(nonatomic,retain)NSMutableDictionary * CellData;
@property(nonatomic,readwrite)NSInteger cellid;
@property(nonatomic,readwrite)NSInteger sectionid;
-(void)loadCellData;
@property(nonatomic,readwrite)BOOL CheckAllChecked;
@property(nonatomic,readwrite)BOOL CheckAllFlag;
@property (nonatomic, assign) id<PermitionCellDelegate> Delegate;
@end
