//
//  ConversationSenderTableViewCell.h
//  wiizme
//
//  Created by Amira on 3/9/17.
//  Copyright © 2017 Amira. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AsyncImageView.h"
@interface ConversationSenderTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet AsyncImageView *senderimage;
@property (weak, nonatomic) IBOutlet UILabel *senderMsg;
@property (weak, nonatomic) IBOutlet UILabel *sendDate;
@property (weak, nonatomic) IBOutlet UILabel *senderName;
@property (weak, nonatomic) IBOutlet UIView *senderView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *viewSenderHeightConstraint;

@end
