//
//  FeedsTextTableViewCell.h
//  FamillyLocator
//
//  Created by Amira on 12/3/16.
//  Copyright © 2016 Amira. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AsyncImageView.h"

@protocol FeedsTextCellDelegate
@optional
-(void)actionButtonsDelegateWith:(int)actionType andTagNumber:(int)tagNum andLikeSelect:(BOOL)isLike andsocialNum:(int)socialNum;
-(void)GotoMemeberView:(NSDictionary *)SelectedUserData;
-(void)EditPostAction:(NSDictionary *)PostData;
-(void)DeletePostAction:(NSDictionary *)PostData forIndex:(NSInteger)index;
@end

@interface FeedsTextTableViewCell : UITableViewCell{

    NSDictionary * UserData;

}
@property (weak, nonatomic) IBOutlet AsyncImageView *image;
@property (weak, nonatomic) IBOutlet UILabel *name;
@property (weak, nonatomic) IBOutlet UILabel *namedetails;
@property (weak, nonatomic) IBOutlet UILabel *time;
@property (weak, nonatomic) IBOutlet UITextView *contentTxt;
@property (weak, nonatomic) IBOutlet UIButton *likeBtn;
@property (weak, nonatomic) IBOutlet UIButton *commentBTN;
@property (weak, nonatomic) IBOutlet UILabel *likeNum;
@property (weak, nonatomic) IBOutlet UILabel *commentNum;
@property (weak, nonatomic) IBOutlet UIButton *securityBTN;
@property (weak, nonatomic) IBOutlet UIButton *shareBTN;
@property (weak, nonatomic) IBOutlet UIView *socialView;
@property (weak, nonatomic) IBOutlet UIButton *twiBtn;
@property (weak, nonatomic) IBOutlet UIButton *googleBTN;
@property (weak, nonatomic) IBOutlet UIButton *linBtn;
@property (weak, nonatomic) IBOutlet UIButton *instBTN;
@property (weak, nonatomic) IBOutlet UIButton *faceBtn;
@property(weak,nonatomic) IBOutlet UIButton * UserBt;
@property(nonatomic,readwrite)NSInteger index;

@property (nonatomic, weak) id<FeedsTextCellDelegate> delegate;
-(void)LoadCell:(NSDictionary *)CellDictionary setTag:(int )tag;
@end
