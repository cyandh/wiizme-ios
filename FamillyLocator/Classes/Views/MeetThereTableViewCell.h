//
//  MeetThereTableViewCell.h
//  FamillyLocator
//
//  Created by Amira on 10/19/16.
//  Copyright © 2016 Amira. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MeetThereTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *eventName;
@property(weak ,nonatomic) IBOutlet UILabel *enviters;
@property (weak, nonatomic) IBOutlet UILabel *dateLBL;
@property (weak, nonatomic) IBOutlet UILabel *timeLBL;

@end
