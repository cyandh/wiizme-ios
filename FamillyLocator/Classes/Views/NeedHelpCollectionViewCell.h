//
//  NeedHelpCollectionViewCell.h
//  wiizme
//
//  Created by Amira on 12/22/16.
//  Copyright © 2016 Amira. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AsyncImageView.h"
@interface NeedHelpCollectionViewCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet AsyncImageView *Img;
@property (weak, nonatomic) IBOutlet UILabel *Lbl;
@end
