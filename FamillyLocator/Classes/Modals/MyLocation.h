//
//  MyLocation.h
//  Mapkitmvc
//
//  Created by Mahmoud Wageh on 7/29/15.
//  Copyright (c) 2015 training. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <MapKit/MapKit.h>
@interface MyLocation : NSObject <MKAnnotation>


- (id)initWithName:(NSString*)name  coordinate:(CLLocationCoordinate2D)coordinate;
- (MKMapItem*)mapItem;

@end
