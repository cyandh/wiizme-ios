
//
//  LastPosition.m
//  wiizme
//
//  Created by Amira on 1/10/17.
//  Copyright © 2017 Amira. All rights reserved.
//

#import "LastPosition.h"
#import "Constant.h"
#import "AppDelegate.h"
#import "SOMotionDetector.h"
#import "SOStepDetector.h"
#import "ZonesModel.h"
#define SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(v)  ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedAscending)
@implementation LastPosition{
    NSNumber * latitudeNum,*longitudeNum;
    NSString *address;
    WebServiceFunc *webService,*webServiceTrack,*webServiceZone;
    NSString *movState;
}

- (id) init {
    self = [super init];
    
    if (self) {

    }
    return self;
}

-(void)startLoad{

  
    self.locationManager = [[CLLocationManager alloc] init];
  //  dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^(void){
        webService=[[WebServiceFunc alloc]init];
        [webService setDelegate:self];
        webServiceTrack=[[WebServiceFunc alloc]init];
        [webServiceTrack setDelegate:self];
        self.locationManager.delegate = self;
        self.locationManager.desiredAccuracy = kCLLocationAccuracyBest;
        self.locationManager.distanceFilter = 30.0f;
        NSLog(@"PSCoordinates init");
        //            if ([self.locationManager respondsToSelector:@selector(requestWhenInUseAuthorization)]) {
        //                [self.locationManager requestWhenInUseAuthorization];
        //            }
        //            [self.locationManager startUpdatingLocation];
        if ( (![timer isValid]) || (timer==nil)){
            NSString *time=[[NSUserDefaults standardUserDefaults] objectForKey:@"Timerfrequence"];
            double tim=time.doubleValue;
            if ((time==nil)||(tim==0)) {
                
            }else{
                timer= [NSTimer scheduledTimerWithTimeInterval:tim*60.0
                                                        target:self
                                                      selector:@selector(sendLocation)
                                                      userInfo:nil
                                                       repeats:YES];
                
              [[NSRunLoop currentRunLoop] addTimer:timer forMode:NSRunLoopCommonModes];
            
            }
        }
        
  //  });
    

}
-(void)sendLocation{
    if ([[NSUserDefaults standardUserDefaults] objectForKey:@"Timerfrequence"]==nil) {
        
    }else{
    if ([self.locationManager respondsToSelector:@selector(requestWhenInUseAuthorization)]) {
        [self.locationManager requestWhenInUseAuthorization];
    }
    [self.locationManager startMonitoringSignificantLocationChanges];
    }
}
#pragma mark - CLLocationManagerDelegate

- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error
{
    NSLog(@"didFailWithError: %@", error);
//    UIAlertView *errorAlert = [[UIAlertView alloc]
//                               initWithTitle:@"Error" message:@"Failed to Get Your Location" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
//    [errorAlert show];
}

- (void)locationManager:(CLLocationManager *)manager didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation
{
    NSLog(@"didUpdateToLocation: %@", newLocation);
    CLLocation *currentLocation = newLocation;
    
    
    UserOldLocation= ((AppDelegate *)[[UIApplication sharedApplication] delegate]).UserOldLocation;
    if (currentLocation != nil) {
        /*
        CLLocationDistance meters = [newLocation distanceFromLocation:UserOldLocation];
        NSString *str=[NSString stringWithFormat:@"%f",meters];

        if (meters>=10|| UserOldLocation==nil) {
            longitudeNum =[NSNumber numberWithDouble:currentLocation.coordinate.longitude] ;
            latitudeNum =[NSNumber numberWithDouble:currentLocation.coordinate.latitude];
            
            UserOldLocation = currentLocation;
            ((AppDelegate *)[[UIApplication sharedApplication] delegate]).UserOldLocation=currentLocation;
            [self getStatus];
            NSLog(@"hello");

        }*/
        longitudeNum =[NSNumber numberWithDouble:currentLocation.coordinate.longitude] ;
        latitudeNum =[NSNumber numberWithDouble:currentLocation.coordinate.latitude];
        UserOldLocation = currentLocation;
        ((AppDelegate *)[[UIApplication sharedApplication] delegate]).UserOldLocation=currentLocation;
        [self getStatus];
        [self.locationManager stopUpdatingLocation];
        
    }
    
}

-(double)batteryState{
    UIDevice *myDevice = [UIDevice currentDevice];
    [myDevice setBatteryMonitoringEnabled:YES];
    
    int state = [myDevice batteryState];
    NSLog(@"battery status: %d",state); // 0 unknown, 1 unplegged, 2 charging, 3 full
    
    double batLeft = (float)[myDevice batteryLevel] * 100;
    return batLeft;
}
-(void)getStatustoserver:(CLLocation *)Location{

    movState=@"";
    
    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")) {
        [SOMotionDetector sharedInstance].useM7IfAvailable = YES; //Use M7 chip if available, otherwise use lib's algorithm
    }
    
    //This is required for iOS > 9.0 if you want to receive location updates in the background
    [SOLocationManager sharedInstance].allowsBackgroundLocationUpdates = YES;
    
    //Starting motion detector
    [[SOMotionDetector sharedInstance] startDetection];
    
    //Starting pedometer
    [[SOStepDetector sharedInstance] startDetectionWithUpdateBlock:^(NSError *error) {
        if (error) {
            NSLog(@"%@", error.localizedDescription);
            return;
        }
        
    }];
    [SOMotionDetector sharedInstance].motionTypeChangedBlock = ^(SOMotionType motionType) {
        NSString *type = @"";
        switch (motionType) {
            case MotionTypeNotMoving:
                type = @"Not moving";
                break;
            case MotionTypeWalking:
                type = @"Walking";
                break;
            case MotionTypeRunning:
                type = @"Running";
                break;
            case MotionTypeAutomotive:
                type = @"Automotive";
                break;
        }
        
        movState=type;
        NSMutableDictionary *userData = [[NSMutableDictionary alloc] initWithObjectsAndKeys:latitudeNum,@"lastKnowingLatitude",
                                         longitudeNum,@"lastKnowingLangitude",
                                         movState,@"UserStatue",
                                         [NSNumber numberWithDouble:[self batteryState]],@"BatteryStatue",
                                         nil];
        
        
        [webService postDataWithUrlString:LastLocation withData:userData withflagLogin:YES];
        
        NSMutableArray *trackers=[[NSUserDefaults standardUserDefaults] objectForKey:@"TrackersId"];
        if (trackers==nil) {
            trackers=[[NSMutableArray alloc]init];
            [[NSUserDefaults standardUserDefaults] setObject:trackers forKey:@"TrackersId"];
        }
        [webServiceTrack postDataWithUrlString:sendLiveTrackers withData:trackers withflagLogin:YES];
        
        RLMResults<ZonesModel *> * artists = [ZonesModel allObjects];
        for (int i=0; i<artists.count; i++) {
            ZonesModel *dic=[artists objectAtIndex:i];
            CLLocation *zonlocation=[[CLLocation alloc] initWithLatitude:dic.Latitude longitude:dic.Longitude];
            CLLocation *curntLocation=[[CLLocation alloc] initWithLatitude:latitudeNum.doubleValue longitude:longitudeNum.doubleValue];
            CLLocationDistance meters = [zonlocation distanceFromLocation:curntLocation];
            NSString *str=[NSString stringWithFormat:@"%f",meters];
            
            if (meters<dic.Radius) {
                webServiceZone =[[WebServiceFunc alloc]init];
                [webServiceZone setDelegate:self];
                NSString *urlString=[NSString stringWithFormat:@"%@%@",SendOutZone,dic.Id];
                [webServiceZone getData:urlString withflagLogin:YES];
            }
        }
    };
    
    


}


-(void)getStatus{
    movState=@"";
    
    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")) {
        [SOMotionDetector sharedInstance].useM7IfAvailable = YES; //Use M7 chip if available, otherwise use lib's algorithm
    }
    
    //This is required for iOS > 9.0 if you want to receive location updates in the background
    [SOLocationManager sharedInstance].allowsBackgroundLocationUpdates = YES;
    
    //Starting motion detector
    [[SOMotionDetector sharedInstance] startDetection];
    
    //Starting pedometer
    [[SOStepDetector sharedInstance] startDetectionWithUpdateBlock:^(NSError *error) {
        if (error) {
            NSLog(@"%@", error.localizedDescription);
            return;
        }
        
    }];
    [SOMotionDetector sharedInstance].motionTypeChangedBlock = ^(SOMotionType motionType) {
        NSString *type = @"";
        switch (motionType) {
            case MotionTypeNotMoving:
                type = @"Not moving";
                break;
            case MotionTypeWalking:
                type = @"Walking";
                break;
            case MotionTypeRunning:
                type = @"Running";
                break;
            case MotionTypeAutomotive:
                type = @"Automotive";
                break;
        }
        
        movState=type;
        NSMutableDictionary *userData = [[NSMutableDictionary alloc] initWithObjectsAndKeys:latitudeNum,@"lastKnowingLatitude",
                                         longitudeNum,@"lastKnowingLangitude",
                                         movState,@"UserStatue",
                                         [NSNumber numberWithDouble:[self batteryState]],@"BatteryStatue",
                                         nil];
        
        
        [webService postDataWithUrlString:LastLocation withData:userData withflagLogin:YES];
        
        NSMutableArray *trackers=[[NSUserDefaults standardUserDefaults] objectForKey:@"TrackersId"];
        if (trackers==nil) {
            trackers=[[NSMutableArray alloc]init];
            [[NSUserDefaults standardUserDefaults] setObject:trackers forKey:@"TrackersId"];
        }
        [webServiceTrack postDataWithUrlString:sendLiveTrackers withData:trackers withflagLogin:YES];
        
        RLMResults<ZonesModel *> * artists = [ZonesModel allObjects];
        for (int i=0; i<artists.count; i++) {
            ZonesModel *dic=[artists objectAtIndex:i];
            CLLocation *zonlocation=[[CLLocation alloc] initWithLatitude:dic.Latitude longitude:dic.Longitude];
            CLLocation *curntLocation=[[CLLocation alloc] initWithLatitude:latitudeNum.doubleValue longitude:longitudeNum.doubleValue];
            CLLocationDistance meters = [zonlocation distanceFromLocation:curntLocation];
            NSString *str=[NSString stringWithFormat:@"%f",meters];
            
            if (meters<dic.Radius) {
                webServiceZone =[[WebServiceFunc alloc]init];
                [webServiceZone setDelegate:self];
                NSString *urlString=[NSString stringWithFormat:@"%@%@",SendOutZone,dic.Id];
                [webServiceZone getData:urlString withflagLogin:YES];
        }
        }
    };
    

}



-(void)userWebserviceCallDidSuccessWithRespone:(NSMutableArray *)response{
    
      NSLog(@"send");
}

-(void)userWebserviceCallDidFaildwithError:(NSError *)error andMessage:(NSString *)message{
        NSLog(@"error%@",error.localizedDescription);
   
    
}
@end
