//
//  GroupData.h
//  FamillyLocator
//
//  Created by Amira on 10/26/16.
//  Copyright © 2016 Amira. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface GroupData : NSObject
@property(nonatomic)BOOL member,admin,pending,onlyAdmin;
@property(nonatomic,strong)NSString *coverImg,*profileImg,*groupName,*privacy,*privacyId,*activityType,*activityTypeId,*desc,*policy,*groupNum,*groupID,*coverID,*logoID;
@property(nonatomic,strong)NSArray *activities;

@property(nonatomic)int rowIndx,arrIndx ,unSeenFeeds,unSeenChat;
@property(nonatomic,strong)NSDictionary *groupObj;
@end
