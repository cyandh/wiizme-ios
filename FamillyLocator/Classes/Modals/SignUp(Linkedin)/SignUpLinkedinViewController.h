//
//  SignUpLinkedinViewController.h
//  YaDoc
//
//  Created by Ahmed on 2/16/16.
//  Copyright © 2016 nasr. All rights reserved.
//

#import <UIKit/UIKit.h>
@protocol linkedinSignDelegate <NSObject>
@required
@optional
-(void)getLinkedInData:(NSDictionary*)linkedInData;
-(void)failWithResponseLinkedin:(NSString*)message;
@end

@interface SignUpLinkedinViewController : UIViewController<UIWebViewDelegate>
@property(nonatomic)BOOL SignUp;
//@property(nonatomic,weak)DoctorModel* docOBJ;
@property(nonatomic,weak)id<linkedinSignDelegate>delegate;
-(void)startAuthorization;
@end
