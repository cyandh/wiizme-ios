//
//  SignUpLinkedinViewController.m
//  YaDoc
//
//  Created by Ahmed on 2/16/16.
//  Copyright © 2016 nasr. All rights reserved.
//

#import "SignUpLinkedinViewController.h"
#import "AFNetworking.h"
#import "Reachability.h"
#import "MBProgressHUD.h"
#import "MMProgressHUD.h"

@interface SignUpLinkedinViewController ()<UIWebViewDelegate>
{
    __weak IBOutlet UIWebView *linkedinWebView;
    NSString* accessToken;
    MBProgressHUD* progress;
}
@end

@implementation SignUpLinkedinViewController
// MARK: Constants
NSString* const linkedInKey = @"869m6m5ducsbl9";
NSString* const linkedInSecret = @"NigPwnHNAhkMhGGz";
NSString* const authorizationEndPoint = @"https://www.linkedin.com/oauth/v2/authorization";
NSString* const accessTokenEndPoint = @"https://www.linkedin.com/oauth/v2/accessToken";

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    linkedinWebView.delegate = self;
    //    NSLog(@"%@",self.SignUp);
    [self startAuthorization];
    // [self setBack];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)startAuthorization{
    
    NSString *responseType=@"code";
    NSString*urlStr=@"https://com.appcoda.linkedin.oauth/oauth";
    NSString *redirectURL=[urlStr stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet alphanumericCharacterSet]];
    NSString*state=[NSString stringWithFormat:@"%@%f",@"linkedin",NSTimeIntervalSince1970];
    NSString* scope = @"r_basicprofile,r_emailaddress,rw_company_admin,w_share";
    NSString* authorizationURL =[NSString stringWithFormat:@"%@?response_type=%@&client_id=%@&redirect_uri=%@&state=%@&scope=%@",authorizationEndPoint,responseType,linkedInKey,redirectURL,state,scope];
    NSLog(@"%@",authorizationURL);
    NSURLRequest*request=[NSURLRequest requestWithURL:[NSURL URLWithString:authorizationURL]];
    [linkedinWebView loadRequest:request];
    
}


-(void)requestForAccessToken:(NSString *)authorizationCode {
    
    NSString*grantType=@"authorization_code";
    NSString *redirectURL=[NSString stringWithFormat:@"%@",[@"https://com.appcoda.linkedin.oauth/oauth" stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet alphanumericCharacterSet]]];
    NSString *postParams=[NSString stringWithFormat:@"grant_type=%@&code=%@&redirect_uri=%@&client_id=%@&client_secret=%@",grantType,authorizationCode,redirectURL,linkedInKey,linkedInSecret];
    NSData * postData=[postParams dataUsingEncoding:NSUTF8StringEncoding];
    NSMutableURLRequest*request=[NSMutableURLRequest requestWithURL:[NSURL URLWithString:accessTokenEndPoint]];
    request.HTTPMethod=@"POST";
    request.HTTPBody = postData;
    [request addValue:@"application/x-www-form-urlencoded;" forHTTPHeaderField:@"Content-Type"];
    
    NSURLSession *session =[NSURLSession sessionWithConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
    
    NSURLSessionDataTask *task = [session dataTaskWithRequest:request completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
        
        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
        int statusCode=[httpResponse statusCode];
        if(statusCode == 200) {
            
            @try {
                NSDictionary* dataDictionary = [NSJSONSerialization JSONObjectWithData:data
                                                                               options:NSJSONReadingMutableContainers
                                                                                 error:&error];
                accessToken = [dataDictionary objectForKey:@"access_token"];
                [[NSUserDefaults standardUserDefaults] setObject:accessToken forKey:@"LIAccessToken"];
                [[NSUserDefaults standardUserDefaults] synchronize];
                [self getUserProfile];
                //                dispatch_async(dispatch_get_main_queue(), ^{
                //
                //                    [self.navigationController popViewControllerAnimated:YES];
                //
                //                });
                
            }
            @catch (NSException *exception) {
                NSLog(@"Could not convert JSON data into a dictionary.");
            }
            
            
        }
        
    }];
    [task resume];
}
-(void)getUserProfile
{
    [[MMProgressHUD sharedHUD] setOverlayMode:MMProgressHUDWindowOverlayModeGradient];
    [MMProgressHUD setPresentationStyle:MMProgressHUDPresentationStyleExpand];
    [MMProgressHUD setDisplayStyle:MMProgressHUDDisplayStyleBordered];
    //  [MMProgressHUD showWithTitle:nil status:stringForKey(@"Loading")];
    
    NSString *url =[NSString stringWithFormat:@"https://api.linkedin.com/v1/people/~:(id,first-name,current-share,headline,numConnections,email-address,formatted-name,phone-numbers,public-profile-url)?oauth2_access_token=%@&format=json",accessToken];
    AFSecurityPolicy *securityPolicy = [[AFSecurityPolicy alloc] init];
    [securityPolicy setAllowInvalidCertificates:YES];
    AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc]initWithSessionConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
    [manager setSecurityPolicy:securityPolicy];
    
    manager.requestSerializer = [AFHTTPRequestSerializer serializer];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    
    [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [manager GET:url parameters:nil progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        NSLog(@"success");
        NSError* error;
        NSDictionary* json=responseObject;
        [self.navigationController popViewControllerAnimated:YES];
        [self.delegate getLinkedInData:json];
        
        
        //        if (self.SignUp) {
        //            self.docOBJ.Email=[json valueForKey:@"emailAddress"];
        //            self.docOBJ.=[json valueForKey:@"emailAddress"];
        //            self.docOBJ.Email=[json valueForKey:@"emailAddress"];
        //
        //        }
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        NSLog(@"%@",error);
        NSString*ResponseString;
        if ([self isReachable]) {
            NSData *errorData = error.userInfo[AFNetworkingOperationFailingURLResponseDataErrorKey];
            if (errorData!=nil) {
                
                
                NSDictionary *serializedData = [NSJSONSerialization JSONObjectWithData: errorData options:kNilOptions error:nil];
            }else{
                [self.delegate failWithResponseLinkedin:ResponseString];
                
            }
            NSHTTPURLResponse* r = (NSHTTPURLResponse*)task.response;
            NSLog(@"%ld",(long)r.statusCode);
            ResponseString=error.localizedDescription;
            
            
        }else
        {
            ResponseString=[NSString stringWithFormat:@"%@",@"There is no internet"];
        }
        dispatch_async(dispatch_get_main_queue(), ^{
            
            [self.delegate failWithResponseLinkedin:ResponseString];
            
        });
    }];
}
-(BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType{
    NSURL*url=request.URL;
    NSLog(@"%@",url);
    [[MMProgressHUD sharedHUD] setOverlayMode:MMProgressHUDWindowOverlayModeGradient];
    [MMProgressHUD setPresentationStyle:MMProgressHUDPresentationStyleExpand];
    [MMProgressHUD setDisplayStyle:MMProgressHUDDisplayStyleBordered];
    // [MMProgressHUD showWithTitle:nil status:stringForKey(@"Loading")];
    if ([url.host isEqualToString:@"com.appcoda.linkedin.oauth"]) {
        
        if ([url.absoluteString rangeOfString:@"code"].length!=0) {
            
            NSString*urlParts=[url.absoluteString componentsSeparatedByString:@"?"][1];
            NSString*code= [urlParts componentsSeparatedByString:@"="][1];
            
            [self requestForAccessToken:code];
        }
        
    }
    
    
    
    
    
    
    return YES;
    
}
- (void)webViewDidFinishLoad:(UIWebView *)webView
{
    [MMProgressHUD dismissWithSuccess:@"Success"];
}
- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error
{
    [MMProgressHUD dismissWithError:@"Fail"];
}
-(BOOL)isReachable{
    Reachability *networkReachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus networkStatus = [networkReachability currentReachabilityStatus];
    if (((networkStatus != ReachableViaWiFi)&&(networkStatus != ReachableViaWWAN))|| (networkStatus== NotReachable)) {
        NSLog(@"There is no Conaction");
        return NO;
    }
    else
    {
        NSLog(@"There is a Conaction");
        return YES;
    }
}
-(IBAction)dissmiss:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}
/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
