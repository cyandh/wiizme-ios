//
//  WebServiceFunc.m
//  FamillyLocator
//
//  Created by Amira on 10/26/16.
//  Copyright © 2016 Amira. All rights reserved.
//

#import "WebServiceFunc.h"
#import "Reachability.h"
#import "Constant.h"


@implementation WebServiceFunc



-(void)updateUserImagewithimageobject:(NSMutableArray*)imageObject{
    
    NSError *error;
    
    NSURL* url = [NSURL URLWithString:[NSString stringWithFormat:@"%@/common/files/AddMultiple",MainAPi]]; //1
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:200.0];
    
    [request addValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    
    [request addValue:@"application/json" forHTTPHeaderField:@"Accept"];
    
    [request setHTTPMethod:@"POST"];
    
    NSURLSessionConfiguration* config = [NSURLSessionConfiguration defaultSessionConfiguration];
    
    NSURLSession* session = [NSURLSession sessionWithConfiguration:config];
    
    NSString *userID=[[NSUserDefaults standardUserDefaults]objectForKey:@"UserID"];
    
    NSData *postData = [NSJSONSerialization dataWithJSONObject:imageObject options:0 error:&error];
    
    NSString * Test =[NSString stringWithUTF8String:[postData bytes]];
    
    [request setHTTPBody:postData];
    
    NSURLSessionDataTask *postDataTask = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        if (error==nil) {
            
            dispatch_async(dispatch_get_main_queue(), ^{
                //get status code
                NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
                
                long status=(long)[httpResponse statusCode];
                
                if(status>=500&&status<=599)
                {
                    UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Error" message:@"Server Error" delegate:nil cancelButtonTitle:@"Cancel" otherButtonTitles:nil];
                    
                    [alert show];
                    
                    [self.delegate userWebserviceCallDidFaildwithError:error andMessage:@"Server Error"];
                    
                }else if (error==nil) {
                    
                    NSError *errorParsing=nil;
                    
                    NSArray *imageid =[NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:&errorParsing];
                    
                    if(imageid){
                        
                        [self.delegate userImagesWebserviceCallDidSuccessWithRespone:imageid];
                        
                        
                    }else{
                        
                        [self.delegate userImagesWebserviceCallDidFaildwithError:error andMessage:@"Error in Upload Image"];
                    }
                    
                }
                else {
                    [self.delegate userWebserviceCallDidFaildwithError:error andMessage:@" Error in connection"];
                }
                
            });
            
        }
        else {
            NSLog(@"%@",error.localizedDescription);
            [self.delegate userWebserviceCallDidFaildwithError:error andMessage:@"Error in connection"];
        }
    }];
    [postDataTask resume];
    
}

-(void)postDataWithUrlString:(NSString*)urlString withData:(NSMutableDictionary *)dicData  withflagLogin:(BOOL)loginFlag{
    
    NSError *error;
    
    NSURL* url = [NSURL URLWithString:urlString];
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:200.0];
    
    NSString *string=[NSString stringWithFormat:@"bearer %@",[[NSUserDefaults standardUserDefaults]objectForKey:@"Token"]];
    
    [request addValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    
    if (loginFlag==YES) {
        
        [request addValue:string forHTTPHeaderField:@"Authorization"];
        
    }
    
    [request addValue:@"application/json" forHTTPHeaderField:@"Accept"];
    
    [request setHTTPMethod:@"POST"];
    
    NSURLSessionConfiguration* config = [NSURLSessionConfiguration defaultSessionConfiguration];
    
    NSURLSession* session = [NSURLSession sessionWithConfiguration:config];
    
    NSData *postData;
    
    if ([dicData isKindOfClass:[NSMutableArray class]]) {
        
        NSMutableArray *data=(NSMutableArray *)dicData;
        
        postData= [NSJSONSerialization dataWithJSONObject:data options:0 error:&error];
        
    }else{
        
        postData= [NSJSONSerialization dataWithJSONObject:dicData options:0 error:&error];
    }
    
    NSString * Test= [[NSString alloc] initWithData:postData encoding:NSUTF8StringEncoding];
    
    [request setHTTPBody:postData];
    
    NSURLSessionDataTask *postDataTask = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        
        if (error==nil) {
            
            dispatch_async(dispatch_get_main_queue(), ^{
                //get status code
                NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
                
                long status=(long)[httpResponse statusCode];
                 NSString * Test2= [[NSString alloc] initWithData:postData encoding:NSUTF8StringEncoding];
                if(status>=500&&status<=599)
                {
                    UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Error" message:@"Server Error" delegate:nil cancelButtonTitle:@"Cancel" otherButtonTitles:nil];
                    
                    [alert show];
                    [self.delegate userWebserviceCallDidFaildwithError:error andMessage:@"500"];
                }else if (status==401){
                    UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Error" message:@"please login again" delegate:nil cancelButtonTitle:@"ok" otherButtonTitles:nil];
                    
                    [alert show];
                    [self.delegate userWebserviceCallDidFaildwithError:error andMessage:@"401"];
                    
                }else if (error==nil) {
                    
                    NSError *errorParsing=nil;
                    
                    NSString * sttr= [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
                    
                    if ([NSJSONSerialization JSONObjectWithData:data
                                                        options:kNilOptions
                                                          error:&errorParsing] == nil)
                    {
                        
                        [self.delegate userWebserviceCallDidSuccessWithRespone:nil];
                        
                    }else{
                        
                        
                        NSMutableArray *array =[NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:&errorParsing];
                        
                        if(array){
                            [self.delegate userWebserviceCallDidSuccessWithRespone:array];
                            
                            
                        }else{
                            
                            [self.delegate userWebserviceCallDidFaildwithError:error andMessage:@"Error in Retrieve Data"];
                        }
                        
                    }
                }
                else {
                    
                    [self.delegate userWebserviceCallDidFaildwithError:error andMessage:@" Error in connection"];
                    
                }
                
            });
        }else {

            [self.delegate userWebserviceCallDidFaildwithError:error andMessage:@"Error in connection"];
            
        }
        
    }];
    
    [postDataTask resume];
    
}

-(void)postDataLogin:(NSString*)urlString withData:(NSString *)dicData {
    
    NSError *error;
    
    NSURL* url = [NSURL URLWithString:urlString]; //1
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:200.0];
    [request addValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    
    [request setHTTPMethod:@"POST"];
    
    NSURLSessionConfiguration* config = [NSURLSessionConfiguration defaultSessionConfiguration];
    
    NSURLSession* session = [NSURLSession sessionWithConfiguration:config];
    
    
    NSData *postData = [dicData dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
    
    NSString * Test =[NSString stringWithUTF8String:[postData bytes]];
    
    [request setHTTPBody:postData];
    
    NSURLSessionDataTask *postDataTask = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        if (error==nil) {
            
            dispatch_async(dispatch_get_main_queue(), ^{
                //get status code
                NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
                // NSLog(@"response status code: %ld", (long)[httpResponse statusCode]);
                long status=(long)[httpResponse statusCode];
                
                if(status>=500&&status<=599)
                {
                    UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Error" message:@"Server Error" delegate:nil cancelButtonTitle:@"Cancel" otherButtonTitles:nil];
                    
                    [alert show];
                    [self.delegate userWebserviceCallDidFaildwithError:error andMessage:@"Server Error"];
                }else if (status==401){
                    
                    UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Error" message:@"please login again" delegate:nil cancelButtonTitle:@"ok" otherButtonTitles:nil];
                    
                    [alert show];
                    [self.delegate userWebserviceCallDidFaildwithError:error andMessage:@"401"];
                    
                }else if (error==nil) {
                    NSError *errorParsing=nil;
                    if ([NSJSONSerialization JSONObjectWithData:data
                                                        options:kNilOptions
                                                          error:&errorParsing] == nil)
                    {
                        [self.delegate userWebserviceCallDidSuccessWithRespone:nil];
                    }else{
                        
                        NSMutableArray *array =[NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:&errorParsing];
                        
                        if(array){
                            if([array valueForKey:@"error"]==nil ) {
                                [self.delegate userWebserviceCallDidSuccessWithRespone:array];
                            }else{
                                [self.delegate userWebserviceCallDidFaildwithError:error andMessage:[array valueForKey:@"error"]];
                            }
                        }else{
                            
                            [self.delegate userWebserviceCallDidFaildwithError:error andMessage:@"Error in Retrieve Data"];
                        }
                        
                    }
                }
                else {
                    
                    [self.delegate userWebserviceCallDidFaildwithError:error andMessage:@" Error in connection"];
                    
                }
                
            });
            
        }
        else {
            
            [self.delegate userWebserviceCallDidFaildwithError:error andMessage:@"Error in connection"];
            
        }
        
    }];
    
    [postDataTask resume];
    
}

-(void)getData:(NSString*)urlString withflagLogin:(BOOL)loginFlag
{
    if (![self checkReachability]) {
        //        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Error" message:@"Please,Check your connection and try again" delegate:nil cancelButtonTitle:@"Cancel" otherButtonTitles:nil];
        //
        //        [alert show];
        [self.delegate userWebserviceCallDidFaildwithError:nil andMessage:@" Error in connection"];
        
    }else{
        NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
        NSURLSession *session = [NSURLSession sessionWithConfiguration:configuration delegate:self delegateQueue:nil];
        urlString =[urlString stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        NSURL *url = [NSURL URLWithString:urlString];
        
        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url
                                                               cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                           timeoutInterval:60.0];
        NSString *string=[NSString stringWithFormat:@"bearer %@",[[NSUserDefaults standardUserDefaults]objectForKey:@"Token"]];
        
        if (loginFlag==YES) {
            [request addValue:string forHTTPHeaderField:@"Authorization"];
        }
        [request addValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
        [request addValue:@"application/json" forHTTPHeaderField:@"Accept"];
        
        [request setHTTPMethod:@"Get"];
        
        
        NSURLSessionDataTask *postDataTask = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
            dispatch_async(dispatch_get_main_queue(), ^{
                
                //get status code
                NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
                // NSLog(@"response status code: %ld", (long)[httpResponse statusCode]);
                long status=(long)[httpResponse statusCode];
                  NSString * Test= [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
                if(status>=500&&status<=599)
                {
                    UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Error" message:@"Server Error" delegate:nil cancelButtonTitle:@"Cancel" otherButtonTitles:nil];
                    
                    [alert show];
                    [self.delegate userWebserviceCallDidFaildwithError:error andMessage:@"Server Error"];
                }else if ((status==401)||(status==403)){
                    
                    
                    NSString *msg=[NSString stringWithFormat:@"%li",status];
                    [self.delegate userWebserviceCallDidFaildwithError:error andMessage:msg];
                    
                }else if ((status==404)||(status==302)||(status==205)||(status==406)||(status==409)){
                    [self.delegate userWebserviceCallDidFaildwithError:error andMessage:[NSString stringWithFormat:@"%li",status]];
                }else if (error==nil) {
                    NSError *errorParsing=nil;
                    NSString * sttr= [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
                    if ([NSJSONSerialization JSONObjectWithData:data
                                                        options:kNilOptions
                                                          error:&errorParsing] == nil)
                    {
                        
                        [self.delegate userWebserviceCallDidSuccessWithRespone:nil];
                    }else{
                        NSString * sttr= [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
                        NSMutableArray *array =[NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:&errorParsing];
                        if(array){
                            
                            [self.delegate userWebserviceCallDidSuccessWithRespone:array];
                            
                        }else{
                            
                            [self.delegate userWebserviceCallDidFaildwithError:error andMessage:@"Error in retrieve Data"];
                        }
                    }
                    
                }
                
                else {
                    
                    [self.delegate userWebserviceCallDidFaildwithError:error andMessage:@" Error in connection"];
                    
                    
                }
            });
        }];
        
        [postDataTask resume];
        
    }
    
}

-(BOOL)checkReachability{
    
    Reachability *networkReachability = [Reachability reachabilityForInternetConnection];
    
    NetworkStatus networkStatus = [networkReachability currentReachabilityStatus];
    
    if (networkStatus == NotReachable) {
        
        return false;
    } else {
        
        return true;
    }
}

-(NSString *)encodeToBase64String:(UIImage *)userimage {
    
    NSData *image=UIImageJPEGRepresentation(userimage, 0.9);
    
    return [image base64EncodedStringWithOptions:NSDataBase64Encoding64CharacterLineLength];
    
}-

(NSString *)contentTypeForImageData:(NSData *)data {
    uint8_t c;
    [data getBytes:&c length:1];
    
    switch (c) {
        case 0xFF:
            return @"image/jpeg";
        case 0x89:
            return @"image/png";
        case 0x47:
            return @"image/gif";
        case 0x49:
        case 0x4D:
            return @"image/tiff";
    }
    return nil;
}


#pragma mark - Compress Image Size

- (UIImage *)imageWithImage:(UIImage *)image scaledToSize:(CGSize)newSize {
    
    UIGraphicsBeginImageContext(newSize);
    
    [image drawInRect:CGRectMake(0, 0, newSize.width, newSize.height)];
    
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    
    UIGraphicsEndImageContext();
    
    return newImage;
}

@end
