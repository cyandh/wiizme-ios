//
//  FacebookLogin.h
//  YaDoc
//
//  Created by Ahmed on 2/18/16.
//  Copyright © 2016 nasr. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <Social/Social.h>
@protocol faceBookDelegate <NSObject>
@required
@optional
-(void)getFacebookData:(NSDictionary*)linkedInData;
-(void)failWithResponseFaceBook:(NSString*)message;
@end
@interface FacebookLogin : NSObject
-(void)useExistingFBAccount;
+ (id)sharedManager;
@property(nonatomic,strong)UIViewController* viewController;
@property(nonatomic,weak)id<faceBookDelegate>delegate;
@end
