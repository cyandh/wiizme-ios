//
//  FacebookLogin.m
//  YaDoc
//
//  Created by Ahmed on 2/18/16.
//  Copyright © 2016 nasr. All rights reserved.
//

#import "FacebookLogin.h"
#import <FBSDKShareKit/FBSDKShareKit.h>
#import <FBSDKLoginKit/FBSDKLoginKit.h>
#import <FBSDKCoreKit/FBSDKCoreKit.h>
@interface FacebookLogin ()
{
    ACAccount *facebookAccount;
    ACAccountStore *account1 ;
    NSString* accessToken;
}
@end
@implementation FacebookLogin
+ (id)sharedManager {
    static FacebookLogin *sharedMyManager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedMyManager = [[self alloc] init];
    });
    return sharedMyManager;
}
-(id)init
{
    self=[super init];
    if (self) {
        
    }return  self;
}
-(void)useExistingFBAccount
{
    
    account1 = [[ACAccountStore alloc] init];
    ACAccountType *accountType = [account1 accountTypeWithAccountTypeIdentifier:ACAccountTypeIdentifierFacebook];
    
    
    
    
    
    [account1 requestAccessToAccountsWithType:accountType
                                      options:@{ACFacebookAppIdKey: @"1829340227340606", ACFacebookPermissionsKey: @[@"user_about_me",@"email"]}
                                   completion:^(BOOL granted, NSError *error) {
                                       if(granted){
                                           NSArray *accounts = [account1 accountsWithAccountType:accountType];
                                           facebookAccount = [accounts lastObject];
                                           
                                           [self me];
                                       }else{
                                           // ouch
                                           dispatch_async(dispatch_get_main_queue(), ^{
                                               //                                               if (error.code==6) {
                                               NSLog(@"error %@",error);
                                               [self requestusingBrowser];
                                               //                                               }else
                                               //                                               {
                                               //                                               }
                                               
                                           });
                                       }
                                   }];
    
}


- (void)fbDidLogout
{
    
}
//connect to facebook through api
-(void)requestusingBrowser
{
    FBSDKLoginManager *login = [[FBSDKLoginManager alloc] init];
    [login logOut];
    
    [login logInWithReadPermissions: @[@"public_profile",@"email"]
                 fromViewController:self.viewController
                            handler:^(FBSDKLoginManagerLoginResult *result, NSError *error) {
                                if (error) {
                                    // [self.delegate failWithResponseFaceBook:FBSDKErrorLocalizedDescriptionKey];
                                    
                                    if(![FBSDKErrorLocalizedDescriptionKey  isEqualToString:@"com.facebook.sdk:FBSDKErrorLocalizedDescriptionKey"])
                                    {
                                        UIAlertView *  alert = [[UIAlertView alloc]initWithTitle:@"" message:@"" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
                                        [alert show];
                                    }
                                    NSLog(@"Process error %@",FBSDKErrorLocalizedDescriptionKey);
                                    
                                    return;
                                } else if (result.isCancelled) {
                                    NSLog(@"Cancelled");
                                } else {
                                    NSLog(@"Logged in");
                                    if ([FBSDKAccessToken currentAccessToken]) {
                                        NSMutableDictionary* parameters = [NSMutableDictionary dictionary];
                                        [parameters setValue:@"id,name,email,first_name,last_name,picture.width(100).height(100),age_range,gender,locale,updated_time,verified,timezone" forKey:@"fields"];
                                        [[[FBSDKGraphRequest alloc] initWithGraphPath:@"me" parameters:parameters]
                                         startWithCompletionHandler:^(FBSDKGraphRequestConnection *connection, id result, NSError *error) {
                                             if (!error) {
                                                 NSLog(@"fetched user:%@", result);
                                                 
                                                 [self.delegate getFacebookData:result];
                                                 
                                                 
                                                 
                                             }else
                                             {
                                                 [self.delegate failWithResponseFaceBook:error.localizedDescription];
                                                 
                                                 
                                             }
                                         }];
                                    }
                                    
                                    NSLog(@"Logged in %@",result);
                                    
                                    //             [self getFriends];
                                    accessToken=result.token.tokenString;
                                }
                            }];
    
}
-(void)getFriends
{
    NSMutableDictionary *params = [NSMutableDictionary dictionaryWithObjectsAndKeys:@"id,name,picture",@"fields",nil];
    
    FBSDKGraphRequest *request = [[FBSDKGraphRequest alloc]
                                  initWithGraphPath:@"/me/taggable_friends"
                                  parameters:params
                                  HTTPMethod:@"GET"];
    [request startWithCompletionHandler:^(FBSDKGraphRequestConnection *connection,
                                          id result,
                                          NSError *error) {
        // Handle the result
        NSLog(@"%@",result);
    }];
}


#pragma mark - connect to facebook through setting -
//connect to face through settings
- (void)me{
    
    
    //    [[MMProgressHUD sharedHUD] setOverlayMode:MMProgressHUDWindowOverlayModeGradient];
    //    [MMProgressHUD setPresentationStyle:MMProgressHUDPresentationStyleExpand];
    //    [MMProgressHUD setDisplayStyle:MMProgressHUDDisplayStyleBordered];
    //    [MMProgressHUD showWithTitle:nil status:stringForKey(@"Loading")];
    NSURL *meurl = [NSURL URLWithString:@"https://graph.facebook.com/me"];
    NSMutableDictionary* parameters = [NSMutableDictionary dictionary];
    //  [parameters setValue:@"id,name,email" forKey:@"fields"];
    [parameters setValue:@"id,name,email,first_name,last_name,picture.width(100).height(100),age_range,gender,locale,updated_time,verified,timezone" forKey:@"fields"];
    
    SLRequest *merequest = [SLRequest requestForServiceType:SLServiceTypeFacebook
                                              requestMethod:SLRequestMethodGET
                                                        URL:meurl
                                                 parameters:parameters];
    
    merequest.account = facebookAccount;
    
    [merequest performRequestWithHandler:^(NSData *responseData, NSHTTPURLResponse *urlResponse, NSError *error) {
        if (error) {
            dispatch_async(dispatch_get_main_queue(), ^{
                [self.delegate failWithResponseFaceBook:error.localizedDescription];
            });
            
        }else{
            dispatch_async(dispatch_get_main_queue(), ^{
                
                NSError *e = nil;
                
                NSDictionary *jsonDictionary = [NSJSONSerialization JSONObjectWithData:responseData options: NSJSONReadingMutableContainers error: &e];
                NSLog(@"jsonDictionaryjsonDictionaryjsonDictionaryjsonDictionaryjsonDictionary%@",jsonDictionary);
                //                [self getFriends1];
                [self.delegate getFacebookData:jsonDictionary];
                
            });
        }
        //        NSString *meDataString = [[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding];\
        //         NSLog(@"%@", meDataString);
        
        
    }];
    
}
//get friends through settings
- (void)getFriends1{
    NSURL *meurl = [NSURL URLWithString:@"https://graph.facebook.com/me/taggable_friends"];
    
    SLRequest *merequest = [SLRequest requestForServiceType:SLServiceTypeFacebook
                                              requestMethod:SLRequestMethodGET
                                                        URL:meurl
                                                 parameters:nil];
    
    merequest.account = facebookAccount;
    
    [merequest performRequestWithHandler:^(NSData *responseData, NSHTTPURLResponse *urlResponse, NSError *error) {
        if (error) {
            dispatch_async(dispatch_get_main_queue(), ^{
                
            });
            
        }else{
            dispatch_async(dispatch_get_main_queue(), ^{
                
                NSError *e = nil;
                
                NSDictionary *jsonDictionary = [NSJSONSerialization JSONObjectWithData:responseData options: NSJSONReadingMutableContainers error: &e];
                NSLog(@"%@",jsonDictionary);
                
            });
        }
        //        NSString *meDataString = [[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding];\
        //         NSLog(@"%@", meDataString);
        
        
    }];
    
}
@end
