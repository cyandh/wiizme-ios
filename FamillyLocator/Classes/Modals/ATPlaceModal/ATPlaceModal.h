//
//  ATPlaceModal.h
//  ZakatFund
//
//  Created by Ahmed Osama on 10/27/13.
//  Copyright (c) 2013 asgatech. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ATPlaceModal : NSObject

@property (nonatomic, strong) NSString * title;
@property (nonatomic, strong) NSString * stationID;
@property (nonatomic, strong) NSString * address;
@property (nonatomic, strong) NSString * fullDescription;
@property (nonatomic, strong) NSString * latitude;
@property (nonatomic, strong) NSString * longitude;
@property (nonatomic, strong) NSString * radius;
@property(nonatomic,strong)NSString* image;

@end
