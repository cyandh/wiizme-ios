//
//  ConversationModel.m
//  wiizme
//
//  Created by Amira on 3/14/17.
//  Copyright © 2017 Amira. All rights reserved.
//

#import "ConversationModel.h"

@implementation ConversationModel

+ (NSString *)primaryKey {
    return @"Id";
}

+ (NSDictionary *)defaultPropertyValues {
    return @{@"UserId" : @"00000000-0000-0000-0000-000000000000", @"GroupId": @"00000000-0000-0000-0000-000000000000"};
}


@end
