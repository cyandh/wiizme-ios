//
//  ConversationModel.h
//  wiizme
//
//  Created by Amira on 3/14/17.
//  Copyright © 2017 Amira. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Realm/Realm.h>
@interface ConversationModel : RLMObject
@property NSString *Id;
@property NSString *ChatName;
@property NSString *LogoId;
@property NSString *UserId;
@property NSString *GroupId;
@property NSDate *LastUpdateTime;
@property int UnseenMsgNo;
@property NSString *CreatorId;
@property NSDate *CreationDate;
@property NSString *LastText;
@property BOOL Mute;

@end
