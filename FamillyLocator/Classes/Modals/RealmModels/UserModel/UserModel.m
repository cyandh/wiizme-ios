//
//  UserModel.m
//  wiizme
//
//  Created by Amira on 3/14/17.
//  Copyright © 2017 Amira. All rights reserved.
//

#import "UserModel.h"

@implementation UserModel

+ (NSString *)primaryKey {
    return @"Id";
}

@end
