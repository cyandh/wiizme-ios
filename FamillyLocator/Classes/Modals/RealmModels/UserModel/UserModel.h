//
//  UserModel.h
//  wiizme
//
//  Created by Amira on 3/14/17.
//  Copyright © 2017 Amira. All rights reserved.
//

#import <Realm/Realm.h>

@interface UserModel : RLMObject

@property NSString *Id;
@property NSString *UserName;


@end
