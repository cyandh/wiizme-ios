//
//  SeenModel.m
//  wiizme
//
//  Created by Amira on 3/14/17.
//  Copyright © 2017 Amira. All rights reserved.
//

#import "SeenModel.h"

@implementation SeenModel

+ (NSString *)primaryKey {
    return @"SeenID";
}

@end
