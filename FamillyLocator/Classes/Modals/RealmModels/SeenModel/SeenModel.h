//
//  SeenModel.h
//  wiizme
//
//  Created by Amira on 3/14/17.
//  Copyright © 2017 Amira. All rights reserved.
//

#import <Realm/Realm.h>

@interface SeenModel : RLMObject
@property NSString *SeenID;
@property NSString *msgId;
@property NSString *UserID;
@property NSDate *SeenAt;


@end
