//
//  MessageModel.h
//  wiizme
//
//  Created by Amira on 3/14/17.
//  Copyright © 2017 Amira. All rights reserved.
//

#import <Realm/Realm.h>

@interface MessageModel : RLMObject
@property NSString *Id;
@property NSString *ConversationId;
@property NSString *Text;
@property NSDate *Date;
@property NSString *UserId;
@property NSString *FileId;
@property NSString *FileTypeTd;
@property BOOL IsSend;
@property NSString *SavedFilePath;
@property NSString *MessageType;

@property BOOL IsSeen;

@end
