//
//  MessageModel.m
//  wiizme
//
//  Created by Amira on 3/14/17.
//  Copyright © 2017 Amira. All rights reserved.
//

#import "MessageModel.h"

@implementation MessageModel
+ (NSString *)primaryKey {
    return @"Id";
}

+ (NSDictionary *)defaultPropertyValues {
    return @{@"FileId" : @"00000000-0000-0000-0000-000000000000", @"FileTypeTd": @"00000000-0000-0000-0000-000000000000",@"SavedFilePath":@""};
}
@end
