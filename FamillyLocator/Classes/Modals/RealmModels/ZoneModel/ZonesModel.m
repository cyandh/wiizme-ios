//
//  ZonesModel.m
//  wiizme
//
//  Created by Amira El Samahy on 3/29/17.
//  Copyright © 2017 Amira. All rights reserved.
//

#import "ZonesModel.h"

@implementation ZonesModel

+ (NSString *)primaryKey {
    return @"Id";
}

+ (NSDictionary *)defaultPropertyValues {
    return @{@"Status" : @"NewZone"};
}

@end
