//
//  ZonesModel.h
//  wiizme
//
//  Created by Amira El Samahy on 3/29/17.
//  Copyright © 2017 Amira. All rights reserved.
//

#import <Realm/Realm.h>

@interface ZonesModel : RLMObject
@property NSString * Id;
@property double Latitude;
@property double Longitude;
@property double Radius;
@property NSString *Status;
@end
