//
//  WebServiceFunc.h
//  FamillyLocator
//
//  Created by Amira on 10/26/16.
//  Copyright © 2016 Amira. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Foundation/Foundation.h>
#import "WebServiceProtocol.h"
#import "GroupData.h"
@interface WebServiceFunc : NSObject<NSURLSessionDelegate>

@property (nonatomic,weak)id<WebServiceProtocol>delegate;

-(void)updateUserImagewithimageobject:(NSMutableArray*)imageObject;
-(void)postDataWithUrlString:(NSString*)urlString withData:(NSMutableDictionary *)dicData  withflagLogin:(BOOL)loginFlag;
-(void)postDataLogin:(NSString*)urlString withData:(NSString *)dicData;
-(void)getData:(NSString*)urlString withflagLogin:(BOOL)loginFlag;
-(NSString *)encodeToBase64String:(UIImage *)userimage;
@end
