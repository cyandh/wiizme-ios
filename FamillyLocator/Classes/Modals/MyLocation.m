//
//  MyLocation.m
//  Mapkitmvc
//
//  Created by Mahmoud Wageh on 7/29/15.
//  Copyright (c) 2015 training. All rights reserved.
//

#import "MyLocation.h"
#import <AddressBook/AddressBook.h>

@interface MyLocation ()
@property (nonatomic, copy) NSString *name;
@property (nonatomic, copy) NSString *address;
@property (nonatomic, assign) CLLocationCoordinate2D theCoordinate;
@end

@implementation MyLocation

//note the required title method (from MKAnnotation protocol) returns the name, and the required subtitle method returns the address.
- (id)initWithName:(NSString*)name  coordinate:(CLLocationCoordinate2D)coordinate {
    if ((self = [super init])) {
        if ([name isKindOfClass:[NSString class]]) {
//            NSNumber *myDoubleNumber = [NSNumber numberWithDouble:coordinate.longitude];
//            self.name =[myDoubleNumber stringValue];
            self.name=name;
        } else {
            self.name = @"Unknown charge";
        }
       
        self.theCoordinate = coordinate;
    }
    return self;
}

- (NSString *)title {
    return _name;
}



- (CLLocationCoordinate2D)coordinate {
    return _theCoordinate;
}

//This is creating an instance of a special class called MKMapItem to represent this location. What this class does is provide a way for you to pass information to the Maps app
- (MKMapItem*)mapItem {
   // NSDictionary *addressDict = @{(NSString*)kABPersonAddressStreetKey : _address};
    
    MKPlacemark *placemark = [[MKPlacemark alloc]
                              initWithCoordinate:self.coordinate
                              addressDictionary:nil];
    
    MKMapItem *mapItem = [[MKMapItem alloc] initWithPlacemark:placemark];
    mapItem.name = self.title;
    
    return mapItem;
}
@end
