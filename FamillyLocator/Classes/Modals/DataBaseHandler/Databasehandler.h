//
//  Databasehandler.h
//  Developwithsqlite
//
//  Created by amira on 8/7/15.
//  Copyright (c) 2015 amira. All rights reserved.
//

#import <Foundation/Foundation.h>
@interface Databasehandler : NSObject

+(void)copyDatabaseIfNeeded;
+(NSMutableArray *)allrecords;
+(void)saveNewRecord:(const char *)sqlQuery andarray:(NSArray*)data;
+(void)deleteWithName:(NSString *)name;
+(void)updateWithName:(NSString *)name Phone:(NSString *)phone;
+(int) insert:(NSString *)sqlQuery;
@end
