//
//  SqlSearch.h
//  indicator
//
//  Created by abdelrhman gamil on 6/6/15.
//  Copyright (c) 2015 cyandh. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <sqlite3.h>


@interface SqlSearch : NSObject{


    
    //Intrnal variables to keep track of the state of the object.
    BOOL isDirty;
    BOOL isDetailViewHydrated;
}
-(NSMutableArray *) getInitialDataToDisplay:(const char *)SqlQuery SetBdName:(NSString*)DNName;
-(BOOL)copyDatabaseIfNeeded:(NSString *)databasename;
@end
