//
//  SqlSearch.m
//  indicator
//
//  Created by abdelrhman gamil on 6/6/15.
//  Copyright (c) 2015 cyandh. All rights reserved.
//

#import "SqlSearch.h"
#define databasenam @"ChatDB.sqlite"

static sqlite3 *database = nil;
static sqlite3_stmt *deleteStmt = nil;
static sqlite3_stmt *addStmt = nil;




@implementation SqlSearch


-(NSMutableArray *) getInitialDataToDisplay:(const char *)SqlQuery SetBdName:(NSString*)DNName{
     NSMutableArray * Results = [[NSMutableArray alloc] init];  
    
    
    NSString *dbPath = [self pathofdatabase:DNName];
    
    
    if (sqlite3_open([dbPath UTF8String], &database) == SQLITE_OK)
    {
        const char *sql = SqlQuery;
        sqlite3_stmt *selectstmt;
        NSLog(@"sqlite3_prepare_v2(database, sql, -1, &selectstmt, NULL)=%d",sqlite3_prepare_v2(database, sql, -1, &selectstmt, NULL));
        if(sqlite3_prepare_v2(database, sql, -1, &selectstmt, NULL) == SQLITE_OK)
        {
            int countColumn = sqlite3_column_count(selectstmt);
           
            while(sqlite3_step(selectstmt) == SQLITE_ROW)                
            {
                NSMutableDictionary * column = [[NSMutableDictionary alloc] init];
                
                for(int i=0;i<countColumn;i++){
                    
                    NSString * ColumnName = [NSString stringWithUTF8String:sqlite3_column_name(selectstmt,i)];
                    
                //    NSLog(@"%@",[NSString stringWithFormat:@"Type=%d Key=%@",sqlite3_column_type(selectstmt,i),[NSString stringWithUTF8String:sqlite3_column_name(selectstmt,i)]]);
                    
                    
                    switch (sqlite3_column_type(selectstmt,i)) {
                        case 1:{
                            
                            [column setValue:[NSNumber numberWithInt:sqlite3_column_int(selectstmt, i)] forKey:ColumnName];
                            
                            break;
                        }
                            
                        case 2:{
                            [column setValue:[NSNumber numberWithDouble:sqlite3_column_double(selectstmt, i)] forKey:ColumnName];
                            break;
                        }
                            
                        case 3:{
                            [column setValue:[NSString stringWithUTF8String:(char *)sqlite3_column_text(selectstmt, i)] forKey:ColumnName];
                            break;
                        }
                            
                        case 5:{
                            
                            [column setValue:@"" forKey:ColumnName];
                            
                            break;
                        }
                            
                        default:
                            break;
                    }
                    
                    
                    
                }
                [Results addObject:column];
                
            }
        }
    }
    else{
        sqlite3_close(database); //Even though the open call failed, close the database connection to release all the memory.
    }
    
    
    return Results;
}





- (id) initWithPrimaryKey:(NSInteger) pk {
    
    
    isDetailViewHydrated = NO;
    
    return self;
}



+ (void) finalizeStatements {
    
    if(database) sqlite3_close(database);
    if(deleteStmt) sqlite3_finalize(deleteStmt);
    if(addStmt) sqlite3_finalize(addStmt);
}
- (NSString *) applicationDocumentsDirectory
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *basePath = ([paths count] > 0) ? [paths objectAtIndex:0] : nil;
    return basePath;
}


-(BOOL)copyDatabaseIfNeeded:(NSString *)databasename
{
    //define file manger
    NSFileManager *filemanger=[NSFileManager defaultManager];
    NSError *error;
    
    //create database path
    NSString *databasepath=[self pathofdatabase:databasename];
    //check if this file exists
    BOOL success=[filemanger fileExistsAtPath:databasepath];
    if(!success)
    {
        // copy from bundle to directory
        NSString *defaultdbpath=[[[NSBundle mainBundle]resourcePath]stringByAppendingPathComponent:databasename];
        success=[filemanger copyItemAtPath:defaultdbpath toPath:databasepath error:&error];
        if(!success){
            //exception
            
            return NO;
            
        }else
        {
            return YES;
        }
    }else
    {
        return NO;
    }
}

-(NSString *)pathofdatabase:(NSString *)databasename
{
    
    //get all pathes
    NSArray *pathes=NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    
    //get documents directory
    NSString *documentsdirectory=[pathes objectAtIndex:0];
    //append file url
    NSString *databasepath=[documentsdirectory stringByAppendingPathComponent:databasename];
    
    return databasepath;
}


//var/mobile/Containers/Data/Application/5ACE06E7-9EDB-4310-85BE-05ED3F40A2F4/Documents/Department1Map16052016.xlsx.sqlite
//var/mobile/Containers/Data/Application/917EDE96-7D22-40FF-8D5E-117253D046AA/Documents/Department1Map16052016.xlsx.sqlite
//var/mobile/Containers/Data/Application/5ACE06E7-9EDB-4310-85BE-05ED3F40A2F4/Documents/Department1Project03052016.xlsx.sqlite
//var/mobile/Containers/Data/Application/5ACE06E7-9EDB-4310-85BE-05ED3F40A2F4/Documents/mainDB.sqlite


@end
