//
//  Databasehandler.m
//  Developwithsqlite
//
//  Created by amira on 8/7/15.
//  Copyright (c) 2015 amira. All rights reserved.
//

#import "Databasehandler.h"
#import <sqlite3.h>
#define databasename @"ChatDB.sqlite"
static sqlite3_stmt *statement;
static sqlite3 *database=nil;

@implementation Databasehandler

#pragma Mark -  Get database Path Method

+(NSString *)pathofdatabase
{
    //get all pathes
    NSArray *pathes=NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    
    //get documents directory
    NSString *documentsdirectory=[pathes objectAtIndex:0];
    //append file url
    NSString *databasepath=[documentsdirectory stringByAppendingPathComponent:databasename];
    
    return databasepath;
}
#pragma End -  Get database Path Method


#pragma Mark - Copy first time database from bundle to directory


+(void)copyDatabaseIfNeeded
{
//    //define file manger
//    NSFileManager *filemanger=[NSFileManager defaultManager];
//    NSError *error;
//    
//    //create database path
//    NSString *databasepath=[self pathofdatabase];
//    //check if this file exists
//    BOOL success=[filemanger fileExistsAtPath:databasepath];
//    if(!success)
//    {
//        // copy from bundle to directory
//        NSString *defaultdbpath=[[[NSBundle mainBundle]resourcePath]stringByAppendingPathComponent:databasename];
//        success=[filemanger copyItemAtPath:defaultdbpath toPath:databasepath error:&error];
//        if(!success){
//            //exception
//            
//            return NO;
//            
//        }else
//        {
//            return YES;
//        }
//    }else
//    {
//        return NO;
//    }
    
    NSError *error;
    NSString *dbName = databasename;
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentationDirectory, NSUserDomainMask, YES);
    NSString *documentDirectory = [paths objectAtIndex:0];
    NSString *dbPath = [documentDirectory stringByAppendingString:dbName];
    NSString *defaultDBPath = [[[NSBundle mainBundle] resourcePath] stringByAppendingPathComponent:dbName];
    
    if ([fileManager fileExistsAtPath:defaultDBPath]) { //check if file exists in NSBundle
        
        BOOL success = [fileManager fileExistsAtPath:dbPath]; // check if exists in document directory
        if (!success) {
            
            success = [fileManager copyItemAtPath:defaultDBPath toPath:dbPath error:&error]; //copy to document directory
            if (success) {
                NSLog(@"Database successfully copied to document directory");
            }
            else{
                NSLog(@"Error %@", [error localizedDescription]);
            }
        }
        else {
            NSLog(@"Already exists in document directory");
        }
        
    }
    else{
        NSLog(@"Does not exists in NSBundle");
    }
}
#pragma End- Copy first time database from bundle to directory

#pragma Mark - Select Data Method

+(NSMutableArray *)allrecords
{
    //retrieve database path from directory
    NSString *databasepath=[Databasehandler pathofdatabase];
    //prepare mutable array to save the data
    NSMutableArray *array=[NSMutableArray new];
    //create the query as string
    NSString *query=@"select * from Developers";
    //check if the database opened  succesfully
    //&database is refrence to get data on it
    if(sqlite3_open([databasepath UTF8String], &database)==SQLITE_OK){//open database
        
        if (sqlite3_prepare_v2(database, [query UTF8String], -1, & statement,NULL)==SQLITE_OK) {
            while (sqlite3_step(statement)==SQLITE_ROW) {
                //receiving row by row
                //the hold each row as DTO "model"
                char *namechars=(char *)sqlite3_column_text(statement, 0);
                NSString *name=[[NSString alloc]initWithUTF8String:namechars];
                
                char *phonechar=(char *)sqlite3_column_text(statement,1);
                NSString *phone=[[NSString alloc]initWithUTF8String:phonechar];
                
                char *emailchar=(char *)sqlite3_column_text(statement,2);
                NSString *email=[[NSString alloc]initWithUTF8String:emailchar];
                
                NSLog(@"%@,\n,%@",name,phone);
                
//                DeveloperData*dev=[[DeveloperData alloc]init];
//                dev.name=name;
//                dev.phone=phone;
//                dev.email=email;
//                [array addObject:dev];
                
            }
            //we have to call sqlite3_finalize to clean up the memory used for the statement
            sqlite3_finalize(statement);
            
        }else
        {
            NSLog(@"%s",sqlite3_errmsg(database));
        }
        
    }else
    {
        NSLog(@"%s",sqlite3_errmsg(database));
    }
    // Note that when we are done we should close adatabase handle with sqlite3_close
    sqlite3_close(database);
    // then we return the data
    return array;
}

#pragma End - Select Data Method


#pragma Mark - insert Data Method

+(void)saveNewRecord:(const char *)sqlQuery andarray:(NSArray*)data
{
    
    //retrieive database path
    NSString * dataBasePath = [Databasehandler pathofdatabase];
    // open database
    if (sqlite3_open([dataBasePath UTF8String], &database) == SQLITE_OK) {
        //decalre statement handle
        sqlite3_stmt * addStmt;
        //create insert query as char
        const char *sql = sqlQuery;
        if(sqlite3_prepare_v2(database, sql, -1, &addStmt, NULL) != SQLITE_OK){//execute
            NSLog( @"Error while creating add statement. '%s'", sqlite3_errmsg(database));
        }
        //bind the value to the insert statemnet
        for (int i=0; i<data.count; i++) {
            if ([[data objectAtIndex:i] isKindOfClass:[NSNumber class]]) {
                sqlite3_bind_int(addStmt, i+1, [[data objectAtIndex:i] integerValue]);
            }else{
            
                sqlite3_bind_text(addStmt, i+1, [[data objectAtIndex:i] UTF8String], -1, SQLITE_TRANSIENT);
            }
            
        }
        
        
//        sqlite3_bind_text(addStmt, 2, [newItem.phone UTF8String], -1, SQLITE_TRANSIENT);
//        
//        sqlite3_bind_text(addStmt, 3, [newItem.email UTF8String], -1, SQLITE_TRANSIENT);
        
        if(SQLITE_DONE != sqlite3_step(addStmt)){
            NSLog( @"Error while inserting data. '%s'", sqlite3_errmsg(database));
        }else
        {
            NSLog(@"data insert successfully");
        }
        // reflict the statement to its original state;
        sqlite3_reset(addStmt);
        //close database
        sqlite3_close(database);
    }
}

+(int) insert:(NSString *)sqlQuery
{
    sqlite3* db = NULL;
    int rc=0;
    rc = sqlite3_open_v2([[Databasehandler pathofdatabase] cStringUsingEncoding:NSUTF8StringEncoding], &db, SQLITE_OPEN_READWRITE , NULL);
    if (SQLITE_OK != rc)
    {
        sqlite3_close(db);
        NSLog(@"Failed to open db connection");
    }
    else
    {
        NSString * query  =sqlQuery;
        char * errMsg;
        rc = sqlite3_exec(db, [query UTF8String] ,NULL,NULL,&errMsg);
        if(SQLITE_OK != rc)
        {
            NSLog(@"Failed to insert record  rc:%d, msg=%s",rc,errMsg);
        }
        sqlite3_close(db);
    }
    return rc;
}
#pragma Mark - insert Data Method


#pragma Mark - Delete Data Method

+(void)deleteWithName:(NSString *)name
{
    
    //retrieive database path
    NSString * dataBasePath = [Databasehandler pathofdatabase];
    // open database
    if (sqlite3_open([dataBasePath UTF8String], &database) == SQLITE_OK) {
        //decalre statement handle
        sqlite3_stmt * addStmt;
        //create insert query as char
        const char *sql = "DELETE FROM \"main\".\"Developers\" WHERE Name like (?)";
        if(sqlite3_prepare_v2(database, sql, -1, &addStmt, NULL) != SQLITE_OK){//execute
            NSLog( @"Error while creating add statement. '%s'", sqlite3_errmsg(database));
        }
        //bind the value to the insert statemnet
        
        sqlite3_bind_text(addStmt, 1, [name UTF8String], -1, SQLITE_TRANSIENT);
        
        if(SQLITE_DONE != sqlite3_step(addStmt)){
            NSLog( @"Error while inserting data. '%s'", sqlite3_errmsg(database));
        }else
        {
            NSLog(@"delete successfully");
        }
        // reflict the statement to its original state;
        sqlite3_reset(addStmt);
        //close database
        sqlite3_close(database);
    }
}

#pragma End - Delete Data Method

#pragma Mark - Update Data Method

+(void)updateWithName:(NSString *)name Phone:(NSString*)phone
{
    
    //retrieive database path
    NSString * dataBasePath = [Databasehandler pathofdatabase];
    // open database
    if (sqlite3_open([dataBasePath UTF8String], &database) == SQLITE_OK) {
        //decalre statement handle
        sqlite3_stmt * addStmt;
        //create insert query as char
        const char *sql="UPDATE \"main\".\"Developers\" SET Phone=(?) WHERE Name=(?)";
        //const char *sql = "UPDATE FROM \"main\".\"Developer\" WHERE Name like (?)";
        if(sqlite3_prepare_v2(database, sql, -1, &addStmt, NULL) != SQLITE_OK){//execute
            NSLog( @"Error while creating add statement. '%s'", sqlite3_errmsg(database));
        }
        //bind the value to the insert statemnet
        
        sqlite3_bind_text(addStmt, 1, [phone UTF8String], -1, SQLITE_TRANSIENT);
        sqlite3_bind_text(addStmt, 2, [name UTF8String], -1, SQLITE_TRANSIENT);
        
        if(SQLITE_DONE != sqlite3_step(addStmt)){
            NSLog( @"Error while inserting data. '%s'", sqlite3_errmsg(database));
        }else
        {
            NSLog(@"update successfully");
        }
        // reflict the statement to its original state;
        sqlite3_reset(addStmt);
        //close database
        sqlite3_close(database);
    }
}

#pragma End - Update Data Method




@end
