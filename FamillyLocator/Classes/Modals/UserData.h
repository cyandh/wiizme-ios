//
//  UserData.h
//  FamillyLocator
//
//  Created by Amira on 10/26/16.
//  Copyright © 2016 Amira. All rights reserved.
//
#import <UIKit/UIKit.h>
#import <Foundation/Foundation.h>

@interface UserData : NSObject
@property(nonatomic,strong)NSString *email,*password,*userImage,*userImgId,*fieldId,*value,*name,*age,*city,*gender,*job,*friendsNum,*groupsNum,*message,*UserID;
@property(nonatomic)BOOL addImg;

@property(nonatomic,strong)IBOutlet UIImage *uploadedImg;

@end
 
