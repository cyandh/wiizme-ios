//
//  LastPosition.h
//  wiizme
//
//  Created by Amira on 1/10/17.
//  Copyright © 2017 Amira. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "WebServiceFunc.h"
#import <CoreLocation/CoreLocation.h>


@interface LastPosition : NSObject<WebServiceProtocol,CLLocationManagerDelegate>{
    CLLocation * UserOldLocation;
    NSTimer* timer ;
}
-(void)getStatustoserver:(CLLocation *)Location;
@property(nonatomic, strong) CLLocationManager *locationManager;
-(void)startLoad;

@end
