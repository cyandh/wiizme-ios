
//
//  AppDelegate.m
//  FamilyLocator
//
//  Created by Amira on 9/18/16.
//  Copyright © 2016 Amira. All rights reserved.
//

#import "AppDelegate.h"
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import "SignUpLinkedinViewController.h"
#include <AudioToolbox/AudioToolbox.h>
#import "Constant.h"
#import <Realm/Realm.h>

@interface AppDelegate ()
{
    WebServiceFunc *webService;
}
@end

@implementation AppDelegate




- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    
    
    UIAlertView * alert;
    
    //We have to make sure that the Background App Refresh is enable for the Location updates to work in the background.
    if([[UIApplication sharedApplication] backgroundRefreshStatus] == UIBackgroundRefreshStatusDenied){
        
        alert = [[UIAlertView alloc]initWithTitle:@""
                                          message:@"The app doesn't work without the Background App Refresh enabled. To turn it on, go to Settings > General > Background App Refresh"
                                         delegate:nil
                                cancelButtonTitle:@"Ok"
                                otherButtonTitles:nil, nil];
        [alert show];
        
    }else if([[UIApplication sharedApplication] backgroundRefreshStatus] == UIBackgroundRefreshStatusRestricted){
        
        alert = [[UIAlertView alloc]initWithTitle:@""
                                          message:@"The functions of this app are limited because the Background App Refresh is disable."
                                         delegate:nil
                                cancelButtonTitle:@"Ok"
                                otherButtonTitles:nil, nil];
        [alert show];
        
    } else{
        
        self.locationTracker = [[LocationTracker alloc]init];
        [self.locationTracker startLocationTracking];
        
        //Send the best location to server every 60 seconds
        //You may adjust the time interval depends on the need of your app.
        NSTimeInterval time = 60.0;
        self.locationUpdateTimer =
        [NSTimer scheduledTimerWithTimeInterval:time
                                         target:self
                                       selector:@selector(updateLocation)
                                       userInfo:nil
                                        repeats:YES];
    }
    
    
    _ChatMessage = [[SaveChatMessage alloc] init];
    [_ChatMessage AddObserver];
    // Override point for customization after application launch.
    
    if(![[NSUserDefaults standardUserDefaults]valueForKey:@"FirstRun"]){
        [[UIApplication sharedApplication] cancelAllLocalNotifications];
    }else{
        NSString *ted=@"True";
        [[NSUserDefaults standardUserDefaults]setValue:ted forKey:@"FirstRun"];
    }
    self.client = [PTPusher pusherWithKey:@"19f081da812551e533a1" delegate:self encrypted:YES];
    [UIApplication sharedApplication].applicationIconBadgeNumber = 0;
    UIUserNotificationSettings* notificationSettings = [UIUserNotificationSettings settingsForTypes:UIUserNotificationTypeAlert | UIUserNotificationTypeBadge | UIUserNotificationTypeSound categories:nil];
    [[UIApplication sharedApplication] registerUserNotificationSettings:notificationSettings];
    [[UIApplication sharedApplication] registerForRemoteNotifications];
    
    [[FBSDKApplicationDelegate sharedInstance] application:application
                             didFinishLaunchingWithOptions:launchOptions];
    
    self.window=[[UIWindow alloc]initWithFrame:[[UIScreen mainScreen]bounds]];
    UIStoryboard *storyboard=[UIStoryboard storyboardWithName:@"Main" bundle:nil];
    
    UIViewController *viewController;
    if ([[NSUserDefaults standardUserDefaults]objectForKey:@"UserEmail"]) {
        if (self.lastLocation==nil) {
            
            webService=[[WebServiceFunc alloc]init];
            [webService setDelegate:self];
            NSString *urlString=TimeFrequency;
            [webService getData:urlString withflagLogin:YES];
        }
        viewController=[storyboard instantiateViewControllerWithIdentifier:@"HomeController"];
        
    }else
    {
        viewController=[storyboard instantiateViewControllerWithIdentifier:@"LoginController"];
    }
    
    if(![[NSUserDefaults standardUserDefaults]objectForKey:@"TrackersId"]){
        NSMutableArray *trackersId=[[NSMutableArray alloc]init];
        [[NSUserDefaults standardUserDefaults ]setObject:trackersId forKey:@"TrackersId"];
    }
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(logoutview:) name:@"Logout" object:nil];
    [self.window setRootViewController:viewController];
    [self.window makeKeyAndVisible];
    
    
    return YES;
}

- (BOOL)application:(UIApplication *)application openURL:(NSURL *)url sourceApplication:(NSString *)sourceApplication annotation:(id)annotation {
    return [[FBSDKApplicationDelegate sharedInstance] application:application
                                                          openURL:url
                                                sourceApplication:sourceApplication
                                                       annotation:annotation
            ];
}
-(void)updateLocation {
    NSLog(@"updateLocation");
    
    [self.locationTracker updateLocationToServer];
}
-(void)logoutview:(NSNotification *)notification
{
    
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"UserEmail"];
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"UserId"];
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"TrackersId"];
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"Token"];
    [self.lastLocation.locationManager stopUpdatingLocation];
    self.lastLocation=nil;
    RLMRealm *realm = [RLMRealm defaultRealm];
    
    [realm beginWriteTransaction];
    [realm deleteAllObjects];
    [realm commitWriteTransaction];
    
    UIStoryboard *storyboard=[UIStoryboard storyboardWithName:@"Main" bundle:nil];
    
    UIViewController *viewController= [storyboard instantiateViewControllerWithIdentifier:@"LoginController"];
    
    [self.window setRootViewController:viewController];
    
}

- (void)application:(UIApplication*)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData*)deviceToken
{
    NSString *newToken = [deviceToken description];
    newToken = [newToken stringByTrimmingCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:@"<>"]];
    newToken = [newToken stringByReplacingOccurrencesOfString:@" " withString:@""];
    
    NSLog(@"My token is: %@", newToken);
    [[NSUserDefaults standardUserDefaults]setObject:newToken forKey:@"DeviceToken"];
    
}

- (void)application:(UIApplication*)application didFailToRegisterForRemoteNotificationsWithError:(NSError*)error
{
    NSLog(@"Failed to get token, error: %@", error);
}

-(void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo{
    
    
    NSError *jsonError;
    
    NSData *objectData = [[userInfo objectForKey:@"content-available" ] dataUsingEncoding:NSUTF8StringEncoding];
    
    NSDictionary *json = [NSJSONSerialization JSONObjectWithData:objectData
                                                         options:NSJSONReadingMutableContainers
                                                           error:&jsonError];
    
    //    UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"title" message:[json objectForKey:@"json"] delegate:self cancelButtonTitle:@"OK" otherButtonTitles:@"CANCEL", nil];
    //    [alert show];
    
    UIApplicationState state = [application applicationState];
    if (state == UIApplicationStateActive) {
        SystemSoundID soundID;
        //        CFBundleRef mainBundle = CFBundleGetMainBundle();
        //        CFURLRef ref = CFBundleCopyResourceURL(mainBundle, (CFStringRef)@"mySoundName.wav", NULL, NULL);
        //        AudioServicesCreateSystemSoundID(ref, &soundID);
        //        AudioServicesPlaySystemSound(soundID);
        AudioServicesPlaySystemSound(1007);
        AudioServicesPlaySystemSound(kSystemSoundID_Vibrate);
        [[NSNotificationCenter defaultCenter] postNotificationName:@"UnseenNotification"
                                                            object:nil
                                                          userInfo:nil];
        
    } else {
        //Do stuff that you would do if the application was not active
        [[NSNotificationCenter defaultCenter] postNotificationName:@"RemoteNotifications"
                                                            object:nil
                                                          userInfo:json];
    }
    
    // [userInfo writeToFile:@"userinfo.plist" atomically:YES];
    
    
}
-(void)userWebserviceCallDidSuccessWithRespone:(NSMutableArray *)response{
    NSDictionary *dic=(NSDictionary*)response;
    [[NSUserDefaults standardUserDefaults] setObject:[dic objectForKey:@"positionUpdateFrequency"] forKey:@"Timerfrequence"];
    self.lastLocation=[[LastPosition alloc]init];
    [self.lastLocation startLoad];
    
}

-(void)userWebserviceCallDidFaildwithError:(NSError *)error andMessage:(NSString *)message{
    NSLog(@"%@",error.localizedDescription);
    
    
}
- (void)application:(UIApplication *)application didReceiveLocalNotification:(UILocalNotification *)notification {
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"ShowChatMsg"
                                                        object:nil
                                                      userInfo:notification.userInfo];
    
    WebServiceFunc *webService=[[WebServiceFunc alloc]init];
    [webService setDelegate:self];
    NSString *urlString=[NSString stringWithFormat:@"%@%@",SeenMessage,[notification.userInfo objectForKey:@"Id"]];
    [webService getData:urlString withflagLogin:YES];
}


- (void)pusher:(PTPusher *)pusher connectionDidConnect:(PTPusherConnection *)connection{
    NSLog(@"pusher connectionDidConnect");
    
}
- (void)pusher:(PTPusher *)pusher connection:(PTPusherConnection *)connection didDisconnectWithError:(NSError *)error willAttemptReconnect:(BOOL)willAttemptReconnect{
    NSLog(@"pusher didDisconnectWithError");
}


- (void)pusher:(PTPusher *)pusher connection:(PTPusherConnection *)connection failedWithError:(NSError *)error{
    
    NSLog(@"pusher failedWithError");
}

- (BOOL)pusher:(PTPusher *)pusher connectionWillAutomaticallyReconnect:(PTPusherConnection *)connection afterDelay:(NSTimeInterval)delay{
    return true;
}


- (void)pusher:(PTPusher *)pusher willAuthorizeChannel:(PTPusherChannel *)channel withAuthOperation:(PTPusherChannelAuthorizationOperation *)operation{
    NSLog(@"pusher willAuthorizeChannel");
}


- (void)pusher:(PTPusher *)pusher didSubscribeToChannel:(PTPusherChannel *)channel{
    NSLog(@"pusher didSubscribeToChannel");
}

- (void)pusher:(PTPusher *)pusher didUnsubscribeFromChannel:(PTPusherChannel *)channel{
    
    NSLog(@"pusher didUnsubscribeFromChannel");
    
}


- (void)pusher:(PTPusher *)pusher didFailToSubscribeToChannel:(PTPusherChannel *)channel withError:(NSError *)error{
    
    NSLog(@"pusher didFailToSubscribeToChannel");
    
}

- (void)pusher:(PTPusher *)pusher didReceiveErrorEvent:(PTPusherErrorEvent *)errorEvent{
    
    NSLog(@"pusher didReceiveErrorEvent");
    
}


- (void)applicationWillResignActive:(UIApplication *)application {
    
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    
    
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    
}

-(void)application:(UIApplication *)application performFetchWithCompletionHandler:(void (^)(UIBackgroundFetchResult))completionHandler {
    
    NSLog(@"Background fetch started...");
    
    //---do background fetch here---
    // You have up to 30 seconds to perform the fetch
    
    BOOL downloadSuccessful = YES;
    
    if (downloadSuccessful) {
        //---set the flag that data is successfully downloaded---
        completionHandler(UIBackgroundFetchResultNewData);
    } else {
        //---set the flag that download is not successful---
        completionHandler(UIBackgroundFetchResultFailed);
    }
    
    NSLog(@"Background fetch completed...");
}





@end
