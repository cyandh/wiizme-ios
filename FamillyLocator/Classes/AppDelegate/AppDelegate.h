//
//  AppDelegate.h
//  FamilyLocator
//
//  Created by Amira on 9/18/16.
//  Copyright © 2016 Amira. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Pusher/Pusher.h>
#import "LastPosition.h"
#import "WebServiceFunc.h"
#import "SaveChatMessage.h"
#import "GeofenceMonitor.h"
#import "LocationTracker.h"
@interface AppDelegate : UIResponder <UIApplicationDelegate,PTPusherDelegate,WebServiceProtocol>{

  
        GeofenceMonitor * gfm;
        NSMutableArray* locationsArray;
    
}
@property LocationTracker * locationTracker;
@property (nonatomic) NSTimer* locationUpdateTimer;
@property (strong, nonatomic) UIWindow *window;
@property(nonatomic,strong)PTPusher *client;
@property(nonatomic,strong) LastPosition *lastLocation;
@property(nonatomic,strong) CLLocation * UserOldLocation;
@property(nonatomic,strong) SaveChatMessage * ChatMessage;

@end

