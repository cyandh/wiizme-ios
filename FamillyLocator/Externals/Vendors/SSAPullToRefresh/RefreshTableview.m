//
//  RefreshTableview.m
//  ProxiLead
//
//  Created by abdelrhman gamil on 12/4/16.
//  Copyright © 2016 Amira. All rights reserved.
//

#import "RefreshTableview.h"


#define HEIGHT_LOADING_VIEW 44.0


@implementation RefreshTableview
@synthesize delegate;

- (void) initialization:(UITableView *)TargetTableView{
    [self SetTableView:TargetTableView];
    
    self.shouldLoad = YES;
    RefreshFlag=NO;
    
    self.refreshControl = [[SSARefreshControl alloc] initWithScrollView:Tableview andRefreshViewLayerType:SSARefreshViewLayerTypeOnScrollView];
    self.refreshControl.delegate = self;
    
    _backgroundView = [[UIView alloc] initWithFrame:Tableview.frame];
    Tableview.backgroundView = _backgroundView;
    UIColor *backgroundColor = Tableview.backgroundColor;
    if(backgroundColor){
        _backgroundView.backgroundColor = backgroundColor;
    }
    
    _loadingView = [[UIView alloc] initWithFrame:CGRectMake(0, _backgroundView.frame.size.height - HEIGHT_LOADING_VIEW, _backgroundView.frame.size.width, HEIGHT_LOADING_VIEW)];
    _loadingView.backgroundColor = [UIColor blueColor];
    [_backgroundView addSubview:_loadingView];
    [_loadingView setHidden:YES];
    
}

- (void) initLoadingView{
    
    
    _activitiIndicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    [_activitiIndicator setFrame:CGRectMake(_backgroundView.frame.size.width/2, 11, 30, 30)];
    [_activitiIndicator startAnimating];
    [_loadingView addSubview:_activitiIndicator];
    
    _loadingLabel = [[UILabel alloc] initWithFrame:CGRectMake(_activitiIndicator.frame.origin.x- _activitiIndicator.frame.size.width-20, 14, 220, 20)];
    [_loadingView addSubview:_loadingLabel];
    _loadingLabel.text = @"";
    // _loadingLabel.textAlignment=NSTextAlignmentCenter;
    [_loadingView setHidden:YES];
}





#pragma Refresh

- (void)beganRefreshing {
    
    RefreshFlag=YES;
    [self.delegate loadDataSource];
    
}



- (void) showLoadingView:(BOOL)isAnimated{
    
    if(_activitiIndicator == nil || _loadingLabel == nil) {
        [self initLoadingView];
    }
    
    [_loadingView setHidden:NO];
    
    _isLoading = YES;
    [Tableview setContentOffset:CGPointMake(0, Tableview.contentSize.height - Tableview.bounds.size.height + HEIGHT_LOADING_VIEW -10) animated:isAnimated];
}

- (void) hideLoadingView{
    [_loadingView setHidden:YES];
    _isLoading = NO;
    _shouldLoad=NO;
    RefreshFlag=NO;
    [self.refreshControl endRefreshing];
    // [tableview setContentOffset:CGPointMake(0, 0) animated:YES];
}


- (void) setBackgroundColorLoadingView:(UIColor *) color{
    if(_loadingView)
        _loadingView.backgroundColor = color;
}


-(void)SetTableView:(UITableView *)tableview{
    Tableview = tableview;

}

-(UITableView *)getTableView{

    return Tableview;

}


@end
