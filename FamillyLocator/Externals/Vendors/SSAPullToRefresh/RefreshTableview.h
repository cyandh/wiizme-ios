//
//  RefreshTableview.h
//  ProxiLead
//
//  Created by abdelrhman gamil on 12/4/16.
//  Copyright © 2016 Amira. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "SSARefreshControl.h"


@protocol RefreshTVDelegate <NSObject>

@required

- (void)loadDataSource;

@end


@interface RefreshTableview : UIViewController{

 
    BOOL RefreshFlag;
    BOOL _isLoading;
    UIView *_backgroundView;
    UIView *_loadingView;
    UILabel *_loadingLabel;
    UIActivityIndicatorView *_activitiIndicator;
    UITableView * Tableview;
}
@property (nonatomic, strong) SSARefreshControl *refreshControl;
@property(nonatomic,readwrite)BOOL shouldLoad;
@property (nonatomic, weak) id <RefreshTVDelegate> delegate;


- (void) showLoadingView:(BOOL)isAnimated;
- (void) hideLoadingView;
- (void) initialization:(UITableView *)TableView;
@end
